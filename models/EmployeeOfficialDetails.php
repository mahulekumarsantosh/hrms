<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "employee_official_details".
 *
 * @property int $id
 * @property int $employee_id
 * @property int|null $department_id
 * @property int|null $designation_id
 * @property float|null $basic_salary
 * @property string|null $doj
 * @property float|null $security_deposit
 * @property int|null $shift
 * @property string|null $created_at
 * @property int|null $created_by
 * @property string $updated_at
 * @property int $updated_by
 */
class EmployeeOfficialDetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employee_official_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['employee_id', 'department_id', 'designation_id', 'basic_salary', 'doj'], 'required'],
            [['employee_id', 'department_id', 'designation_id', 'shift', 'created_by', 'updated_by'], 'integer'],
            [['basic_salary', 'security_deposit'], 'number'],
            [['doj', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'employee_id' => 'Employee ID',
            'department_id' => 'Department',
            'designation_id' => 'Designation',
            'basic_salary' => 'Basic Salary',
            'doj' => 'Doj',
            'security_deposit' => 'Security Deposit',
            'shift' => 'Shift',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
}
