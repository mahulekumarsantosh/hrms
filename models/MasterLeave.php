<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "master_leave".
 *
 * @property int $id
 * @property string $name
 * @property int $leave_days
 * @property int $company_id
 * @property int $emp_grade
 * @property int|null $order_no
 * @property int $status
 * @property string $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 */
class MasterLeave extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master_leave';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'leave_days', 'company_id', 'emp_grade'], 'required'],
            [['leave_days', 'company_id', 'emp_grade', 'order_no', 'status', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 200],
            [['name', 'company_id'], 'unique', 'targetAttribute' => ['name'], 'message' => 'Name is already exist'],
            [['name'], 'match', 'pattern'=> '/[a-zA-Z ]+$/'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'leave_days' => 'Leave Days',
            'company_id' => 'Company ID',
            'emp_grade' => 'Emp Grade',
            'order_no' => 'Order No',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
}
