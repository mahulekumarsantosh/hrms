<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "master_district".
 *
 * @property int $id
 * @property int $state_id
 * @property string $name
 * @property string $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 */
class MasterDistrict extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master_district';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['state_id', 'name'], 'required'],
            [['state_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 200],
            [['name'], 'match', 'pattern'=> '/[a-zA-Z]+$/'],
            [['name', 'company_id'], 'unique', 'targetAttribute' => ['name'], 'message' => 'Name is already exist']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'state_id' => 'State',
            'name' => 'Name',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    // Relationship between state and district
    public function getStateDetail()
    {
        return $this->hasOne(MasterState::className(), ['id' => 'state_id']);
    }
}
