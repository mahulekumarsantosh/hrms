<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_otp".
 *
 * @property int $id
 * @property int $type
 * @property int $user_id
 * @property string $session_id
 * @property float $otp
 * @property string $token
 * @property string $expiry_datetime
 * @property int|null $attempt_count
 * @property int $send_count
 * @property int $is_expired
 * @property string $created_at
 */
class UserOtp extends \yii\db\ActiveRecord {

    const TYPE_OTP = 1;
    const TYPE_TOKEN = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'user_otp';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['user_id', 'type', 'expiry_datetime'], 'required'],
            [['session_id'], 'required', 'on' => ['session']],
            ['otp', 'required', 'on' => 'otp'],
            ['token', 'required', 'on' => 'token'],
            [['type', 'user_id', 'attempt_count', 'send_count', 'is_expired'], 'integer'],
            [['otp'], 'number'],
            [['token'], 'string'],
            [['expiry_datetime', 'created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'otp' => 'OTP',
            'expiry_datetime' => 'Expiry Datetime',
            'attempt_count' => 'Attempt Count',
            'send_count' => 'Send Count',
            'created_at' => 'Created At',
        ];
    }

    public static function expireOldToken($userId, $type) {
        if (is_numeric($userId)) {
            return UserOtp::updateAll(['is_expired' => 1], ['user_id' => $userId, 'type' => $type]);
        }
    }

    public static function generateOtp($userId = null) {
        @session_start();
        $model = new UserOtp;
        $model->type = UserOtp::TYPE_OTP;
        $model->scenario = 'otp';
        $model->user_id = $userId;
        $model->session_id = Yii::$app->session->id;
        $model->otp = rand(102030, 987987);
        $model->expiry_datetime = date('Y-m-d H:i:s', strtotime('+10 min'));
        UserOtp::expireOldToken($model->user_id, UserOtp::TYPE_TOKEN);
        if ($model->save()) {
            return $model;
        }

        return false;
    }

    public function generateToken($userId = null) {
        @session_start();
        $model = new UserOtp;
        $model->type = UserOtp::TYPE_TOKEN;
        $model->scenario = 'token';
        $model->user_id = $userId;
        $model->session_id = Yii::$app->session->id;
        $model->token = Yii::$app->security->generateRandomString();
        $model->expiry_datetime = date('Y-m-d H:i:s', strtotime('+10 min'));
        UserOtp::expireOldToken($model->user_id, UserOtp::TYPE_TOKEN);
        if ($model->save()) {
            return $model;
        }

        return false;
    }

}
