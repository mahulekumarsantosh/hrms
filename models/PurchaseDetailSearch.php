<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PurchaseDetail;

/**
 * PurchaseDetailSearch represents the model behind the search form of `app\models\PurchaseDetail`.
 */
class PurchaseDetailSearch extends PurchaseDetail
{
    /**
     * {@inheritdoc}
     */
    public $date;
    public function rules()
    {
        return [
            [['id', 'purchase_id', 'item_id', 'quantity', 'price'], 'integer'],
            [['date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PurchaseDetail::find()->joinWith('purchase');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'purchase_id' => $this->purchase_id,
            'item_id' => $this->item_id,
            'quantity' => $this->quantity,
            'price' => $this->price,
            'purchases.date' => $this->date,
        ]);

        return $dataProvider;
    }
}
