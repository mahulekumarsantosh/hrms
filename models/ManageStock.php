<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "manage_stock".
 *
 * @property int $id
 * @property int $item_id
 * @property float $opening_stock
 * @property float $in_stock
 * @property float $out_stock
 * @property float $closing_stock
 * @property string $date
 */
class ManageStock extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'manage_stock';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['item_id', 'opening_stock', 'date', 'company_id'], 'required'],
            [['item_id'], 'integer'],
            [['opening_stock', 'in_stock', 'out_stock', 'closing_stock'], 'number'],
            [['date', 'in_stock', 'out_stock', 'closing_stock', 'company_id'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'item_id' => 'Item ID',
            'opening_stock' => 'Opening Stock',
            'in_stock' => 'In Stock',
            'out_stock' => 'Out Stock',
            'closing_stock' => 'Closing Stock',
            'date' => 'Date',
        ];
    }

    public function getItem()
    {
        return $this->hasOne(Item::className(), ['id' => 'item_id']);
    }
}
