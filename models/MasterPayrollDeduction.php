<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "master_payroll_deduction".
 *
 * @property int $id
 * @property int $company_id
 * @property string $name
 * @property int $percent
 * @property int|null $order_no
 * @property string $status 0 - Inactive, 1 - Active
 
 * @property string $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 */
class MasterPayrollDeduction extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master_payroll_deduction';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'name', 'percent'], 'required'],
            [['company_id', 'percent', 'order_no', 'created_by', 'updated_by'], 'integer'],
            [['status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 200],
            [['name', 'company_id'], 'unique', 'targetAttribute' => ['name'], 'message' => 'Name is already exist'],
            [['name'], 'match', 'pattern'=> '/[a-zA-Z ]+$/'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'name' => 'Name',
            'percent' => 'Percent',
            'order_no' => 'Order No',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
}
