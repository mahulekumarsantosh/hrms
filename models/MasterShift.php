<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "master_shift".
 *
 * @property int $id
 * @property int $company_id
 * @property int $company_shift_id
 * @property string $shift_name
 * @property string $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 */
class MasterShift extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master_shift';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'company_shift_id', 'shift_name'], 'required'],
            [['company_id', 'company_shift_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['shift_name'], 'string', 'max' => 200],
            [['company_id', 'shift_name'], 'unique', 'targetAttribute' => ['shift_name'], 'message' => 'Name is already exist'],
            [['company_shift_id', 'company_id'], 'unique', 'targetAttribute' => ['company_shift_id'], 'message' => 'Shift Id is already exist'],
            [['shift_name'], 'match', 'pattern'=> '/[a-zA-Z ]+$/'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company',
            'company_shift_id' => 'Shift No.',
            'shift_name' => 'Shift Name',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
}
