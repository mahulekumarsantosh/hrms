<?php

namespace app\models;

//use Yii;

class Master {

    public static function getTaxslab() {
        return [
            5 => '5% GST',
            12 => '12% GST',
            18 => '18% GST',
            28 => '28% GST',
        ];
    }

    public static function getGender() {
        return [
            1 => 'Male',
            2 => 'female',
            3 => 'Other',
        ];
    }

    public static function getCondition() {
        return [
            1 => 'New',
            2 => 'Old',
        ];
    }

    public static function getPermitType() {
        return [
            1 => 'All India Permit',
            2 => 'State Permit',
        ];
    }

    public static function getInsuranceType() {
        return [
            1 => 'First Party',
            2 => 'Second Party',
            3 => 'Third Party'
//            
        ];
    }

    public static function getAllowanceType() {
        return [
            1 => 'Diesel Allowance',
            2 => 'Other Allowance',
            3 => 'Monthly Allowance'
        ];
    }

    public static function getGstType() {
        return [
            1 => 'Central',
            2 => 'State',
        ];
    }

    public static function getMaterial() {
        return [
            1 => 'BITUMEN-VG10',
            2 => 'BITUMEN-VG30',
            3 => 'BITUMEN-VG40',
            4 => 'LPG',
        ];
    }

    public static function getBloodGroup() {
        return [
            0 => ' --select--',
            1 => 'A+',
            2 => 'A-',
            3 => 'B+',
            4 => 'B-',
            5 => 'O+',
            6 => 'O-',
            7 => 'AB+',
            8 => 'AB-',
        ];
    }

    // 3 => 'FORM 3 (LDO Plant)',
    public static $TripFormType = [
        1 => 'FORM 1 (HPCL)',
        2 => 'FORM 2 (BPCL/LDO Plant)',
    ];
    public static $TripType = [
        1 => 'One Way',
        2 => 'Round Trip',
    ];
    public static $bloodGroup = [
        1 => 'A+',
        2 => 'A-',
        3 => 'B+',
        4 => 'B-',
        5 => 'O+',
        6 => 'O-',
        7 => 'AB+',
        8 => 'AB-',
    ];
    public static $getCatagory = [
        1 => 'Tyre',
        2 => 'Battary',
    ];
    public static $getPaymentmethod = [
        1 => 'Cash',
        2 => 'Cheque',
        3 => 'Other',
    ];

}
