<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "company".
 *
 * @property int $id
 * @property string $company_name
 * @property int $mobile
 * @property string $email
 * @property string|null $landline_no
 * @property int|null $state_id
 * @property int|null $district_id
 * @property string|null $city
 * @property string $full_address
 * @property string|null $pincode
 * @property string|null $gst_number
 * @property string|null $website
 * @property string $logo
 * @property int|null $no_of_employee
 * @property string|null $company_code
 * @property string $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 */
class Company extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_name', 'mobile', 'email', 'full_address'], 'required'],
            [['mobile', 'state_id', 'district_id', 'no_of_employee', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['company_name', 'logo'], 'string', 'max' => 100],
            [['email', 'website'], 'string', 'max' => 50],
            [['landline_no', 'city', 'pincode'], 'string', 'max' => 200],
            [['full_address'], 'string', 'max' => 250],
            [['gst_number'], 'string', 'max' => 25],
            [['company_code'], 'string', 'max' => 10],
            [['email'], 'email'],
            [['logo'], 'safe'],
            ['gst_number', 'match', 'pattern' => '/^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$/', 'message' => 'Invalid GSIN no.'],
            [['mobile'], 'match', 'pattern' => '/^[6-9][0-9]{9}$/', 'message' => 'Invalid moble no.'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_name' => 'Company Name',
            'mobile' => 'Mobile',
            'email' => 'Email',
            'landline_no' => 'Landline No',
            'state_id' => 'State',
            'district_id' => 'District',
            'city' => 'City',
            'full_address' => 'Full Address',
            'pincode' => 'Pincode',
            'gst_number' => 'Gst Number',
            'website' => 'Website',
            'logo' => 'Logo',
            'no_of_employee' => 'No Of Employee',
            'company_code' => 'Company Code',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
}
