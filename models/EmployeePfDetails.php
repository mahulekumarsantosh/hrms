<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "employee_pf_details".
 *
 * @property int $id
 * @property int $employee_id
 * @property string $is_pf
 * @property string|null $uan_no
 * @property string|null $is_esic
 * @property string|null $ip_number
 * @property string|null $is_lic
 * @property string|null $policy_no
 * @property string|null $is_insurance
 * @property string $insurance_no
 * @property string $created_at
 * @property int|null $created_by
 * @property string $updated_at
 * @property int|null $updated_by
 */
class EmployeePfDetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employee_pf_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['employee_id'], 'required'],
            [['employee_id', 'created_by', 'updated_by'], 'integer'],
            [['is_pf', 'is_esic', 'is_lic', 'is_insurance'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['uan_no'], 'string', 'max' => 12, 'min' => 12],            
            [['ip_number', 'policy_no'], 'string', 'max' => 30],
            [['insurance_no'], 'string', 'max' => 20],
            ['uan_no', 'required', 'when' => function ($model) {  return $model->is_pf == '1'; }],
            ['ip_number', 'required', 'when' => function ($model) {  return $model->is_esic == '1'; }],
            ['policy_no', 'required', 'when' => function ($model) {  return $model->is_lic == '1'; }],
            ['insurance_no', 'required', 'when' => function ($model) {  return $model->is_insurance == '1'; }],
            [['uan_no'], 'match', 'pattern'=> '/[0-9]+$/'],  
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'employee_id' => 'Employee ID',
            'is_pf' => 'Is Pf',
            'uan_no' => 'Uan No',
            'is_esic' => 'Is Esic',
            'ip_number' => 'IP Number',
            'is_lic' => 'Is LIC',
            'policy_no' => 'Policy No',
            'is_insurance' => 'Is Insurance',
            'insurance_no' => 'Insurance No',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
}
