<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "customer".
 *
 * @property int|null $id
 * @property int $type 1=> vendor 2=> customer
 * @property string $firm_name
 * @property int $mobile
 * @property string|null $email
 * @property string $gst_number
 * @property string $full_address
 * @property string $created_at
 * @property int|null $created_by
 * @property string $updated_at
 * @property int|null $updated_by
 */
class Customer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'customer';
    }
  const VENDOR = 'Vendor';
  const CUSTOMER = 'Customer';
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'type', 'mobile', 'created_by', 'updated_by', 'company_id'], 'integer'],
            [['type', 'firm_name', 'mobile', 'full_address'], 'required'],
            [['full_address'], 'string'],
            [['created_at', 'updated_at', 'gst_number', 'company_id'], 'safe'],
            [['firm_name'], 'string', 'max' => 250],
            [['email'], 'string', 'max' => 50],
            [['email'], 'email'],
            [['gst_number'], 'string', 'max' => 20],
            ['gst_number', 'match', 'pattern' => '/^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$/', 'message' => 'Invalid GSIN no.'],
            [['mobile'], 'match', 'pattern' => '/^[6-9][0-9]{9}$/', 'message' => 'Invalid moble no.'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Customer Type',
            'firm_name' => 'Firm Name',
            'mobile' => 'Mobile',
            'email' => 'Email',
            'gst_number' => 'Gst Number',
            'full_address' => 'Full Address',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'company_id' => 'Company Name'
        ];
    }
}
