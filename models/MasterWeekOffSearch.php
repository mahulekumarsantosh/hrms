<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MasterWeekOff;

/**
 * MasterWeekOffSearch represents the model behind the search form of `app\models\MasterWeekOff`.
 */
class MasterWeekOffSearch extends MasterWeekOff
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'company_id', 'working_hour', 'min_present_hour', 'min_half_day_hour', 'created_by', 'updated_by'], 'integer'],
            [['day_ids', 'check_in', 'check_out', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MasterWeekOff::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'company_id' => $this->company_id,
            'working_hour' => $this->working_hour,
            'check_in' => $this->check_in,
            'check_out' => $this->check_out,
            'min_present_hour' => $this->min_present_hour,
            'min_half_day_hour' => $this->min_half_day_hour,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'day_ids', $this->day_ids]);

        return $dataProvider;
    }
}
