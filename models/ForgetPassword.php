<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ForgetPassword is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class ForgetPassword extends Model {

    public $mobile;
    public $email;
    public $otp;
    public $password;
    public $cpassword;
    public $is_first_login;
//    public $rememberMe = true;
    private $_user = false;

    /**
     * @return array the validation rules.
     */
    public function rules() {
        return [
            ['mobile', 'required', 'on' => ['reset-password']],
            ['email', 'required', 'on' => ['email-reset-password']],
            ['email', 'email'],
            //otp
            ['otp', 'required', 'on' => ['reset-password-otp']],
            //reset
            [['password', 'cpassword','is_first_login'], 'required', 'on' => ['reset-password-reset']],
            [['cpassword'], 'compare', 'compareAttribute' => 'password', 'on' => ['reset-password-reset']],
            //common
            [['mobile'], 'match', 'pattern' => '/^[6-9][0-9]{9}$/', 'message' => 'Invalid Phone Number'],
        ];
    }

    public function attributeLabels() {
        return [
            'cpassword' => 'Confirm Password'
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validateUser() {
        if (!$this->hasErrors()) {
            if ($this->getUser()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function generateOtp() {
        if ($this->getUser()) {
            $model = new UserOtp;
            $model->type = UserOtp::TYPE_OTP;
            $model->scenario = 'otp';
            $model->user_id = $this->_user->id;
            $model->otp = rand(102030, 987987);
            $model->expiry_datetime = date('Y-m-d H:i:s', strtotime('+10 min'));
            UserOtp::expireOldToken($model->user_id, UserOtp::TYPE_OTP);
            $model->save();
            return $model;
        }
        return false;
    }

    public function generateToken() {
        if ($this->getUser()) {
            $model = new UserOtp;
            $model->type = UserOtp::TYPE_TOKEN;
            $model->scenario = 'token';
            $model->user_id = $this->_user->id;
            $model->token = Yii::$app->security->generateRandomString();
            $model->expiry_datetime = date('Y-m-d H:i:s', strtotime('+10 min'));
            UserOtp::expireOldToken($model->user_id, UserOtp::TYPE_TOKEN);
            if ($model->save()) {
                return $model;
            }
        }
        return false;
    }

    public function findOtp() {
        if ($this->getUser()) {
            $model = UserOtp::find()
                    ->andWhere(['type' => 1])
                    ->andWhere(['user_id' => $this->_user->id])
                    ->andWhere(['>=', 'expiry_datetime', date('Y-m-d H:i:s')])
                    ->andWhere(['is_expired' => 0])
                    ->one();
            return $model;
        }
        return false;
    }

    public function validateOtp() {
        if ($this->getUser()) {
            if ($model = $this->findOtp()) {
                if ($model->otp == $this->otp) {
                    return true;
                } else {
                    $model->attempt_count = $model->attempt_count + 1;
                    $model->save();
                    return $model;
                }
            }
        }
        return false;
    }

    public function validateToken($token) {
        return UserOtp::find()
                        ->andWhere(['type' => 2])
                        ->andWhere(['token' => $token])
                        ->andWhere(['>=', 'expiry_datetime', date('Y-m-d H:i:s')])
                        ->andWhere(['is_expired' => 0])
                        ->one();
    }

    public function resetPassword() {
        if ($this->validate() && $user = $this->getUser()) {
            $user->password_hash = Yii::$app->security->generatePasswordHash($this->password);
            $user->pass_updated_at = time();
            $user->is_first_login = 0;
            return $user->save(true, ['password_hash', 'pass_updated_at','is_first_login']);
        }
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser() {
        if ($this->_user === false) {
            $this->_user = User::findByEmail($this->email);
        }
        if ($this->_user === false) {
            $this->_user = User::findByMobile($this->mobile);
        }
        return $this->_user;
    }

}
