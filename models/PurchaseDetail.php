<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "purchases_detail".
 *
 * @property integer $id
 * @property integer $purchase_id
 * @property integer $item_id
 * @property integer $quantity
 * @property string $price
 *
 * @property Items $item
 * @property Purchases $purchase
 */
class PurchaseDetail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'purchases_detail';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_id', 'quantity', 'price', 'brand', 'company_id'], 'required'],
            [['purchase_id', 'item_id', 'quantity'], 'integer'],
            [['total', 'discount_per', 'discount_amount', 'tax_type', 'cgst', 'sgst', 'igst', 'grand_total', 'tax_per', 'brand', 'company_id'], 'safe'],
            [['price'], 'number'],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => Item::className(), 'targetAttribute' => ['item_id' => 'id']],
            [['purchase_id'], 'exist', 'skipOnError' => true, 'targetClass' => Purchase::className(), 'targetAttribute' => ['purchase_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'purchase_id' => 'Purchase ID',
            'item_id' => 'Item_name',
            'quantity' => 'Quantity',
            'price' => 'Price',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Item::className(), ['id' => 'item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchase()
    {
        return $this->hasOne(Purchase::className(), ['id' => 'purchase_id']);
    }
}
