<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "master_week_off".
 *
 * @property int $id
 * @property int $company_id
 * @property string $day_ids
 * @property int $working_hour
 * @property string $check_in
 * @property string $check_out
 * @property int $min_present_hour
 * @property int $min_half_day_hour
 * @property string $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 */
class MasterWeekOff extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master_week_off';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'day_ids', 'working_hour', 'check_in', 'check_out', 'min_present_hour', 'min_half_day_hour'], 'required'],
            [['company_id', 'min_present_hour', 'min_half_day_hour', 'created_by', 'updated_by'], 'integer'],
            [['working_hour', 'check_in', 'check_out', 'created_at', 'updated_at'], 'safe'],
            [['day_ids'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company',
            'day_ids' => 'Days',
            'working_hour' => 'Working Hour',
            'check_in' => 'Check In',
            'check_out' => 'Check Out',
            'min_present_hour' => 'Min Present Hour',
            'min_half_day_hour' => 'Min Half Day Hour',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
}
