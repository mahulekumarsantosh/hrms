<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "employee_bank_details".
 *
 * @property int $id
 * @property int $employee_id
 * @property string|null $account_number
 * @property string|null $bank_name
 * @property string|null $ifsc_code
 * @property string|null $bank_code
 * @property string|null $bank_address
 * @property string $created_at
 * @property int|null $created_by
 * @property string $updated_at
 * @property int|null $updated_by
 */
class EmployeeBankDetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employee_bank_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['employee_id', 'account_number', 'bank_name', 'ifsc_code', 'bank_code', 'bank_address'], 'required'],
            [['employee_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['account_number'], 'string', 'max' => 20],
            [['bank_name'], 'string', 'max' => 30],
            [['ifsc_code'], 'string', 'max' => 15],
            [['bank_code'], 'string', 'max' => 10],
            [['bank_address'], 'string', 'max' => 250],
            [['account_number'], 'match', 'pattern'=> '/[0-9]+$/'],
            [['ifsc_code'], 'match', 'pattern'=> '/[A-Z0-9]+$/'],
            [['bank_name'], 'match', 'pattern'=> '/[A-Za-z ]+$/'],
            [['bank_code'], 'match', 'pattern'=> '/[A-Z0-9]+$/'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'employee_id' => 'Employee ID',
            'account_number' => 'Account Number',
            'bank_name' => 'Bank Name',
            'ifsc_code' => 'Ifsc Code',
            'bank_code' => 'Bank Code',
            'bank_address' => 'Bank Address',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
}
