<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "employee".
 *
 * @property int $id
 * @property string $first_name
 * @property string|null $middle_name
 * @property string $last_name
 * @property string|null $employee_code
 * @property int $mobile
 * @property int|null $alternate_mobile
 * @property string|null $email
 * @property int|null $gender
 * @property string|null $dob
 * @property int|null $state
 * @property int|null $district
 * @property string|null $city
 * @property string|null $address
 * @property int|null $pincode
 * @property string|null $profile_photo
 * @property int|null $company_id
 * @property int|null $user_id
 * @property int $aadhar_no
 * @property string|null $pan_no
 * @property string $created_at
 * @property int|null $created_by
 * @property string $updated_at
 * @property int|null $updated_by
 */
class Employee extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employee';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'mobile', 'aadhar_no', 'state', 'district', 'city', 'address', 'pincode', 'gender'], 'required'],
            [['mobile', 'alternate_mobile', 'gender', 'state', 'pincode', 'company_id', 'user_id', 'aadhar_no', 'created_by', 'updated_by'], 'integer'],
            [['dob', 'created_at', 'updated_at'], 'safe'],
            [['address'], 'string'],
            [['first_name', 'middle_name', 'last_name', 'profile_photo'], 'string', 'max' => 30],
            [['employee_code', 'pan_no'], 'string', 'max' => 10],
            [['email', 'city'], 'string', 'max' => 50],
            [['email'], 'email'],
            [['first_name', 'last_name', 'middle_name', 'city'], 'match', 'pattern'=> '/[a-zA-Z ]+$/'],
            [['employee_code'], 'match', 'pattern'=> '/[a-zA-Z0-9-]+$/'],            
            [['mobile', 'alternate_mobile'], 'match', 'pattern' => '/^[6-9][0-9]{9}$/', 'message' => 'Invalid Mobile Number'],           
            [['pan_no'], 'match', 'pattern'=> '/^([A-Z]){5}([0-9]){4}([A-Z]){1}?$/'],
            [['aadhar_no'], 'match', 'pattern'=> '/[2-9]{1}[0-9]{11}$/'],
            [['pincode'], 'match', 'pattern'=> '/[0-9]+$/'],  
            [['pincode'], 'string', 'min'=> '6'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'middle_name' => 'Middle Name',
            'last_name' => 'Last Name',
            'employee_code' => 'Employee Code',
            'mobile' => 'Mobile',
            'alternate_mobile' => 'Alternate Mobile',
            'email' => 'Email',
            'gender' => 'Gender',
            'dob' => 'Dob',
            'state' => 'State',
            'district' => 'District',
            'city' => 'City',
            'address' => 'Address',
            'pincode' => 'Pincode',
            'profile_photo' => 'Profile Photo',
            'company_id' => 'Company ID',
            'user_id' => 'User ID',
            'aadhar_no' => 'Aadhar No',
            'pan_no' => 'Pan No',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
}
