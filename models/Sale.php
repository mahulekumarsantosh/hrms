<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "sales".
 *
 * @property integer $id
 * @property string $code
 * @property string $customer
 * @property string $date
 * @property string $total
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 *
 * @property SalesDetail[] $salesDetails
 * @property CustomerDetail $customerDetails
 */
class Sale extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sales';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'customer', 'tax_type', 'code', 'company_id'], 'required'],
            [['date', 'created_at', 'updated_at', 'company_id'], 'safe'],
            [['total'], 'number'],
            [[ 'created_by', 'updated_by'], 'integer'],
            [['code', 'customer'], 'string', 'max' => 255],
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Invoice No',
            'customer' => 'Sale To',
            'date' => 'Date',
            'total' => 'Total',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalesDetails()
    {
        return $this->hasMany(SaleDetail::className(), ['sale_id' => 'id']);
    }

    public function getCustomerDetail()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer']);
    }
}
