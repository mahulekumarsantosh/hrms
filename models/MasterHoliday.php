<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "master_holiday".
 *
 * @property int $id
 * @property int $company_id
 * @property string $name
 * @property string $from_date
 * @property string $to_date
 * @property string $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 */
class MasterHoliday extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master_holiday';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'name', 'from_date', 'to_date'], 'required'],
            [['company_id', 'created_by', 'updated_by'], 'integer'],
            [['from_date', 'to_date', 'created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 200],
            [['name'], 'match', 'pattern'=> '/[a-zA-Z]+$/'],
            [['name', 'company_id'], 'unique', 'targetAttribute' => ['name'], 'message' => 'Name is already exist'],
            ['to_date', 'compare', 'compareAttribute'=> 'from_date', 'operator' => '>=', 'enableClientValidation' =>false],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'name' => 'Name',
            'from_date' => 'From Date',
            'to_date' => 'To Date',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
}
