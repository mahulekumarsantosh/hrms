<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "items".
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 * @property string $unit
 * @property integer $quantity
 * @property integer $stock
 * @property string $price
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 *
 * @property PurchasesDetail[] $purchasesDetails
 * @property SalesDetail[] $salesDetails
 */
class Item extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'items';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'quantity', 'stock', 'unit', 'brand', 'company_id', 'current_price'], 'required'],
            [['quantity', 'stock', 'created_by', 'updated_by'], 'integer'],            
            [['created_at', 'updated_at', 'model_no', 'code', 'brand', 'current_price', 'company_id'], 'safe'],
            [['code', 'name', 'unit', 'brand'], 'string', 'max' => 255],
            [['name', 'brand', 'company_id'], 'unique', 'targetAttribute' => ['name', 'brand', 'company_id']]
        ];
    }

    public function behaviors()
    {
        return [
           // TimestampBehavior::className(),
           // BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Serial No.',
            'name' => 'Item Name',
            'unit' => 'Unit',
            'quantity' => 'Quantity',
            'stock' => 'Available Stok',
            'brand' => 'Brand',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'model_no' => 'Model No',
            'current_price' => 'Price/Unit',
            
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchasesDetails()
    {
        return $this->hasMany(PurchasesDetail::className(), ['item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalesDetails()
    {
        return $this->hasMany(SalesDetail::className(), ['item_id' => 'id']);
    }
}
