<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "setting".
 *
 * @property int $id
 * @property int $form_id
 * @property string $key
 * @property string $val
 * @property string $label
 * @property string $description
 * @property string $input_type
 * @property string $validation_rules
 * @property string $options
 */
class Setting extends \yii\db\ActiveRecord {

  
    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'setting';
    }

    /**
     * {@inheritdoc}
     */
    public $rules = false;

    public function rules() {
        if ($this->validation_rules) {
            return eval("return $this->validation_rules;");
        }
        return [
            [['val'], 'required'],
            [['val'], 'string'],
        ];
    }

    public static $deletedPaths = [];

    public static function DocumentDelete() {
        foreach (Setting::$deletedPaths as $deletedPath) {
            if (is_file($deletedPath)) {
                unlink($deletedPath);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public $attributeLabels = [
        'id' => 'ID',
        'key' => 'Key',
        'val' => 'Value',
        'label' => 'Label',
    ];

    public function attributeLabels() {
        return $this->attributeLabels;
    }

    protected $logMasterId = 106;

    public function logAttribute() {
        return [
            'value' => [
                "val",
            ],
        ];
    }

    public static $val = false;
    public static $label = false;

    public static function valueInit() {
        $res = self::find()->select(['key', 'val', 'label'])
                        ->cache(Yii::$app->params['cacheTime'], new \yii\caching\DbDependency(['sql' => 'SELECT MAX(u_at) FROM ' . Setting::tableName()]))
                        ->asArray()->all();
        self::$val = \yii\helpers\ArrayHelper::map($res, "key", "val");
        self::$label = \yii\helpers\ArrayHelper::map($res, "key", "label");
    }

    public static function label($key) {
        if (self::$val === false || self::$label === false) {
            self::valueInit();
        }
        return self::$label[$key];
    }

    public static function value($key) {
        if (self::$val === false || self::$label === false) {
            self::valueInit();
        }
        return self::$val[$key];
    }

}
