<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EmployeePfDetails;

/**
 * EmployeePfDetailsSearch represents the model behind the search form of `app\models\EmployeePfDetails`.
 */
class EmployeePfDetailsSearch extends EmployeePfDetails
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'employee_id', 'created_by', 'updated_by'], 'integer'],
            [['is_pf', 'uan_no', 'is_esic', 'ip_number', 'is_lic', 'policy_no', 'is_insurance', 'insurance_no', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EmployeePfDetails::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'employee_id' => $this->employee_id,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'is_pf', $this->is_pf])
            ->andFilterWhere(['like', 'uan_no', $this->uan_no])
            ->andFilterWhere(['like', 'is_esic', $this->is_esic])
            ->andFilterWhere(['like', 'ip_number', $this->ip_number])
            ->andFilterWhere(['like', 'is_lic', $this->is_lic])
            ->andFilterWhere(['like', 'policy_no', $this->policy_no])
            ->andFilterWhere(['like', 'is_insurance', $this->is_insurance])
            ->andFilterWhere(['like', 'insurance_no', $this->insurance_no]);

        return $dataProvider;
    }
}
