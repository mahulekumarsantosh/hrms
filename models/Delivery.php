<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "delivery".
 *
 * @property int $id
 * @property string $code
 * @property string $customer
 * @property string $date
 * @property int $total
 * @property int $tax_type
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 */
class Delivery extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'delivery';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code', 'customer', 'date', 'created_by', 'company_id'], 'required'],
            [['date', 'created_at', 'updated_at', 'updated_by', 'total', 'company_id'], 'safe'],
            [['total', 'tax_type', 'created_by', 'updated_by'], 'integer'],
            [['code'], 'string', 'max' => 50],
            [['customer'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Challan No.',
            'customer' => 'Customer',
            'date' => 'Date',
            'total' => 'Total',
            'tax_type' => 'Tax Type',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryDetails()
    {
        return $this->hasMany(DeliveryDetail::className(), ['delivery_id' => 'id']);
    }

    public function getCustomerDetail()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer']);
    }

}
