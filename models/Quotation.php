<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "quotation".
 *
 * @property int $id
 * @property string $code
 * @property string $customer
 * @property string $date
 * @property int|null $total
 * @property int $tax_type
 * @property string $created_at
 * @property int|null $created_by
 * @property string $updated_at
 * @property int|null $updated_by
 */
class Quotation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'quotation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer', 'date', 'tax_type', 'company_id', 'subject'], 'required'],
            [['date', 'created_at', 'updated_at', 'code', 'company_id', 'message'], 'safe'],
            [['total', 'tax_type', 'created_by', 'updated_by'], 'integer'],
            [['code'], 'string', 'max' => 50],
            [['customer'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'customer' => 'Customer Name',
            'date' => 'Date',
            'total' => 'Total',
            'tax_type' => 'Tax Type',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

      /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuotationDetails()
    {
        return $this->hasMany(QuotationDetail::className(), ['quotation_id' => 'id']);
    }

    public function getCustomerDetail()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer']);
    }

}
