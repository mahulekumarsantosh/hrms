<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "quotation_detail".
 *
 * @property int $id
 * @property int $quotation_id
 * @property int $item_id
 * @property int $quantity
 * @property int $price
 * @property float|null $total
 * @property float|null $discount_per
 * @property float $discount_amount
 * @property int|null $tax_per
 * @property float|null $cgst
 * @property float|null $sgst
 * @property float|null $igst
 * @property float|null $grand_total
 */
class QuotationDetail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'quotation_detail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['item_id', 'quantity', 'price', 'brand'], 'required'],
            [['quotation_id', 'item_id', 'quantity'], 'integer'],
            [['total', 'discount_per', 'discount_amount', 'tax_type', 'cgst', 'sgst', 'igst', 'grand_total', 'tax_per', 'company_id'], 'safe'],
            [['price'], 'number'],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => Item::className(), 'targetAttribute' => ['item_id' => 'id']],
            [['quotation_id'], 'exist', 'skipOnError' => true, 'targetClass' => Purchase::className(), 'targetAttribute' => ['quotation_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'quotation_id' => 'Quotation ID',
            'item_id' => 'Item ID',
            'quantity' => 'Quantity',
            'price' => 'Price/Unit',
            'total' => 'Total',
            'discount_per' => 'Discount Per',
            'discount_amount' => 'Discount Amount',
            'tax_per' => 'Tax Per',
            'cgst' => 'Cgst',
            'sgst' => 'Sgst',
            'igst' => 'Igst',
            'grand_total' => 'Grand Total',
            'brand' => 'Brand'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Item::className(), ['id' => 'item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuatation()
    {
        return $this->hasOne(Purchase::className(), ['id' => 'quotation_id']);
    }
    
}
