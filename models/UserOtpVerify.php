<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_otp".
 */
class UserOtpVerify extends UserOtp {

    public function rules() {
        return [
            ['otp', 'required', 'on' => ['session_otp_validate']],
            ['otp', 'validateSessionOtp', 'on' => ['session_otp_validate']],
        ];
    }

    public $otpModel = null;

    public function validateSessionOtp($attribute) {
        $this->otpModel = UserOtp::find()
                ->andWhere(['type' => UserOtp::TYPE_OTP])
                ->andWhere(['session_id' => Yii::$app->session->id])
                ->andWhere(['otp' => $this->otp])
                ->orderBy('id desc')
                ->one();

        if ($this->otpModel == null) {
            $this->addError($attribute, 'Invalid OTP.');
        } elseif ($this->otpModel->is_expired == 1 || $this->otpModel->expiry_datetime < date('Y-m-d H:i:s')) {
            $this->addError($attribute, 'Expired OTP.');
        }
    }

}
