/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function () {
    //Uppercase
    $('.input-uppercase').keyup(function () {        
        this.value = this.value.toUpperCase();
    });
});


$(document).on('select2:open', () => {
    document.querySelector('.select2-search__field').focus();
});

function numberFormat(number) {
    if (number == "") {
        return "";
    }
    afterDecimal = "";
    if (number.indexOf('.') > -1) {
        decimal = number.indexOf('.')
        afterDecimal = number.substr(decimal);
        number = number.substr(0, decimal);
    }
    number = numberParse(number);
    return number.toLocaleString('en-IN') + afterDecimal;
}

function numberParse(number) {
    return +number.replace(/[^0-9\.]/g, '');
}

$("input").attr("autocomplete", "off");
$('[data-toggle="tooltip"]').tooltip();


/*
 * Example
 * MyToast('error', "Something Went Wrong");
 * MyToast('success', "Data has been save successfully!!");
 */
//MyToast('success', "Data has been save successfully!!");

function MyToast(key, body) {
    keys = {
        success: {
            class: 'bg-success',
            title: 'Success',
            body: body,
            autohide : true,
            delay : 5000
        },
        warning: {
            class: 'bg-warning',
            title: 'Warning',
            body: body,
            autohide : true,
            delay : 5000
        },
        error: {
            class: 'bg-danger',
            title: 'Error',
            body: body,
            autohide : true,
            delay : 5000
        }
    };
    if (keys.hasOwnProperty(key)) {
        $(document).Toasts('create', keys[key]);
    } else {
        $(document).Toasts('create', {
            class: 'bg-default',
            title: 'Default',
            body: body,
            autohide : true,
            delay : 5000
        });
    }
}

// Function for only number input for text field
function isNumber(evt) {
    var iKeyCode = (evt.which) ? evt.which : evt.keyCode
    if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
        return false;
    return true;
}