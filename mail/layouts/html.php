<?php

use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */

$imgdir = Yii::getAlias('@web');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <style>
            body{background-color: #F7F7F7; font-family: Arial, sans-serif, 'Open Sans';}
            *{padding: 0; margin: 0;}
            h1, h2, h3, h4, h5, h6,
            .h1, .h2, .h3, .h4, .h5, .h6 {
              margin-bottom: 0.5rem;
              margin-top: 0.5rem;
              font-family: inherit;
              font-weight: 500;
              line-height: 1.2;
              color: inherit;
            }

            h1, .h1 {
              font-size: 2.5rem;
            }

            h2, .h2 {
              font-size: 2rem;
            }

            h3, .h3 {
              font-size: 1.75rem;
            }

            h4, .h4 {
              font-size: 1.5rem;
            }

            h5, .h5 {
              font-size: 1.25rem;
            }

            h6, .h6 {
              font-size: 1rem;
            }

            .bg-gray {
                background-color: #6c757d!important;
            }
            .bg-gray, .bg-gray>a {
                color: #fff!important;
            }
            .table {
                width: 90%;
                margin-bottom: 1rem;
                color: #212529;
                margin: 0 auto;
                border-collapse: collapse;
                background-color: #fff;
            }

            .table th,
            .table td {
                padding: 0.75rem;
                vertical-align: top;
                border-top: 1px solid #dee2e6;
                text-align: left;
            }

            .table thead th {
                vertical-align: bottom;
                border-bottom: 2px solid #dee2e6;
            }

            .table tbody + tbody {
                border-top: 2px solid #dee2e6;
            }

            .table-sm th,
            .table-sm td {
                padding: 0.3rem;
            }

            .table-bordered {
                border: 1px solid #dee2e6;
            }

            .table-bordered th,
            .table-bordered td {
                border: 1px solid #dee2e6;
            }

            .table-bordered thead th,
            .table-bordered thead td {
                border-bottom-width: 2px;
            }

            .table-borderless th,
            .table-borderless td,
            .table-borderless thead th,
            .table-borderless tbody + tbody {
                border: 0;
            }

            .table-striped tbody tr:nth-of-type(odd) {
                background-color: rgba(0, 0, 0, 0.05);
            }

            .table-hover tbody tr:hover {
                color: #212529;
                background-color: rgba(0, 0, 0, 0.075);
            }

            .table-primary,
            .table-primary > th,
            .table-primary > td {
                background-color: #b8daff;
            }

            .table-primary th,
            .table-primary td,
            .table-primary thead th,
            .table-primary tbody + tbody {
                border-color: #7abaff;
            }

            .table-hover .table-primary:hover {
                background-color: #9fcdff;
            }

            .table-hover .table-primary:hover > td,
            .table-hover .table-primary:hover > th {
                background-color: #9fcdff;
            }

            .table-secondary,
            .table-secondary > th,
            .table-secondary > td {
                background-color: #d6d8db;
            }

            .table-secondary th,
            .table-secondary td,
            .table-secondary thead th,
            .table-secondary tbody + tbody {
                border-color: #b3b7bb;
            }

            .table-hover .table-secondary:hover {
                background-color: #c8cbcf;
            }

            .table-hover .table-secondary:hover > td,
            .table-hover .table-secondary:hover > th {
                background-color: #c8cbcf;
            }

            .table-success,
            .table-success > th,
            .table-success > td {
                background-color: #c3e6cb;
            }

            .table-success th,
            .table-success td,
            .table-success thead th,
            .table-success tbody + tbody {
                border-color: #8fd19e;
            }

            .table-hover .table-success:hover {
                background-color: #b1dfbb;
            }

            .table-hover .table-success:hover > td,
            .table-hover .table-success:hover > th {
                background-color: #b1dfbb;
            }

            .table-info,
            .table-info > th,
            .table-info > td {
                background-color: #bee5eb;
            }

            .table-info th,
            .table-info td,
            .table-info thead th,
            .table-info tbody + tbody {
                border-color: #86cfda;
            }

            .table-hover .table-info:hover {
                background-color: #abdde5;
            }

            .table-hover .table-info:hover > td,
            .table-hover .table-info:hover > th {
                background-color: #abdde5;
            }

            .table-warning,
            .table-warning > th,
            .table-warning > td {
                background-color: #ffeeba;
            }

            .table-warning th,
            .table-warning td,
            .table-warning thead th,
            .table-warning tbody + tbody {
                border-color: #ffdf7e;
            }

            .table-hover .table-warning:hover {
                background-color: #ffe8a1;
            }

            .table-hover .table-warning:hover > td,
            .table-hover .table-warning:hover > th {
                background-color: #ffe8a1;
            }

            .table-danger,
            .table-danger > th,
            .table-danger > td {
                background-color: #f5c6cb;
            }

            .table-danger th,
            .table-danger td,
            .table-danger thead th,
            .table-danger tbody + tbody {
                border-color: #ed969e;
            }

            .table-hover .table-danger:hover {
                background-color: #f1b0b7;
            }

            .table-hover .table-danger:hover > td,
            .table-hover .table-danger:hover > th {
                background-color: #f1b0b7;
            }

            .table-light,
            .table-light > th,
            .table-light > td {
                background-color: #fdfdfe;
            }

            .table-light th,
            .table-light td,
            .table-light thead th,
            .table-light tbody + tbody {
                border-color: #fbfcfc;
            }

            .table-hover .table-light:hover {
                background-color: #ececf6;
            }

            .table-hover .table-light:hover > td,
            .table-hover .table-light:hover > th {
                background-color: #ececf6;
            }

            .table-dark,
            .table-dark > th,
            .table-dark > td {
                background-color: #c6c8ca;
            }

            .table-dark th,
            .table-dark td,
            .table-dark thead th,
            .table-dark tbody + tbody {
                border-color: #95999c;
            }

            .table-hover .table-dark:hover {
                background-color: #b9bbbe;
            }

            .table-hover .table-dark:hover > td,
            .table-hover .table-dark:hover > th {
                background-color: #b9bbbe;
            }

            .table-active,
            .table-active > th,
            .table-active > td {
                background-color: rgba(0, 0, 0, 0.075);
            }

            .table-hover .table-active:hover {
                background-color: rgba(0, 0, 0, 0.075);
            }

            .table-hover .table-active:hover > td,
            .table-hover .table-active:hover > th {
                background-color: rgba(0, 0, 0, 0.075);
            }

            .table .thead-dark th {
                color: #fff;
                background-color: #343a40;
                border-color: #454d55;
            }

            .table .thead-light th {
                color: #495057;
                background-color: #e9ecef;
                border-color: #dee2e6;
            }

            .table-dark {
                color: #fff;
                background-color: #343a40;
            }

            .table-dark th,
            .table-dark td,
            .table-dark thead th {
                border-color: #454d55;
            }

            .table-dark.table-bordered {
                border: 0;
            }

            .table-dark.table-striped tbody tr:nth-of-type(odd) {
                background-color: rgba(255, 255, 255, 0.05);
            }

            .table-dark.table-hover tbody tr:hover {
                color: #fff;
                background-color: rgba(255, 255, 255, 0.075);
            }

            @media (max-width: 575.98px) {
                .table-responsive-sm {
                    display: block;
                    width: 100%;
                    overflow-x: auto;
                    -webkit-overflow-scrolling: touch;
                }
                .table-responsive-sm > .table-bordered {
                    border: 0;
                }
            }

            @media (max-width: 767.98px) {
                .table-responsive-md {
                    display: block;
                    width: 100%;
                    overflow-x: auto;
                    -webkit-overflow-scrolling: touch;
                }
                .table-responsive-md > .table-bordered {
                    border: 0;
                }
            }

            @media (max-width: 991.98px) {
                .table-responsive-lg {
                    display: block;
                    width: 100%;
                    overflow-x: auto;
                    -webkit-overflow-scrolling: touch;
                }
                .table-responsive-lg > .table-bordered {
                    border: 0;
                }
            }

            @media (max-width: 1199.98px) {
                .table-responsive-xl {
                    display: block;
                    width: 100%;
                    overflow-x: auto;
                    -webkit-overflow-scrolling: touch;
                }
                .table-responsive-xl > .table-bordered {
                    border: 0;
                }
            }

            .table-responsive {
                display: block;
                width: 100%;
                overflow-x: auto;
                -webkit-overflow-scrolling: touch;
            }

            .table-responsive > .table-bordered {
                border: 0;
            }

            .table-responsive {
                display: block;
                width: 100%;
                overflow-x: auto;
                -webkit-overflow-scrolling: touch;
            }

            .table-responsive > .table-bordered {
                border: 0;
            }

            .notification-wrap{background-color: #fff; border-radius: 8px; box-shadow: 0 8px 5px #ccc; border: 1px solid #f2f2f2; width: 60%; margin: 0 auto;}
            .notification-wrap h1, .notification-user{text-align: left; width: 90%; margin: 20px auto;}
            .position-80{position: relative; top: -80px; background: #fff; padding: 8px; width: 90%; margin: 0 auto;border-radius: 8px;}
            .notification-icon{
                background-color: #001f3f;
                padding: 20px;
                color: #fff;
                text-align: center;
                margin-bottom: 20px;
                padding-bottom: 80px;
            }
            .notification-icon img,.notification-icon h2{
                display: inline-block; vertical-align: middle;
            }
            .odd-row h3, .even-row h3{ color: #000; font-weight: bold; font-size: 1.2em; margin: 25px auto; width: 90%;}
            .odd-row{background-color: #fff; padding: 10px 0;}
            .even-row{background-color: #F7F7F7; padding: 10px 0;}
            .text-danger{color: #dc3545;}
            .text-warning{color: #ffc107;}
            .text-success{color: #28a745;}
        </style>
    </head>
    <body>
        <?php $this->beginBody() ?>
        <div class="notification-wrap">
            <h1>ims dsaf</h1>
            <div class="notification-icon">
                <h2>Dues Notification d</h2>
            </div>
            <div class="position-80">
                <div class="notification-user">
                    Hi, Username!
                </div>
                <?= $content ?>        
            </div>
        </div>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
