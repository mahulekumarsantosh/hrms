-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 16, 2022 at 06:13 PM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 8.0.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hrms`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_assignment`
--

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `auth_item`
--

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `auth_item_child`
--

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `auth_rule`
--

CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(11) NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `company_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `name`, `company_id`) VALUES
(1, 'BINATONE', 1),
(2, 'BRANDED', 1),
(3, 'CONSISTENT', 1),
(4, 'CPPLUS', 1),
(5, 'DAHUA', 1),
(6, 'DAICHI', 1),
(7, 'EZVIZ', 1),
(8, 'HI-FOCUS', 1),
(9, 'HIKVISION', 1),
(10, 'HONEYWELL IMPACT', 1),
(11, 'MULTYBYTE', 1),
(12, 'RAPOO', 1),
(13, 'SEAGATE', 1),
(14, 'SECUREYE', 1),
(15, 'SECURNET', 1),
(16, 'SMART-I ', 1),
(17, 'TERABYTE', 1),
(18, 'WBOX', 1),
(19, 'XPIA', 1),
(20, 'ZEBRONIC', 1),
(21, 'INSTALLATION', 1),
(22, 'LENOVO', 1),
(23, 'DELL', 1),
(24, 'TOSHIBA', 1),
(25, 'WD', 1),
(26, 'RELICELL', 1),
(27, 'Zimpack', 1),
(28, 'TOSHIBA', 1),
(29, 'D-LINK', 1),
(30, 'FOXIN', 1),
(31, 'ENTER', 2),
(32, 'SECUREYE', 1),
(33, 'XPIA', 1),
(34, 'SANDISK', 1),
(35, 'EVM', 1),
(36, 'LOGITECH', 1),
(37, 'HP', 1),
(38, 'ASUS', 1),
(39, 'tesla', 3);

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `id` int(11) NOT NULL,
  `company_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `mobile` bigint(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `landline_no` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `district_id` int(11) DEFAULT NULL,
  `city` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `full_address` varchar(250) NOT NULL,
  `pincode` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `gst_number` varchar(25) DEFAULT NULL,
  `website` varchar(50) DEFAULT NULL,
  `logo` varchar(100) NOT NULL,
  `no_of_employee` bigint(20) DEFAULT NULL,
  `company_code` varchar(10) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`id`, `company_name`, `mobile`, `email`, `landline_no`, `state_id`, `district_id`, `city`, `full_address`, `pincode`, `gst_number`, `website`, `logo`, `no_of_employee`, `company_code`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 'Weblinto Technologies', 8103965666, 'contact@weblinto.com', NULL, NULL, NULL, NULL, '9/3, Capital Palace, Avanti Vihar Raipur(CG)', NULL, '22AMYPG4556F1ZV', 'http://www.weblinto.com', 'weblinto.png', NULL, 'WT', '2022-07-03 12:26:25', NULL, NULL, NULL),
(2, 'Entit Consultancy Service Pvt. Ltd', 8103965666, 'contact@weblinto.com', NULL, NULL, NULL, NULL, '406, National corporate park, GE Road Raipur(CG)', NULL, '22AMYPG4556F1ZV', 'http://www.weblinto.com', 'entit.png', NULL, 'ENTIT', '2022-07-03 12:26:25', NULL, NULL, NULL),
(3, 'Suntechnos Technology', 8602770288, 'santoshmahule12@gmail.com', '', NULL, NULL, '', 'Near Kala Putla, New Changorabhata Raipur(C G)', '', '22AMYPG4556F1ZV', 'http://www.suntechnos.in', 'project-management.png', NULL, 'SUNT', '2022-07-03 12:26:25', NULL, '2022-07-07 18:25:37', 1);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `type` int(11) NOT NULL COMMENT '1=> vendor 2=> customer',
  `firm_name` varchar(250) NOT NULL,
  `mobile` bigint(20) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `gst_number` varchar(20) NOT NULL,
  `full_address` text NOT NULL,
  `company_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `type`, `firm_name`, `mobile`, `email`, `gst_number`, `full_address`, `company_id`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 1, 'ARIHANT SECURITY SOLUTION', 9893230157, 'ARIHANTSECURITY11@GMAIL.COM', '22AAUFA3614L1ZD', 'RAIPUR', 1, '2022-01-06 06:18:42', NULL, '2022-01-16 18:00:37', NULL),
(2, 1, 'HITECH SOLUTION', 9826160789, 'HITECHRP@GMAIL.COM', '22ADZPA0988P1ZU', 'RAIPUR', 1, '2022-01-06 06:18:42', NULL, '2022-01-16 18:00:37', NULL),
(3, 1, 'OM COMPUTECH', 9893100629, 'OMCOMPUTECHSERVICE@YAHOO.IN', '22AZCPV5228Q1ZA', 'RAIPUR', 1, '2022-01-06 06:18:42', NULL, '2022-01-16 18:00:37', NULL),
(4, 1, 'PRINTLINK ENTERPRISES', 9755288697, 'PRINTLINK.RAIPUR123@GMAIL.COM', '22EKNPS0120L1Z5', 'RAIPUR', 1, '2022-01-06 06:18:42', NULL, '2022-01-16 18:00:37', NULL),
(5, 1, 'VANSHIKA INFOTECH', 8517895153, 'VANSHIKA.RAIPUR@GMAIL.COM', '22ACZPA9242R1ZV', 'RAIPUR', 1, '2022-01-06 06:18:42', NULL, '2022-01-16 18:00:37', NULL),
(6, 1, 'BALAJI SOLUTION', 9826195153, 'SUNIL@BALAJIRAIPUR.COM', '22ACIPA4176B1ZA', 'RAIPUR', 1, '2022-01-06 06:18:42', NULL, '2022-01-16 18:00:37', NULL),
(7, 1, 'PLANET INFOTECH', 9753318577, 'PLANETINFOTECHRPR@GMAIL.COM', '22AFFPA6848A1Z2', 'RAIPUR', 1, '2022-01-06 06:18:42', NULL, '2022-01-16 18:00:37', NULL),
(8, 1, 'SUCHITA ENTERPRISES', 9584955585, 'SUCHITAENTERPRISES@GMAIL.COM', '22AAXPKC8C8K1ZV', 'RAIPUR', 1, '2022-01-06 06:18:42', NULL, '2022-01-16 18:00:37', NULL),
(9, 1, 'SMART WORLD SYSTEM', 8319339760, 'SMARTWORLD364@GMAIL.COM', '22AXBPJ1251G1Z5', 'RAIPUR', 1, '2022-01-06 06:18:42', NULL, '2022-01-16 18:00:37', NULL),
(10, 1, 'SIDHDHARTH MARKETTING', 9617530000, 'SIDDHARTHMKT@GMAIL.COM', '22AEGPM4702H1ZW', 'RAIPUR', 1, '2022-01-06 06:18:42', NULL, '2022-01-16 18:00:37', NULL),
(11, 2, 'OM PROJECT', 9926300081, 'akansh@entitcs.com', '22AMYPG4556F2ZU', '9/3  CAPITAL PALACE AVANTI VIHAR RAIPUR', 1, '2022-01-10 16:14:54', NULL, '2022-01-16 18:00:37', 1),
(12, 2, 'ANGELS VALLEY ENGLISH SCHO0L,AMLIDIH RAIPUR', 9826750290, '', '', 'GOMTI VIHAR AMLIDIH RAIPUR (C.G.)', 1, '2022-01-11 06:56:15', NULL, '2022-01-16 18:00:37', 1),
(13, 2, 'MR,VIHAN MISHRA JI', 7828672167, '', '', 'RAIPUR (C.G.)', 1, '2022-01-11 08:38:02', NULL, '2022-01-16 18:00:37', NULL),
(14, 2, 'SHIKHAR PANDEY', 9907744000, 'shikhar.pandey@entitcs.com', '', 'KUSHALPUR RAIPUR', 1, '2022-01-12 06:05:51', NULL, '2022-01-16 18:00:37', NULL),
(15, 1, 'VANSHIKA INFOTECH', 8517895153, 'vanshika.raipur@gmail.com', '22ACZPA9242R1ZV', '9/23 2ND FLOOR STYAM HOUSE KALI MATA WARD PANDRI, RAIPUR', 1, '2022-01-14 13:46:46', NULL, '2022-01-16 18:00:37', NULL),
(16, 2, 'RMC ANURAG PATKAR', 8602398220, '', '', 'RAMC RAIPUR', 1, '2022-01-14 13:49:57', NULL, '2022-01-16 18:00:37', NULL),
(17, 2, 'ENTIT CONSULTANCY SERVICES PVT.LTD', 9926300081, 'info@entitcs.com', '22AACCE6981D1ZU', '406,NATIONAL CORPORATE PARK,GE ROAD RAIPUR', 1, '2022-01-17 10:56:58', NULL, '2022-01-17 10:56:58', NULL),
(18, 2, 'MR. AMAN GILL JI', 9301313135, '', '', 'BARSANA ENCLAVE RAIPUR', 1, '2022-01-17 11:28:29', NULL, '2022-01-17 11:28:29', NULL),
(19, 2, 'SHRI BALAJI VIDYA MANDIR ', 9406400007, 'shribalajischoolraipur@gmail.com', '', 'DEVENDRA NAGAR RAIPUR', 1, '2022-01-18 06:31:51', NULL, '2022-01-18 06:31:51', NULL),
(20, 2, 'Cholamandalam Investment And Finance Company Limited', 8823872000, 'jaideepc@chola.murugappa.com', '22AAACC1226H1ZE', 'Cholamandalam Investment And Finance Company Limited,Saraipali (C.G.)', 1, '2022-01-19 11:50:38', NULL, '2022-01-19 11:50:38', NULL),
(21, 2, 'Cholamandalam Investment And Finance Company Limited', 8823872000, 'jaideepc@chola.murugappa.com', '22AAACC1226H1ZE', '506-509 National corporate park, Opp- Maruti business park Raipur', 1, '2022-01-19 11:52:29', NULL, '2022-01-19 11:52:29', NULL),
(22, 2, 'MR,BIKASH JI', 9668529898, '', '', 'SRABONG,NUAPADA ORISA.', 2, '2022-01-20 12:22:01', NULL, '2022-01-20 12:22:01', NULL),
(23, 2, 'MR,BIKASH JI', 9668529898, '', '', 'SARABONG,NUAPADA ORISA.', 1, '2022-01-20 12:24:05', NULL, '2022-01-20 12:24:05', NULL),
(24, 2, ' MR.NAYAK JI', 8658165164, '', '', 'SARABONG,NUAPADA ORISA.', 1, '2022-01-20 13:23:32', NULL, '2022-01-20 13:23:32', NULL),
(25, 2, 'MR. AGAS DADSENA JI', 7894872666, '', '', 'SRABONG,NUAPADA ORISA.', 1, '2022-01-20 13:35:56', NULL, '2022-01-20 13:35:56', NULL),
(26, 2, 'DAV PUBLIC SCHOOL ACC JAMUL', 7222999539, '', '', 'ACC JAMUL', 1, '2022-01-21 05:56:28', NULL, '2022-01-21 05:56:28', NULL),
(27, 1, 'AGRAWAL TRADERS', 9893292010, 'agrawaltraders@hotmail.com', '22ACPPA7284D1ZS', 'CHOUBEY COLONY RAMKUND RAIPUR ', 1, '2022-01-24 12:54:54', NULL, '2022-01-24 12:54:54', NULL),
(28, 2, 'MR.AKANSH GOYAL', 9926300081, '', '', 'DHAMTARI PLANT', 1, '2022-01-27 05:32:52', NULL, '2022-01-27 05:32:52', NULL),
(29, 2, 'GANESH GUPTA', 9109775544, '', '', 'PANDRI RAIPUR', 1, '2022-01-27 05:34:56', NULL, '2022-01-27 05:34:56', NULL),
(30, 2, 'UNATHORIZED ', 8103965666, '', '', 'Raipur', 1, '2022-01-27 05:37:25', NULL, '2022-01-27 05:37:25', NULL),
(31, 2, 'PRABHAT SHASHTRI SIR', 7587104100, '', '', 'MAULSHREE VIHAR RAIPUR', 1, '2022-01-27 05:40:04', NULL, '2022-01-27 05:40:04', NULL),
(32, 2, 'MR.SANKAR DEWANGAN JI', 9348536224, '', '', 'LANJIMAR NUAPADA ORISA', 1, '2022-01-27 05:44:01', NULL, '2022-01-27 05:44:01', NULL),
(33, 2, 'LOHIYA JI BALOD', 7024887777, '', '', 'BALOD', 1, '2022-01-27 05:49:31', NULL, '2022-01-27 05:49:31', NULL),
(34, 2, 'OM COMPUTECH SERVICES', 9893100629, '', '', 'TATYA PARA RAIPUR', 1, '2022-01-27 05:52:59', NULL, '2022-01-27 05:52:59', NULL),
(35, 2, 'MR.PRATEEK TANWANI JI', 9755824545, '', '', '319 LALGANGA MIDAS FAFADIH RAIPUR', 1, '2022-01-28 07:03:36', NULL, '2022-01-28 07:03:36', NULL),
(36, 2, 'LIFE WORTH HOSPITAL', 9826131347, '', '', 'SMATA COLONY RAIPUR.', 1, '2022-01-29 06:05:57', NULL, '2022-01-29 06:05:57', NULL),
(37, 2, 'VINAYAKA SILVER', 9675477000, '', '', 'SADAR BAZAR RAIPUR', 1, '2022-02-01 10:21:52', NULL, '2022-02-01 10:21:52', NULL),
(38, 2, 'Mr. Pradeep Nikhare Ji', 9827191508, '', '', 'Face - 4,Near Avinash Asiyana Kabir Nagar Raipur', 1, '2022-02-02 06:57:10', NULL, '2022-02-02 06:57:10', NULL),
(39, 2, 'MR,SHYAM AGRAWAL JI', 9329101948, '', '', 'SAMTA COLONY RAIPUR,', 1, '2022-02-08 06:26:32', NULL, '2022-02-08 06:26:32', NULL),
(40, 2, 'S.R PETROL PUMP', 8770492231, '', '', 'SILTARA RAIPUR', 1, '2022-02-10 11:49:41', NULL, '2022-02-10 11:49:41', NULL),
(41, 2, 'MR.DINESH JI', 8109928217, '', '', 'RAIPUR', 1, '2022-02-18 10:12:02', NULL, '2022-02-18 10:12:02', NULL),
(42, 2, 'MANGAL BHAWAN NEW RAIPUR', 9329101949, '', '', 'NEW RAIPUR ', 1, '2022-02-18 10:23:34', NULL, '2022-02-18 10:23:34', NULL),
(43, 1, 'MUSIC WORLD', 9893230157, '', '22AAUFA3614L1ZD', 'JAIRAM COMPLEX RAIPUR', 1, '2022-02-18 10:36:59', NULL, '2022-02-18 10:36:59', NULL),
(44, 1, 'BALAJI COMPUTER', 9826195153, '', '22ACIPA4176B1ZA', 'PANDRI RAIPUR', 1, '2022-02-18 10:49:40', NULL, '2022-02-18 10:49:40', NULL),
(45, 2, 'DAV MPS SCHOOL VISHRAMPURI', 9426226228, '', '', 'VISHRAMPURI (C.G.)', 1, '2022-02-18 11:01:47', NULL, '2022-02-18 11:01:47', NULL),
(46, 1, 'SIDDHARTH INFO SOLUTION', 9617530000, '', '22BFFPM0372C1Z2', 'JAIRAM COMPLEX SARDA CHOWK RAIPUR', 1, '2022-02-18 11:08:29', NULL, '2022-02-18 11:08:29', NULL),
(47, 2, 'DR.ALOK AGRAWAL JI', 8085497360, '', '', 'MOVA RAIPUR', 1, '2022-02-18 11:16:09', NULL, '2022-02-18 11:16:09', NULL),
(48, 2, 'MR.KULDEEP JI', 8815070714, '', '', 'WOOD I LAND AMLESHWAR RAIPUR', 1, '2022-02-23 05:43:58', NULL, '2022-02-23 05:43:58', NULL),
(49, 2, 'GOYAL COLD', 9826414021, '', '', 'RAIPUR (C.G.)', 1, '2022-03-05 06:52:58', NULL, '2022-03-05 06:52:58', NULL),
(50, 2, 'Konark Febrication', 9977028800, '', '', 'Raipur', 1, '2022-03-24 10:18:54', NULL, '2022-03-24 10:18:54', NULL),
(51, 2, 'Rupendra', 9587458887, 'test@gmail.com', '22AAUFA3614L1ZE', 'Raipur', 3, '2022-07-03 10:31:13', NULL, '2022-07-03 10:31:13', NULL);

-- --------------------------------------------------------

--
-- Stand-in structure for view `dates`
-- (See below for the actual view)
--
CREATE TABLE `dates` (
`date` date
);

-- --------------------------------------------------------

--
-- Table structure for table `delivery`
--

CREATE TABLE `delivery` (
  `id` int(11) NOT NULL,
  `code` varchar(50) NOT NULL,
  `customer` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `total` int(11) DEFAULT NULL,
  `tax_type` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `bill_code` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `delivery`
--

INSERT INTO `delivery` (`id`, `code`, `customer`, `date`, `total`, `tax_type`, `company_id`, `bill_code`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 'WT/JAN/001', '12', '2022-01-11', NULL, 0, 1, 0, '2022-01-11 06:58:22', 1, '2022-01-16 18:01:16', NULL),
(2, 'WT/JAN/002', '11', '2022-01-01', NULL, 0, 1, 0, '2022-01-14 13:34:36', 1, '2022-01-16 18:01:16', NULL),
(3, 'WT/JAN/003', '16', '2022-01-14', NULL, 0, 1, 0, '2022-01-14 13:50:51', 1, '2022-01-16 18:01:16', NULL),
(4, 'WT/JAN/004', '11', '2022-01-17', NULL, 0, 1, 0, '2022-01-17 17:15:24', 27, '2022-01-17 17:15:24', NULL),
(5, 'WT/JAN/005', '18', '2022-01-18', NULL, 0, 1, 0, '2022-01-18 05:59:21', 27, '2022-01-19 05:32:07', 27),
(6, 'WT/JAN/006', '19', '2022-01-18', NULL, 0, 1, 0, '2022-01-18 06:33:23', 27, '2022-01-18 06:33:23', NULL),
(7, 'WT/JAN/007', '20', '2022-01-20', NULL, 0, 1, 0, '2022-01-19 11:55:37', 27, '2022-01-20 09:17:52', 27),
(9, 'WT/JAN/008', '23', '2022-01-21', NULL, 0, 1, 8, '2022-01-20 12:33:46', 27, '2022-01-24 10:30:01', 27),
(10, 'WT/JAN/009', '23', '2022-01-21', NULL, 0, 1, 9, '2022-01-20 13:15:22', 27, '2022-01-24 10:31:54', 27),
(11, 'WT/JAN/010', '23', '2022-01-21', NULL, 0, 1, 10, '2022-01-20 13:24:42', 27, '2022-01-24 10:29:08', 27),
(12, 'WT/JAN/011', '24', '2022-01-21', NULL, 0, 1, 11, '2022-01-20 13:33:56', 27, '2022-01-24 10:36:46', 27),
(13, 'WT/JAN/012', '25', '2022-01-21', NULL, 0, 1, 12, '2022-01-20 13:36:38', 27, '2022-01-24 10:32:23', 27),
(14, 'WT/JAN/013', '26', '2022-01-21', NULL, 0, 1, 13, '2022-01-22 05:27:26', 27, '2022-01-22 05:27:26', NULL),
(15, 'WT/JAN/014', '26', '2022-01-24', NULL, 0, 1, 14, '2022-01-24 12:59:36', 27, '2022-01-24 12:59:36', NULL),
(16, 'WT/JAN/015', '28', '2022-01-27', NULL, 0, 1, 15, '2022-01-27 05:33:39', 27, '2022-01-27 05:33:39', NULL),
(17, 'WT/JAN/016', '29', '2022-01-27', NULL, 0, 1, 16, '2022-01-27 05:35:41', 27, '2022-01-27 05:35:41', NULL),
(18, 'WT/JAN/017', '30', '2022-01-27', NULL, 0, 1, 17, '2022-01-27 05:38:01', 27, '2022-01-27 05:38:01', NULL),
(19, 'WT/JAN/018', '31', '2022-01-27', NULL, 0, 1, 18, '2022-01-27 05:41:03', 27, '2022-01-27 05:42:17', 27),
(20, 'WT/JAN/019', '32', '2022-01-27', NULL, 0, 1, 19, '2022-01-27 05:45:12', 27, '2022-01-27 05:45:12', NULL),
(21, 'WT/JAN/020', '33', '2022-01-27', NULL, 0, 1, 20, '2022-01-27 05:50:14', 27, '2022-01-27 05:50:14', NULL),
(22, 'WT/JAN/021', '34', '2022-01-25', NULL, 0, 1, 21, '2022-01-27 05:55:48', 27, '2022-01-27 05:55:48', NULL),
(23, 'WT/JAN/022', '35', '2022-01-29', NULL, 0, 1, 22, '2022-01-29 06:02:43', 27, '2022-01-29 06:02:43', NULL),
(24, 'WT/JAN/023', '28', '2022-01-31', NULL, 0, 1, 23, '2022-01-31 05:35:55', 27, '2022-01-31 05:35:55', NULL),
(25, 'WT/JAN/024', '23', '2022-02-01', NULL, 0, 1, 24, '2022-01-31 13:07:50', 27, '2022-01-31 13:23:38', 27),
(26, 'WT/FEB/025', '38', '2022-02-02', NULL, 0, 1, 25, '2022-02-02 07:02:25', 27, '2022-02-02 08:14:59', 27),
(27, 'WT/FEB/026', '26', '2022-02-02', NULL, 0, 1, 26, '2022-02-02 10:53:32', 27, '2022-02-02 10:53:32', NULL),
(28, 'WT/FEB/027', '39', '2022-02-08', NULL, 0, 1, 27, '2022-02-09 06:26:09', 27, '2022-02-09 06:26:09', NULL),
(29, 'WT/FEB/028', '40', '2022-02-11', NULL, 0, 1, 28, '2022-02-10 11:59:31', 27, '2022-02-10 11:59:31', NULL),
(30, 'WT/FEB/029', '40', '2022-02-12', NULL, 0, 1, 29, '2022-02-12 12:41:48', 27, '2022-02-12 12:41:48', NULL),
(31, 'WT/FEB/030', '41', '2022-02-18', NULL, 0, 1, 30, '2022-02-18 10:18:40', 27, '2022-02-18 10:18:40', NULL),
(32, 'WT/FEB/031', '42', '2022-02-18', NULL, 0, 1, 31, '2022-02-18 10:27:16', 27, '2022-02-18 10:27:16', NULL),
(33, 'WT/FEB/032', '12', '2022-02-18', NULL, 0, 1, 32, '2022-02-18 10:54:59', 27, '2022-02-18 10:54:59', NULL),
(34, 'WT/FEB/033', '39', '2022-02-07', NULL, 0, 1, 33, '2022-02-18 10:57:28', 27, '2022-02-18 10:57:28', NULL),
(35, 'WT/FEB/034', '45', '2022-02-18', NULL, 0, 1, 34, '2022-02-18 11:14:14', 27, '2022-02-18 11:14:14', NULL),
(36, 'WT/FEB/035', '47', '2022-02-18', NULL, 0, 1, 35, '2022-02-18 11:17:42', 27, '2022-02-18 11:17:42', NULL),
(37, 'WT/FEB/036', '36', '2022-02-18', NULL, 0, 1, 36, '2022-02-18 11:20:01', 27, '2022-02-18 11:20:01', NULL),
(38, 'WT/FEB/037', '40', '2022-02-21', NULL, 0, 1, 37, '2022-02-21 06:35:52', 27, '2022-02-21 06:35:52', NULL),
(39, 'WT/FEB/038', '48', '2022-02-23', NULL, 0, 1, 38, '2022-02-23 05:48:03', 27, '2022-02-23 05:48:03', NULL),
(40, 'WT/MAR/039', '49', '2022-03-05', NULL, 0, 1, 39, '2022-03-05 06:58:37', 27, '2022-03-05 06:58:37', NULL),
(41, 'test11', '51', '2022-07-01', NULL, 0, 3, 0, '2022-07-03 10:33:14', 1, '2022-07-03 10:33:14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `delivery_detail`
--

CREATE TABLE `delivery_detail` (
  `id` int(11) NOT NULL,
  `delivery_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `brand` varchar(50) NOT NULL,
  `quantity` int(11) NOT NULL,
  `company_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `delivery_detail`
--

INSERT INTO `delivery_detail` (`id`, `delivery_id`, `item_id`, `brand`, `quantity`, `company_id`) VALUES
(1, 1, 70, 'BINATONE', 1, 1),
(2, 2, 69, 'SECURNET', 270, 1),
(3, 3, 75, 'DAICHI', 1, 1),
(4, 4, 46, 'DAICHI', 1, 1),
(5, 5, 22, 'HIKVISION', 1, 1),
(6, 5, 1, 'DAHUA', 4, 1),
(7, 5, 47, 'SEAGATE', 1, 1),
(8, 5, 34, 'CPPLUS', 1, 1),
(9, 5, 30, 'XPIA', 1, 1),
(10, 5, 71, 'SMART-I ', 1, 1),
(11, 5, 49, 'WBOX', 10, 1),
(12, 5, 50, 'WBOX', 5, 1),
(13, 5, 54, 'HI-FOCUS', 1, 1),
(14, 5, 66, 'CPPLUS', 88, 1),
(15, 6, 74, 'RELICELL', 1, 1),
(16, 7, 78, 'TOSHIBA', 1, 1),
(22, 9, 16, 'DAHUA', 1, 1),
(23, 9, 8, 'DAHUA', 4, 1),
(24, 9, 9, 'DAHUA', 1, 1),
(25, 9, 63, 'DAICHI', 1, 1),
(26, 9, 61, 'DAHUA', 1, 1),
(27, 9, 30, 'XPIA', 1, 1),
(28, 10, 59, 'BRANDED', 4, 1),
(29, 10, 65, 'RAPOO', 1, 1),
(30, 10, 87, 'MULTYBYTE', 0, 1),
(31, 10, 54, 'HI-FOCUS', 0, 1),
(32, 10, 83, 'SECURNET', 127, 1),
(33, 10, 80, 'FOXIN', 1, 1),
(34, 11, 73, 'SECURNET', 12, 1),
(35, 11, 45, 'HIKVISION', 0, 1),
(36, 12, 40, 'ZEBRONIC', 1, 1),
(37, 12, 23, 'SMART-I ', 0, 1),
(38, 12, 4, 'DAHUA', 0, 1),
(39, 13, 1, 'DAHUA', 0, 1),
(40, 14, 23, 'SMART-I ', 2, 1),
(41, 14, 26, 'CPPLUS', 1, 1),
(42, 14, 1, 'DAHUA', 2, 1),
(43, 14, 6, 'DAHUA', 1, 1),
(44, 14, 49, 'WBOX', 10, 1),
(45, 14, 50, 'WBOX', 6, 1),
(46, 15, 88, 'SECUREYE', 1, 1),
(47, 15, 58, 'HI-FOCUS', 1, 1),
(48, 15, 83, 'SECURNET', 73, 1),
(49, 15, 84, 'HI-FOCUS', 1, 1),
(50, 15, 73, 'SECURNET', 2, 1),
(51, 16, 68, 'HIKVISION', 90, 1),
(52, 17, 68, 'HIKVISION', 90, 1),
(53, 18, 77, 'HIKVISION', 305, 1),
(54, 19, 43, 'HIKVISION', 1, 1),
(55, 19, 65, 'RAPOO', 1, 1),
(56, 20, 43, 'HIKVISION', 1, 1),
(57, 21, 42, 'HIKVISION', 1, 1),
(58, 22, 91, 'SMART-I ', 181, 1),
(59, 23, 83, 'SECURNET', 105, 1),
(60, 24, 19, 'DAHUA', 1, 1),
(61, 25, 8, 'DAHUA', 3, 1),
(62, 25, 93, 'MULTYBYTE', 1, 1),
(63, 25, 51, 'MULTYBYTE', 1, 1),
(64, 25, 94, 'XPIA', 1, 1),
(65, 25, 73, 'SECURNET', 12, 1),
(66, 25, 76, 'Zimpack', 0, 1),
(67, 25, 83, 'SECURNET', 265, 1),
(68, 25, 59, 'BRANDED', 3, 1),
(69, 26, 66, 'CPPLUS', 0, 1),
(70, 26, 54, 'HI-FOCUS', 0, 1),
(71, 26, 35, 'CPPLUS', 0, 1),
(72, 26, 49, 'WBOX', 0, 1),
(73, 26, 50, 'WBOX', 0, 1),
(74, 27, 66, 'CPPLUS', 75, 1),
(75, 28, 34, 'CPPLUS', 1, 1),
(76, 28, 96, 'D-LINK', 1, 1),
(77, 28, 49, 'WBOX', 8, 1),
(78, 29, 3, 'DAHUA', 6, 1),
(79, 29, 4, 'DAHUA', 2, 1),
(80, 29, 19, 'DAHUA', 1, 1),
(81, 29, 34, 'CPPLUS', 1, 1),
(82, 29, 71, 'SMART-I ', 1, 1),
(83, 29, 49, 'WBOX', 16, 1),
(84, 29, 50, 'WBOX', 8, 1),
(85, 29, 81, 'D-LINK', 270, 1),
(86, 29, 59, 'BRANDED', 8, 1),
(87, 29, 95, 'SEAGATE', 1, 1),
(88, 30, 81, 'D-LINK', 90, 1),
(89, 30, 52, 'MULTYBYTE', 1, 1),
(90, 31, 23, 'SMART-I ', 1, 1),
(91, 31, 24, 'SMART-I ', 1, 1),
(92, 31, 48, 'CONSISTENT', 1, 1),
(93, 31, 20, 'DAHUA', 1, 1),
(94, 31, 66, 'CPPLUS', 25, 1),
(95, 31, 71, 'SMART-I ', 1, 1),
(96, 31, 35, 'CPPLUS', 1, 1),
(97, 31, 49, 'WBOX', 4, 1),
(98, 31, 50, 'WBOX', 2, 1),
(99, 31, 30, 'XPIA', 1, 1),
(100, 32, 2, 'DAHUA', 2, 1),
(101, 32, 66, 'CPPLUS', 148, 1),
(102, 32, 49, 'WBOX', 9, 1),
(103, 32, 50, 'WBOX', 1, 1),
(104, 33, 98, 'D-LINK', 1, 1),
(105, 34, 96, 'D-LINK', 1, 1),
(106, 35, 104, 'ASUS', 1, 1),
(107, 35, 102, 'LOGITECH', 1, 1),
(108, 35, 101, 'EVM', 2, 1),
(109, 36, 89, 'DAHUA', 1, 1),
(110, 36, 100, 'SANDISK', 1, 1),
(111, 37, 89, 'DAHUA', 1, 1),
(112, 37, 92, 'SANDISK', 1, 1),
(113, 38, 99, 'SECUREYE', 1, 1),
(114, 39, 77, 'HIKVISION', 305, 1),
(115, 39, 59, 'BRANDED', 4, 1),
(116, 40, 83, 'SECURNET', 196, 1),
(117, 40, 91, 'SMART-I ', 75, 1),
(118, 40, 73, 'SECURNET', 8, 1),
(119, 40, 7, 'DAHUA', 2, 1),
(120, 40, 9, 'DAHUA', 1, 1),
(121, 41, 110, 'tesla', 10, 3);

-- --------------------------------------------------------

--
-- Stand-in structure for view `digits`
-- (See below for the actual view)
--
CREATE TABLE `digits` (
`digit` int(1)
);

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `code` varchar(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `unit` varchar(30) NOT NULL,
  `quantity` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `brand` varchar(100) NOT NULL,
  `model_no` varchar(50) DEFAULT NULL,
  `current_price` double(14,2) NOT NULL DEFAULT 0.00,
  `company_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_by` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `code`, `name`, `unit`, `quantity`, `stock`, `brand`, `model_no`, `current_price`, `company_id`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, '', '1 MP HD BULLETE CAMERA', 'Pics', 12, 6, 'DAHUA', 'DH-HAC-B1A11P', 776.40, 1, '2022-01-10 16:50:48', 0, '2022-01-24 10:32:23', 27),
(2, '', '2 MP HD BULLETE CAMERA', 'Pics', 11, 9, 'DAHUA', 'DH-HAC-B1A21P', 1038.00, 1, '2022-01-10 16:50:48', 0, '2022-02-18 10:27:16', 27),
(3, '', '2 MP HD COLOUR  BULLETE CAMERA', 'Pics', 8, 2, 'DAHUA', 'DH-HAC-HFW1209CP-LED', 1418.00, 1, '2022-01-10 16:50:48', 0, '2022-02-10 11:59:31', 27),
(4, '', '2 MP HD COLOUR DOME CAMERA', 'Pics', 5, 3, 'DAHUA', 'DH-HAC-HFW1239-TLP-LED', 1523.00, 1, '2022-01-10 16:50:48', 0, '2022-02-10 11:59:31', 27),
(5, '', '2 MP HD DOME CAMERA', 'Pics', 7, 7, 'DAHUA', 'DH-HAC-T1A21P', 935.00, 1, '2022-01-10 16:50:48', 0, '2022-01-17 17:36:00', 27),
(6, '', '1 MP HD DOME CAMERA', 'Pics', 3, 2, 'DAHUA', 'DH-HAC-TA111P', 665.50, 1, '2022-01-10 16:50:48', 0, '2022-01-22 05:27:26', 27),
(7, '', '2 MP IP COLOUR BULLETE CAMERA NEW STOCK', 'Pics', 15, 13, 'DAHUA', 'DH-IPC-HFW1239S1P-LED-S4', 2993.66, 1, '2022-01-10 16:50:48', 0, '2022-03-05 06:58:37', 27),
(8, '', '2 MP IP COLOUR BULLETE CAMERA', 'Pics', 8, 1, 'DAHUA', 'DH-IPC-HFW1239S1P-LED-S4', 2783.00, 1, '2022-01-10 16:50:48', 0, '2022-01-31 13:07:50', 27),
(9, '', '2 MP IP COLOUR DOME CAMERA', 'Pics', 4, 2, 'DAHUA', 'HDW1239-T1P', 2678.00, 1, '2022-01-10 16:50:48', 0, '2022-03-05 06:58:37', 27),
(10, '', '2 MP IP COLOUR BULLETE WITH MIC AND SSD  SLOT', 'Pics', 2, 2, 'DAHUA', 'DH-IPC-HFW1230MP-AS-LED-B', 2783.00, 1, '2022-01-10 16:50:48', 0, '2022-01-17 17:32:58', 27),
(11, '', '2 MP IP DOME  VERIFOCAL CAMERA ', 'Pics', 1, 1, 'DAHUA', 'DH-IPC-D2B20P-ZS', 3540.00, 1, '2022-01-10 16:50:48', 0, '2022-01-17 17:32:09', 27),
(12, '', '2 MP IP BULLETE CAMERA', 'Pics', 1, 1, 'HIKVISION', 'DS-2CD1013G0E-I', 3068.00, 1, '2022-01-10 16:50:48', 0, '2022-01-17 17:31:37', 27),
(13, '', '2 MP IP DOME CAMERA', 'Pics', 20, 20, 'DAHUA', 'DS-IPCHDW1230T1P-S4', 2537.00, 1, '2022-01-10 16:50:48', 0, '2022-01-17 16:38:14', 27),
(14, '', '2 MP IP DOME CAMERA', 'Pics', 1, 1, 'HIKVISION', 'DS-2CD1323GO-IU', 3658.00, 1, '2022-01-10 16:50:48', 0, '2022-01-17 17:30:05', 27),
(15, '', '4 MP IP BULLETE CAMERA ', 'Pics', 1, 1, 'DAHUA', 'DH-IPC-HFW14B0SP', 4125.00, 1, '2022-01-10 16:50:48', 0, '2022-01-17 17:29:28', 27),
(16, '', '2 MP 16 CHANNEL NVR', 'Pics', 1, 0, 'DAHUA', 'DH-NVR2116HS-4KS2', 5157.00, 1, '2022-01-10 16:50:49', 0, '2022-01-20 12:33:46', 27),
(17, '', '2 MP 8 CHANNEL NVR', 'Pics', 1, 1, 'DAHUA', 'DH-NVR1108HS-S3/H', 3582.00, 1, '2022-01-10 16:50:49', 0, '2022-01-17 17:28:15', 27),
(18, '', '2 MP 4 CHANNEL NVR', 'Pics', 3, 3, 'DAHUA', 'DH-NVR1104HS-S3', 3011.00, 1, '2022-01-10 16:50:49', 0, '2022-01-17 17:27:41', 27),
(19, '', '2 MP 8 CHANNEL DVR', 'Pics', 2, 0, 'DAHUA', 'DH-XVR-4B08', 3605.00, 1, '2022-01-10 16:50:49', 0, '2022-02-10 11:59:31', 27),
(20, '', '2 MP 4 CHANNEL DVR', 'Pics', 1, 0, 'DAHUA', 'DH-XVR-4B04', 2285.00, 1, '2022-01-10 16:50:49', 0, '2022-02-18 10:18:40', 27),
(21, '', '2 MP16 CHANNEL DVR', 'Pics', 1, 1, 'CPPLUS', 'CP-UVR-1601E1-HC', 5632.00, 1, '2022-01-10 16:50:49', 0, '2022-01-17 17:25:30', 27),
(22, '', '1 MP 8 CHANNEL DVR', 'Pics', 1, 0, 'HIKVISION', 'DH-7108HGHI-F1', 4300.00, 1, '2022-01-10 16:50:49', 0, '2022-01-18 05:59:21', 27),
(23, '', '2 MP HD DOME CAMERA', 'Pics', 4, 1, 'SMART-I ', 'SI-HD-CP', 650.00, 1, '2022-01-10 16:50:49', 0, '2022-02-18 10:18:40', 27),
(24, '', '2 MP HD BULLETE CAMERA', 'Pics', 1, 0, 'SMART-I ', 'SI-HB-2-SL', 750.00, 1, '2022-01-10 16:50:49', 0, '2022-02-18 10:18:40', 27),
(25, '', '2 MP IP BULLETE CAMERA', 'Pics', 2, 2, 'SMART-I ', 'SI-1B--3AD', 1400.00, 1, '2022-01-10 16:50:49', 0, '2022-01-17 17:20:38', 27),
(26, '', '1 MP HD BULLETE CAMERA', 'Pics', 1, 0, 'CPPLUS', 'CP-USC-TC10PL2V3-0360', 784.00, 1, '2022-01-10 16:50:49', 0, '2022-01-22 05:27:26', 27),
(27, '', '2 MP HD BULLETE CAMERA', 'Pics', 2, 2, 'HONEYWELL IMPACT', 'I-HABC-2005PI-L', 0.00, 1, '2022-01-10 16:50:49', 0, '2022-01-16 18:02:31', 0),
(28, '', '2 MP HD BULLETE CAMERA', 'Pics', 3, 3, 'HONEYWELL IMPACT', 'I-HADC-2005PI-L', 0.00, 1, '2022-01-10 16:50:49', 0, '2022-01-16 18:02:31', 0),
(29, '', '360 C6N WIRELESS CAMERA', 'Pics', 0, 0, 'EZVIZ', 'DS-C6N', 0.00, 1, '2022-01-10 16:50:49', 0, '2022-01-16 18:02:31', 0),
(30, '', '4 G ROUTER ALL SIM SUPPORTED', 'Pics', 4, 0, 'XPIA', 'WR-411-GU', 2200.00, 1, '2022-01-10 16:50:49', 0, '2022-02-18 10:18:40', 1),
(31, '', '20 AMP SMPS FOR CAMERAS', 'Pics', 1, 1, 'HIKVISION ', 'DS-AFA120K-DW-IN', 0.00, 1, '2022-01-10 16:50:49', 0, '2022-01-16 18:02:31', 0),
(32, '', '5 AMP SMPS FOR CAMERAS', 'Pics', 0, 0, 'HIKVISION', 'DS-2FA1205-DW-IN', 0.00, 1, '2022-01-10 16:50:49', 0, '2022-01-16 18:02:31', 0),
(33, '', '10 AMP SMPS FOR CAMERAS', 'Pics', 0, 0, 'HIKVISION', 'DS-AFA120A-DW-IN', 0.00, 1, '2022-01-10 16:50:49', 0, '2022-01-16 18:02:31', 0),
(34, '', '10 AMP SMPS FOR CAMERAS', 'Pics', 5, 7, 'CPPLUS', 'CP-DPS-MD100-12D', 644.28, 1, '2022-01-10 16:50:49', 0, '2022-02-24 08:24:02', 27),
(35, '', '5 AMP SMPS FOR CAMERAS', 'Pics', 4, 3, 'CPPLUS', 'CP-DPS-MD50-12D', 444.86, 1, '2022-01-10 16:50:49', 0, '2022-02-18 10:18:40', 27),
(36, '', '10 AMP SMPS FOR CAMERAS', 'Pics', 1, 1, 'SECUREYE', '', 0.00, 1, '2022-01-10 16:50:49', 0, '2022-01-16 18:02:31', 0),
(37, '', '3 AMP POWER ADAPTOR', 'Pics', 2, 2, 'SECURNET', 'SECURNET12V-3A', 207.00, 1, '2022-01-10 16:50:49', 0, '2022-01-17 16:54:03', 27),
(38, '', '1 PORT POE SWITCH', 'Pics', 2, 2, 'SMART-I ', 'SINGLE POE', 0.00, 1, '2022-01-10 16:50:49', 0, '2022-01-16 18:02:31', 0),
(39, '', '20 AMP SMPS FOR CAMERAS', 'Pics', 2, 2, 'CPPLUS', 'DPS-MD200-12D', 1080.00, 1, '2022-01-10 16:50:49', 0, '2022-01-17 16:46:28', 27),
(40, '', '20 AMP SMPS FOR CAMERAS', 'Pics', 1, 0, 'ZEBRONIC', 'ZEB-1020A250-R', 1080.00, 1, '2022-01-10 16:50:49', 0, '2022-01-20 13:33:56', 27),
(41, '', '20 AMP SMPS FOR CAMERAS', 'Pics', 2, 2, 'SMART-I ', 'SMART20A', 0.00, 1, '2022-01-10 16:50:49', 0, '2022-01-16 18:02:31', 0),
(42, '', '16 PORT POE SWITCH', 'Pics', 1, 0, 'HIKVISION ', 'Ds-3e0518p-e/m', 0.00, 1, '2022-01-10 16:50:49', 0, '2022-01-27 05:50:14', 0),
(43, '', '8 PORT POE SWITCH', 'Pics', 2, 0, 'HIKVISION', 'DS-3E0109P-E/M', 0.00, 1, '2022-01-10 16:50:49', 0, '2022-01-27 05:45:12', 0),
(44, '', '8 PORT NETWORKING SWITCH 10/100', 'Pics', 1, 1, 'HIKVISION ', 'DS - 3E0108D-E', 0.00, 1, '2022-01-10 16:50:49', 0, '2022-01-16 18:02:31', 0),
(45, '', '4 PORT NETWORKING SWITCH 10/100', 'Pics', 1, 1, 'HIKVISION', 'DS - 3E0105D-E ', 0.00, 1, '2022-01-10 16:50:49', 0, '2022-01-24 10:29:08', 0),
(46, '', '1 TB SERVEILLANCE HARD DISK DRIVE', 'Pics', 1, 0, 'DAICHI', 'DI-D00AB', 2800.00, 1, '2022-01-10 16:50:49', 0, '2022-01-17 17:47:00', 27),
(47, '', '1 TB SERVEILLANCE HARD DISK DRIVE', 'Pics', 3, 2, 'SEAGATE', 'ST-1000VX005', 3000.00, 1, '2022-01-10 16:50:49', 0, '2022-01-18 05:59:21', 1),
(48, '', '500GB  SERVEILLANCE HARD DISK DRIVE', 'Pics', 1, 2, 'CONSISTENT', 'CT-3500-SC', 1260.00, 1, '2022-01-10 16:50:49', 0, '2022-02-18 10:18:40', 27),
(49, '', 'BNC CONNECTOR', 'Pics', 125, 68, 'WBOX', 'BNC', 12.00, 1, '2022-01-10 16:50:50', 0, '2022-02-18 10:27:16', 27),
(50, '', 'DC CONNECTORS', 'Pics', 100, 78, 'WBOX', 'DC', 5.99, 1, '2022-01-10 16:50:50', 0, '2022-02-18 10:27:16', 27),
(51, '', '5 MTR USB EXTENTION', 'Pics', 5, 4, 'MULTYBYTE', '5MTRUSBEXT', 0.00, 1, '2022-01-10 16:50:50', 0, '2022-01-31 13:07:50', 0),
(52, '', '3 MTR USB EXTENTION', 'Pics', 2, 1, 'MULTYBYTE', '3MTRUSBEXT', 0.00, 1, '2022-01-10 16:50:50', 0, '2022-02-12 12:41:48', 0),
(53, '', '5 MTR HDMI CABLE', 'Pics', 0, 0, 'HI-FOCUS', '5MTRHDMI', 0.00, 1, '2022-01-10 16:50:50', 0, '2022-01-16 18:02:31', 0),
(54, '', '3 MTR HDMI CABLE', 'Pics', 2, 3, 'HI-FOCUS', '3MTRHDMI', 0.00, 1, '2022-01-10 16:50:50', 0, '2022-02-02 08:14:59', 0),
(55, '', '5 MTR VGA CABLE', 'Pics', 2, 4, 'HI-FOCUS', '5MTRVGA', 0.00, 1, '2022-01-10 16:50:50', 0, '2022-01-20 12:04:34', 0),
(56, '', '3 MTR VGA CABLE', 'Pics', 0, 0, 'HI-FOCUS', '3MTRVGA', 0.00, 1, '2022-01-10 16:50:50', 0, '2022-01-16 18:02:31', 0),
(57, '', '1.5 MTR VGA CABLE', 'Pics', 3, 3, 'HI-FOCUS', '1.5MTRVGA', 60.00, 1, '2022-01-10 16:50:50', 0, '2022-01-17 16:33:19', 27),
(58, '', '1.5 MTR HDMI CABLE', 'Pics', 1, 0, 'HI-FOCUS', '1.5MTRHDMI', 0.00, 1, '2022-01-10 16:50:50', 0, '2022-01-24 12:59:36', 0),
(59, '', '4*4 PVC BOX FOR CAMERAS', 'Pics', 26, 7, 'BRANDED', '4*4PVCBOX', 18.00, 1, '2022-01-10 16:50:50', 0, '2022-02-23 05:48:03', 27),
(60, '', '2 MP 8CHANNEL DVR', 'Pics', 1, 1, 'HONEYWELL IMPACT', '2108-L', 4684.00, 1, '2022-01-10 16:50:50', 0, '2022-01-17 17:02:53', 27),
(61, '', '8 PORT POE SWITCH GIGA UPLINK', 'Pics', 3, 2, 'DAHUA', 'DH-PFS3009-8ET1GT-96', 3593.00, 1, '2022-01-10 16:50:50', 0, '2022-01-20 12:33:46', 27),
(62, '', '8 PORT DUAL UPLINK POE SWITCH', 'Pics', 1, 0, 'DAHUA', 'DH-PFS3006-4ET-36', 1997.00, 1, '2022-01-10 16:50:50', 0, '2022-01-27 05:41:03', 27),
(63, '', '2 TB HARD DISK DRIVE', 'Pics', 1, 0, 'DAICHI', '2TBDAICHI', 3835.00, 1, '2022-01-10 16:50:50', 0, '2022-01-20 12:33:46', 27),
(64, '', '1.5MTR VGA CABLE', 'Pics', 5, 5, 'TERABYTE', '1.5MTRVGA', 59.00, 1, '2022-01-10 16:50:50', 0, '2022-01-17 17:08:28', 27),
(65, '', 'WIRELESS MOUSE', 'Pics', 2, 0, 'RAPOO', 'RAPOO1620', 539.30, 1, '2022-01-10 16:50:50', 0, '2022-01-27 05:41:03', 27),
(66, '', '3+1 COPPER CABLE', 'Meter', 180, 24, 'CPPLUS', '', 14.00, 1, '2022-01-10 17:03:04', 1, '2022-02-18 10:27:16', 1),
(67, '', '4U LOADED NETWORKING RACK', 'Pics', 1, 2, 'SECURNET', '', 2000.00, 1, '2022-01-10 17:03:48', 1, '2022-01-20 12:04:34', 1),
(68, '', '3+1 COPPER CABLE', 'Meter', 180, 0, 'HIKVISION', '', 17.50, 1, '2022-01-10 17:06:28', 1, '2022-01-27 05:35:41', 27),
(69, '', '3+1 COPPER CABLE', 'Meter', 270, 0, 'SECURNET', '', 12.00, 1, '2022-01-10 17:08:20', 1, '2022-01-16 18:02:31', 1),
(70, '', 'CORDED LANDLINE PHONE WITH DISPLAY', 'Pics', 0, 0, 'BINATONE', 'SPIRIT 211N', 690.00, 1, '2022-01-11 06:49:00', 1, '2022-01-16 18:02:31', 1),
(71, '', '2 U NETWORKING RACK', 'Pics', 0, 6, 'SMART-I ', '2U RACK', 580.00, 1, '2022-01-11 08:54:53', 1, '2022-02-18 10:18:40', 1),
(72, '', 'INSTALLATION AND COMMISSIONING CHARGES', 'Pics', 0, 0, 'INSTALLATION', '', 0.00, 1, '2022-01-11 09:35:35', 1, '2022-01-16 18:02:31', 0),
(73, '', 'RJ-45 CONNECTORS', 'Pics', 0, 19, 'SECURNET', 'Rj-45', 5.00, 1, '2022-01-11 10:29:35', 1, '2022-03-05 06:58:37', 1),
(74, '', '1/2 KVA UPS BATTERY', 'Pics', 0, 1, 'RELICELL', '', 720.00, 1, '2022-01-14 08:11:53', 1, '2022-01-18 06:33:23', 0),
(75, '', '128 GB SSD ', 'Pics', 0, 4, 'DAICHI', 'DSSD128', 1250.00, 1, '2022-01-14 13:44:15', 1, '2022-01-16 18:02:31', 0),
(76, '', '4G WIRELESS ROUTER', 'Pics', 2, 2, 'Zimpack', '4G ROUTER', 2100.00, 1, '2022-01-18 09:43:50', 27, '2022-02-02 08:35:18', 27),
(77, '', 'CAT 6 CABLE', 'Meter', 305, 0, 'HIKVISION', 'DS-1LN6U-G', 16.24, 1, '2022-01-18 09:45:31', 27, '2022-02-23 05:48:03', 0),
(78, '9121S5IKSC', '2 TB SERVEILLANCE HARD DISK DRIVE ', 'Pics', 0, 0, 'TOSHIBA', '', 4000.00, 1, '2022-01-19 10:58:41', 27, '2022-01-19 11:55:37', 0),
(79, '', '2MTR NETWORKING PATCH CORD', 'Pics', 0, 26, 'D-LINK', 'C6UGRYR1-2M', 135.00, 1, '2022-01-19 11:01:34', 27, '2022-01-19 11:03:13', 0),
(80, '2421105366', '1/2 KVA UPS 600VA', 'Pics', 0, 0, 'FOXIN', 'FPS-755', 2025.00, 1, '2022-01-20 10:52:35', 27, '2022-01-20 13:15:22', 0),
(81, '', '3+1 COOPER CABLE', 'Meter', 0, 90, 'D-LINK', 'D-LINK90M', 13.00, 1, '2022-01-20 10:58:25', 27, '2022-02-12 12:41:48', 27),
(82, 'CTB5002A22', 'Consistent Hdd 500gb ', 'Pics', 0, 0, 'CONSISTENT', 'CT3500SC', 1200.00, 1, '2022-01-20 11:04:48', 27, '2022-01-20 11:04:48', 0),
(83, '', 'CAT-6 COPPER CABLE', 'Meter', 0, 109, 'SECURNET', 'SECCAT-6', 22.00, 1, '2022-01-20 11:41:54', 27, '2022-03-05 06:58:37', 27),
(84, '', 'WIRELESS MOUSE', 'Pics', 0, 1, 'HI-FOCUS', 'HF-M189', 300.00, 1, '2022-01-20 11:45:05', 27, '2022-01-24 12:59:36', 0),
(85, '2104065343', 'DESKTOP SMPS', 'Pics', 0, 0, 'ENTER', 'E-500F', 470.00, 2, '2022-01-20 12:15:56', 28, '2022-01-20 12:15:56', 0),
(86, '', 'POWER EXTENTION BOARD', 'Pics', 0, 0, 'MULTYBYTE', '', 165.00, 1, '2022-01-20 12:41:36', 27, '2022-01-20 12:41:36', 0),
(87, '', '1.5 MTR USB EXTENTION CABLE', 'Pics', 1, 1, 'MULTYBYTE', '', 100.00, 1, '2022-01-20 12:43:04', 27, '2022-01-24 10:31:54', 0),
(88, 'SN20210806', 'HDMI EXETENDER KVM SUPPORT ', 'Pics', 0, 0, 'SECUREYE', 'S-HDKEC-120M', 7667.00, 1, '2022-01-24 12:50:50', 27, '2022-01-24 12:59:36', 0),
(89, '', 'RANGER 2 ', 'Pics', 0, 1, 'DAHUA', 'IPC-A22EP', 2553.00, 1, '2022-01-27 05:27:50', 27, '2022-02-18 11:20:01', 0),
(90, '', '16 PORT DUAL UPLINK POE SWITCH', 'Pics', 1, 1, 'XPIA', '', 7200.00, 1, '2022-01-27 05:47:56', 27, '2022-01-27 05:47:56', 0),
(91, '', 'CAT-6 COPPER CABLE', 'Meter', 305, 49, 'SMART-I ', '', 20.98, 1, '2022-01-27 05:54:58', 27, '2022-03-05 06:58:37', 0),
(92, '', '32 GB FAST MINI SD CARD', 'Pics', 0, 0, 'SANDISK', '', 350.00, 1, '2022-01-29 06:07:26', 27, '2022-02-18 11:20:01', 0),
(93, '', '10 MTR HDMI CABLE', 'Pics', 0, 0, 'MULTYBYTE', '10MTRHDMI', 330.00, 1, '2022-01-31 12:46:15', 27, '2022-01-31 13:07:50', 0),
(94, '', '3 MI IP DOME WITH MIC', 'Pics', 0, 0, 'XPIA', 'XP-1424C30-IP', 1400.00, 1, '2022-01-31 12:47:42', 27, '2022-01-31 13:07:50', 0),
(95, 'wfm3pk6b', '2 TB SERVEILLANCE HARD DISK DRIVE ', 'Pics', 0, 1, 'SEAGATE', 'ST200VX015', 3988.00, 1, '2022-02-01 08:58:41', 27, '2022-02-18 10:59:45', 27),
(96, 'TW01107044', 'WI-FI EXTENDER ROUTER N300 DIR 615', 'Pics', 0, 0, 'D-LINK', 'N300', 900.00, 1, '2022-02-08 05:54:54', 27, '2022-02-18 10:57:28', 0),
(97, 'G-8146553', '1 MP TURBO HD DOME CAMERA WITH SWITCHER', 'Pics', 0, 0, 'HIKVISION', '2CE5ACOT-IRPF', 1098.00, 1, '2022-02-09 06:38:03', 27, '2022-02-09 06:38:03', 0),
(98, '', 'D-LINK ROUTER DIR-841', 'Pics', 0, 0, 'D-LINK', 'DIR-841', 2125.00, 1, '2022-02-10 11:43:16', 27, '2022-02-18 10:54:59', 0),
(99, '', 'WI-FI RESEIVER', 'Pics', 0, 0, 'SECUREYE', '', 325.00, 1, '2022-02-12 12:44:21', 27, '2022-02-21 06:35:52', 0),
(100, '', '64 GB MICRO SD CARD', 'Pics', 0, 0, 'SANDISK', '', 650.00, 1, '2022-02-18 10:37:56', 27, '2022-02-18 11:17:42', 0),
(101, '', '8 GB PENDRIVE STEEL BODY', 'Pics', 0, 0, 'EVM', '', 300.00, 1, '2022-02-18 10:40:13', 27, '2022-02-18 11:14:14', 0),
(102, '', 'WIRELESS MOUSE', 'Pics', 0, 1, 'LOGITECH', 'M170', 575.00, 1, '2022-02-18 10:46:33', 27, '2022-02-18 11:14:14', 0),
(103, '', 'LASERJET MFP M126 NW  PRINTER', 'Pics', 0, 1, 'HP', 'MFP M126 NW', 18500.00, 1, '2022-02-18 10:50:50', 27, '2022-02-18 10:52:29', 0),
(104, '', 'ASUS LAPTOP', 'Pics', 0, 0, 'ASUS', 'X415EA', 39250.00, 1, '2022-02-18 11:09:42', 27, '2022-02-18 11:14:14', 0),
(105, '7F03A15PAJ', 'DAHUA IP  PTZ', 'Pics', 0, 1, 'DAHUA', '', 21600.00, 1, '2022-03-09 13:15:23', 27, '2022-03-10 12:36:01', 0),
(106, 'ZTT46DW3', '4 TB SERVEILLANCE HARD DISK DRIVE ', 'Pics', 0, 0, 'SEAGATE', 'SG 4TB AV', 7200.00, 1, '2022-03-09 13:17:05', 27, '2022-03-09 13:17:05', 0),
(107, '', '5 PORT SWITCH', 'Pics', 0, 0, 'D-LINK', '5 PORT SWITCH', 560.00, 1, '2022-03-09 13:20:48', 27, '2022-03-09 13:20:48', 0),
(108, 'J22344162', '32 CHANNEL HD NVR', 'Pics', 0, 0, 'HIKVISION', 'DS-7632NI-K2', 13275.00, 1, '2022-03-10 12:39:02', 27, '2022-03-10 12:39:02', 0),
(109, '', '6U FULLY DOADED RACK', 'Pics', 0, 0, 'SECURNET', '6U RACK', 3450.00, 1, '2022-03-10 12:45:27', 27, '2022-03-10 12:45:27', 0),
(110, '12345', 'test11', 'Meter', 50, 40, 'tesla', 'test11', 100.00, 3, '2022-07-03 10:32:35', 1, '2022-07-03 10:33:14', 0);

-- --------------------------------------------------------

--
-- Table structure for table `manage_stock`
--

CREATE TABLE `manage_stock` (
  `id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `opening_stock` double NOT NULL,
  `in_stock` double NOT NULL,
  `out_stock` double NOT NULL,
  `closing_stock` double NOT NULL,
  `date` date NOT NULL,
  `company_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `manage_stock`
--

INSERT INTO `manage_stock` (`id`, `item_id`, `opening_stock`, `in_stock`, `out_stock`, `closing_stock`, `date`, `company_id`) VALUES
(1, 1, 12, 0, 0, 0, '2022-01-10', 1),
(2, 2, 11, 0, 0, 0, '2022-01-10', 1),
(3, 3, 8, 0, 0, 0, '2022-01-10', 1),
(4, 4, 5, 0, 0, 0, '2022-01-10', 1),
(5, 5, 7, 0, 0, 0, '2022-01-10', 1),
(6, 6, 3, 0, 0, 0, '2022-01-10', 1),
(7, 7, 15, 0, 0, 0, '2022-01-10', 1),
(8, 8, 8, 0, 0, 0, '2022-01-10', 1),
(9, 9, 4, 0, 0, 0, '2022-01-10', 1),
(10, 10, 2, 0, 0, 0, '2022-01-10', 1),
(11, 11, 1, 0, 0, 0, '2022-01-10', 1),
(12, 12, 1, 0, 0, 0, '2022-01-10', 1),
(13, 13, 20, 0, 0, 0, '2022-01-10', 1),
(14, 14, 1, 0, 0, 0, '2022-01-10', 1),
(15, 15, 1, 0, 0, 0, '2022-01-10', 1),
(16, 16, 1, 0, 0, 0, '2022-01-10', 1),
(17, 17, 1, 0, 0, 0, '2022-01-10', 1),
(18, 18, 3, 0, 0, 0, '2022-01-10', 1),
(19, 19, 2, 0, 0, 0, '2022-01-10', 1),
(20, 20, 1, 0, 0, 0, '2022-01-10', 1),
(21, 21, 1, 0, 0, 0, '2022-01-10', 1),
(22, 22, 1, 0, 0, 0, '2022-01-10', 1),
(23, 23, 4, 0, 0, 0, '2022-01-10', 1),
(24, 24, 1, 0, 0, 0, '2022-01-10', 1),
(25, 25, 2, 0, 0, 0, '2022-01-10', 1),
(26, 26, 1, 0, 0, 0, '2022-01-10', 1),
(27, 27, 2, 0, 0, 0, '2022-01-10', 1),
(28, 28, 3, 0, 0, 0, '2022-01-10', 1),
(29, 29, 0, 0, 0, 0, '2022-01-10', 1),
(30, 30, 4, 0, 0, 0, '2022-01-10', 1),
(31, 31, 1, 0, 0, 0, '2022-01-10', 1),
(32, 32, 0, 0, 0, 0, '2022-01-10', 1),
(33, 33, 0, 0, 0, 0, '2022-01-10', 1),
(34, 34, 5, 0, 0, 0, '2022-01-10', 1),
(35, 35, 4, 0, 0, 0, '2022-01-10', 1),
(36, 36, 1, 0, 0, 0, '2022-01-10', 1),
(37, 37, 2, 0, 0, 0, '2022-01-10', 1),
(38, 38, 2, 0, 0, 0, '2022-01-10', 1),
(39, 39, 2, 0, 0, 0, '2022-01-10', 1),
(40, 40, 1, 0, 0, 0, '2022-01-10', 1),
(41, 41, 2, 0, 0, 0, '2022-01-10', 1),
(42, 42, 1, 0, 0, 0, '2022-01-10', 1),
(43, 43, 2, 0, 0, 0, '2022-01-10', 1),
(44, 44, 1, 0, 0, 0, '2022-01-10', 1),
(45, 45, 1, 0, 0, 0, '2022-01-10', 1),
(46, 46, 1, 0, 0, 0, '2022-01-10', 1),
(47, 47, 3, 0, 0, 0, '2022-01-10', 1),
(48, 48, 1, 0, 0, 0, '2022-01-10', 1),
(49, 49, 125, 0, 0, 0, '2022-01-10', 1),
(50, 50, 100, 0, 0, 0, '2022-01-10', 1),
(51, 51, 5, 0, 0, 0, '2022-01-10', 1),
(52, 52, 2, 0, 0, 0, '2022-01-10', 1),
(53, 53, 0, 0, 0, 0, '2022-01-10', 1),
(54, 54, 2, 0, 0, 0, '2022-01-10', 1),
(55, 55, 2, 0, 0, 0, '2022-01-10', 1),
(56, 56, 0, 0, 0, 0, '2022-01-10', 1),
(57, 57, 3, 0, 0, 0, '2022-01-10', 1),
(58, 58, 1, 0, 0, 0, '2022-01-10', 1),
(59, 59, 26, 0, 0, 0, '2022-01-10', 1),
(60, 60, 1, 0, 0, 0, '2022-01-10', 1),
(61, 61, 3, 0, 0, 0, '2022-01-10', 1),
(62, 62, 1, 0, 0, 0, '2022-01-10', 1),
(63, 63, 1, 0, 0, 0, '2022-01-10', 1),
(64, 64, 5, 0, 0, 0, '2022-01-10', 1),
(65, 65, 2, 0, 0, 0, '2022-01-10', 1),
(66, 66, 180, 0, 0, 0, '2022-01-10', 1),
(67, 67, 1, 0, 0, 0, '2022-01-10', 1),
(68, 68, 180, 0, 0, 0, '2022-01-10', 1),
(69, 69, 270, 0, 0, 0, '2022-01-10', 1),
(128, 70, 0, 1, 1, 0, '2022-01-11', 1),
(129, 71, 0, 9, 0, 0, '2022-01-11', 1),
(130, 72, 0, 0, 0, 0, '2022-01-11', 1),
(131, 73, 0, 50, 0, 0, '2022-01-11', 1),
(132, 74, 0, 2, 0, 0, '2022-01-14', 1),
(133, 69, 270, 0, 270, 0, '2022-01-14', 1),
(134, 75, 0, 5, 1, 0, '2022-01-14', 1),
(135, 46, 1, 0, 1, 0, '2022-01-17', 1),
(136, 22, 1, 0, 1, 0, '2022-01-18', 1),
(137, 1, 12, 0, 5, 0, '2022-01-18', 1),
(138, 47, 3, 0, 1, 0, '2022-01-18', 1),
(139, 34, 5, 0, 1, 0, '2022-01-18', 1),
(140, 30, 4, 0, 1, 0, '2022-01-18', 1),
(141, 71, 9, 0, 1, 0, '2022-01-18', 1),
(142, 49, 125, 0, 10, 0, '2022-01-18', 1),
(143, 50, 100, 0, 5, 0, '2022-01-18', 1),
(144, 54, 2, 0, 1, 0, '2022-01-18', 1),
(145, 66, 180, 0, 128, 0, '2022-01-18', 1),
(146, 74, 2, 0, 1, 0, '2022-01-18', 1),
(147, 76, 0, 2, 0, 0, '2022-01-18', 1),
(148, 77, 305, 305, 0, 0, '2022-01-18', 1),
(149, 1, 7, 0, 4, 0, '2022-01-19', 1),
(150, 66, 52, 0, 88, 0, '2022-01-19', 1),
(151, 78, 0, 1, 1, 0, '2022-01-19', 1),
(152, 79, 0, 26, 0, 0, '2022-01-19', 1),
(153, 66, 92, 0, 0, 0, '2022-01-20', 1),
(154, 1, 3, 0, 2, 0, '2022-01-20', 1),
(155, 6, 3, 0, 1, 0, '2022-01-20', 1),
(156, 49, 115, 0, 2, 0, '2022-01-20', 1),
(157, 50, 95, 0, 1, 0, '2022-01-20', 1),
(158, 80, 0, 1, 1, 0, '2022-01-20', 1),
(159, 81, 0, 450, 0, 0, '2022-01-20', 1),
(160, 82, 0, 0, 0, 0, '2022-01-20', 1),
(161, 83, 0, 875, 305, 0, '2022-01-20', 1),
(162, 84, 0, 2, 0, 0, '2022-01-20', 1),
(163, 48, 1, 2, 0, 0, '2022-01-20', 1),
(164, 54, 1, 2, 1, 0, '2022-01-20', 1),
(165, 55, 2, 2, 0, 0, '2022-01-20', 1),
(166, 67, 1, 1, 0, 0, '2022-01-20', 1),
(167, 73, 50, 3, 20, 0, '2022-01-20', 1),
(168, 85, 0, 0, 0, 0, '2022-01-20', 2),
(169, 16, 1, 0, 1, 0, '2022-01-20', 1),
(170, 8, 8, 0, 6, 0, '2022-01-20', 1),
(171, 9, 4, 0, 2, 0, '2022-01-20', 1),
(172, 63, 1, 0, 1, 0, '2022-01-20', 1),
(173, 61, 3, 0, 1, 0, '2022-01-20', 1),
(174, 30, 3, 0, 1, 0, '2022-01-20', 1),
(175, 86, 0, 0, 0, 0, '2022-01-20', 1),
(176, 87, 1, 0, 1, 0, '2022-01-20', 1),
(177, 59, 26, 0, 8, 0, '2022-01-20', 1),
(178, 65, 2, 0, 1, 0, '2022-01-20', 1),
(179, 45, 1, 0, 1, 0, '2022-01-20', 1),
(180, 40, 1, 0, 1, 0, '2022-01-20', 1),
(181, 23, 4, 0, 1, 0, '2022-01-20', 1),
(182, 4, 5, 0, 1, 0, '2022-01-20', 1),
(183, 23, 3, 0, 2, 0, '2022-01-22', 1),
(184, 26, 1, 0, 1, 0, '2022-01-22', 1),
(185, 1, 1, 0, 2, 0, '2022-01-22', 1),
(186, 6, 2, 0, 1, 0, '2022-01-22', 1),
(187, 49, 113, 0, 10, 0, '2022-01-22', 1),
(188, 50, 94, 0, 6, 0, '2022-01-22', 1),
(189, 73, 33, 0, 14, 0, '2022-01-24', 1),
(190, 45, 0, 0, 0, 0, '2022-01-24', 1),
(191, 8, 2, 0, 4, 0, '2022-01-24', 1),
(192, 9, 2, 0, 1, 0, '2022-01-24', 1),
(193, 59, 18, 0, 4, 0, '2022-01-24', 1),
(194, 87, 0, 0, 0, 0, '2022-01-24', 1),
(195, 54, 2, 0, 0, 0, '2022-01-24', 1),
(196, 83, 570, 0, 200, 0, '2022-01-24', 1),
(197, 1, -1, 0, 0, 0, '2022-01-24', 1),
(198, 23, 1, 0, 0, 0, '2022-01-24', 1),
(199, 4, 4, 0, 0, 0, '2022-01-24', 1),
(200, 88, 0, 1, 1, 0, '2022-01-24', 1),
(201, 58, 1, 0, 1, 0, '2022-01-24', 1),
(202, 84, 2, 0, 1, 0, '2022-01-24', 1),
(203, 66, 92, 180, 0, 0, '2022-01-27', 1),
(204, 89, 0, 1, 0, 0, '2022-01-27', 1),
(205, 68, 180, 0, 180, 0, '2022-01-27', 1),
(206, 77, 610, 0, 305, 0, '2022-01-27', 1),
(207, 62, 1, 0, 1, 0, '2022-01-27', 1),
(208, 65, 1, 0, 1, 0, '2022-01-27', 1),
(209, 43, 2, 0, 2, 0, '2022-01-27', 1),
(210, 90, 1, 0, 0, 0, '2022-01-27', 1),
(211, 42, 1, 0, 1, 0, '2022-01-27', 1),
(212, 91, 305, 0, 181, 0, '2022-01-27', 1),
(213, 83, 370, 0, 105, 0, '2022-01-29', 1),
(214, 92, 0, 1, 0, 0, '2022-01-29', 1),
(215, 19, 2, 0, 1, 0, '2022-01-31', 1),
(216, 93, 0, 1, 1, 0, '2022-01-31', 1),
(217, 94, 0, 1, 1, 0, '2022-01-31', 1),
(218, 8, -2, 0, 3, 0, '2022-01-31', 1),
(219, 51, 5, 0, 1, 0, '2022-01-31', 1),
(220, 73, 19, 0, 12, 0, '2022-01-31', 1),
(221, 30, 2, 0, 1, 0, '2022-01-31', 1),
(222, 83, 265, 0, 265, 0, '2022-01-31', 1),
(223, 59, 14, 0, 3, 0, '2022-01-31', 1),
(224, 76, 2, 0, 1, 0, '2022-01-31', 1),
(225, 95, 0, 0, 0, 0, '2022-02-01', 1),
(226, 66, 272, 0, 75, 0, '2022-02-02', 1),
(227, 54, 2, 0, 0, 0, '2022-02-02', 1),
(228, 35, 4, 0, 0, 0, '2022-02-02', 1),
(229, 49, 103, 0, 0, 0, '2022-02-02', 1),
(230, 50, 88, 0, 0, 0, '2022-02-02', 1),
(231, 76, 1, 0, 0, 0, '2022-02-02', 1),
(232, 95, 0, 2, 0, 0, '2022-02-02', 1),
(233, 96, 0, 1, 0, 0, '2022-02-08', 1),
(234, 34, 4, 0, 1, 0, '2022-02-09', 1),
(235, 96, 1, 0, 1, 0, '2022-02-09', 1),
(236, 49, 103, 0, 8, 0, '2022-02-09', 1),
(237, 97, 0, 0, 0, 0, '2022-02-09', 1),
(238, 98, 0, 1, 0, 0, '2022-02-10', 1),
(239, 3, 8, 0, 6, 0, '2022-02-10', 1),
(240, 4, 4, 0, 2, 0, '2022-02-10', 1),
(241, 19, 1, 0, 1, 0, '2022-02-10', 1),
(242, 34, 3, 0, 1, 0, '2022-02-10', 1),
(243, 71, 8, 0, 1, 0, '2022-02-10', 1),
(244, 49, 95, 0, 16, 0, '2022-02-10', 1),
(245, 50, 88, 0, 8, 0, '2022-02-10', 1),
(246, 81, 450, 0, 270, 0, '2022-02-10', 1),
(247, 59, 11, 0, 8, 0, '2022-02-10', 1),
(248, 95, 2, 0, 1, 0, '2022-02-10', 1),
(249, 81, 180, 0, 90, 0, '2022-02-12', 1),
(250, 52, 2, 0, 1, 0, '2022-02-12', 1),
(251, 99, 0, 1, 0, 0, '2022-02-12', 1),
(252, 23, 1, 0, 1, 0, '2022-02-18', 1),
(253, 24, 1, 0, 1, 0, '2022-02-18', 1),
(254, 48, 3, 0, 1, 0, '2022-02-18', 1),
(255, 20, 1, 0, 1, 0, '2022-02-18', 1),
(256, 66, 197, 0, 173, 0, '2022-02-18', 1),
(257, 71, 7, 0, 1, 0, '2022-02-18', 1),
(258, 35, 4, 0, 1, 0, '2022-02-18', 1),
(259, 49, 79, 0, 13, 0, '2022-02-18', 1),
(260, 50, 80, 0, 3, 0, '2022-02-18', 1),
(261, 30, 1, 0, 1, 0, '2022-02-18', 1),
(262, 2, 11, 0, 2, 0, '2022-02-18', 1),
(263, 89, 1, 2, 2, 0, '2022-02-18', 1),
(264, 100, 0, 1, 1, 0, '2022-02-18', 1),
(265, 101, 0, 2, 2, 0, '2022-02-18', 1),
(266, 102, 0, 2, 1, 0, '2022-02-18', 1),
(267, 103, 0, 1, 0, 0, '2022-02-18', 1),
(268, 98, 1, 0, 1, 0, '2022-02-18', 1),
(269, 96, 0, 1, 1, 0, '2022-02-18', 1),
(270, 104, 0, 1, 1, 0, '2022-02-18', 1),
(271, 92, 1, 0, 1, 0, '2022-02-18', 1),
(272, 99, 1, 0, 1, 0, '2022-02-21', 1),
(273, 77, 305, 0, 305, 0, '2022-02-23', 1),
(274, 59, 3, 0, 4, 0, '2022-02-23', 1),
(275, 34, 2, 5, 0, 0, '2022-02-24', 1),
(276, 83, 0, 0, 196, 0, '2022-03-05', 1),
(277, 91, 124, 0, 75, 0, '2022-03-05', 1),
(278, 73, 7, 0, 8, 0, '2022-03-05', 1),
(279, 7, 15, 0, 2, 0, '2022-03-05', 1),
(280, 9, 1, 0, 1, 0, '2022-03-05', 1),
(281, 105, 0, 0, 0, 0, '2022-03-09', 1),
(282, 106, 0, 0, 0, 0, '2022-03-09', 1),
(283, 107, 0, 0, 0, 0, '2022-03-09', 1),
(284, 105, 0, 1, 0, 0, '2022-03-10', 1),
(285, 108, 0, 0, 0, 0, '2022-03-10', 1),
(286, 109, 0, 0, 0, 0, '2022-03-10', 1),
(287, 110, 50, 0, 10, 0, '2022-07-03', 3);

-- --------------------------------------------------------

--
-- Table structure for table `master_department`
--

CREATE TABLE `master_department` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `master_department`
--

INSERT INTO `master_department` (`id`, `company_id`, `name`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 3, 'Sales', '2022-07-03 16:58:08', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `master_designation`
--

CREATE TABLE `master_designation` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `master_designation`
--

INSERT INTO `master_designation` (`id`, `company_id`, `name`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 3, 'Senior Developer', '2022-07-02 11:24:31', 1, '2022-07-02 11:24:53', 1);

-- --------------------------------------------------------

--
-- Table structure for table `master_district`
--

CREATE TABLE `master_district` (
  `id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_district`
--

INSERT INTO `master_district` (`id`, `state_id`, `name`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 1, 'BALOD', '2022-07-01 07:40:23', 1, '2022-07-01 05:41:13', 1),
(2, 1, 'BALODA BAZAR', '2022-07-01 07:40:23', 1, '2022-07-01 05:41:13', 1),
(3, 1, 'BALRAMPUR', '2022-07-01 07:40:23', 1, '2022-07-01 05:41:13', 1),
(4, 1, 'BASTAR', '2022-07-01 07:40:23', 1, '2022-07-01 05:41:13', 1),
(5, 1, 'BEMETARA', '2022-07-01 07:40:23', 1, '2022-07-01 05:41:13', 1),
(6, 1, 'BIJAPUR', '2022-07-01 07:40:23', 1, '2022-07-01 05:41:13', 1),
(7, 1, 'BILASPUR', '2022-07-01 07:40:23', 1, '2022-07-01 05:41:13', 1),
(8, 1, 'CHIRMIRI', '2022-07-01 07:40:23', 1, '2022-07-01 05:41:13', 1),
(9, 1, 'DANTEWADA', '2022-07-01 07:40:23', 1, '2022-07-01 05:41:13', 1),
(10, 1, 'DHAMTARI', '2022-07-01 07:40:23', 1, '2022-07-01 05:41:13', 1),
(11, 1, 'DURG', '2022-07-01 07:40:23', 1, '2022-07-01 05:41:13', 1),
(12, 1, 'GARIYABAND', '2022-07-01 07:40:23', 1, '2022-07-01 05:41:13', 1),
(13, 1, 'GAURELA-PENDRA-MARWAHI', '2022-07-01 07:40:23', 1, '2022-07-01 05:41:13', 1),
(14, 1, 'JANJGIR-CHAMPA', '2022-07-01 07:40:23', 1, '2022-07-01 05:41:13', 1),
(15, 1, 'JASHPUR', '2022-07-01 07:40:23', 1, '2022-07-01 05:41:13', 1),
(16, 1, 'KABIRDHAM', '2022-07-01 07:40:23', 1, '2022-07-01 05:41:13', 1),
(17, 1, 'KANKER', '2022-07-01 07:40:23', 1, '2022-07-01 05:41:13', 1),
(18, 1, 'KONDAGAON', '2022-07-01 07:40:23', 1, '2022-07-01 05:41:13', 1),
(19, 1, 'KORBA', '2022-07-01 07:40:23', 1, '2022-07-01 05:41:13', 1),
(20, 1, 'KOREA', '2022-07-01 07:40:23', 1, '2022-07-01 05:41:13', 1),
(21, 1, 'MAHASAMUND', '2022-07-01 07:40:23', 1, '2022-07-01 05:41:13', 1),
(22, 1, 'MOHLA-MANPUR-CHOWKI', '2022-07-01 07:40:23', 1, '2022-07-01 05:41:13', 1),
(23, 1, 'MUNGELI', '2022-07-01 07:40:23', 1, '2022-07-01 05:41:13', 1),
(24, 1, 'NARAYANPUR', '2022-07-01 07:40:23', 1, '2022-07-01 05:41:13', 1),
(25, 1, 'RAIGARH', '2022-07-01 07:40:23', 1, '2022-07-01 05:41:13', 1),
(26, 1, 'RAIPUR', '2022-07-01 07:40:23', 1, '2022-07-01 05:41:13', 1),
(27, 1, 'RAJNANDGAON', '2022-07-01 07:40:23', 1, '2022-07-01 05:41:13', 1),
(28, 1, 'SAKTI', '2022-07-01 07:40:23', 1, '2022-07-01 05:41:13', 1),
(29, 1, 'SARANGARH', '2022-07-01 07:40:23', 1, '2022-07-01 05:41:13', 1),
(30, 1, 'SARGUJA', '2022-07-01 07:40:23', 1, '2022-07-01 05:41:13', 1),
(31, 1, 'SUKMA', '2022-07-01 07:40:23', 1, '2022-07-01 05:41:13', 1),
(32, 1, 'SURAJPUR', '2022-07-01 07:40:23', 1, '2022-07-01 05:41:13', 1);

-- --------------------------------------------------------

--
-- Table structure for table `master_emp_grade`
--

CREATE TABLE `master_emp_grade` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `master_emp_grade`
--

INSERT INTO `master_emp_grade` (`id`, `company_id`, `name`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 3, 'A', '2022-07-03 16:59:08', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `master_emp_type`
--

CREATE TABLE `master_emp_type` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `master_emp_type`
--

INSERT INTO `master_emp_type` (`id`, `company_id`, `name`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 3, 'Permanent', '2022-07-03 17:01:07', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `master_holiday`
--

CREATE TABLE `master_holiday` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `master_holiday`
--

INSERT INTO `master_holiday` (`id`, `company_id`, `name`, `from_date`, `to_date`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 3, 'Independence Day', '2022-08-15', '2022-08-15', '2022-07-03 17:03:56', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `master_leave`
--

CREATE TABLE `master_leave` (
  `id` int(11) NOT NULL,
  `name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `leave_days` bigint(20) NOT NULL,
  `company_id` int(11) NOT NULL,
  `emp_grade` int(11) NOT NULL,
  `order_no` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `master_payroll_deduction`
--

CREATE TABLE `master_payroll_deduction` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `percent` int(11) NOT NULL,
  `order_no` int(11) DEFAULT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0 - Inactive, 1 - Active\r\n',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `master_payroll_deduction`
--

INSERT INTO `master_payroll_deduction` (`id`, `company_id`, `name`, `percent`, `order_no`, `status`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 3, 'PF', 10, 2, '1', '2022-07-12 18:58:01', 1, '2022-07-12 19:01:15', 1);

-- --------------------------------------------------------

--
-- Table structure for table `master_payroll_earning`
--

CREATE TABLE `master_payroll_earning` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `percent` int(11) NOT NULL,
  `order_no` int(11) DEFAULT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0 - Inactive, 1 - Active',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `master_payroll_earning`
--

INSERT INTO `master_payroll_earning` (`id`, `company_id`, `name`, `percent`, `order_no`, `status`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 3, 'TA', 10, 1, '1', '2022-07-13 15:35:09', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `master_role`
--

CREATE TABLE `master_role` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `master_role`
--

INSERT INTO `master_role` (`id`, `company_id`, `name`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 3, 'Employee', '2022-07-03 16:58:53', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `master_shift`
--

CREATE TABLE `master_shift` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `company_shift_id` int(11) NOT NULL,
  `shift_name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `master_shift`
--

INSERT INTO `master_shift` (`id`, `company_id`, `company_shift_id`, `shift_name`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 3, 1, 'first shift', '2022-07-15 17:54:19', 1, '2022-07-15 17:57:04', 1),
(2, 3, 2, 'second shift', '2022-07-15 17:56:35', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `master_state`
--

CREATE TABLE `master_state` (
  `id` int(11) NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_state`
--

INSERT INTO `master_state` (`id`, `name`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 'CHHATTISHGARH', '2022-07-01 05:18:38', NULL, '2022-07-01 15:18:47', NULL),
(2, 'MADHYA PRADESH', '2022-07-03 17:09:34', 1, '2022-07-03 17:09:47', 1);

-- --------------------------------------------------------

--
-- Table structure for table `master_week_off`
--

CREATE TABLE `master_week_off` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `day_ids` varchar(200) NOT NULL,
  `working_hour` varchar(200) NOT NULL,
  `check_in` time NOT NULL,
  `check_out` time NOT NULL,
  `min_present_hour` varchar(200) NOT NULL,
  `min_half_day_hour` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `master_week_off`
--

INSERT INTO `master_week_off` (`id`, `company_id`, `day_ids`, `working_hour`, `check_in`, `check_out`, `min_present_hour`, `min_half_day_hour`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 3, '1,3,4,7', '9', '09:30:00', '18:30:00', '4', '2', '2022-07-07 15:58:34', 1, '2022-07-11 15:31:10', 1);

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1583561991),
('m130524_201442_init', 1583562000),
('m140506_102106_rbac_init', 1585911455),
('m170907_052038_rbac_add_index_on_auth_assignment_user_id', 1585911455),
('m180523_151638_rbac_updates_indexes_without_prefix', 1585911455);

-- --------------------------------------------------------

--
-- Stand-in structure for view `numbers`
-- (See below for the actual view)
--
CREATE TABLE `numbers` (
`number` bigint(16)
);

-- --------------------------------------------------------

--
-- Table structure for table `purchases`
--

CREATE TABLE `purchases` (
  `id` int(11) NOT NULL,
  `code` varchar(50) NOT NULL,
  `supplier` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `total` int(11) DEFAULT NULL,
  `tax_type` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `purchases`
--

INSERT INTO `purchases` (`id`, `code`, `supplier`, `date`, `total`, `tax_type`, `company_id`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 'ASS/JAN205', '1', '2022-01-08', NULL, 1, 1, '2022-01-11 06:52:42', 1, '2022-01-16 18:03:48', NULL),
(2, 'SWS211282', '9', '2021-12-22', NULL, 1, 1, '2022-01-11 09:01:48', 1, '2022-01-16 18:03:48', NULL),
(3, 'HT/JAN/0314', '2', '2022-01-05', NULL, 1, 1, '2022-01-11 10:31:53', 1, '2022-01-16 18:03:48', NULL),
(4, 'OMCS/21-22/640', '3', '2022-01-14', NULL, 1, 1, '2022-01-14 08:13:20', 1, '2022-01-16 18:03:48', NULL),
(5, 'VI/2122/JN-00326', '5', '2022-01-14', NULL, 1, 1, '2022-01-14 13:48:25', 1, '2022-01-16 18:03:48', NULL),
(6, 'SE\\1878\\21-22', '8', '2022-01-18', NULL, 1, 1, '2022-01-18 09:48:09', 27, '2022-01-18 09:48:09', NULL),
(7, 'HT/JAN/1683', '2', '2022-01-19', NULL, 1, 1, '2022-01-19 11:03:13', 27, '2022-01-19 11:03:13', NULL),
(8, 'HT/JAN/1566', '2', '2022-01-19', NULL, 1, 1, '2022-01-19 11:04:16', 27, '2022-01-19 11:04:16', NULL),
(9, 'HT/JAN/1782', '2', '2022-01-20', NULL, 1, 1, '2022-01-20 12:04:34', 27, '2022-01-20 12:04:34', NULL),
(10, '4180', '27', '2022-01-24', NULL, 1, 1, '2022-01-24 12:56:24', 27, '2022-01-24 12:56:24', NULL),
(11, 'HT/JAN/2322', '2', '2022-01-25', NULL, 2, 1, '2022-01-27 05:25:39', 27, '2022-01-27 05:25:39', NULL),
(12, 'ASS/JAN705', '1', '2022-01-25', NULL, 1, 1, '2022-01-27 05:30:11', 27, '2022-01-27 05:30:11', NULL),
(13, '001', '3', '2022-01-29', NULL, 2, 1, '2022-01-29 06:08:35', 27, '2022-01-29 06:08:35', NULL),
(14, 'HT/JAN/2827', '2', '2022-01-31', NULL, 2, 1, '2022-01-31 12:52:49', 27, '2022-01-31 12:52:49', NULL),
(15, 'SE\\1974\\21-22', '8', '2022-02-02', NULL, 1, 1, '2022-02-02 10:51:36', 27, '2022-02-02 10:51:36', NULL),
(16, 'HT/FEB/0622', '2', '2022-02-07', NULL, 1, 1, '2022-02-08 05:57:00', 27, '2022-02-08 05:57:00', NULL),
(17, 'HT/FEB/0939', '2', '2022-02-10', NULL, 2, 1, '2022-02-10 11:48:18', 27, '2022-02-10 11:48:18', NULL),
(18, 'HT/FEB/1013', '2', '2022-02-10', NULL, 2, 1, '2022-02-12 12:45:38', 27, '2022-02-12 12:45:38', NULL),
(19, 'ASS/FEB/539', '1', '2022-02-17', NULL, 2, 1, '2022-02-18 10:34:37', 27, '2022-02-18 10:34:37', NULL),
(20, '6854', '43', '2022-02-17', NULL, 2, 1, '2022-02-18 10:38:48', 27, '2022-02-18 10:38:48', NULL),
(21, '6834', '43', '2022-02-16', NULL, 2, 1, '2022-02-18 10:44:08', 27, '2022-02-18 10:44:08', NULL),
(22, 'HT/FEB/1555', '2', '2022-02-16', NULL, 2, 1, '2022-02-18 10:47:20', 27, '2022-02-18 10:47:20', NULL),
(23, 'BC/21-22/FB/00639', '44', '2022-02-17', NULL, 2, 1, '2022-02-18 10:52:29', 27, '2022-02-18 10:52:29', NULL),
(24, 'HT/FEB/0622', '2', '2022-02-07', NULL, 2, 1, '2022-02-18 10:56:36', 27, '2022-02-18 10:56:36', NULL),
(25, 'FEB/149', '46', '2022-02-14', NULL, 2, 1, '2022-02-18 11:13:01', 27, '2022-02-18 11:13:01', NULL),
(26, 'ASS/FEB/661', '1', '2022-02-21', NULL, 2, 1, '2022-02-24 08:24:02', 27, '2022-02-24 08:24:02', NULL),
(27, 'HT/MAR/0867', '2', '2022-03-09', NULL, 2, 1, '2022-03-10 12:36:01', 27, '2022-03-10 12:36:01', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `purchases_detail`
--

CREATE TABLE `purchases_detail` (
  `id` int(11) NOT NULL,
  `purchase_id` int(11) NOT NULL,
  `brand` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `item_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `total` double(13,2) DEFAULT NULL,
  `discount_per` float DEFAULT NULL,
  `discount_amount` double(13,2) NOT NULL,
  `tax_per` int(11) DEFAULT NULL,
  `cgst` double(13,2) DEFAULT NULL,
  `sgst` double(13,2) DEFAULT NULL,
  `igst` double(13,2) DEFAULT NULL,
  `grand_total` double(13,2) DEFAULT NULL,
  `company_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `purchases_detail`
--

INSERT INTO `purchases_detail` (`id`, `purchase_id`, `brand`, `item_id`, `quantity`, `price`, `total`, `discount_per`, `discount_amount`, `tax_per`, `cgst`, `sgst`, `igst`, `grand_total`, `company_id`) VALUES
(1, 1, 'BINATONE', 70, 1, 584, 584.75, 0, 0.00, 18, 52.63, 52.63, 105.25, 690.00, 1),
(2, 2, 'SMART-I ', 71, 9, 491, 4427.37, NULL, 0.00, 18, 398.46, 398.46, 796.93, 5224.30, 1),
(3, 3, 'SECURNET', 73, 50, 3, 150.00, 0, 0.00, 18, 13.50, 13.50, 27.00, 177.00, 1),
(4, 4, 'RELICELL', 74, 2, 720, 1440.00, NULL, 0.00, 0, 0.00, 0.00, 0.00, 1440.00, 1),
(5, 5, 'DAICHI', 75, 5, 1250, 6250.00, NULL, 0.00, 0, 0.00, 0.00, 0.00, 6250.00, 1),
(6, 6, 'HIKVISION', 77, 305, 16, 4953.20, NULL, 0.00, 0, 0.00, 0.00, 0.00, 4953.20, 1),
(7, 6, 'Zimpack', 76, 2, 2100, 4200.00, NULL, 0.00, 0, 0.00, 0.00, 0.00, 4200.00, 1),
(8, 7, 'D-LINK', 79, 26, 135, 3510.00, NULL, 0.00, 0, 0.00, 0.00, 0.00, 3510.00, 1),
(9, 8, 'TOSHIBA', 78, 1, 4000, 4000.00, NULL, 0.00, 0, 0.00, 0.00, 0.00, 4000.00, 1),
(10, 9, 'HI-FOCUS', 84, 2, 300, 600.00, NULL, 0.00, 0, 0.00, 0.00, 0.00, 600.00, 1),
(11, 9, 'D-LINK', 81, 450, 12, 5484.60, NULL, 0.00, 0, 0.00, 0.00, 0.00, 5484.60, 1),
(12, 9, 'FOXIN', 80, 1, 2025, 2025.00, NULL, 0.00, 0, 0.00, 0.00, 0.00, 2025.00, 1),
(13, 9, 'CONSISTENT', 48, 2, 1200, 2400.00, NULL, 0.00, 0, 0.00, 0.00, 0.00, 2400.00, 1),
(14, 9, 'SECURNET', 83, 875, 21, 18646.25, NULL, 0.00, 0, 0.00, 0.00, 0.00, 18646.25, 1),
(15, 9, 'HI-FOCUS', 54, 2, 141, 283.20, NULL, 0.00, 0, 0.00, 0.00, 0.00, 283.20, 1),
(16, 9, 'HI-FOCUS', 55, 2, 224, 448.00, NULL, 0.00, 0, 0.00, 0.00, 0.00, 448.00, 1),
(17, 9, 'SECURNET', 67, 1, 1950, 1950.00, NULL, 0.00, 0, 0.00, 0.00, 0.00, 1950.00, 1),
(18, 9, 'SECURNET', 73, 3, 150, 450.00, 100, 450.00, 0, 0.00, 0.00, 0.00, 0.00, 1),
(19, 10, 'SECUREYE', 88, 1, 7667, 7667.00, NULL, 0.00, 0, 0.00, 0.00, 0.00, 7667.00, 1),
(20, 11, 'CPPLUS', 66, 180, 13, 2399.40, NULL, 0.00, NULL, 0.00, 0.00, 0.00, 2399.40, 1),
(21, 12, 'DAHUA', 89, 1, 2553, 2553.00, NULL, 0.00, 0, 0.00, 0.00, 0.00, 2553.00, 1),
(22, 13, 'SANDISK', 92, 1, 350, 350.00, NULL, 0.00, NULL, 0.00, 0.00, 0.00, 350.00, 1),
(23, 14, 'XPIA', 94, 1, 1400, 1400.00, NULL, 0.00, NULL, 0.00, 0.00, 0.00, 1400.00, 1),
(24, 14, 'MULTYBYTE', 93, 1, 280, 280.00, NULL, 0.00, 18, 25.20, 25.20, 50.40, 330.40, 1),
(25, 15, 'SEAGATE', 95, 2, 3988, 7977.00, NULL, 0.00, 0, 0.00, 0.00, 0.00, 7977.00, 1),
(26, 16, 'D-LINK', 96, 1, 900, 900.00, NULL, 0.00, 0, 0.00, 0.00, 0.00, 900.00, 1),
(27, 17, 'D-LINK', 98, 1, 1800, 1800.85, NULL, 0.00, 18, 162.08, 162.08, 324.15, 2125.00, 1),
(28, 18, 'SECUREYE', 99, 1, 325, 325.00, NULL, 0.00, NULL, 0.00, 0.00, 0.00, 325.00, 1),
(29, 19, 'DAHUA', 89, 2, 2500, 5000.00, NULL, 0.00, NULL, 0.00, 0.00, 0.00, 5000.00, 1),
(30, 20, 'SANDISK', 100, 1, 650, 650.00, NULL, 0.00, NULL, 0.00, 0.00, 0.00, 650.00, 1),
(31, 21, 'EVM', 101, 2, 300, 600.00, NULL, 0.00, NULL, 0.00, 0.00, 0.00, 600.00, 1),
(32, 22, 'LOGITECH', 102, 2, 575, 1150.00, NULL, 0.00, NULL, 0.00, 0.00, 0.00, 1150.00, 1),
(33, 23, 'HP', 103, 1, 18500, 18500.00, NULL, 0.00, NULL, 0.00, 0.00, 0.00, 18500.00, 1),
(34, 24, 'D-LINK', 96, 1, 900, 900.00, NULL, 0.00, NULL, 0.00, 0.00, 0.00, 900.00, 1),
(35, 25, 'ASUS', 104, 1, 39250, 39250.00, NULL, 0.00, NULL, 0.00, 0.00, 0.00, 39250.00, 1),
(36, 26, 'CPPLUS', 34, 5, 528, 2640.00, NULL, 0.00, 18, 237.60, 237.60, 475.20, 3115.20, 1),
(37, 27, 'DAHUA', 105, 1, 21000, 21000.00, 13, 2730.00, 18, 1644.30, 1644.30, 3288.60, 21558.60, 1);

-- --------------------------------------------------------

--
-- Table structure for table `quotation`
--

CREATE TABLE `quotation` (
  `id` int(11) NOT NULL,
  `code` varchar(50) NOT NULL,
  `customer` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `total` int(11) DEFAULT NULL,
  `tax_type` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `subject` varchar(250) NOT NULL,
  `message` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `quotation`
--

INSERT INTO `quotation` (`id`, `code`, `customer`, `date`, `total`, `tax_type`, `company_id`, `subject`, `message`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, '', '13', '2022-01-11', NULL, 1, 1, '', NULL, '2022-01-11 08:49:22', 1, '2022-01-16 18:04:54', NULL),
(2, '', '18', '2022-01-17', NULL, 2, 1, 'QUOTATION FOR 2 MP 24*7  COLOUR SERIES CCTV SYSTEM.', '', '2022-01-17 11:49:20', 27, '2022-01-17 11:49:20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `quotation_detail`
--

CREATE TABLE `quotation_detail` (
  `id` int(11) NOT NULL,
  `quotation_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `brand` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `total` double(13,2) DEFAULT NULL,
  `discount_per` float DEFAULT NULL,
  `discount_amount` double(13,2) NOT NULL,
  `tax_per` int(11) DEFAULT NULL,
  `cgst` double(13,2) DEFAULT NULL,
  `sgst` double(13,2) DEFAULT NULL,
  `igst` double(13,2) DEFAULT NULL,
  `grand_total` double(13,2) DEFAULT NULL,
  `company_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `quotation_detail`
--

INSERT INTO `quotation_detail` (`id`, `quotation_id`, `item_id`, `brand`, `quantity`, `price`, `total`, `discount_per`, `discount_amount`, `tax_per`, `cgst`, `sgst`, `igst`, `grand_total`, `company_id`) VALUES
(1, 1, 3, 'DAHUA', 3, 2000, 6000.00, 3, 180.00, NULL, 0.00, 0.00, 0.00, 5820.00, 1),
(2, 1, 20, 'DAHUA', 1, 3500, 3500.00, 2, 70.00, NULL, 0.00, 0.00, 0.00, 3430.00, 1),
(3, 1, 35, 'CPPLUS', 1, 850, 850.00, NULL, 0.00, NULL, 0.00, 0.00, 0.00, 850.00, 1),
(4, 1, 69, 'SECURNET', 150, 17, 2550.00, 2, 51.00, NULL, 0.00, 0.00, 0.00, 2499.00, 1),
(5, 1, 48, 'CONSISTENT', 1, 2000, 2000.00, 3, 60.00, NULL, 0.00, 0.00, 0.00, 1940.00, 1),
(6, 1, 49, 'WBOX', 6, 20, 120.00, NULL, 0.00, NULL, 0.00, 0.00, 0.00, 120.00, 1),
(7, 1, 50, 'WBOX', 3, 15, 45.00, NULL, 0.00, NULL, 0.00, 0.00, 0.00, 45.00, 1),
(8, 1, 71, 'SMART-I ', 1, 1000, 1000.00, 2, 20.00, NULL, 0.00, 0.00, 0.00, 980.00, 1),
(9, 1, 72, 'INSTALLATION', 3, 300, 900.00, 3, 27.00, NULL, 0.00, 0.00, 0.00, 873.00, 1),
(10, 2, 3, 'DAHUA', 4, 2200, 8800.00, 8, 704.00, NULL, 0.00, 0.00, 0.00, 8096.00, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `id` int(11) NOT NULL,
  `code` varchar(50) NOT NULL,
  `customer` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `total` int(11) NOT NULL,
  `tax_type` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `bill_code` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_by` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`id`, `code`, `customer`, `date`, `total`, `tax_type`, `company_id`, `bill_code`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 'WT/JAN/001', '12', '2022-01-11', 0, 1, 1, 0, '0000-00-00 00:00:00', 1, '2022-01-16 18:06:13', 1),
(2, 'WT/JAN/001', '12', '2022-01-12', 0, 2, 1, 2, '0000-00-00 00:00:00', 1, '2022-01-20 13:03:15', 1),
(3, 'WT/JAN/003', '26', '2022-01-25', 0, 2, 1, 3, '0000-00-00 00:00:00', 27, '0000-00-00 00:00:00', 27),
(4, 'WT/JAN/004', '34', '2022-01-25', 0, 2, 1, 4, '0000-00-00 00:00:00', 27, '0000-00-00 00:00:00', 27),
(5, 'WT/JAN/005', '36', '2022-01-29', 0, 1, 1, 5, '0000-00-00 00:00:00', 27, '0000-00-00 00:00:00', 27),
(6, 'WT/FEB/006', '37', '2022-02-01', 0, 1, 1, 6, '0000-00-00 00:00:00', 27, '0000-00-00 00:00:00', 27),
(7, 'WT/MAR/007', '50', '2022-03-24', 0, 2, 1, 7, '0000-00-00 00:00:00', 27, '0000-00-00 00:00:00', 27),
(8, 'WT/MAR/007', '50', '2022-03-24', 0, 2, 1, 8, '0000-00-00 00:00:00', 27, '0000-00-00 00:00:00', 27),
(9, 'ds', '51', '2022-07-01', 0, 1, 3, 0, '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sales_detail`
--

CREATE TABLE `sales_detail` (
  `id` int(11) NOT NULL,
  `sale_id` int(11) NOT NULL,
  `brand` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `item_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `total` double(13,2) DEFAULT NULL,
  `discount_per` int(11) DEFAULT NULL,
  `discount_amount` double(13,2) DEFAULT NULL,
  `tax_per` int(11) DEFAULT NULL,
  `cgst` double(13,2) DEFAULT NULL,
  `sgst` double(13,2) DEFAULT NULL,
  `igst` double(13,2) DEFAULT NULL,
  `grand_total` double(13,2) DEFAULT NULL,
  `company_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sales_detail`
--

INSERT INTO `sales_detail` (`id`, `sale_id`, `brand`, `item_id`, `quantity`, `price`, `total`, `discount_per`, `discount_amount`, `tax_per`, `cgst`, `sgst`, `igst`, `grand_total`, `company_id`) VALUES
(1, 1, 'BINATONE', 70, 1, 850, 850.00, NULL, NULL, 1, 4.25, 4.25, 8.50, 858.50, 1),
(2, 2, 'BINATONE', 70, 1, 850, 850.00, 0, NULL, NULL, 0.00, 0.00, 0.00, 850.00, 1),
(3, 3, 'SECUREYE', 88, 1, 11800, 11800.00, NULL, NULL, NULL, 0.00, 0.00, 0.00, 11800.00, 1),
(4, 3, 'SECURNET', 83, 80, 32, 2560.00, NULL, NULL, NULL, 0.00, 0.00, 0.00, 2560.00, 1),
(5, 3, 'HI-FOCUS', 57, 2, 300, 600.00, NULL, NULL, NULL, 0.00, 0.00, 0.00, 600.00, 1),
(6, 3, 'DAHUA', 1, 2, 1800, 3600.00, NULL, NULL, NULL, 0.00, 0.00, 0.00, 3600.00, 1),
(7, 3, 'SMART-I ', 23, 2, 2200, 4400.00, NULL, NULL, NULL, 0.00, 0.00, 0.00, 4400.00, 1),
(8, 3, 'CPPLUS', 26, 1, 2200, 2200.00, NULL, NULL, NULL, 0.00, 0.00, 0.00, 2200.00, 1),
(9, 3, 'DAHUA', 6, 1, 2200, 2200.00, NULL, NULL, NULL, 0.00, 0.00, 0.00, 2200.00, 1),
(10, 3, 'CPPLUS', 66, 150, 22, 3300.00, NULL, NULL, NULL, 0.00, 0.00, 0.00, 3300.00, 1),
(11, 3, 'HI-FOCUS', 84, 1, 800, 800.00, NULL, NULL, NULL, 0.00, 0.00, 0.00, 800.00, 1),
(12, 3, 'WBOX', 49, 20, 50, 1000.00, NULL, NULL, NULL, 0.00, 0.00, 0.00, 1000.00, 1),
(13, 3, 'WBOX', 50, 6, 30, 180.00, NULL, NULL, NULL, 0.00, 0.00, 0.00, 180.00, 1),
(14, 3, 'INSTALLATION', 72, 8, 500, 4000.00, NULL, NULL, NULL, 0.00, 0.00, 0.00, 4000.00, 1),
(15, 4, 'SMART-I ', 91, 181, 25, 4525.00, NULL, NULL, NULL, 0.00, 0.00, 0.00, 4525.00, 1),
(16, 4, 'INSTALLATION', 72, 5, 500, 2500.00, NULL, NULL, NULL, 0.00, 0.00, 0.00, 2500.00, 1),
(17, 5, 'SANDISK', 92, 1, 1000, 1000.00, NULL, NULL, 0, 0.00, 0.00, 0.00, 1000.00, 1),
(18, 5, 'DAHUA', 89, 1, 3000, 3000.00, NULL, NULL, 0, 0.00, 0.00, 0.00, 3000.00, 1),
(19, 5, 'INSTALLATION', 72, 1, 300, 300.00, NULL, NULL, 0, 0.00, 0.00, 0.00, 300.00, 1),
(20, 6, 'SMART-I ', 91, 12, 35, 420.00, NULL, NULL, 0, 0.00, 0.00, 0.00, 420.00, 1),
(21, 6, 'SECURNET', 73, 5, 20, 100.00, NULL, NULL, 0, 0.00, 0.00, 0.00, 100.00, 1),
(22, 6, 'INSTALLATION', 72, 1, 700, 700.00, NULL, NULL, 0, 0.00, 0.00, 0.00, 700.00, 1),
(23, 7, 'SMART-I ', 71, 2, 1050, 2100.00, NULL, NULL, NULL, 0.00, 0.00, 0.00, 2100.00, 1),
(24, 8, 'SMART-I ', 71, 2, 1050, 2100.00, NULL, NULL, NULL, 0.00, 0.00, 0.00, 2100.00, 1),
(25, 9, 'tesla', 110, 10, 10, 100.00, 10, 10.00, 0, 0.00, 0.00, 0.00, 90.00, 3);

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `key` varchar(255) CHARACTER SET latin1 NOT NULL,
  `val` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) CHARACTER SET latin1 NOT NULL,
  `description` text CHARACTER SET latin1 NOT NULL,
  `input_type` varchar(255) CHARACTER SET latin1 NOT NULL,
  `validation_rules` text CHARACTER SET latin1 NOT NULL,
  `options` text CHARACTER SET latin1 NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `u_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `form_id`, `key`, `val`, `label`, `description`, `input_type`, `validation_rules`, `options`, `status`, `u_at`) VALUES
(1, 1, 'company_name', 'Weblinto Technologies', 'Company Name', '', 'textInput', '[\r\n    [\'val\', \'required\'],\r\n    [\'val\', \'string\'],\r\n]', '[]', 1, '2022-01-03 14:47:18'),
(2, 1, 'company_address', '9/3, Capital Palace, Avanti Vihar Raipur(CG)', 'Company Address', '', 'textInput', '[\r\n    [\'val\', \'required\'],\r\n    [\'val\', \'string\'],\r\n]', '[]', 1, '2022-01-08 17:25:13'),
(3, 1, 'company_address_line1', '', 'Address Line1', '', 'textInput', '[\r\n    [\'val\', \'required\'],\r\n    [\'val\', \'string\'],\r\n]', '[]', 1, '2022-01-09 13:23:12'),
(4, 1, 'company_address_line2', '', 'Address Line2', '', 'textInput', '[\r\n     [\'val\', \'string\'],\r\n]', '[]', 1, '2022-01-06 03:03:41'),
(5, 1, 'company_city', '444', ' City', '', 'dropdownList', '[\r\n     [\'val\', \'string\'],\r\n]', '\\app\\models\\MasterCity::DList()', 1, '2021-12-11 05:17:14'),
(6, 1, 'company_state', '22', ' State', '', 'dropdownList', '[\r\n     [\'val\', \'string\'],\r\n]', '\\app\\models\\MasterState::DList()', 1, '2021-12-11 05:17:14'),
(7, 1, 'company_pincode', '492007', 'Pincode', '', 'textInput', '[[\'val\', \'integer\'], [\'val\', \'match\', \'pattern\' => \'/^[0-9]{6}$/\', \'message\' => \'Invalid Pincode\']\r\n]', '[]', 1, '2021-12-11 05:17:14'),
(8, 1, 'company_country', 'India', 'Country', '', 'textInput', '[\r\n    [\'val\', \'required\'],\r\n    [\'val\', \'string\'],\r\n]', '[]', 1, '2021-12-11 05:17:14'),
(9, 1, 'company_email', 'contact@weblinto.com', 'Company Email', '', 'textInput', '[\r\n    [\'val\', \'required\'],\r\n    [\'val\', \'email\'],\r\n]', '[]', 1, '2022-01-06 03:04:04'),
(10, 1, 'company_website', 'http://www.weblinto.com', 'Company Website', '', 'textInput', '[\r\n    [\'val\', \'required\'],\r\n    [\'val\', \'string\'],\r\n]', '[]', 1, '2022-01-06 03:04:38'),
(11, 1, 'contact_no_one', '8103965666', 'Contact No One', '', 'textInput', '[\r\n    [\'val\', \'required\'],\r\n    [\'val\', \'match\', \'pattern\' => \'/^[6-9][0-9]{9}$/\', \'message\' => \'Invalid Mobile Number\'],\r\n]', '[]', 1, '2022-01-06 03:04:52'),
(12, 1, 'contact_no_two', '7869858464', 'Contact No Two', '', 'textInput', '[\r\n    [\'val\', \'required\'],\r\n    [\'val\', \'match\', \'pattern\' => \'/^[6-9][0-9]{9}$/\', \'message\' => \'Invalid Mobile Number\'],\r\n]', '[]', 1, '2021-12-11 05:17:14'),
(13, 1, 'whats_app_no', '7869858464', 'WhatsApp No', '', 'textInput', '[\r\n    [\'val\', \'required\'],\r\n    [\'val\', \'match\', \'pattern\' => \'/^[6-9][0-9]{9}$/\', \'message\' => \'Invalid Mobile Number\'],\r\n]', '[]', 1, '2021-12-11 05:17:14'),
(14, 1, 'landline_no', '6234456985', 'Landline No', '', 'textInput', '[\r\n    [\'val\', \'required\'],\r\n    [\'val\', \'match\', \'pattern\' => \'/^[6-9][0-9]{9}$/\', \'message\' => \'Invalid Mobile Number\'],\r\n]', '[]', 1, '2021-12-11 05:17:14'),
(15, 1, 'company_gstin', '22AMYPG4556F1ZV', 'Company GSTIN', '', 'textInput', '[\r\n    [\'val\', \'required\'],\r\n    [\'val\', \'string\'],\r\n]', '[]', 1, '2022-01-06 03:06:04'),
(16, 1, 'company_logo', '100.29829000 1611996485.png', 'Company Logo', '', 'fileInput', '[\r\n[\'val\', \'file\', \'extensions\' => \'jpg,png,jpeg\'],\r\n]', '[]', 1, '2021-12-11 05:17:14'),
(17, 1, 'company_pan', 'AAMCS2853E', 'Company Pan', '', 'textInput', '[\r\n    [\'val\', \'required\'],\r\n    [\'val\', \'string\'],\r\n]', '[]', 1, '2021-12-11 05:17:14'),
(18, 0, 'currency_label', '₹', 'Currency Label', 'Currency Label', 'textInput', '[ [\'val\',\'string\']]', '[]', 1, '2021-12-11 05:17:14');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT 10,
  `fname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `lname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` bigint(20) DEFAULT NULL,
  `is_first_login` tinyint(1) NOT NULL DEFAULT 1 COMMENT '{1 =>yes, 0 => no} ',
  `change_status` tinyint(4) NOT NULL DEFAULT 0,
  `profile_file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pass_updated_at` int(11) NOT NULL DEFAULT 0,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `verification_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `fname`, `lname`, `mobile`, `is_first_login`, `change_status`, `profile_file`, `pass_updated_at`, `created_at`, `updated_at`, `verification_token`, `company_id`) VALUES
(1, 'admin', 't-scsX-eBA5L2Esa5QOVF3O6bYMwjHzy', '$2y$13$GLARXOv18.mVKeNeLlaDuu5xWqT0VpkIbOsvd2L8XuZQiFoUSlMhK', NULL, 'admin@admin.com', 10, 'Main', 'Admin', 7999251595, 0, 0, '', 1608531047, 1583582869, 1642401327, NULL, 3),
(27, 'weblinto', 'i6600WpQU3kpqMYH2wbIeTTWlFjqMw0e', '$2y$13$lP4y6zkNfZxqI3wjBtbNweogaqpSDUBTnn6INzL2rkqk9kxYR6cgS', NULL, 'contact@weblinto.com', 10, 'Weblinto', 'Technology', 8103965666, 1, 0, '', 0, 1642321638, 1642321638, NULL, 1),
(28, 'entit', '4ILDZVz0oE72dO-4gCeFHaKn7fBfmMTu', '$2y$13$4R4SEKiBsT6elTOdFJmhfOYwkwncWPmYd5gq5CnOGFXWkQQ3ZgPt.', NULL, 'entit@gmail.com', 10, 'Entit', 'CS', 9926300081, 1, 0, '', 0, 1642321897, 1657217728, NULL, 2);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT 10,
  `fname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `lname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` bigint(20) DEFAULT NULL,
  `is_first_login` tinyint(1) NOT NULL DEFAULT 1 COMMENT '{1 =>yes, 0 => no} ',
  `change_status` tinyint(4) NOT NULL DEFAULT 0,
  `profile_file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pass_updated_at` int(11) NOT NULL DEFAULT 0,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `verification_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_otp`
--

CREATE TABLE `user_otp` (
  `id` int(11) NOT NULL,
  `type` int(11) NOT NULL COMMENT '1=>OTP, 2=>Link Token',
  `user_id` int(11) DEFAULT NULL,
  `session_id` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `otp` decimal(6,0) DEFAULT NULL,
  `token` text COLLATE utf8_swedish_ci DEFAULT NULL,
  `expiry_datetime` datetime NOT NULL,
  `attempt_count` int(11) DEFAULT 0,
  `is_expired` tinyint(4) NOT NULL DEFAULT 0,
  `send_count` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Dumping data for table `user_otp`
--

INSERT INTO `user_otp` (`id`, `type`, `user_id`, `session_id`, `otp`, `token`, `expiry_datetime`, `attempt_count`, `is_expired`, `send_count`, `created_at`) VALUES
(1, 2, 26, NULL, NULL, 'M5REqb0z_H9b8WO0zJx-YykPlw7q28Uj', '2021-11-01 13:11:30', 0, 1, 1, '2021-11-01 07:31:30');

-- --------------------------------------------------------

--
-- Structure for view `dates`
--
DROP TABLE IF EXISTS `dates`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `dates`  AS SELECT curdate() - interval `numbers`.`number` day AS `date` FROM `numbers` ;

-- --------------------------------------------------------

--
-- Structure for view `digits`
--
DROP TABLE IF EXISTS `digits`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `digits`  AS SELECT 0 AS `digit` ;

-- --------------------------------------------------------

--
-- Structure for view `numbers`
--
DROP TABLE IF EXISTS `numbers`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `numbers`  AS SELECT `ones`.`digit`+ `tens`.`digit` * 10 + `hundreds`.`digit` * 100 + `thousands`.`digit` * 1000 AS `number` FROM (((`digits` `ones` join `digits` `tens`) join `digits` `hundreds`) join `digits` `thousands`) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`),
  ADD KEY `idx-auth_assignment-user_id` (`user_id`);

--
-- Indexes for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`);

--
-- Indexes for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Indexes for table `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `delivery`
--
ALTER TABLE `delivery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `delivery_detail`
--
ALTER TABLE `delivery_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `manage_stock`
--
ALTER TABLE `manage_stock`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_department`
--
ALTER TABLE `master_department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_designation`
--
ALTER TABLE `master_designation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_district`
--
ALTER TABLE `master_district`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_emp_grade`
--
ALTER TABLE `master_emp_grade`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_emp_type`
--
ALTER TABLE `master_emp_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_holiday`
--
ALTER TABLE `master_holiday`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_leave`
--
ALTER TABLE `master_leave`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_payroll_deduction`
--
ALTER TABLE `master_payroll_deduction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_payroll_earning`
--
ALTER TABLE `master_payroll_earning`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_role`
--
ALTER TABLE `master_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_shift`
--
ALTER TABLE `master_shift`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_state`
--
ALTER TABLE `master_state`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_week_off`
--
ALTER TABLE `master_week_off`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `purchases`
--
ALTER TABLE `purchases`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchases_detail`
--
ALTER TABLE `purchases_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quotation`
--
ALTER TABLE `quotation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quotation_detail`
--
ALTER TABLE `quotation_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_detail`
--
ALTER TABLE `sales_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`),
  ADD KEY `key` (`key`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`),
  ADD UNIQUE KEY `mobile` (`mobile`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`),
  ADD UNIQUE KEY `mobile` (`mobile`);

--
-- Indexes for table `user_otp`
--
ALTER TABLE `user_otp`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `delivery`
--
ALTER TABLE `delivery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `delivery_detail`
--
ALTER TABLE `delivery_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;

--
-- AUTO_INCREMENT for table `manage_stock`
--
ALTER TABLE `manage_stock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=288;

--
-- AUTO_INCREMENT for table `master_department`
--
ALTER TABLE `master_department`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `master_designation`
--
ALTER TABLE `master_designation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `master_district`
--
ALTER TABLE `master_district`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `master_emp_grade`
--
ALTER TABLE `master_emp_grade`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `master_emp_type`
--
ALTER TABLE `master_emp_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `master_holiday`
--
ALTER TABLE `master_holiday`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `master_leave`
--
ALTER TABLE `master_leave`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `master_payroll_deduction`
--
ALTER TABLE `master_payroll_deduction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `master_payroll_earning`
--
ALTER TABLE `master_payroll_earning`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `master_role`
--
ALTER TABLE `master_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `master_shift`
--
ALTER TABLE `master_shift`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `master_state`
--
ALTER TABLE `master_state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `master_week_off`
--
ALTER TABLE `master_week_off`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `purchases`
--
ALTER TABLE `purchases`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `purchases_detail`
--
ALTER TABLE `purchases_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `quotation`
--
ALTER TABLE `quotation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `quotation_detail`
--
ALTER TABLE `quotation_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `sales_detail`
--
ALTER TABLE `sales_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_otp`
--
ALTER TABLE `user_otp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
