<?php

namespace app\api\modules\v1\controllers;

//use yii\rest\ActiveController;

//use app\models\MasterCity

class CountryController extends ActiveController {

    public $modelClass = 'app\models\MasterCity';

    public $noAuthActions = ['index'];

    public function actions() {
        $actions = parent::actions();
        unset($actions['update'], $actions['index'], $actions['delete'], $actions['create']);
        return $actions;
    }
    
    public function actionIndex(){
        return [1,2,3];
    }
}
