<?php

if (file_exists(__DIR__ . '/../.env.php')) {
    require __DIR__ . '/../.env.php';
}

// comment out the following two lines when deployed to production

// comment out the following two lines when deployed to production
require(__DIR__ . '/../vendor/autoload.php');
//defined('YII_DEBUG') or define('YII_DEBUG', true);
//defined('YII_ENV') or define('YII_ENV', 'dev');

// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', YII_DEBUG == 'true');
defined('YII_ENV') or define('YII_ENV', YII_DEBUG ?: 'dev');




require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');



$config = require(__DIR__ . '/config/web.php');


(new yii\web\Application($config))->run();
