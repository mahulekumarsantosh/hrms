<?php

//$db = require(__DIR__ . '/../../config/db.php');
$common = require(__DIR__ . '/../../config/common.php');
$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
//    'name' => 'RESTFull',
    'basePath' => dirname(__DIR__) . '/..',
//    'bootstrap' => ['log'],
    'modules' => [
        'v1' => [
            'basePath' => '@app/api/modules/v1',
            'class' => 'app\api\modules\v1\Module' // here is our v1 modules
        ],
    ],
    'components' => [
        'request' => [
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => COOKIE_VALIDATION_KEY
        ],
        'response' => [
            'class' => 'yii\web\Response',
            'on beforeSend' => function ($event) {
                yii::createObject([
                    'class' => \app\components\ResponseHandler::class,
                    'event' => $event,
                ])->formatResponse();
            },
        ],
        'formatter' => [
            'dateFormat' => 'yyyy-MM-dd',
            'datetimeFormat' => 'yyyy-MM-dd HH:mm:ss',
            'decimalSeparator' => ',',
            'thousandSeparator' => ' ',
            'currencyCode' => 'INR',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => false,
        ],
        'jwt' => [
            'class' => \sizeg\jwt\Jwt::class,
            'key' => JWT_SECRET,
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'showScriptName' => false,
            'enablePrettyUrl' => true,
            'enableStrictParsing' => false,
            'rules' => [
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/country','v1/user', 'v1/labour']
                ],
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ],
        ],
    ],
    'params' => $params,
];

return \yii\helpers\ArrayHelper::merge($common, $config);
//return $config;
