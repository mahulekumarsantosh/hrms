<?php

return [
    'super' => [
        'class' => 'mdm\admin\Module',
        'layout' => 'left-menu',
    ],
    'gridview' => [
        'class' => '\kartik\grid\Module'
    ],
    'manage' => [
        'class' => 'app\modules\manage\Manage',
    ],
];
