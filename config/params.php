<?php

return [
    'authTimeout' => 60 * 60,
    'authRedirect' => 'http://suntechnos.in/',
    'bsVersion' => '4',
    'adminEmail' => 'admin@example.com',
    'commonStatus' => [
        1 => "Active",
        0 => "Inactive",
    ],
    'gridPageSize' => [
        10 => 10,
        50 => 50,
        100 => 100,
        0 => 'All',
    ],
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'domain' => 'http://localhost',
    'user.passwordResetTokenExpire' => 3600,
    'cacheTime' => 3600,

    'customer_type' => [1 => 'Vendor', 2 => 'Customer'],
    'week_days' => [1 => 'Sunday', 2 => 'Monday', 3 => 'Tuesday', 4 => 'Wednesday', 5 => 'Thrusday', 6 => 'Friday', 7 => 'Saturday'],
    'unit' => ['Pics' => 'Pics', 'Meter' => 'Meter', 'Bundle' => 'Bundle'],
    'tax-per' => ['5' => '@5%', '12' => '@12%', '18' => '@18%', '28' => '@28%'],
    'tax-type' => ['2' => 'Without GST', '1' => 'With GST'],
    'gender' => [1=> 'Male', 2 => 'Female', 3 => 'Other']
];
