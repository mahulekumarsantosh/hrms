<?php

return [
    'timeZone' => APP_TIME_ZONE,
    'language' => APP_LANGUAGE,
    'name' => APP_NAME,
    'bootstrap' => ['log'],
    'components' => [

        'requestId' => [
            'class' => \yiier\helpers\RequestId::class,
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => DB_DSN,
            'username' => DB_USERNAME,
            'password' => DB_PASSWORD,
            'tablePrefix' => DB_TABLE_PREFIX,
            'charset' => 'utf8mb4',
//            'enableSchemaCache' => YII_ENV_PROD,
//            'schemaCacheDuration' => 60,
//            'schemaCache' => 'cache',
        ],
//        'i18n' => [
//            'translations' => [
//                'app*' => [
//                    'class' => 'yii\i18n\PhpMessageSource',
//                    'basePath' => '@app/core/messages',
//                    'fileMap' => [
//                        'app' => 'app.php',
//                        'app/error' => 'exception.php',
//                    ],
//                ],
//            ],
//        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yiier\helpers\FileTarget',
                    'levels' => ['error'],
                    'logVars' => ['_GET', '_POST', '_FILES', '_COOKIE', '_SESSION'],
                    'logFile' => '@app/runtime/logs/error/app.log',
                    'enableDatePrefix' => true,
                ],
                [
                    'class' => 'yiier\helpers\FileTarget',
                    'levels' => ['warning'],
                    'logVars' => ['_GET', '_POST', '_FILES', '_COOKIE', '_SESSION'],
                    'logFile' => '@app/runtime/logs/warning/app.log',
                    'enableDatePrefix' => true,
                ],
                [
                    'class' => 'yiier\helpers\FileTarget',
                    'levels' => ['info'],
                    'categories' => ['request'],
                    'logVars' => [],
                    'maxFileSize' => 1024,
                    'logFile' => '@app/runtime/logs/request/app.log',
                    'enableDatePrefix' => true
                ],
                [
                    'class' => 'yiier\helpers\FileTarget',
                    'levels' => ['warning'],
                    'categories' => ['debug'],
                    'logVars' => [],
                    'maxFileSize' => 1024,
                    'logFile' => '@app/runtime/logs/debug/app.log',
                    'enableDatePrefix' => true
                ],
            ],
        ],
    ],
];
