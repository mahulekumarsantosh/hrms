<?php

use yii\helpers\Url;

$params = require __DIR__ . '/params.php';
$common = require(__DIR__ . '/common.php');
$module = require(__DIR__ . '/module.php');
//$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
        '@mdm/admin' => '@app/extensions/mdm/yii2-admin-2.0.0',
    ],
    'modules' => $module,
    'container' => [
        'definitions' => [
            'yii\widgets\LinkPager' => [
                'maxButtonCount' => 6,
                'options' => [
                    'tag' => 'ul',
                    'class' => 'pagination', //pagination-sm
                ]
            ],
        ],
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => COOKIE_VALIDATION_KEY,
            'baseUrl' => BASE_URL,
        ],
        'formatter' => [
            'class' => '\app\components\MyFormatter',
            'nullDisplay' => '',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => false,
            'authTimeout' => $params['authTimeout'],
        ],
        'avatar' => [
            'class' => \zertex\avatar_generator\AvatarGenerator::class,
            'images_folder' => Url::to('upload/profile_img'),
            'images_url' => Url::to('upload/profile_img'),
            'size_width' => 500, // default: 300
            'font_size' => 200, // default: 200             
            'texture' => ['sun'], // texture name
            'text_over_image' => true, // draw text over image (for avatar from file)             
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
//             'enableCaching' => true,
        ],
        'settings' => [
            'class' => '\app\components\Settings',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
//            'viewPath' => '@backend/mail',
            'useFileTransport' => false, //set this property to false to send mails to real email addresses
            //comment the following array to send mail using php's mail function
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => env('MAILER_HOST'),
                'username' => env('MAILER_USERNAME'),
                'password' => env('MAILER_PASSWORD'),
                'port' => env('MAILER_PORT'),
                'encryption' => env('MAILER_ENCRYPTION'),
            ],
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
    ],
//    'as access' => [
//        'class' => 'mdm\admin\components\AccessControl',
//        'allowActions' => [
////            'super/*', // add or remove allowed actions to this list
//            'site/*',
//            'doc/*',
//            'profile/*',
//            'debug/*',
////            'gii/*',
//        ]
//    ],
    'as beforeRequest' => [
        'class' => 'yii\filters\AccessControl',
        'rules' => [
            [
                'allow' => true,
                'actions' => [
                    'login', 'error', 'login-otp', 'login-with-otp', 'email-reset-password', 'reset-password', 'download',
                //'forgot-password', 'forgot-password-otp', 'forgot-password-otp-resend', 'forgot-password-reset','signup', 
                ],
            ],
            [
                'allow' => true,
                'roles' => ['@'],
            ],
        ],
        'denyCallback' => function () {
            return Yii::$app->response->redirect(['site/login']);
        },
    ],
    'on beforeRequest' => function ($event) {
        Yii::$app->layout = Yii::$app->user->isGuest ?
                '@app/views/layouts/main-login.php' : // or just use 'GuestUser' and 
                '@app/views/layouts/main.php';  // 'RegisteredUser' since the path 


        $authUrl = Yii::$app->params['authRedirect'];
        $authTimeout = (Yii::$app->user->authTimeout + 5);
        Yii::$app->view->registerMetaTag(['http-equiv' => 'refresh', "content" => "$authTimeout;url=$authUrl"]);
    },
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
            // uncomment the following to add your IP if you are not connecting from localhost.
            //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
            // uncomment the following to add your IP if you are not connecting from localhost.
            //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return \yii\helpers\ArrayHelper::merge($common, $config);
//return $config;
