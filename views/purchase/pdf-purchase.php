<?php

use app\models\Purchase;
use app\components\Encryption;

$id = Encryption::Decrypt(Yii::$app->request->get('id'));
$model = Purchase::findOne($id);
print $this->render('purchase-chalan', [
            'model' => $model,            
            'export' => $export,
]);
?>
