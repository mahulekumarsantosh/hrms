<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use app\components\GridView;
use app\components\Encryption;
/* @var $this yii\web\View */
/* @var $searchModel app\models\MasterDesignationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Master Designations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-designation-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'exportTitle' => 'Designation',
        'panel' => ['type' => GridView::TYPE_SECONDARY, 'heading' => $this->title],
        'export' => ['showConfirmAlert' => false,],
        'pjax' => true,
        'toolbar' => [
            Html::a('<i class="fa fa-plus"></i> Add New', ['create'], ['class' => 'btn btn-sm btn-success designation float-right', 'title' => 'Add Designation', 'data-pjax' => 0]),
            '{export}',
            '{perPage}',
        ],
        'options' => [
            'class' => 'grid-primary',
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'company_id',
            'name',
            [
                'header' => "Action",
                'headerOptions' => ['class' => "skip-export text-center", 'width' => 150],
                'format' => 'raw',
                'contentOptions' => ['class' => "skip-export text-center", 'width' => 150],
                'value' => function ($model) {
                    $list = [];
                    $list[] = Html::a('<i class="fa fa-pencil-alt" title="Update"></i> ', ['update', "id" => Encryption::Encrypt($model->id)], ['class' => 'btn btn-xs btn-info designation', 'title' => 'Update Designation', 'data-pjax' => 0]);
                    return implode('', $list);
                },
            ],
        ],
    ]); ?>
</div>
<?php
    $script = <<< JS
    $("body").on('click', '.designation',function(e) {
        e.preventDefault();
        $("#designationModalContent").load($(this).attr('href'));
        $("#designationModalTitle").text($(this).attr('title'));
        $('#designationModal').modal('show');
    });        
    
JS;
    $this->registerJs($script);
    echo $this->render("@app/views/layouts/models/_ajaxRenderModal", [
        'headerTitleId' => 'designationModalTitle',
        'modalId' => "designationModal",
        'contentId' => "designationModalContent",
        'size' => 'lg'
    ]);

?>