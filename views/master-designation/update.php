<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MasterDesignation */

$this->title = 'Update Master Designation: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Master Designations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="master-designation-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>