<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MasterDesignation */

$this->title = 'Create Master Designation';
$this->params['breadcrumbs'][] = ['label' => 'Master Designations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-designation-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>