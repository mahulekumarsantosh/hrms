<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MasterRole */

$this->title = 'Update Master Role: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Master Roles', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="master-role-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>