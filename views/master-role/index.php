<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use app\components\GridView;
use app\components\Encryption;
/* @var $this yii\web\View */
/* @var $searchModel app\models\MasterRoleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Master Roles';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-role-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'exportTitle' => 'Role',
        'panel' => ['type' => GridView::TYPE_SECONDARY, 'heading' => $this->title],
        'export' => ['showConfirmAlert' => false,],
        'pjax' => true,
        'toolbar' => [
            Html::a('<i class="fa fa-plus"></i> Add New', ['create'], ['class' => 'btn btn-sm btn-success role float-right', 'title' => 'Add Role', 'data-pjax' => 0]),
            '{export}',
            '{perPage}',
        ],
        'options' => [
            'class' => 'grid-primary',
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'company_id',
            'name',
            [
                'header' => "Action",
                'headerOptions' => ['class' => "skip-export text-center", 'width' => 150],
                'format' => 'raw',
                'contentOptions' => ['class' => "skip-export text-center", 'width' => 150],
                'value' => function ($model) {
                    $list = [];
                    $list[] = Html::a('<i class="fa fa-pencil-alt" title="Update"></i> ', ['update', "id" => Encryption::Encrypt($model->id)], ['class' => 'btn btn-xs btn-info role', 'title' => 'Update Role', 'data-pjax' => 0]);
                    return implode('', $list);
                },
            ],
        ],
    ]); ?>
</div>
<?php
    $script = <<< JS
    $("body").on('click', '.role',function(e) {
        e.preventDefault();
        $("#roleModalContent").load($(this).attr('href'));
        $("#roleModalTitle").text($(this).attr('title'));
        $('#roleModal').modal('show');
    });        
    
JS;
    $this->registerJs($script);
    echo $this->render("@app/views/layouts/models/_ajaxRenderModal", [
        'headerTitleId' => 'roleModalTitle',
        'modalId' => "roleModal",
        'contentId' => "roleModalContent",
        'size' => 'lg'
    ]);

?>