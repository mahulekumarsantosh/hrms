<?php

use yii\helpers\Html;
use app\components\GridView;
use app\components\Encryption;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BrandsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Brands List';
$this->params['breadcrumbs'][] = $this->title;
?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'exportTitle' => $this->title,
        'panel' => ['type' => GridView::TYPE_SECONDARY, 'heading' => $this->title],
    'export' => ['showConfirmAlert' => false,],
    'pjax' => true,
    'toolbar' => [
        Html::a('<i class="fa fa-plus"></i> Add New', ['create'], ['class' => 'btn btn-sm btn-success brands float-right', 'title' => 'Add Brand', 'data-pjax' => 0]),
        '{export}',
        '{perPage}',
    ],
    'options' => [
        'class' => 'grid-primary',
    ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',           
        ],
    ]); ?>

<?php
$script = <<< JS
    $("body").on('click', '.brands',function(e) {
        e.preventDefault();
        $("#brandsModalContent").load($(this).attr('href'));
        $("#brandsModalTitle").text($(this).attr('title'));
        $('#brandsModal').modal('show');
    });        
    
JS;
$this->registerJs($script);
echo $this->render("@app/views/layouts/models/_ajaxRenderModal", [
    'headerTitleId' => 'brandsModalTitle',
    'modalId' => "brandsModal",
    'contentId' => "brandsModalContent",
    'size' => 'lg'
]);

?>
