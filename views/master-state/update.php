<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MasterState */

$this->title = 'Update Master State: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Master States', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="master-state-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>