<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MasterState */

$this->title = 'Create Master State';
$this->params['breadcrumbs'][] = ['label' => 'Master States', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-state-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>