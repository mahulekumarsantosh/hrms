<?php

use yii\helpers\Html;
use app\components\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PurchaseDetailSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Purchase Details';
$this->params['breadcrumbs'][] = $this->title;
?>
<?= $this->render('_search', ['model' => $searchModel]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,  
        'exportTitle' => $this->title,      
        'panel' => ['type' => GridView::TYPE_SECONDARY, 'heading' => $this->title],
        'export' => ['showConfirmAlert' => false,],
        'pjax' => true,
        'toolbar' => [       
            '{export}',
            '{perPage}',
        ],
        'options' => [
            'class' => 'grid-primary',
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],           
            [
                'attribute' => 'Firm Name',
                'value' => function($model){               
                    return $model->purchase->customerDetail ? $model->purchase->customerDetail->firm_name : '';
                }   
            ],           
            [
                'attribute' => 'purchase_id',
                'label' => 'Challan No',
                'value' => function($model){                    
                    return $model->purchase? $model->purchase->code : '';
                }   
            ],
            [
                'label' => 'Date',
                'value' => function($model){                    
                    return $model->purchase? $model->purchase->date : '';
                }   
            ],
            [
             'attribute' => 'item_id',
             'value' => function($model){
                 return $model->item->name;
             }   
            ],
            'quantity',
            [
                'attribute' => 'price',
                'label' => 'Price/Unit',
                'value' => function($model){
                    return number_format($model->price, 2);
                }
            ],
            [
                'attribute' => 'total',
                'label' => 'Total',
                'value' => function($model){
                    return number_format($model->total, 2);
                }
            ],
            [
                'attribute' => 'discount_per',
                'label' => 'Discount(%)'
            ],
            [
                'attribute' => 'discount_amount',
                'label' => 'Discount Amount',
                'value' => function($model){
                    return number_format($model->discount_amount, 2);
                }
            ],
            [
                'attribute' => 'igst',
                'label' => 'Tax Amount',
                'value' => function($model){
                    return number_format($model->igst, 2);
                }
            ],
            [
                'attribute' => 'grand_total',
                'label' => 'Grand Total',
                'value' => function($model){
                    return number_format($model->grand_total, 2);
                }
            ],


            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
