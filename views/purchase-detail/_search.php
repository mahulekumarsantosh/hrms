<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use app\models\Item;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\SaleDetailSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="card card-info">
    <div class="card-header">
        <h5>Search</h5>
    </div>
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'class' => 'form-horizontal',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-3 col-form-label',
                'wrapper' => 'col-sm-6',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>
    <div class="card-body">
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'date')->widget(DatePicker::className(), [
                'options' => ['class' => 'form-control'],
                'dateFormat' => 'yyyy-MM-dd'
            ]) ?>
            </div>

            <div class="col-md-6">
                <?= $form->field($model, "item_id")->widget(Select2::className(), [
                                'data' => ArrayHelper::map(Item::find()->where(['company_id' => Yii::$app->user->identity->company_id])->all(), 'id', 'name'),                                
                                'options' => [
                                    'placeholder' => 'Select Item...',
                                ],
            
            ]); ?>

            </div>
        </div>
    </div>
    <div class="card-footer">
        <div class="form-group">
            <?php // Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
            <?php // Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<?php
$script = <<< JS
    $('#purchasedetailsearch-item_id, #purchasedetailsearch-date').change(function(){
        $(this).closest('form').submit();
    });
JS;
$this->registerJs($script);
?>