<?php

use yii\helpers\Inflector;

$titleContent = array_keys(reset($contents));
?>
<div class="odd-row">
    <h3><?= $title ?></h3>
    <table class="table table-hover table-bordered">
        <thead>
            <tr>
                <?php foreach ($titleContent as $content) { ?>
                    <th><?= ucwords(Inflector::camel2id($content, " ")) ?></th>
                <?php } ?>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($contents as $content) { ?>
                <tr>
                    <?php foreach ($content as $content2) { ?>
                        <td><?= $content2 ?></td>
                    <?php } ?>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<?php return;?>
<table class="table" width='100%'>
    <tr>
        <th colspan="<?= count($titleContent) ?>">
            <h3><?= $title ?></h3>
        </th>
    </tr>
    <tr>
        <?php foreach ($titleContent as $content) { ?>
            <th><?= ucwords(Inflector::camel2id($content, " ")) ?></th>
        <?php } ?>
    </tr>
    <?php foreach ($contents as $content) { ?>
        <tr>
            <?php foreach ($content as $content2) { ?>
                <td><?= $content2 ?></td>
            <?php } ?>
        </tr>
    <?php } ?>
</table>