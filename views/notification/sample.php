<?php 
    
$imgdir = Yii::getAlias('@web');
?>

<div class="notification-wrap">
    <h1>ims</h1>
    <div class="notification-icon">
        <h2>Dues Notification</h2>
    </div>
    <div class="position-80">
        <div class="notification-user">
            Hi, Username!
        </div>
        <div class="odd-row">
            <h3>Notification Heading</h3>
            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th>Vahicle No.</th>
                        <th>Expiry Date</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>CG04AS2145</td>
                        <td><span class="text-danger">28-12-2020<span></td>
                    </tr>
                    <tr>
                        <td>CG04AS2145</td>
                        <td><span class="text-warning">28-12-2020<span></td>
                    </tr>
                    <tr>
                        <td>CG04AS2145</td>
                        <td><span class="text-success">28-12-2020<span></td>
                    </tr>
                    <tr>
                        <td>CG04AS2145</td>
                        <td><span class="text-warning">28-12-2020<span></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="even-row">
            <h3>Notification Heading</h3>
            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th>Vahicle No.</th>
                        <th>Expiry Date</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>CG04AS2145</td>
                        <td><span class="text-danger">28-12-2020<span></td>
                    </tr>
                    <tr>
                        <td>CG04AS2145</td>
                        <td><span class="text-warning">28-12-2020<span></td>
                    </tr>
                    <tr>
                        <td>CG04AS2145</td>
                        <td><span class="text-success">28-12-2020<span></td>
                    </tr>
                    <tr>
                        <td>CG04AS2145</td>
                        <td><span class="text-warning">28-12-2020<span></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>