<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MasterLeave */

$this->title = 'Update Master Leave: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Master Leaves', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="master-leave-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>