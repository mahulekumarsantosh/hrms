<?php

use app\models\MasterEmpGrade;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\MasterLeave */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="card">

    <?php
    $form = ActiveForm::begin([
        'enableClientValidation' => false,
        'enableAjaxValidation' => true,
        'id' => 'form-add-user',
        'class' => 'form-horizontal',        
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        ],
    ]);
    ?>

    <div class="card-body">
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'name')->textInput() ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'leave_days')->textInput(['onkeypress' => 'avascript:return isNumber(event)', 'maxlength' => '3']) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, "emp_grade")->widget(Select2::className(), [
                    'data' => ArrayHelper::map(MasterEmpGrade::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Select Grade...'
                    ],
                ]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'order_no')->textInput(['onkeypress' => 'avascript:return isNumber(event)', 'maxlength' => '2']) ?>
            </div>
        </div>
    </div>
    <div class="card-footer">
        <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Cancel', ['company/companyleave'], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>