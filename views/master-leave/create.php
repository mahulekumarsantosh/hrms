<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MasterLeave */

$this->title = 'Create Master Leave';
$this->params['breadcrumbs'][] = ['label' => 'Master Leaves', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-leave-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>