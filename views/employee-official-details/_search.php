<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EmployeeOfficialDetailsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="employee-official-details-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'employee_id') ?>

    <?= $form->field($model, 'department_id') ?>

    <?= $form->field($model, 'designation_id') ?>

    <?= $form->field($model, 'basic_salary') ?>

    <?php // echo $form->field($model, 'doj') ?>

    <?php // echo $form->field($model, 'security_deposit') ?>

    <?php // echo $form->field($model, 'shift') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
