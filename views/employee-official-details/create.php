<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EmployeeOfficialDetails */

$this->title = 'Create Employee Official Details';
$this->params['breadcrumbs'][] = ['label' => 'Employee Official Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employee-official-details-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
