<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use app\models\MasterRole;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Entity */
/* @var $form yii\bootstrap\ActiveForm */

$this->title = 'Change Password';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
$form = ActiveForm::begin([
            'enableClientValidation' => false,
            'enableAjaxValidation' => true,
            'id' => 'form-doctor',
            'layout' => 'horizontal',
            'class' => 'form-horizontal',
            'fieldConfig' => [
                'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                'horizontalCssClasses' => [
                    'label' => 'col-sm-3 col-form-label',
                    'wrapper' => 'col-sm-6',
                    'error' => '',
                    'hint' => '',
                ],
            ],
        ]);
?>

<div class="row">
    <div class="col-md-6">
        <?= $form->field($model, 'username')->textInput(['readonly' => true, 'tabindex' => '-1']) ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'email')->textInput(['readonly' => true, 'tabindex' => '-1']) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <?= $form->field($model, 'password')->passwordInput() ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'cpassword')->passwordInput() ?>
    </div>
</div>


<div class="box-footer">
    <?= Html::submitButton('<i class="fa fa-retweet fa-fw"></i> Change', ['class' => 'btn btn btn-success']) ?>
    <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-default']) ?>
</div>
<?php ActiveForm::end(); ?>