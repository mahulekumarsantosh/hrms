<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\MasterRole;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Entity */
/* @var $form yii\bootstrap\ActiveForm */

$this->title = 'Change Password';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
$form = ActiveForm::begin([
            'layout' => 'horizontal',
            'class' => 'form-horizontal',
//            'enableClientValidation' => false,
//            'enableAjaxValidation' => true,
            'enableAjaxValidation' => true,
            'validateOnChange' => false,
            'validateOnBlur' => false,
            'validateOnSubmit' => true,
            'id' => 'form-passwordchange'
        ]);
?>

<div class="row">
    <div class="col-md-12">
        <?= $form->field($model, 'old_password')->passwordInput()->label('Current password') ?>
    </div>
    <div class="col-md-12">
        <?= $form->field($model, 'password')->passwordInput() ?>
    </div>
    <div class="col-md-12">
        <?= $form->field($model, 'cpassword')->passwordInput() ?>
    </div>
</div>


<div class="box-footer">
    <?= Html::submitButton('Save', ['class' => 'btn btn btn-success']) ?>
</div>
<?php ActiveForm::end(); ?>