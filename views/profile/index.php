<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use app\models\MasterRole;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Entity */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Profile';
$this->params['breadcrumbs'][] = $this->title;
//unset($_COOKIE["ims_user_1"]);
//$data = json_decode($_COOKIE["ims_user_1"]) ?? 'navbar-white navbar-light';
//echo $class = isset($data->headerbar) && $data->user_id == Yii::$app->user->id ? $data->headerbar : 'navbar-white navbar-light';
//unset($_COOKIE['ims_user_1']);die;

//die;
?>

<?php
$form = ActiveForm::begin([
            'enableClientValidation' => false,
            'enableAjaxValidation' => true,
            'id' => 'form-doctor',
            'layout' => 'horizontal',
            'class' => 'form-horizontal',
            'fieldConfig' => [
                'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                'horizontalCssClasses' => [
                    'label' => 'col-sm-3 col-form-label',
                    'wrapper' => 'col-sm-6',
                    'error' => '',
                    'hint' => '',
                ],
            ],
        ]);
?>

<div class="row">
    <div class="col-md-6">
        <?= $form->field($model, 'fname')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'mobile')->textInput() ?>

    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'lname')->textInput() ?>   

    </div>

</div>

<div class="box-footer">
    <?= Html::submitButton('Save', ['class' => 'btn btn btn-success']) ?>
    <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-default']) ?>
</div>
<?php ActiveForm::end(); ?>