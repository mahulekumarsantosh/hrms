<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\ActiveForm;
use app\components\Encryption;

/* @var $this yii\web\View */
/* @var $model app\models\User */
extract($data);
$this->title = 'Update Profile: ' . $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
$action = Yii::$app->controller->action->id;
$userImagePath = Yii::getAlias("@web/") . Yii::$app->avatar->show($model->fname . " " . $model->lname,550);
?>
<div class="user-update">
    <div class="row">
        <div class="col-md-3">
            <div class="card card-primary card-outline">
                <div class="card-body box-profile">
                    <?php
                    $filePath = Yii::getAlias("@webroot/upload/user/$model->id/$model->profile_file");
                    $imgUrl = is_file($filePath) ? ["/upload/user/$model->id/$model->profile_file"] : $userImagePath;
                    ?>
                    <div class="text-center">
                        <?= Html::img($imgUrl, ['class' => "profile-user-img img-responsive img-circle",]) ?>
                    </div>

                    <h3 class="profile-username text-center"><?= $model->fname ?></h3>
                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item text-center">  <?php if ($user->status == 10) { ?>
                                <span class="badge bg-success">Active</span>
                            <?php } else { ?>
                                <span class="badge bg-warning">Inactive</span>
                            <?php } ?>
                        </li>
                        <li class="list-group-item">
                            <b>Username</b> <a class="float-right"><?= $model->username ?></a>
                        </li>
                        <li class="list-group-item">
                            <b>Name</b> <a href="#" class="float-right"><?= $model->fname . ' ' . $model->lname ?></a>
                        </li>
                        <li class="list-group-item">
                            <b>Email</b> <a href="#" class="float-right"><?= $model->email ?></a>
                        </li>
                        <li class="list-group-item">
                            <b>Contact</b> <a class="float-right"><?= $model->mobile ?></a>
                        </li>
                    </ul>
                </div>
                <!-- /.box-body -->
            </div>            
        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <div class="card card-primary card-outline card-outline-tabs">
                <div class="card-header p-0 border-bottom-0">
                    <ul class="nav nav-tabs">
                        <li class="nav-item"><a class="nav-link <?= $action == 'index' ? 'active' : '' ?>"  href="<?= Url::to(['index']) ?>" >Profile</a></li></li>
                        <li class="nav-item"><a class="nav-link <?= $action == 'update-password' ? 'active' : '' ?>"  href="<?= Url::to(['update-password']) ?>" >Change Password</a></li>
                        
                    </ul>
                </div>
                <div class="card-body">
                    <div class="tab-content py-3 ">
                        <?= $this->render("$action", $data) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
