<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use app\models\MasterRole;
use yii\helpers\ArrayHelper;
use app\components\Encryption;

/* @var $this yii\web\View */
/* @var $model app\models\Entity */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Theme';
$this->params['breadcrumbs'][] = $this->title;
//print_r($_COOKIE);
?>




<div>
    <h5><i>Header Navigation Color Variants </i></h5>
    <!--<hr>-->
    <div class="d-flex">
        <?php
        foreach ($navbar as $key => $value) {
            $keyid = "main-header navbar navbar-expand $key";
            print "<div class='$key elevation-2 setDataBtn p-3 border flex-fill m-2' data-id = '$keyid' data-class= 'main-header' data-type='headerbar' style='opacity: 0.8; cursor: pointer;' ></div>";
        }
        ?>
    </div>
</div>
<hr>
<div>
    <h5><i>Sidebar Light Color Variants </i></h5>
    <!--<hr>-->
    <div class="d-flex">
        <?php
        foreach ($sidebar as $key => $value) {
            $val = explode("-", $key)[1];
            $key1 = 'main-sidebar elevation-4 sidebar-light-' . $val;
            echo " <div class='$key elevation-2 p-3 border flex-fill m-2 setDataBtn' data-id = '$key1' data-class='main-sidebar' data-type='sidebar' style='opacity: 0.8; cursor: pointer;'></div>";
        }
        ?>

    </div>
</div>
<hr>
<div>
    <h5><i>Sidebar Dark Color Variants</i></h5>
    <!--<hr>-->
    <div class="d-flex">
        <?php
        foreach ($sidebar as $key => $value) {
            $val = explode("-", $key)[1];
            $key1 = 'main-sidebar elevation-4 sidebar-dark-' . $val;
            echo " <div class='$key elevation-2 p-3 border flex-fill m-2 setDataBtn' data-id = '$key1' data-class='main-sidebar' data-type='sidebar' style='opacity: 0.8; cursor: pointer;'></div>";
        }
        ?>

    </div>
</div>

<hr>
<div>
    <h5><i>Brand Box Color Variants</i></h5>
    <!--<hr>-->
    <div class="d-flex">
        <?php
        foreach ($brandbar as $key => $value) {
            echo " <div class='$key elevation-2 p-3 border flex-fill m-2 setDataBtn' data-id = 'brand-link $key' data-class='brand-link' data-type='brandbar' style='opacity: 0.8; cursor: pointer;' ></div>";
        }
        ?>

    </div>
</div>


<?php
$userid = Yii::$app->request->get('id') ? Yii::$app->request->get('id') : Encryption::Encrypt(Yii::$app->user->id);
$current_user_id = Encryption::Encrypt(Yii::$app->user->id);
$url = yii\helpers\Url::to(['/profile/set-theme', 'user_id' => $userid]);

$script1 = <<< JS
$("body").on('click',".setDataBtn",function(){
            var userid = '$userid';
            var current_user_id = '$current_user_id';
            var type = $(this).data('type'); 
            var constClasses = $(this).data('class');
            var dataid = $(this).data('id'); 
            if(current_user_id == userid){
             $("." +constClasses).attr("class", dataid);
            } 
            $.post("$url",{keyvalue:dataid, userid:'$userid' ,keytype:type});
        
        });
            
JS;
$this->registerJs($script1);
?>
