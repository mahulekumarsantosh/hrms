<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\ActiveForm;
use app\components\Encryption;

/* @var $this yii\web\View */
/* @var $model app\models\User */
extract($data);
$this->title = 'Update Company Details ';
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
$action = Yii::$app->controller->action->id;
?>
<div class="user-update">
    <div class="row">
        <!-- /.col -->
        <div class="col-md-12">
            <div class="card card-primary card-outline card-outline-tabs">
                <div class="card-header p-0 border-bottom-0">
                    <ul class="nav nav-tabs">
                        <li class="nav-item"><a class="nav-link <?= $action == 'update' ? 'active' : '' ?>"  href="<?= Url::to(['update']) ?>" >Basic Details</a></li></li>
                        <li class="nav-item"><a class="nav-link <?= $action == 'companyweekoff' ? 'active' : '' ?>"  href="<?= Url::to(['companyweekoff']) ?>" >Week Off</a></li>
                        <li class="nav-item"><a class="nav-link <?= $action == 'payrolldeduction' ? 'active' : '' ?>"  href="<?= Url::to(['payrolldeduction']) ?>" >Payroll Deduction</a></li>
                        <li class="nav-item"><a class="nav-link <?= $action == 'payrollearning' ? 'active' : '' ?>"  href="<?= Url::to(['payrollearning']) ?>" >Payroll Earning</a></li>
                        <li class="nav-item"><a class="nav-link <?= $action == 'companyshift' ? 'active' : '' ?>"  href="<?= Url::to(['companyshift']) ?>" >Shift</a></li>
                        <li class="nav-item"><a class="nav-link <?= $action == 'companyleave' ? 'active' : '' ?>"  href="<?= Url::to(['companyleave']) ?>" >Leave</a></li>
                    </ul>
                </div>
                <div class="card-body">
                    <div class="tab-content py-3 ">
                        <?= $this->render("$action", $data) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
