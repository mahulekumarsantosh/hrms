<?php

use yii\helpers\Html;
use app\components\GridView;
use app\components\Encryption;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Companies';
$this->params['breadcrumbs'][] = $this->title;
?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'exportTitle' => $this->title,
        'panel' => ['type' => GridView::TYPE_SECONDARY, 'heading' => $this->title],
        'export' => ['showConfirmAlert' => false,],
        'pjax' => true,
        'toolbar' => [
            Html::a('<i class="fa fa-plus"></i> Add New', ['create'], ['class' => 'btn btn-sm btn-success company float-right', 'title' => 'Add Company',  'data-pjax' => 0, 'style' => ['display' => Yii::$app->user->id == 1 ? '' : 'none']]),
            '{export}',
            '{perPage}',
        ],
        'options' => [
            'class' => 'grid-primary',
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'company_name',
            'mobile',
            'email:email',
            //'landline_no',
            //'state_id',
            //'district_id',
            //'city',
            //'full_address',
            //'pincode',
            'gst_number',
            'website',
            //'logo',
            //'no_of_employee',
            //'company_code',
            [
                'header' => "Action",
                'headerOptions' => ['class' => "skip-export text-center", 'width' => 150],
                'format' => 'raw',
                'contentOptions' => ['class' => "skip-export text-center", 'width' => 150],
                'value' => function($model) {
                    $list = [];   
                    $list[] = Html::a('<i class="fa fa-pencil-alt" title="Update"></i> ', ['update', "id" => Encryption::Encrypt($model->id)], ['class' => 'btn btn-xs btn-info', 'title' => 'Update Company Details', 'data-pjax' => 0]);
                    return implode('', $list);
                },
            ],
        ],
    ]); ?>

<?php
$script = <<< JS
    $("body").on('click', '.company',function(e) {
        e.preventDefault();
        $("#companyModalContent").load($(this).attr('href'));
        $("#companyModalTitle").text($(this).attr('title'));
        $('#companyModal').modal('show');
    });        
    
JS;
$this->registerJs($script);
echo $this->render("@app/views/layouts/models/_ajaxRenderModal", [
    'headerTitleId' => 'companyModalTitle',
    'modalId' => "companyModal",
    'contentId' => "companyModalContent",
    'size' => 'xl'
]);

?>

