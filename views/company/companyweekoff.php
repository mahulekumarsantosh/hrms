<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use kartik\time\TimePicker;

?>

<div class="card">

    <?php
    $form = ActiveForm::begin([
        'enableClientValidation' => false,
        'enableAjaxValidation' => true,
        'id' => 'form-add-user',
        'class' => 'form-horizontal',
        //'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-3 col-form-label',
                'wrapper' => 'col-sm-6',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]);
    ?>

    <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'day_ids')->inline(true)->checkboxList(Yii::$app->params['week_days']) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'working_hour')->textInput() ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'check_in')->widget(TimePicker::className(), [
                    'options' => ['class' => 'form-control'],
                    'pluginOptions' => [
                        'minuteStep' => 1,
                        'secondStep' => 1,
                        'defaultTime' => false,
                        'showMeridian' => false
                    ]
                ]) ?>
            </div> 
            <div class="col-md-4">
                <?= $form->field($model, 'check_out')->widget(TimePicker::className(), [
                    'options' => ['class' => 'form-control'],
                    'pluginOptions' => [
                        'minuteStep' => 1,
                        'secondStep' => 1,
                        'defaultTime' => false,
                        'showMeridian' => false
                    ]
                ]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'min_present_hour')->textInput() ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'min_half_day_hour')->textInput() ?>
            </div>
        </div>
    </div>
    <div class="card-footer">
        <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>