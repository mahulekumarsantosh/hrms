<?php

use yii\helpers\Html;
use app\components\GridView;
use app\components\Encryption;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MasterPayrollDeductionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Payroll Deduction';
$this->params['breadcrumbs'][] = $this->title;
?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,       
        'exportTitle' => $this->title,        
        'panel' => ['type' => GridView::TYPE_SECONDARY, 'heading' => $this->title],
        'export' => ['showConfirmAlert' => false,],
        'pjax' => true,
        'toolbar' => [
            Html::a('<i class="fa fa-plus"></i> Add New', ['master-payroll-deduction/create'], ['class' => 'btn btn-sm btn-success master-payroll-deduction float-right', 'title' => 'Add Payroll Deduction', 'data-pjax' => 0]),
            // '{export}',
            // '{perPage}',
        ],
        'options' => [
            'class' => 'grid-primary',
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            // 'id',
            // 'company_id',
            'name',
            'percent',
            'order_no',
            //'status',
            //'created_at',
            //'created_by',
            //'updated_at',
            //'updated_by',

            [
                'header' => "Action",
                'headerOptions' => ['class' => "skip-export text-center", 'width' => 150],
                'format' => 'raw',
                'contentOptions' => ['class' => "skip-export text-center", 'width' => 150],
                'value' => function ($model) {
                    $list = [];
                    $list[] = Html::a('<i class="fa fa-pencil-alt" title="Update"></i> ', ['master-payroll-deduction/update', "id" => Encryption::Encrypt($model->id)], ['class' => 'btn btn-xs btn-info master-payroll-deduction', 'title' => 'Update Company Payroll', 'data-pjax' => 0]);
                    return implode('', $list);
                },
            ],
        ],
    ]); ?>


<?php
$script = <<< JS
    $("body").on('click', '.master-payroll-deduction',function(e) {
        e.preventDefault();
        $("#masterPayrollDeductionModalContent").load($(this).attr('href'));
        $("#masterPayrollDeductionModalTitle").text($(this).attr('title'));
        $('#masterPayrollDeductionModal').modal('show');
    });        
    
JS;
$this->registerJs($script);
echo $this->render("@app/views/layouts/models/_ajaxRenderModal", [
    'headerTitleId' => 'masterPayrollDeductionModalTitle',
    'modalId' => "masterPayrollDeductionModal",
    'contentId' => "masterPayrollDeductionModalContent",
    'size' => 'xl'
]);

?>