<?php

use yii\helpers\Html;
use app\components\GridView;
use app\components\Encryption;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MasterLeaveSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Leave List';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="master-leave-index">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,        
        'exportTitle' => $this->title,        
        'panel' => ['type' => GridView::TYPE_SECONDARY, 'heading' => $this->title],
        'export' => ['showConfirmAlert' => false,],
        'pjax' => true,
        'toolbar' => [
            Html::a('<i class="fa fa-plus"></i> Add New', ['master-leave/create'], ['class' => 'btn btn-sm btn-success master-leave float-right', 'title' => 'Add Leave', 'data-pjax' => 0]),
        ],
        'options' => [
            'class' => 'grid-primary',
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            
            'name',
            'leave_days',
            'emp_grade', 
            [
                'header' => "Action",
                'headerOptions' => ['class' => "skip-export text-center", 'width' => 150],
                'format' => 'raw',
                'contentOptions' => ['class' => "skip-export text-center", 'width' => 150],
                'value' => function ($model) {
                    $list = [];
                    $list[] = Html::a('<i class="fa fa-pencil-alt" title="Update"></i> ', ['master-leave/update', "id" => Encryption::Encrypt($model->id)], ['class' => 'btn btn-xs btn-info master-leave', 'title' => 'Update Company Leave', 'data-pjax' => 0]);
                    return implode('', $list);
                },
            ],
        ],
    ]); ?>
</div>

<?php
$script = <<< JS
    $("body").on('click', '.master-leave',function(e) {
        e.preventDefault();
        $("#masterLeaveModalContent").load($(this).attr('href'));
        $("#masterLeaveModalTitle").text($(this).attr('title'));
        $('#masterLeaveModal').modal('show');
    });        
    
JS;
$this->registerJs($script);
echo $this->render("@app/views/layouts/models/_ajaxRenderModal", [
    'headerTitleId' => 'masterLeaveModalTitle',
    'modalId' => "masterLeaveModal",
    'contentId' => "masterLeaveModalContent",
    'size' => 'xl'
]);

?>