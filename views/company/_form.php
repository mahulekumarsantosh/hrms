<?php

use app\models\MasterDistrict;
use app\models\MasterState;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap4\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Brands */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="card">

    <?php
    $form = ActiveForm::begin([
        'enableClientValidation' => false,
        'enableAjaxValidation' => true,
        'id' => 'form-add-user',
        'class' => 'form-horizontal',
        //'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",            
        ],
    ]);
    ?>
    <div class="card-body">
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'company_name')->textInput() ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'mobile')->textInput(['onkeypress' => 'avascript:return isNumber(event)', 'maxlength' => '10']) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'email')->textInput() ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'landline_no')->textInput() ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'gst_number')->textInput() ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'website')->textInput() ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, "state_id")->widget(Select2::className(), [
                    'data' => ArrayHelper::map(MasterState::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Select State...'
                    ],
                ]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, "district_id")->widget(Select2::className(), [
                    'data' => ArrayHelper::map(MasterDistrict::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Select District...'
                    ],
                ]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'city')->textInput() ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'full_address')->textInput() ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'pincode')->textInput(['onkeypress' => 'avascript:return isNumber(event)', 'maxlength' => '6']) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'no_of_employee')->textInput(['onkeypress' => 'avascript:return isNumber(event)']) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'company_code')->textInput() ?>
            </div>
            <?php if ($model->isNewRecord) { ?>
                <div class="col-md-6">
                    <?= $form->field($model, 'logo')->fileInput() ?>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="card-footer">
        <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>