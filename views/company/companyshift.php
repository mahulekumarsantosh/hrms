<?php

use yii\helpers\Html;
use app\components\GridView;
use app\components\Encryption;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MasterShiftSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Shift List';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="master-shift-index">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,        
        'exportTitle' => $this->title,        
        'panel' => ['type' => GridView::TYPE_SECONDARY, 'heading' => $this->title],
        'export' => ['showConfirmAlert' => false,],
        'pjax' => true,
        'toolbar' => [
            Html::a('<i class="fa fa-plus"></i> Add New', ['master-shift/create'], ['class' => 'btn btn-sm btn-success master-shift float-right', 'title' => 'Add Shift', 'data-pjax' => 0]),
            
        ],
        'options' => [
            'class' => 'grid-primary',
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            
            //'id',
            // 'company_id',
            // 'company_shift_id',
            'shift_name',
            //'created_at',
            //'created_by',
            //'updated_at',
            //'updated_by',

            [
                'header' => "Action",
                'headerOptions' => ['class' => "skip-export text-center", 'width' => 150],
                'format' => 'raw',
                'contentOptions' => ['class' => "skip-export text-center", 'width' => 150],
                'value' => function ($model) {
                    $list = [];
                    $list[] = Html::a('<i class="fa fa-pencil-alt" title="Update"></i> ', ['master-shift/update', "id" => Encryption::Encrypt($model->id)], ['class' => 'btn btn-xs btn-info master-shift', 'title' => 'Update Company Shift', 'data-pjax' => 0]);
                    return implode('', $list);
                },
            ],
        ],
    ]); ?>
</div>

<?php
$script = <<< JS
    $("body").on('click', '.master-shift',function(e) {
        e.preventDefault();
        $("#masterShiftModalContent").load($(this).attr('href'));
        $("#masterShiftModalTitle").text($(this).attr('title'));
        $('#masterShiftModal').modal('show');
    });        
    
JS;
$this->registerJs($script);
echo $this->render("@app/views/layouts/models/_ajaxRenderModal", [
    'headerTitleId' => 'masterShiftModalTitle',
    'modalId' => "masterShiftModal",
    'contentId' => "masterShiftModalContent",
    'size' => 'xl'
]);

?>