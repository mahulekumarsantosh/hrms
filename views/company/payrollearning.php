<?php

use yii\helpers\Html;
use app\components\GridView;
use app\components\Encryption;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MasterPayrollEarningSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Payroll Earnings List';
$this->params['breadcrumbs'][] = $this->title;
?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,        
        'exportTitle' => $this->title,       
        'panel' => ['type' => GridView::TYPE_SECONDARY, 'heading' => $this->title],
        'export' => ['showConfirmAlert' => false,],
        'pjax' => true,
        'toolbar' => [
            Html::a('<i class="fa fa-plus"></i> Add New', ['master-payroll-earning/create'], ['class' => 'btn btn-sm btn-success master-payroll-earning float-right', 'title' => 'Add Payroll Earning', 'data-pjax' => 0]),
         
        ],
        'options' => [
            'class' => 'grid-primary',
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],            
            'name',
            'percent',
            'order_no',
            [
                'header' => "Action",
                'headerOptions' => ['class' => "skip-export text-center", 'width' => 150],
                'format' => 'raw',
                'contentOptions' => ['class' => "skip-export text-center", 'width' => 150],
                'value' => function ($model) {
                    $list = [];
                    $list[] = Html::a('<i class="fa fa-pencil-alt" title="Update"></i> ', ['master-payroll-earning/update', "id" => Encryption::Encrypt($model->id)], ['class' => 'btn btn-xs btn-info master-payroll-earning', 'title' => 'Update Company Earning', 'data-pjax' => 0]);
                    return implode('', $list);
                },
            ],
        ],
    ]); ?>

<?php
$script = <<< JS
    $("body").on('click', '.master-payroll-earning',function(e) {
        e.preventDefault();
        $("#masterPayrollEarningModalContent").load($(this).attr('href'));
        $("#masterPayrollEarningModalTitle").text($(this).attr('title'));
        $('#masterPayrollEarningModal').modal('show');
    });        
    
JS;
$this->registerJs($script);
echo $this->render("@app/views/layouts/models/_ajaxRenderModal", [
    'headerTitleId' => 'masterPayrollEarningModalTitle',
    'modalId' => "masterPayrollEarningModal",
    'contentId' => "masterPayrollEarningModalContent",
    'size' => 'xl'
]);

?>