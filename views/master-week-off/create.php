<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MasterWeekOff */

$this->title = 'Create Master Week Off';
$this->params['breadcrumbs'][] = ['label' => 'Master Week Offs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-week-off-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
