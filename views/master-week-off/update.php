<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MasterWeekOff */

$this->title = 'Update Master Week Off: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Master Week Offs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="master-week-off-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
