<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MasterPayrollDeduction */

$this->title = 'Update Master Payroll Deduction: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Master Payroll Deductions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="master-payroll-deduction-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>