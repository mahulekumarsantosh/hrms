<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MasterPayrollDeduction */

$this->title = 'Create Master Payroll Deduction';
$this->params['breadcrumbs'][] = ['label' => 'Master Payroll Deductions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-payroll-deduction-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>