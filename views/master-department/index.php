<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use app\components\GridView;
use app\components\Encryption;
/* @var $this yii\web\View */
/* @var $searchModel app\models\MasterDepartmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Master Departments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-department-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'exportTitle' => 'Department',
        'panel' => ['type' => GridView::TYPE_SECONDARY, 'heading' => $this->title],
        'export' => ['showConfirmAlert' => false,],
        'pjax' => true,
        'toolbar' => [
            Html::a('<i class="fa fa-plus"></i> Add New', ['create'], ['class' => 'btn btn-sm btn-success department float-right', 'title' => 'Add Department', 'data-pjax' => 0]),
            '{export}',
            '{perPage}',
        ],
        'options' => [
            'class' => 'grid-primary',
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'company_id',
            'name',
            [
                'header' => "Action",
                'headerOptions' => ['class' => "skip-export text-center", 'width' => 150],
                'format' => 'raw',
                'contentOptions' => ['class' => "skip-export text-center", 'width' => 150],
                'value' => function ($model) {
                    $list = [];
                    $list[] = Html::a('<i class="fa fa-pencil-alt" title="Update"></i> ', ['update', "id" => Encryption::Encrypt($model->id)], ['class' => 'btn btn-xs btn-info department', 'title' => 'Update Department', 'data-pjax' => 0]);
                    return implode('', $list);
                },
            ],
        ],
    ]); ?>
</div>
<?php
    $script = <<< JS
    $("body").on('click', '.department',function(e) {
        e.preventDefault();
        $("#departmentModalContent").load($(this).attr('href'));
        $("#departmentModalTitle").text($(this).attr('title'));
        $('#departmentModal').modal('show');
    });        
    
JS;
    $this->registerJs($script);
    echo $this->render("@app/views/layouts/models/_ajaxRenderModal", [
        'headerTitleId' => 'departmentModalTitle',
        'modalId' => "departmentModal",
        'contentId' => "departmentModalContent",
        'size' => 'lg'
    ]);

?>