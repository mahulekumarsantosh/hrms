<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MasterDepartment */

$this->title = 'Create Master Department';
$this->params['breadcrumbs'][] = ['label' => 'Master Departments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-department-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>