<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MasterDepartment */

$this->title = 'Update Master Department: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Master Departments', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="master-department-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>