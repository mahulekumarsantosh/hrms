<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

?>

<div class="card card-primary">

    <?php
        $form = ActiveForm::begin([
                    'enableClientValidation' => false,
                    'enableAjaxValidation' => true,
                    'id' => 'dynamic-form',
                    'class' => 'form-horizontal',
                    'layout' => 'horizontal',
                    'fieldConfig' => [
                        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-3 col-form-label',
                            'wrapper' => 'col-sm-6',
                            'error' => '',
                            'hint' => '',
                        ],
                    ],
                ]);
?>
    <div class="card-body">
        <div class="row">           
            <div class="col-md-6">
            <?= $form->field($model, "type")->widget(Select2::className(), [
                                'data' => Yii::$app->params['customer_type'],    
                                'options' => [
                                    'placeholder' => 'Select Type...',
                                ],
                                        
                            ]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'firm_name')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'mobile')->textInput(['onkeypress' => 'avascript:return isNumber(event)', 'maxlength' => '10']) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'gst_number')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'full_address')->textInput() ?>
            </div>
        </div>
    </div>
    <div class="card-footer">
        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>