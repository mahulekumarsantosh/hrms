<?php

use yii\helpers\Html;
use app\components\GridView;
use app\components\Encryption;
use app\models\Customer;
/* @var $this yii\web\View */
/* @var $searchModel app\models\CustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Customers/Vendor List';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'exportTitle' => 'Customer-Vendor-List',
        'panel' => ['type' => GridView::TYPE_SECONDARY, 'heading' => $this->title],
         'export' => ['showConfirmAlert' => false,],
        'pjax' => true,
    'toolbar' => [
        Html::a('<i class="fa fa-plus"></i> Add New', ['create'], ['class' => 'btn btn-sm btn-success customer float-right', 'title' => 'Add Customer', 'data-pjax' => 0]),
        '{export}',
        '{perPage}',
    ],
    'options' => [
        'class' => 'grid-primary',
    ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],           
            
            [
                'attribute' => 'type',
                'filter' => Yii::$app->params['customer_type'],
                'value' => function($model){
                    return $model->type == 1 ? Customer::VENDOR : Customer::CUSTOMER;
                }
            ],
            'firm_name',
            'mobile',
            'email:email',
            'gst_number',
            'full_address:ntext',          
            [
                'header' => "Action",
                'headerOptions' => ['class' => "skip-export text-center", 'width' => 150],
                'format' => 'raw',
                'contentOptions' => ['class' => "skip-export text-center", 'width' => 150],
                'value' => function($model) {
                    $list = [];   
                    $list[] = Html::a('<i class="fa fa-pencil-alt" title="Update"></i> ', ['update', "id" => Encryption::Encrypt($model->id)], ['class' => 'btn btn-xs btn-info customer', 'title' => 'Update Customer', 'data-pjax' => 0]);
                    return implode('', $list);
                },
            ], 
        ],
    ]); ?>
<?php
$script = <<< JS
    $("body").on('click', '.customer',function(e) {
        e.preventDefault();
        $("#customerModalContent").load($(this).attr('href'));
        $("#customerModalTitle").text($(this).attr('title'));
        $('#customerModal').modal('show');
    });        
    
JS;
$this->registerJs($script);
echo $this->render("@app/views/layouts/models/_ajaxRenderModal", [
    'headerTitleId' => 'customerModalTitle',
    'modalId' => "customerModal",
    'contentId' => "customerModalContent",
    'size' => 'xl'
]);

?>
