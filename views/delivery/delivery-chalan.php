<?php

use yii\helpers\Html;
use app\components\Encryption;
use app\models\Company;
$company = Company::find()->where(['id' => Yii::$app->user->identity->company_id])->one();
if($company && $company->logo != ''){
    $imgUrl = Yii::getAlias("@web/images/logo/".$company->logo);
} else{
    $imgUrl = Yii::getAlias("@web/images/project-management.png");
}

$this->title = 'Delivery challan';
?>

<div class="card">
    <?php if (!isset($export)) { ?>
        <div class="card-header">
            <?= Html::a('<i class="fa fa-file-pdf fa-fw"></i> PDF', ["pdf-delivery-challan", 'id' => Yii::$app->request->get('id')], ['class' => 'btn btn-sm btn-warning float-right', 'target' => '_blank']) ?>
        </div>
    <?php } ?>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size: 14px; border-color: #fff; border-collapse: collapse; margin-bottom: 10px;">
        <tr>
            <td rowspan="2" width="140">
                <?= Html::img($imgUrl, ['alt' => $company->company_name, 'width' => '70']); ?>
            </td>
            <td>
                <h2 style="font-size: 2em;"><?= $company->company_name ?></h2>
            </td>
            <td style="text-align: right;">
                Date: <?= date('d/m/Y', strtotime($model->date)); ?><br/>
                Challan No: <?= $model->code; ?>
            </td>
        </tr>
        <tr>
            <td><span style="background-color: #333;border-radius: 8px; padding: 5px 10px; color:#fff;font-weight: bold;">&nbsp;&nbsp;Delivery Challan
                    &nbsp;&nbsp;</span></td>
        </tr>
    </table>
    <table class="purchase-table" width="100%" border="1" cellspacing="0" cellpadding="2" style="border-color: #ccc; border-collapse: collapse;">
        <tr>
            <td class="width_a4_50" colspan="2" width="50%" valign="top">
                <b>Customer Details:-</b><br />
                <strong><?= $model->customerDetail->firm_name; ?></strong><br />
                <?= $model->customerDetail->mobile; ?><br/>
                <?= $model->customerDetail->gst_number ? $model->customerDetail->gst_number . '</br>' : ''; ?>
                <?= $model->customerDetail->full_address; ?></br>
            </td>
            <td class="width_a4_50" colspan="2" width="50%" valign="top">
                <b>Company Details:-</b><br />
                <strong><?= $company->company_name; ?></strong><br>
                <?= $company->gst_number; ?><br />
                <?= $company->email; ?><br />
                <?= $company->website; ?><br />
                <?= $company->mobile; ?><br />
                <?= $company->full_address; ?>
            </td>
        </tr>
    </table>

    <table class="purchase-table table-font-13 " width="100%" border="1" cellspacing="0" cellpadding="2" style="border-color: #ccc; border-collapse: collapse;">
        <tr>
            <th width="45">S/N</th>
            <th width="150">Brand</th>
            <th width="250">Item Name</th>
            <th width="150">Unit</th>
            <th width="100">QTY</th>            
        </tr>
        <tbody>
            <?php
            $i = 1;
            $total_amount = $total_discount = $total_cgst = $total_sgst = $grand_total = 0;
            foreach ($model->deliveryDetails as $data) { ?>
                <tr>
                    <td><?= $i; ?></td>
                    <td><?= $data->brand; ?></td>
                    <td><?= $data->item->name; ?></td>
                    <td><?= $data->item->unit; ?></td>
                    <td><?= $data->quantity; ?></td>
                </tr>
            <?php
                // $total_discount += $data->discount_amount;
                // $total_amount += $data->total;
                // $total_cgst += $data->cgst;
                // $total_sgst += $data->sgst;
                // $grand_total += $data->grand_total;
                $i++;
            }
            ?>
            
            <tr>
                <td colspan="8">
                    <b>Terms & Condition :</b>
                    <p> 1. After Delivery of material weblinto will not be responsible for above items or any changes.<br />
                        2. Material will be install within 15 working days.<br/>
                        3. After delivery of materials will not be return.<br/>
                        </p>
                </td>
            </tr>
            <tr>
                <td colspan="8">
                    <br /><br />
                    <b>Signature</b>
                </td>
            </tr>
        </tbody>
    </table>
</div>