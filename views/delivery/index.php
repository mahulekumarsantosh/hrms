<?php

use yii\helpers\Html;
use app\components\GridView;
use app\components\Encryption;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PurchaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Delivery List';
$this->params['breadcrumbs'][] = $this->title;
?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'exportTitle' => $this->title,
        'panel' => ['type' => GridView::TYPE_SECONDARY, 'heading' => $this->title],
         'export' => ['showConfirmAlert' => false,],
        'pjax' => true,
    'toolbar' => [
        Html::a('<i class="fa fa-plus"></i> Add Delivery Challan', ['create'], ['class' => 'btn btn-sm btn-primary', 'data-pjax' => 0]),
        '{export}',
        '{perPage}',
    ],
    'options' => [
        'class' => 'grid-primary',
    ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'code',          
            [
                'attribute' => 'Customer',
                'value' => function($model){
                    return $model->customerDetail->firm_name;
                }
              ],
            'date',
           
            [
                'header' => "Action",
                'headerOptions' => ['class' => "skip-export text-center", 'width' => 150],
                'format' => 'raw',
                'contentOptions' => ['class' => "skip-export text-center", 'width' => 150],
                'value' => function($model) {
                    $list = [];   
                    $list[] = Html::a('<i class="fa fa-pencil-alt" title="Update"></i> ', ['update', "id" => Encryption::Encrypt($model->id)], ['class' => 'btn btn-xs btn-info', 'data-pjax' => 0]);
                    $list[] = Html::a('<i class="fa fa-eye" title="View"></i> ', ['delivery-chalan', "id" => Encryption::Encrypt($model->id)], ['class' => 'btn btn-xs btn-warning', 'data-pjax' => 0]);
                    return implode(' ', $list);
                },
            ],        
        ],
    ]); ?>
     
