<?php

use app\models\Item;
use app\models\Customer;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap4\ActiveForm;
use yii\jui\DatePicker;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\select2\Select2;
use app\models\Brands;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\Purchase */
/* @var $form yii\widgets\ActiveForm */
$url = Url::to(['delivery/get-item']);
$js = '
function setIndexNo() {
    
    $(".dynamicform_wrapper .item td:first-child b").each(function(index) {
        $(this).text(index + 1);
        
        $("#deliverydetail-" + index + "-brand").change(function(){
            var brand = $(this).val();
            $.post("getitem", {brand: brand}, function(response) {
                $("#deliverydetail-" + index + "-item_id").html(response);
            });
        });
       
    });
}

$(".dynamicform_wrapper").on({
    afterInsert: function(e, item) {
        $(item).find("select").prop("disabled", false);
        setIndexNo();
    },
    afterDelete: function(e, item) {
        setIndexNo();
    }
}); 

$("body").on("change", ".calculate", function(){
    setIndexNo();
}); 
';

$this->registerJs($js);
?>

<div class="card card-primary">
    <?php
        $form = ActiveForm::begin([
                    'enableClientValidation' => false,
                    'enableAjaxValidation' => true,
                    'id' => 'dynamic-form',
                    'class' => 'form-horizontal',
                    'layout' => 'horizontal',
                    'fieldConfig' => [
                        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-3 col-form-label',
                            'wrapper' => 'col-sm-6',
                            'error' => '',
                            'hint' => '',
                        ],
                    ],
                ]);
?>
    <div class="card-body">
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'date')->widget(DatePicker::className(), [
                'options' => ['class' => 'calculate form-control'],
                'dateFormat' => 'yyyy-MM-dd'
            ]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, "customer")->widget(Select2::className(), [
                                'data' => ArrayHelper::map(Customer::find()->where(['type' => 2, 'company_id' => Yii::$app->user->identity->company_id])->all(), 'id', 'firm_name'),
                                'options' => [
                                    'placeholder' => 'Select Vendor...',
                                    'class' => 'calculate'
                                ],
            
                            ]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'code')->textInput(['maxlength' => true, 'class' => 'calculate form-control']) ?>
            </div>
            
        </div>
        <?php
            DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                'widgetBody' => '.container-items', // required: css class selector
                'widgetItem' => '.item', // required: css class
                'min' => 1, // 0 or 1 (default 1)
                'insertButton' => '.add-item', // css class
                'deleteButton' => '.remove-item', // css class
                'model' => $modelDetails[0],
                'formId' => 'dynamic-form',
                'formFields' => [
                    'item_id',
                    'quantity'                    
                ],
            ]); ?>
        <table class="table table-striped table-bordered container-items">
            <tr>
                <th>S/N</th>
                <th>Brand</th>
                <th>Item Name</th>
                <th>Quantity</th>
               
                <th><button type="button" class="pull-right add-item btn btn-success btn-xs"><i class="fa fa-plus"></i></button></th>
            </tr>
            <?php foreach ($modelDetails as $i => $modelDetail): ?>
            <tr class="item">
                <td>
                    <b><?= $i + 1 ?></b>
                    <?php if (!$modelDetail->isNewRecord) {
                                    echo Html::activeHiddenInput($modelDetail, "[{$i}]id");
                                } ?>
                </td>
                <td><?= $form->field($modelDetail, "[{$i}]brand", ['template' => '{label} <div class="col-sm-12">{input}{error}{hint}</div>','labelOptions' => ['class' => 'col-form-label col-sm-2']])->widget(Select2::className(), [
                                'data' => ArrayHelper::map(Brands::find()->where(['company_id' => Yii::$app->user->identity->company_id])->all(), 'name', 'name'),
                                //'disabled' => !$modelDetail->isNewRecord,
                                'options' => [
                                    'placeholder' => 'Select Brand...',
                                    'class' => 'calculate'
                                ],
                                'pluginOptions' => [                                  
                                    'width' => '100px',
                                ],
            
                            ])->label(false) ?></td>
                <td><?= $form->field($modelDetail, "[{$i}]item_id", ['template' => '{label} <div class="col-sm-12">{input}{error}{hint}</div>','labelOptions' => ['class' => 'col-form-label col-sm-2']])->widget(Select2::className(), [
                                'data' => ArrayHelper::map(Item::find()->where(['company_id' => Yii::$app->user->identity->company_id])->all(), 'id', 'name'),
                                //'disabled' => !$modelDetail->isNewRecord,
                                'options' => [
                                    'placeholder' => 'Select Item...',
                                ],
                                'pluginOptions' => [                                  
                                    'width' => '150px',
                                ],
            
                            ])->label(false) ?></td>
                <td><?= $form->field($modelDetail, "[{$i}]quantity", ['template' => '{label} <div class="col-sm-12">{input}{error}{hint}</div>','labelOptions' => ['class' => 'col-form-label col-sm-2']])->textInput(['class' => 'calculate form-control'])->label(false) ?></td>
                
                <td><button type="button" class="pull-right remove-item btn btn-danger btn-xs"><i
                            class="fa fa-minus"></i> </button></td>
            </tr>

    </div>
    <?php endforeach; ?>    
    </table>
    <?php DynamicFormWidget::end(); ?>

</div>
<div class="card-footer">
    <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-default']) ?>
</div>
<?php ActiveForm::end(); ?>
</div>