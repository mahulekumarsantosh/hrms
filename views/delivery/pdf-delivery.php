<?php

use app\models\Delivery;
use app\components\Encryption;

$id = Encryption::Decrypt(Yii::$app->request->get('id'));
$model = Delivery::findOne($id);
print $this->render('delivery-chalan', [
            'model' => $model,            
            'export' => $export,
]);
?>
