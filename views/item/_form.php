<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use kartik\select2\Select2;
use app\models\Brands;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\Item */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="card card-primary">
    <?php
$form = ActiveForm::begin([
            'enableClientValidation' => false,
            'enableAjaxValidation' => true,
            'id' => 'form-add-user',
            'class' => 'form-horizontal',
            'layout' => 'horizontal',
            'fieldConfig' => [
                'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                'horizontalCssClasses' => [
                    'label' => 'col-sm-3 col-form-label',
                    'wrapper' => 'col-sm-6',
                    'error' => '',
                    'hint' => '',
                ],
            ],
        ]);
?>
    <div class="card-body">
        <div class="row">

        <div class="col-md-6">
        <?= $form->field($model, "brand")->widget(Select2::className(), [
                                'data' => ArrayHelper::map(Brands::find()->where(['company_id' => Yii::$app->user->identity->company_id])->all(), 'name', 'name'),
                                //'disabled' => !$modelDetail->isNewRecord,
                                'options' => [
                                    'placeholder' => 'Select Brand...',
                                ],
            
                            ]); ?>
            </div>
            
            <div class="col-md-6">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'model_no')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>
            </div>
            
            <div class="col-md-6">
                <?= $form->field($model, "unit")->widget(Select2::className(), [
                                'data' => Yii::$app->params['unit'], 
                                'options' => [
                                    'placeholder' => 'Select Unit...',
                                ],
                                           
                            ]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'quantity')->textInput(['disabled' => !$model->isNewRecord ? true : false]); ?>
            </div> 
            <div class="col-md-6">
                <?= $form->field($model, 'current_price')->textInput(); ?>
            </div>

        </div>
    </div>
    <div class="card-footer">
        <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-default']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>