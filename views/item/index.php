<?php

use yii\helpers\Html;
use app\components\GridView;
use app\components\Encryption;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Item List';
$this->params['breadcrumbs'][] = $this->title;
?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'exportTitle' => $this->title,
        'showPageSummary' => true,
        'panel' => ['type' => GridView::TYPE_SECONDARY, 'heading' => $this->title],
    'export' => ['showConfirmAlert' => false,],
    'pjax' => true,
    'toolbar' => [
        Html::a('<i class="fa fa-plus"></i> Add New', ['create'], ['class' => 'btn btn-sm btn-success items float-right', 'title' => 'Add Item', 'data-pjax' => 0]),
        '{export}',
        '{perPage}',
    ],
    'options' => [
        'class' => 'grid-primary',
    ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
           [
              'attribute' => 'model_no',
              'filter' => false,
           ], 
          'brand',
            'name',
            [
                'attribute' => 'stock',
                'filter' => false,
                'pageSummary' => true,
                'value' => function($model){
                    return number_format(($model->stock), 2);
                }
             ],
             [   
                'attribute' => 'current_price',
                'filter' => false,
                'value' => function($model){
                    return number_format(($model->current_price), 2);
                }
             ],
 
             [   
                 'label' => 'Current Value',                
                 'value' => function($model){
                     return number_format(($model->stock * $model->current_price), 2);
                 },
                 'pageSummary' => true
             ],
             
            [
                'header' => "Action",
                'headerOptions' => ['class' => "skip-export text-center", 'width' => 150],
                'format' => 'raw',
                'contentOptions' => ['class' => "skip-export text-center", 'width' => 150],
                'value' => function($model) {
                    $list = [];   
                    $list[] = Html::a('<i class="fa fa-pencil-alt" title="Update"></i> ', ['update', "id" => Encryption::Encrypt($model->id)], ['class' => 'btn btn-xs btn-info items', 'title' => 'Update Item', 'data-pjax' => 0]);
                    return implode('', $list);
                },
            ],
        ],
    ]); ?>
<?php
$script = <<< JS
    $("body").on('click', '.items',function(e) {
        e.preventDefault();
        $("#itemsModalContent").load($(this).attr('href'));
        $("#itemsModalTitle").text($(this).attr('title'));
        $('#itemsModal').modal('show');
    });        
    
JS;
$this->registerJs($script);
echo $this->render("@app/views/layouts/models/_ajaxRenderModal", [
    'headerTitleId' => 'itemsModalTitle',
    'modalId' => "itemsModal",
    'contentId' => "itemsModalContent",
    'size' => 'xl'
]);

?>
