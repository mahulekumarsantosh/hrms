<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MasterDistrict */

$this->title = 'Update Master District: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Master Districts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="master-district-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>