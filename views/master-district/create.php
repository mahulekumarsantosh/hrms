<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MasterDistrict */

$this->title = 'Create Master District';
$this->params['breadcrumbs'][] = ['label' => 'Master Districts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-district-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>