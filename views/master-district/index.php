<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use app\components\GridView;
use app\components\Encryption;
/* @var $this yii\web\View */
/* @var $searchModel app\models\MasterDistrictSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Master District';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-district-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'exportTitle' => 'District',
        'panel' => ['type' => GridView::TYPE_SECONDARY, 'heading' => $this->title],
        'export' => ['showConfirmAlert' => false,],
        'pjax' => true,
        'toolbar' => [
            Html::a('<i class="fa fa-plus"></i> Add New', ['create'], ['class' => 'btn btn-sm btn-success district float-right', 'title' => 'Add District', 'data-pjax' => 0]),
            '{export}',
            '{perPage}',
        ],
        'options' => [
            'class' => 'grid-primary',
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'State',
                'value' => function($model){
                    return $model->stateDetail->name;
                }
              ],
            'name',
            [
                'header' => "Action",
                'headerOptions' => ['class' => "skip-export text-center", 'width' => 150],
                'format' => 'raw',
                'contentOptions' => ['class' => "skip-export text-center", 'width' => 150],
                'value' => function ($model) {
                    $list = [];
                    $list[] = Html::a('<i class="fa fa-pencil-alt" title="Update"></i> ', ['update', "id" => Encryption::Encrypt($model->id)], ['class' => 'btn btn-xs btn-info district', 'title' => 'Update District', 'data-pjax' => 0]);
                    return implode('', $list);
                },
            ],
        ],
    ]); ?>
</div>
<?php
    $script = <<< JS
    $("body").on('click', '.district',function(e) {
        e.preventDefault();
        $("#districtModalContent").load($(this).attr('href'));
        $("#districtModalTitle").text($(this).attr('title'));
        $('#districtModal').modal('show');
    });        
    
JS;
    $this->registerJs($script);
    echo $this->render("@app/views/layouts/models/_ajaxRenderModal", [
        'headerTitleId' => 'districtModalTitle',
        'modalId' => "districtModal",
        'contentId' => "districtModalContent",
        'size' => 'lg'
    ]);

?>