<?php

use yii\helpers\Html;
use app\models\Company;

//$user_id = Yii::$app->user->id;
$user = Yii::$app->user->identity;

$data = !empty($_COOKIE["ims_user_$user->id"]) ? json_decode($_COOKIE["ims_user_$user->id"]) : [];
if(isset($data->headerbar) && !empty($data->headerbar) && isset($data->user_id) && !empty($data->user_id) && $data->user_id == $user->id){
    $class = $data->headerbar;
}else{
    $class = 'navbar-white navbar-light';
}
$company = Company::find()->where(['id' => Yii::$app->user->identity->company_id])->one();
if($company && $company->logo != ''){
    $userImagePath = Yii::getAlias("@web/images/logo/".$company->logo);
} else{
    $userImagePath = Yii::getAlias("@web/images/project-management.png");
}

?>
<nav class="main-header navbar navbar-expand <?= $class ?>">
    
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>       
    </ul>   
    <ul class="navbar-nav ml-auto">        
        <li class="nav-item">
            <?= Html::a('<i class="fas fa-sync-alt"></i> Clear Cache', ['/site/clear-cache'], ['class' => 'nav-link']) ?>
        </li>         
               
        <li class="nav-item dropdown user-menu">
            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                <?php
                $filePath = Yii::getAlias("@webroot/upload/user/$user->id/$user->profile_file");
                $imgUrl = is_file($filePath) ? ["/upload/user/$user->id/$user->profile_file"] : $userImagePath;
                ?>                
                <img src="<?= $imgUrl ?>" class="user-image img-circle elevation-2" alt="User Image">
                <span class="d-none d-md-inline"><?= !Yii::$app->user->isGuest ? Yii::$app->user->identity->username : '' ?></span>
            </a>
            <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <!-- User image -->
                <li class="user-header bg-dark">
                    <img src="<?= $imgUrl ?>" class="img-circle elevation-2" alt="User Image">                   

                    <p>
                        <?= !Yii::$app->user->isGuest ? Yii::$app->user->identity->username : '' ?>
                        <br>
                        <?= !Yii::$app->user->isGuest ? Yii::$app->user->identity->email : '' ?>                        
                    </p>
                </li> 
                <li class="user-footer">                    
                    <?= Html::a('Profile', ['/profile'], ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']) ?>

                    <?= Html::a('Sign out', ['/site/logout'], ['data-method' => 'post', 'class' => 'btn btn-default btn-flat float-right']) ?>
                </li>
            </ul>
        </li>
        
    </ul>
</nav>