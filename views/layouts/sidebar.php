<?php
$user = Yii::$app->user->identity;
?>
<aside class="main-sidebar elevation-4 sidebar-dark-info">   

    <a  href="<?= \yii\helpers\Url::home() ?>" class="brand-link text-center primary">
        <span class="brand-text font-weight-light"> IMS</span>
    </a>
    <div class="slimScrollDiv">
        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <?php
                echo \app\components\AdminLte3menu::widget([
                    'items' => [
                        ['label' => 'Master Module', 'header' => true],
                        ['label' => 'Home', 'icon' => 'home', 'url' => ['/site/index']],
                        ['label' => 'User Management', 'icon' => 'user', 'url' => ['/manage/user/index']],
                        ['label' => 'Company', 'icon' => 'user', 'url' => ['/company/index']],
                        ['label' => 'Employee', 'icon' => 'user', 'url' => ['/employee/index']],
                        [
                            'label' => 'Masters',
                            'icon' => 'cog',
                            'url' => '#',                            
                            'items' => [
                                ['label' => 'Customer/ Vendor', 'icon' => 'users', 'url' => ['/customer/index']],
                                ['label' => 'Brands', 'icon' => 'tasks', 'url' => ['/brands/index']],
                                ['label' => 'Item Master', 'icon' => 'list-alt', 'url' => ['/item/index']],
                                ['label' => 'Designation Master', 'icon' => 'list-alt', 'url' => ['/master-designation/index']],
                                ['label' => 'Department Master', 'icon' => 'list-alt', 'url' => ['/master-department/index']],
                                ['label' => 'Role Master', 'icon' => 'list-alt', 'url' => ['/master-role/index']],
                                ['label' => 'Employee Grade Master', 'icon' => 'list-alt', 'url' => ['/master-emp-grade/index']],
                                ['label' => 'Employee Type Master', 'icon' => 'list-alt', 'url' => ['/master-emp-type/index']],
                                ['label' => 'Holiday Master', 'icon' => 'list-alt', 'url' => ['/master-holiday/index']],
                                ['label' => 'State Master', 'icon' => 'list-alt', 'url' => ['/master-state/index']],
                                ['label' => 'District Master', 'icon' => 'list-alt', 'url' => ['/master-district/index']],
                            ],
                        ],                      

                       [
                        'label' => 'Inventory',
                        'icon' => 'money-check',
                        'url' => '#',                            
                        'items' => [
                        ['label' => 'Purchase', 'icon' => 'money-check-alt', 'url' => ['/purchase/index']],
                        ['label' => 'Quotation', 'icon' => 'wallet', 'url' => ['/quotation/index']],
                        ['label' => 'Delivery', 'icon' => 'money-check', 'url' => ['/delivery/index']],                        
                        ['label' => 'Sales', 'icon' => 'file-alt', 'url' => ['/sale/index']],
                        ],
                    ],                        
                       [
                        'label' => 'Reports',
                        'icon' => 'list',
                        'url' => '#',                            
                        'items' => [                            
                            ['label' => 'Purchase Report', 'icon' => 'tasks', 'url' => ['/purchase-detail/index']],
                            ['label' => 'Sale Report', 'icon' => 'list', 'url' => ['/sale-detail/index']],
                            ['label' => 'Inventory Summary Report', 'icon' => 'list', 'url' => ['/manage-stock/index']],
                        ],
                    ],
                        
                    ],
                ]);
                ?>              
            </nav>            
        </div>       
    </div>
</aside>

