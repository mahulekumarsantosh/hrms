<footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
        <!-- Anything you want -->
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2018-2020 <a href="https://suntechnos.in" target="_blank">Suntechnos</a>.</strong> All rights reserved.
</footer>