<?php
/* @var $content string */

use yii\bootstrap4\Breadcrumbs;
use app\components\AlertHelper;
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <!-- <h1 class="m-0 text-dark">
                        <?php
                        // if (!is_null($this->title)) {
                        //     echo $this->title;
                        // } else {
                        //     echo \yii\helpers\Inflector::camelize($this->context->id);
                        // }
                        ?>
                    </h1> -->
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <?php
                    echo Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                        'options' => [
                            'class' => 'float-sm-right'
                        ]
                    ]);
                    ?>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="alert-block">
            <?= AlertHelper::widget() ?>
        </div>
        <?= $content ?><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
<?php
$script = <<< JS
    setTimeout(function() {
        $(".alert-block").slideUp('slow');
    }, 7000);        
JS;
$this->registerJs($script);
?>