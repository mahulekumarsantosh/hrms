<?php

use yii\helpers\Html;
?>
<div id="preloader" style="display: none">
    <div class="spinner-border text-primary" role="status">
        <span class="sr-only">Loading...</span>
    </div>
</div>


<?php
$script = <<< JS
   var data = [];

        
        
   $('body').on('beforeSubmit', function (event) {
   data.push(event.timeStamp) 
   if(data.length === 1){
        $("#preloader").css({"display":"flex"});
        return true;
        }
        else{
        $("#preloader").css({"display":"none"});
        return false;
        }
    });
      
JS;
$this->registerJs($script);
?>