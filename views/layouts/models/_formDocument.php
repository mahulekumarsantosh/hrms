<?php

use yii\helpers\Html;
use wbraganca\dynamicform\DynamicFormWidget;

//use kartik\file\FileInput;
?>
<div class="">
    <?php
    $prefix = "doc-" . str_shuffle('abcdefg');
    DynamicFormWidget::begin([
        'widgetContainer' => 'dynamicform_wrapper',
        'widgetBody' => ".$prefix-form-options-body",
        'widgetItem' => ".$prefix-form-options-item",
        'min' => false,
        'limit' => 5,
        'insertButton' => ".$prefix-add",
        'deleteButton' => ".$prefix-delete",
        'model' => $model[0],
        'formId' => (isset($formId)) ? $formId : "dynamic-form",
        'formFields' => [
            'remark',
            'doc_path'
        ],
    ]);
//    echo "<pre>";
//    print_r($model);
//    echo "</pre>";
    ?>
    <div class="table-responsive">
        <table class="table table-bordered table-striped margin-b-none">
            <thead>
                <tr>
                    <th>Document</th>
                    <th class="required" style="">Document Name</th>
                    <th style="width: 9px; text-align: center">Actions</th>
                </tr>
            </thead>
            <tbody class="<?= $prefix ?>-form-options-body">
                <?php foreach ($model as $index => $modelOptionValue): ?>
                    <tr class="<?= $prefix ?>-form-options-item">
                        <td>
                            <?php if (!$modelOptionValue->isNewRecord): ?>
                                <?= Html::activeHiddenInput($modelOptionValue, "[{$index}]id"); ?>
                                <?= Html::activeHiddenInput($modelOptionValue, "[{$index}]doc_path"); ?>
                                <?php //= Html::activeHiddenInput($modelOptionValue, "[{$index}]deleteImg"); ?>
                            <?php endif; ?>

                            <?= $form->field($modelOptionValue, "[{$index}]doc_path")->fileInput()->label(false) ?>
                        </td>
                        <td class="vcenter" >
                            <?= $form->field($modelOptionValue, "[{$index}]remark")->textInput()->label(false) ?>
                        </td>
                        <td class="text-center vcenter">
                            <button type="button" class="<?= $prefix ?>-delete btn btn-danger btn-xs"><i class="fa fa-minus"></i></button>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="2"></td>
                    <td class="text-center"><button type="button" class="<?= $prefix ?>-add btn btn-success btn-xs"><span class="fa fa-plus"></span></button></td>
                </tr>
            </tfoot>
        </table>
    </div>
    <?php DynamicFormWidget::end(); ?>
</div>

<?php
$js = <<<'EOD'

$(".optionvalue-img").on("filecleared", function(event) {
    var regexID = /^(.+?)([-\d-]{1,})(.+)$/i;
    var id = event.target.id;
    var matches = id.match(regexID);
    if (matches && matches.length === 4) {
        var identifiers = matches[2].split("-");
        $("#optionvalue-" + identifiers[1] + "-deleteimg").val("1");
    }
});

var fixHelperSortable = function(e, ui) {
    ui.children().each(function() {
        $(this).width($(this).width());
    });
    return ui;
};

EOD;

//JuiAsset::register($this);
//$this->registerJs($js);
?>






<!--    <div class="col-md-4">
        <div class="input-daterange">
            <label for="start_date">Date</label>
            <div class="input-group">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
<?php //= $form->field($insuranceModel, 'start_date', ["template" => "{input}{error}"])->textInput(['id' => "start_date"]) ?>
                <div class="input-group-addon">
                    <span class="input-group-text">To</span>
                </div>
<?php //= $form->field($insuranceModel, 'end_date', ["template" => "{input}{error}", 'options' => ['tag' => false]])->textInput() ?>
            </div>
<?php //= $form->field($insuranceModel, 'start_date')->textInput() ?>
        </div>
    </div>-->