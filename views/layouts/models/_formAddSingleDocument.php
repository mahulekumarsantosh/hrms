<?php

use yii\helpers\Html;
use yii\helpers\Url;
use wbraganca\dynamicform\DynamicFormWidget;
use yii\widgets\ActiveForm;
use app\models\Document;

Document::setFormName("Document");
//$model->m_document_type_id = $documentTypeId;
//$model->public_id = $publicId;
//use kartik\file\FileInput;
?>
<div id="addDocumentModal" class="modal fade" role="dialog"  data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header d-block">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Document</h4>
            </div>
            <?php
            $form = ActiveForm::begin([
                        'action' => ["/doc/create"],
                        'enableAjaxValidation' => true,
                        'validationUrl' => ['/doc/doc-validation'],
                        'options' => [
                            'enctype' => 'multipart/form-data',
                            'id' => "create-doc-form"
                        ]
            ]);
            ?>
            <div class="modal-body">
                <div class="row">
                    <input type="hidden" name="" id="add-single-doc-suffix" />
                    <?= $form->field($model, 'public_id')->hiddenInput(['id' => "add-single-doc-public_id"])->label(false) ?>
                    <?= $form->field($model, 'folder_id')->hiddenInput(['id' => "add-single-doc-folder_id"])->label(false) ?>
                    <?= $form->field($model, 'master_document_type_id')->hiddenInput(['id' => "add-single-doc-m_document_type_id"])->label(false) ?>
                    <div class="col-md-12">
                        <?= $form->field($model, 'doc_path')->fileInput(['maxlength' => true, 'class' => 'form-control add-single-doc-doc_path']) ?>
                        <p class="docError text-danger"></p>
                    </div>
                    <div class="col-md-12"><?= $form->field($model, 'remark')->textarea(['maxlength' => true, 'class' => 'form-control add-single-doc-remark']) ?></div>
                </div>
            </div>

            <div class="modal-footer">

                <div class="form-group">
                    <?= Html::submitButton('<i class="fa fa-save fa-fw"></i> Save', ['class' => 'btn btn-success']) ?>
                    <?=
                    Html::button('<i class="fa fa-close fa-fw"></i> Cancel', [
                        'class' => 'btn btn-danger',
                        'data-dismiss' => "modal"
                    ])
                    ?>
                </div>

            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>


<?php
$script = <<< JS
    $('body').on('beforeSubmit', 'form#create-doc-form', function (e) {
        e.preventDefault();
        //alert();
        var form = $(this);
        if (form.find('.has-error').length) {
            return false;
        }
        // submit form
        $.ajax({
            url    : form.attr('action'),
            method  : 'post',
            data:  new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            success: function (resObj){
                if (resObj.response) {
                    dataObj = resObj.data;
                    newRowContent = '<tr><td>'+dataObj.remark+'</td><td class="text-center"><a target = "_blank" href="'+dataObj.doc_path+'"><i class="fa fa-file-image-o fa-2x"></i></a></td><td><button class="btn btn-danger btn-xs btn-block del_button" type="button" data-id="'+dataObj.id+'"><span class="fa fa-trash"></span> Delete</button></td></tr>';
        
                    
                    suffix = $("#add-single-doc-suffix").val();
                    $("#tblmdoc-"+suffix+" tbody").append(newRowContent);
                    $('#addDocumentModal').modal('hide');
                    $('.add-single-doc-doc_path,.add-single-doc-remark').val('')
                    $('.docError').html('')
                } else {
                    $('.docError').html(resObj.error)
                }
            },
            error  : function (){
                console.log('internal server error');
            }
        });
        return false;
    });
JS;
$this->registerJs($script);
?>


<?php
$script = <<< JS
    $("body").on('click','.btn-addDocument', function() {
//        alert();
        thisObj = $(this);
        //alert();
        publicId = thisObj.data("public-id");
        folderId = thisObj.data("folder-id");
        documentTypeId = thisObj.data("document-type-id");
        suffix = thisObj.data("suffix");
        
        //alert(publicId);
        
        $("#document-doc_path").val('');
        $("#add-single-doc-public_id").val(publicId);
        $("#add-single-doc-folder_id").val(folderId);
        $("#add-single-doc-m_document_type_id").val(documentTypeId);
        $("#add-single-doc-suffix").val(suffix);
        
        $('#addDocumentModal').modal('show');
    });
JS;
$this->registerJs($script);
?>

<?php
$url = Url::to(['/doc/delete']);
$script = <<< JS
    $("body").on('click', '.del_button',function(e) {
        if(confirm("Are You Sure You want to delete this record.?")){
            thisObj = $(this);
            id = thisObj.data("id");
            $.ajax({
               type: 'POST',
               url:'$url',
               data:{id:id},
               success:function(res){
                   //alert(res.response);
                   if(res.response){
                       var whichtr = thisObj.closest("tr");
                       whichtr.remove();      
                   }
               }
           })   
        }
    });
JS;
$this->registerJs($script);
?>