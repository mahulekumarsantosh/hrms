<?php

use app\models\Document;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\StringHelper;
use app\components\Encryption;
use app\models\DocumentType;

$suffix = "$documentTypeId-$publicId";
$enableDelete = isset($enableDelete) ? $enableDelete : true;
$enableAdd = isset($enableAdd) ? $enableAdd : true;

$documentType = DocumentType::findOne($documentTypeId);
$module = \app\models\MasterModule::findOne($documentType->module_id);
$Folder = $module->folder_name;
?>
<table class="table table-bordered table-condensed" id="tblmdoc-<?= $suffix ?>">
    <thead>
        <tr>
            <th>Title</th>
            <th width="90">Document</th>                            
            <th width="80">Action</th>                            
        </tr>
    </thead>
    <tbody>
        <?php if ($document) { ?>
            <?php foreach ($document as $InsuranceDoc) { ?>
                <?php $fafa = StringHelper::getDocIcons(pathinfo($InsuranceDoc['doc_path'], PATHINFO_EXTENSION)); ?>
                <tr>
                    <td><?= $InsuranceDoc['remark'] ?></td>
                    <td class="text-center">
                        <?= Html::a("<i class='$fafa fa-2x'></i>", ["/upload/$Folder/$folderId/$InsuranceDoc[doc_path]"], ['class' => "", 'target' => "_blank"]) ?>
                    </td>

                    <td>
                        <?php if ($enableDelete) { ?>
                            <?=
                            Html::button("<span class='fa fa-trash'></span> Delete", [
                                'class' => "btn btn-danger btn-xs btn-block del_button",
                                'data-id' => Encryption::Encrypt($InsuranceDoc["id"])
                            ]);
                            ?>
                        <?php } ?>
                    </td>
                </tr>
            <?php } ?>
        <?php } ?>
    </tbody>
    <tfoot>
        <tr>
            <td ></td>
            <td></td>
            <td>

                <?php if ($enableAdd) { ?>
                    <?=
                    Html::button("<span class='fa fa-plus'></span> Add New", [
                        'class' => "btn  btn-success  btn-xs btn-addDocument",
//                    'data-toggle' => "modal",
//                    'data-target' => "#addDocumentModal",
                        'data-public-id' => $publicId,
                        'data-folder-id' => $folderId,
                        'data-document-type-id' => $documentTypeId,
                        'data-suffix' => $suffix,
                    ]);
                    ?>
                <?php } ?>
            </td>
        </tr>
    </tfoot>
</table>


<?php
Document::$isModalAdded = true;
?>
 <?php
//$formUrl = Url::to(['/vsm/vehicle/add-vehicle-sale', 'VehicleId' => Encryption::Encrypt($Vehicle['id'])]);
$script = <<< JS
    $("body").on('click', '.btn-addDocument',function(e) {
        
           thisObj = $(this);
        //alert();
        publicId = thisObj.data("public-id");
        folderId = thisObj.data("folder-id");
        documentTypeId = thisObj.data("document-type-id");
        suffix = thisObj.data("suffix");
        
        //alert(publicId);
        
        $("#document-doc_path").val('');
        $("#add-single-doc-public_id").val(publicId);
        $("#add-single-doc-folder_id").val(folderId);
        $("#add-single-doc-m_document_type_id").val(documentTypeId);
        $("#add-single-doc-suffix").val(suffix);
        
        $('#addDocumentModal').modal('show');
    });
JS;
$this->registerJs($script);
 
?>
 
