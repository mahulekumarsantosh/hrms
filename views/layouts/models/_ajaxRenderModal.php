<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */
$size = isset($size) ? $size : 'md';
$headerClass = isset($headerClass) ? $headerClass : '';
$headerTitleClass = isset($headerTitleClass) ? $headerTitleClass : '';
$headerTitleId = isset($headerTitleId) ? $headerTitleId : '';
$contentClass = isset($contentClass) ? $contentClass : '';
$modalClass = isset($modalClass) ? $modalClass : '';
$loader = isset($loader) ? $loader : true;
$closeButton = isset($closeButton) ? $closeButton : true;
$heading = isset($heading) ? $heading : '';
?>
 

<div id="<?= $modalId ?>" class="modal <?= ($loader) ? 'modal-loader' : ''  ?>  fade <?= $modalClass ?>"  data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-<?= $size ?>">
        <div class="modal-content">
            <div class="modal-header d-block<?= $headerClass ?>">
                <?php if ($closeButton) { ?><button type="button" class="close" data-dismiss="modal">&times;</button> <?php } ?>
                <h4 id="<?= $headerTitleId ?>" class="modal-title  <?= $headerTitleClass ?>"><?= $heading ?></h4>
            </div>
            <div class="modal-body">
                 <div id="<?= $contentId ?>" class="modal-content-loader <?= $contentClass ?>">
                    <div class="overlay-wrapper">
                        <div class="overlay">
                            <i class="fa fa-refresh fa-spin"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
