<?php

use app\models\Item;
use app\models\Customer;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap4\ActiveForm;
use yii\jui\DatePicker;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\select2\Select2;
use app\models\Brands;
use dosamigos\ckeditor\CKEditor;
/* @var $this yii\web\View */
/* @var $model app\models\Purchase */
/* @var $form yii\widgets\ActiveForm */

$js = '
function setIndexNo() {
    var total_amt = 0;
    $(".dynamicform_wrapper .item td:first-child b").each(function(index) {
        $(this).text(index + 1);
        var quantity = $("#quotationdetail-" + index + "-quantity").val();
        var price = $("#quotationdetail-" + index + "-price").val();
        var discount = $("#quotationdetail-" + index + "-discount_per").val();
        var discount_amt = (quantity * price * discount) /100;
        var amount = (quantity * price) - discount_amt;
        var tax_type = $("#quotation-tax_type").val();
        if(tax_type == 2){
            var tax_per = $("#quotationdetail-" + index + "-tax_per").val();
            var tax_amount = (amount * tax_per) / 100;
        } else {
            var tax_amount = 0;
        }        
        $("#quotationdetail-" + index + "-igst").val(tax_amount);
        $("#quotationdetail-" + index + "-total").val(quantity * price);
        $("#quotationdetail-" + index + "-grand_total").val(amount + tax_amount);
        total_amt += amount + tax_amount;
        $("#sumBeforeGst").text(total_amt);

        $("#quotationdetail-" + index + "-brand").change(function(){
            var brand = $(this).val();
            $.post("getitem", {brand: brand}, function(response) {
                $("#quotationdetail-" + index + "-item_id").html(response);
            });
        });
        
    });
}

$(".dynamicform_wrapper").on({
    afterInsert: function(e, item) {
        $(item).find("select").prop("disabled", false);
        setIndexNo();
    },
    afterDelete: function(e, item) {
        setIndexNo();
    }
});

$("body").on("change", ".calculate", function(){
    setIndexNo();
}); 

$("#quotation-tax_type").change(function(){
    var type = $(this).val();
    if(type == 1){
        $(".tax").hide();
    } else{
        $(".tax").show();
    }
});
';

$this->registerJs($js);
?>

<div class="card card-primary">
    <?php
        $form = ActiveForm::begin([
                    'enableClientValidation' => false,
                    'enableAjaxValidation' => true,
                    'id' => 'dynamic-form',
                    'class' => 'form-horizontal',
                    'layout' => 'horizontal',
                    'fieldConfig' => [
                        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-3 col-form-label',
                            'wrapper' => 'col-sm-6',
                            'error' => '',
                            'hint' => '',
                        ],
                    ],
                ]);
?>
    <div class="card-body">
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'date')->widget(DatePicker::className(), [
                'options' => ['class' => 'form-control calculate'],
                'dateFormat' => 'yyyy-MM-dd'
            ]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, "customer")->widget(Select2::className(), [
                                'data' => ArrayHelper::map(Customer::find()->where(['type' => 2, 'company_id' => Yii::$app->user->identity->company_id])->all(), 'id', 'firm_name'),
                                'options' => [
                                    'placeholder' => 'Select Vendor...',
                                    'class' => 'calculate'
                                ],
            
                            ]) ?>
            </div>
        </div>
        <div class="row">            
            <div class="col-md-6">
                <?= $form->field($model, "tax_type")->widget(Select2::className(), [
                                'data' => Yii::$app->params['tax-type'],
                                'options' => [
                                    //'placeholder' => 'Select Tax...',
                                    'class' => 'calculate'
                                ],
            
                            ]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'subject')->textInput() ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
            <?=
                 $form->field($model, 'message')->widget(CKEditor::className(), [
                     'options' => ['rows' => 4],
                     'preset' => 'basic',
                 ]);
                 ?>
            </div>
        </div>
        <?php
            DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                'widgetBody' => '.container-items', // required: css class selector
                'widgetItem' => '.item', // required: css class
                'min' => 1, // 0 or 1 (default 1)
                'insertButton' => '.add-item', // css class
                'deleteButton' => '.remove-item', // css class
                'model' => $modelDetails[0],
                'formId' => 'dynamic-form',
                'formFields' => [
                    'item_id',
                    'quantity',
                    'price',
                ],
            ]); ?>
        <table class="table table-striped table-bordered container-items">
            <tr>
                <th>#</th>
                <th>Brand</th>
                <th>Item Name</th>
                <th>Quantity</th>
                <th>Price/Unit</th>
                <th>Total</th>
                <th>Discount(In %)</th>
                <th class="tax">Tax %</th>
                <th class="tax">Tax Amount</th>
                <th>Grand Total</th>
                <th><button type="button" class="pull-right add-item btn btn-success btn-xs"><i class="fa fa-plus"></i> </button></th>
            </tr>
            <?php foreach ($modelDetails as $i => $modelDetail): ?>
            <tr class="item">
                <td>
                    <b><?= $i + 1 ?></b>
                    <?php if (!$modelDetail->isNewRecord) {
                                    echo Html::activeHiddenInput($modelDetail, "[{$i}]id");
                                } ?>
                </td>
                <td><?= $form->field($modelDetail, "[{$i}]brand", ['template' => '{label} <div class="col-sm-12">{input}{error}{hint}</div>','labelOptions' => ['class' => 'col-form-label col-sm-2']])->widget(Select2::className(), [
                                'data' => ArrayHelper::map(Brands::find()->where(['company_id' => Yii::$app->user->identity->company_id])->all(), 'name', 'name'),
                                'disabled' => !$modelDetail->isNewRecord,
                                'options' => [
                                    'placeholder' => 'Select Brand...',
                                ],
                                'pluginOptions' => [                                  
                                    'width' => '100px',
                                ],
            
                            ])->label(false) ?></td>
                <td><?= $form->field($modelDetail, "[{$i}]item_id", ['template' => '{label} <div class="col-sm-12">{input}{error}{hint}</div>','labelOptions' => ['class' => 'col-form-label col-sm-2']])->widget(Select2::className(), [
                                'data' => ArrayHelper::map(Item::find()->where(['company_id' => Yii::$app->user->identity->company_id])->all(), 'id', 'name'),
                                'disabled' => !$modelDetail->isNewRecord,
                                'options' => [
                                    'placeholder' => 'Select Item...',
                                ],
                                'pluginOptions' => [                                  
                                    'width' => '150px',
                                ],
            
                            ])->label(false) ?></td>
                <td><?= $form->field($modelDetail, "[{$i}]quantity", ['template' => '{label} <div class="col-sm-12">{input}{error}{hint}</div>','labelOptions' => ['class' => 'col-form-label col-sm-2']])->textInput(['class' => 'calculate form-control'])->label(false) ?></td>
                <td><?= $form->field($modelDetail, "[{$i}]price", ['template' => '{label} <div class="col-sm-12">{input}{error}{hint}</div>','labelOptions' => ['class' => 'col-form-label col-sm-2']])->textInput(['class' => 'calculate form-control'])->label(false) ?></td>
                <td><?= $form->field($modelDetail, "[{$i}]total", ['template' => '{label} <div class="col-sm-12">{input}{error}{hint}</div>','labelOptions' => ['class' => 'col-form-label col-sm-2']])->textInput(['readonly' => true])->label(false) ?></td>
                <td><?= $form->field($modelDetail, "[{$i}]discount_per", ['template' => '{label} <div class="col-sm-12">{input}{error}{hint}</div>','labelOptions' => ['class' => 'col-form-label col-sm-2']])->textInput(['class' => 'calculate form-control'])->label(false) ?></td>
                <td class="tax"><?= $form->field($modelDetail, "[{$i}]tax_per", ['template' => '{label} <div class="col-sm-12">{input}{error}{hint}</div>','labelOptions' => ['class' => 'col-form-label col-sm-2']])->widget(Select2::className(), [
                                'data' => Yii::$app->params['tax-per'],
                                'disabled' => !$modelDetail->isNewRecord,
                                'options' => [
                                    'placeholder' => 'Select Item...',
                                    'class' => 'calculate form-control'
                                ],
            
                            ])->label(false) ?></td>
                <td class="tax"><?= $form->field($modelDetail, "[{$i}]igst", ['template' => '{label} <div class="col-sm-12">{input}{error}{hint}</div>','labelOptions' => ['class' => 'col-form-label col-sm-2']])->textInput(['readonly' => true])->label(false) ?></td>
                <td><?= $form->field($modelDetail, "[{$i}]grand_total", ['template' => '{label} <div class="col-sm-12">{input}{error}{hint}</div>','labelOptions' => ['class' => 'col-form-label col-sm-2']])->textInput(['readonly' => true])->label(false) ?></td>
                <td><button type="button" class="pull-right remove-item btn btn-danger btn-xs"><i
                            class="fa fa-minus"></i></button></td>
            </tr>

    </div>
    <?php endforeach; ?>
    <tfoot>
    <tr>
                                <td class="text-right" colspan="8">Total</td>
                                <td class="text-right">₹ <span id="sumBeforeGst">00.00</span></td> 
                                  <td></td>                                 
                            </tr>
                            </tfoot>
    </table>
    <?php DynamicFormWidget::end(); ?>

</div>
<div class="card-footer">
    <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-default']) ?>
</div>
<?php ActiveForm::end(); ?>
</div>