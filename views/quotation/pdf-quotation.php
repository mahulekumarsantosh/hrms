<?php

use app\models\Quotation;
use app\components\Encryption;

$id = Encryption::Decrypt(Yii::$app->request->get('id'));
$model = Quotation::findOne($id);
print $this->render('quotation-chalan', [
            'model' => $model,            
            'export' => $export,
]);
?>
