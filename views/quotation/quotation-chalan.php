<?php

use yii\helpers\Html;
use app\components\Encryption;
use app\models\Company;
$company = Company::find()->where(['id' => Yii::$app->user->identity->company_id])->one();
if($company && $company->logo != ''){
    $imgUrl = Yii::getAlias("@web/images/logo/".$company->logo);
} else{
    $imgUrl = Yii::getAlias("@web/images/project-management.png");
}
$this->title = 'Quotation challan';
?>

<div class="card">
    <?php if (!isset($export)) { ?>
        <div class="card-header">
            <?= Html::a('<i class="fa fa-file-pdf fa-fw"></i> PDF', ["pdf-quotation-challan", 'id' => Yii::$app->request->get('id')], ['class' => 'btn btn-sm btn-warning float-right', 'target' => '_blank']) ?>
        </div>
    <?php } ?>
    <div>        
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size: 14px; border-color: #fff; border-collapse: collapse; margin-bottom: 10px;">
        <tr>
            <td rowspan="2" width="140">
                <?= Html::img($imgUrl, ['alt' => $company->company_name, 'width' => '70']); ?>
            </td>
            <td>
                <h2 style="font-size: 2em;"><?= $company->company_name ?></h2>
            </td>
            <td style="text-align: right; font-size: 12px">
                Date: <?= $model->date; ?><br/>                
            </td>
        </tr>
        <tr>
            <td><span style="background-color: #333;border-radius: 8px; padding: 5px 10px; color:#fff;font-weight: bold;">&nbsp;&nbsp;Quotation
                    &nbsp;&nbsp;</span></td>
        </tr>
    </table>
    <p>
        <b>To,<br/>
        Mr, <?= $model->customerDetail->firm_name; ?></strong><br /><?= $model->customerDetail->full_address; ?>
    </b><br/>
      Subject: <?= $model->subject; ?>
   <?= $model->message; ?></p>    

    <table class="purchase-table table-font-13 " width="100%" border="1" cellspacing="0" cellpadding="2" style="border-color: #ccc; border-collapse: collapse;">
        <tr style="text-align: center">
            <th width="45">S/N</th>
            <th width="100">Brand</th>
            <th width="250">Item Name</th>
            <th width="50">Unit</th>
            <th width="50">QTY</th>
            <th width="100">Price/Unit</th> 
            <th width="50">Discount (%)</th>
            <?php if($model->tax_type == 2){ ?>           
            <th width="50">Tax %</th>
            <?php } ?>
            <th width="110">Total Amount</th>
        </tr>
        <tbody>
            <?php
            $i = 1;
            $total_amount = $total_discount = $total_cgst = $total_sgst = $grand_total = 0;
            foreach ($model->quotationDetails as $data) { ?>
                <tr style="text-align: center">
                    <td><?= $i; ?></td>
                    <td><?= $data->brand; ?></td>
                    <td><?= $data->item->name; ?></td>
                    <td><?= $data->item->unit; ?></td>
                    <td><?= $data->quantity; ?></td>
                    <td><?= number_format($data->price, 2); ?></td> 
                    <td><?= $data->discount_per; ?></td>
                    <?php if($model->tax_type == 2){ ?>                
                    <td><?= $data->tax_per; ?></td>
                    <?php } ?>
                    <td style="text-align: right; padding-right: 10px;"><?= number_format($data->total, 2); ?></td>
                </tr>
            <?php
                $total_discount += $data->discount_amount;
                $total_amount += $data->total;
                $total_cgst += $data->cgst;
                $total_sgst += $data->sgst;
                $grand_total += $data->grand_total;
                $i++;
            }
            ?>
           
            <tr>
                <th colspan="<?= $model->tax_type == 2 ? 8 : 7 ?>" style="text-align: right; padding-right: 10px;">Sub Total</th>
                <td style="text-align: right; padding-right: 10px;"><?= number_format($total_amount, 2); ?></td>
            </tr>  
            <?php if($total_discount > 0){ ?>
            <tr>
                <th colspan="<?= $model->tax_type == 2 ? 8 : 7 ?>" style="text-align: right; padding-right: 10px;">Discount Amount</th>
                <td style="text-align: right; padding-right: 10px;"><?= number_format($total_discount, 2); ?></td>
            </tr>
            <?php } ?>
            <?php if($model->tax_type == 2){ ?>           
            <tr>
                <th colspan="8" style="text-align: right; padding-right: 10px;">CGST (9%)</th>
                <td style="text-align: right; padding-right: 10px;"><?= number_format($total_cgst, 2); ?></td>
            </tr>
            <tr>
                <th colspan="8" style="text-align: right; padding-right: 10px;">SGST (9%)</th>
                <td style="text-align: right; padding-right: 10px;"><?= number_format($total_sgst, 2); ?></td>
            </tr>
            <?php } ?>
            <tr>
                <th colspan="<?= $model->tax_type == 2 ? 8 : 7 ?>" style="text-align: right; padding-right: 10px;">Grand Total</th>
                <td style="text-align: right; padding-right: 10px;"><?= number_format($grand_total, 2); ?></td>
            </tr>
            <tr>
                <td colspan="<?= $model->tax_type == 2 ? 9 : 8 ?>">
                    <b>Terms & Condition :</b>
                    <p style="font-size: 14px;"> 1. Terms of payment 75% advance, 25% on delivery.<br />
                        2. The complete system contains warranty for trouble free performance of the system for 1 year from the date of delivery.<br />
                        3. Any failures arising out of natural calamities like lighting, food, surge voltage, burntout, physical damage, etc will not be covered under the above proposal.<br />
                        4. Poll/Pipe will be provided by the customer.<br/>
                        5. Weblinto will deliver material against of purchase order.<br/>
                        6. GST will be extra.</p>
                </td>
            </tr>
            <tr>
                <td colspan="<?= $model->tax_type == 2 ? 9 : 8 ?>">
                    <br /><br /></br>
                    <b>Signature</b>
                </td>
            </tr>            
        </tbody>
    </table>
    <div>
        <b>
            Thanks & Regards<br/>
            <?= $company->company_name; ?>
            <br/>
            +91-<?= $company->mobile; ?>
        </b>
    </div>
</div>