<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MasterPayrollEarning */

$this->title = 'Create Master Payroll Earning';
$this->params['breadcrumbs'][] = ['label' => 'Master Payroll Earnings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-payroll-earning-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>