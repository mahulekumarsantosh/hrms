<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MasterPayrollEarning */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="card">

    <?php
    $form = ActiveForm::begin([
        'enableClientValidation' => false,
        'enableAjaxValidation' => true,
        'id' => 'form-add-user',
        'class' => 'form-horizontal',        
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",            
        ],
    ]);
    ?>

    <div class="card-body">
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'name')->textInput() ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'percent')->textInput(['onkeypress' => 'avascript:return isNumber(event)', 'maxlength' => '3']) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'order_no')->textInput(['onkeypress' => 'avascript:return isNumber(event)', 'maxlength' => '2']) ?>
            </div>
        </div>
    </div>
    <div class="card-footer">
        <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Cancel', ['company/payrollearning'], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>