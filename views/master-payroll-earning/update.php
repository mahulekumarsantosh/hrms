<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MasterPayrollEarning */

$this->title = 'Update Master Payroll Earning: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Master Payroll Earnings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="master-payroll-earning-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>