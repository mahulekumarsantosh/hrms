<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MasterEmpGrade */

$this->title = 'Update Master Emp Grade: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Master Emp Grades', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="master-emp-grade-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>