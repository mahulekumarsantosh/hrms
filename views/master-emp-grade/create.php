<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MasterEmpGrade */

$this->title = 'Create Master Emp Grade';
$this->params['breadcrumbs'][] = ['label' => 'Master Emp Grades', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-emp-grade-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>