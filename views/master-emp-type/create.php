<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MasterEmpType */

$this->title = 'Create Master Emp Type';
$this->params['breadcrumbs'][] = ['label' => 'Master Emp Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-emp-type-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>