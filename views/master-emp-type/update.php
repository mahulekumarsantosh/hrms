<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MasterEmpType */

$this->title = 'Update Master Emp Type: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Master Emp Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="master-emp-type-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>