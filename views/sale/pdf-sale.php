<?php

use app\models\Sale;
use app\components\Encryption;

$id = Encryption::Decrypt(Yii::$app->request->get('id'));
$model = Sale::findOne($id);
print $this->render('delivery-chalan', [
            'model' => $model,            
            'export' => $export,
]);
?>
