<?php

use yii\helpers\Html;
use app\components\Encryption;
use app\models\Company;
$company = Company::find()->where(['id' => Yii::$app->user->identity->company_id])->one();
if($company && $company->logo != ''){
    $imgUrl = Yii::getAlias("@web/images/logo/".$company->logo);
} else{
    $imgUrl = Yii::getAlias("@web/images/project-management.png");
}
$this->title = 'Invoice';
?>

<div class="card">   
<?php if(!isset($export)){ ?>  
<div class="card-header">
  <?= Html::a('<i class="fa fa-file-pdf fa-fw"></i> PDF', ["pdf-sale-challan", 'id' => Yii::$app->request->get('id')], ['class' => 'btn btn-sm btn-warning float-right', 'target' => '_blank']) ?>
</div>
<?php } ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0"
           style="font-size: 14px; border-color: #fff; border-collapse: collapse; margin-bottom: 10px;">
        <tr>
            <td rowspan="2" width="140">
                <?= Html::img($imgUrl, ['alt' => $company->company_name, 'width' => '70']); ?>
            </td>
            <td>
                <h2 style="font-size: 2em;"><?= $company->company_name ?></h2>
            </td>
            <td style="text-align: right; font-size: 12px">
            Date: <?= date('d/m/Y', strtotime($model->date)) ?><br/>
            Invoice No: <?= $model->code; ?>
        </td>
        </tr>
        <tr>
            <td><span
                    style="background-color: #333;border-radius: 8px; padding: 5px 10px; color:#fff;font-weight: bold;">&nbsp;&nbsp;<?= $model->tax_type == 2 ?'Invoice' : 'Cash Memo' ?>&nbsp;&nbsp;</span></td>
        </tr>
    </table>
    <table class="purchase-table" width="100%" border="1" cellspacing="0" cellpadding="2"
           style="border-color: #ccc; border-collapse: collapse;">
           <tr>
           <td class="width_a4_50" colspan="2" width="50%" valign="top">
           <b>Company Details:-</b><br />
                <strong><?= $company->company_name; ?></strong><br>
                <?= $company->gst_number; ?><br />
                <?= $company->email; ?><br />
                <?= $company->website; ?><br />
                <?= $company->mobile; ?><br />
                <?= $company->full_address; ?>
            </td>

           <td class="width_a4_50" colspan="2" width="50%" valign="top"> 
           <b>Customer Details:-</b><br />
           <strong><?= $model->customerDetail->firm_name; ?></strong><br/>
           <?= $model->customerDetail->mobile; ?><br/>
           <?= $model->customerDetail->gst_number ? $model->customerDetail->gst_number.'<br/>' : ''; ?>
           <?= $model->customerDetail->full_address; ?><br/>
           </td>
          
           </tr>
    </table>

    <table class="purchase-table table-font-13 " width="100%" border="1" cellspacing="0" cellpadding="2"
           style="border-color: #ccc; border-collapse: collapse;">
        <tr >
            <th width="45">S.NO.</th>
            <th width="250">Item Name</th>  
            <th width="50">QTY</th>
            <th width="50">Unit</th> 
            <th width="100">Price/Unit</th>            
            <th width="50">Discount(%)</th> 
            <?php if($model->tax_type ==2){ ?>
            <th width="50">Tax %</th>    
    <?php } ?>
            <th width="110">Total Amount</th>                    
        </tr>
        <tbody>
        <?php
        $i = 1;
        $total_amount = $total_discount = $total_cgst = $total_sgst = $grand_total = 0;
        foreach($model->salesDetails as $data){ ?>
        <tr>
        <td><?= $i; ?></td>
        <td><?= $data->item->name;?></td>
        <td><?= $data->quantity; ?></td>
        <td><?= $data->item->unit;?></td>        
        <td><?= number_format($data->price, 2); ?></td>
        <td><?= $data->discount_per; ?></td>
        <?php if($model->tax_type ==2){ ?>
        <td><?= $data->tax_per; ?></td>
        <?php } ?>
        <td style="text-align: right; padding-right: 10px;"><?= number_format($data->total, 2); ?></td>      
        </tr>  
       <?php
       $total_discount += $data->discount_amount;
       $total_amount += $data->total;
       $total_cgst += $data->cgst;
       $total_sgst += $data->sgst;
       $grand_total += $data->grand_total;
       $i++;
     }        
        ?>
        <tr>
            <th colspan="<?= $model->tax_type == 2? 7: 6 ?>" style="text-align: right; padding-right: 10px;">Sub Total</th><td style="text-align: right; padding-right: 10px;"><?= number_format($total_amount, 2); ?></td>
        </tr>
        <tr>
            <th colspan="<?= $model->tax_type == 2? 7: 6 ?>" style="text-align: right; padding-right: 10px;">Discount Amount</th><td style="text-align: right; padding-right: 10px;"><?= number_format($total_discount, 2); ?></td>
        </tr>
        <?php if($model->tax_type ==2){ ?>
        <tr>
            <th colspan="7" style="text-align: right; padding-right: 10px;">CGST (9%)</th><td style="text-align: right; padding-right: 10px;"><?= number_format($total_cgst, 2); ?></td>
        </tr>
        <tr>
            <th colspan="7" style="text-align: right; padding-right: 10px;">SGST (9%)</th><td style="text-align: right; padding-right: 10px;"><?= number_format($total_sgst, 2); ?></td>
        </tr>
        <?php } ?>
        <tr>
            <th colspan="<?= $model->tax_type == 2? 7: 6 ?>" style="text-align: right; padding-right: 10px;">Grand Total</th><td style="text-align: right; padding-right: 10px;"><?= number_format($grand_total, 2); ?></td>
        </tr>        
        <tr>
            <td colspan="<?= $model->tax_type == 2? 8: 7 ?>">
                <br/><br/><br/>
                <b>Signature</b>
            </td>
        </tr>
        </tbody>
</table>
</div>