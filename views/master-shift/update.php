<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MasterShift */

$this->title = 'Update Master Shift: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Master Shifts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="master-shift-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
