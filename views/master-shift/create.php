<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MasterShift */

$this->title = 'Create Master Shift';
$this->params['breadcrumbs'][] = ['label' => 'Master Shifts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-shift-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
