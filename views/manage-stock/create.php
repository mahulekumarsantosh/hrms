<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ManageStock */

$this->title = 'Create Manage Stock';
$this->params['breadcrumbs'][] = ['label' => 'Manage Stocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="manage-stock-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
