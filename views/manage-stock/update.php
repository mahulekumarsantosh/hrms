<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ManageStock */

$this->title = 'Update Manage Stock: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Manage Stocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="manage-stock-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
