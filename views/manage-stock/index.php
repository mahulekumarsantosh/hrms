<?php

use yii\helpers\Html;
use app\components\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ManageStockSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Inventory Day Summary Report';
$this->params['breadcrumbs'][] = $this->title;
?>
   <?= $this->render('_search', ['model' => $searchModel]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,    
        'exportTitle' => $this->title,    
        'panel' => ['type' => GridView::TYPE_SECONDARY, 'heading' => $this->title],
         'export' => ['showConfirmAlert' => false,],
        'pjax' => true,
        'toolbar' => [
        
            '{export}',
            '{perPage}',
        ],
        'options' => [
            'class' => 'grid-primary',
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],            
            [
             'attribute' => 'item_id',
             'value' => function($model){
                 return $model->item->name;
             }
            ],
            [
                'attribute' => 'date',
                'value' => function($model ) use($date){
                    return date('d-m-Y', strtotime($date));
                }
            ],
            'opening_stock',
            'in_stock',
            'out_stock',   
            [
              'label' => 'Closing Stock',
              'value' => function($model){
                  return $model->opening_stock + $model->in_stock - $model->out_stock;
              }
            ],
           
        ],
    ]); ?>
