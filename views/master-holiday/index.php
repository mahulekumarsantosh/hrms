<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use app\components\GridView;
use app\components\Encryption;
/* @var $this yii\web\View */
/* @var $searchModel app\models\MasterHolidaySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Master Holiday';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-holiday-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'exportTitle' => 'Holiday',
        'panel' => ['type' => GridView::TYPE_SECONDARY, 'heading' => $this->title],
        'export' => ['showConfirmAlert' => false,],
        'pjax' => true,
        'toolbar' => [
            Html::a('<i class="fa fa-plus"></i> Add New', ['create'], ['class' => 'btn btn-sm btn-success holiday float-right', 'title' => 'Add Holiday', 'data-pjax' => 0]),
            '{export}',
            '{perPage}',
        ],
        'options' => [
            'class' => 'grid-primary',
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'company_id',
            'name',
            'from_date',
            'to_date',
            [
                'header' => "Action",
                'headerOptions' => ['class' => "skip-export text-center", 'width' => 150],
                'format' => 'raw',
                'contentOptions' => ['class' => "skip-export text-center", 'width' => 150],
                'value' => function ($model) {
                    $list = [];
                    $list[] = Html::a('<i class="fa fa-pencil-alt" title="Update"></i> ', ['update', "id" => Encryption::Encrypt($model->id)], ['class' => 'btn btn-xs btn-info holiday', 'title' => 'Update Holiday', 'data-pjax' => 0]);
                    return implode('', $list);
                },
            ],
        ],
    ]); ?>
</div>
<?php
    $script = <<< JS
    $("body").on('click', '.holiday',function(e) {
        e.preventDefault();
        $("#holidayModalContent").load($(this).attr('href'));
        $("#holidayModalTitle").text($(this).attr('title'));
        $('#holidayModal').modal('show');
    });        
    
JS;
    $this->registerJs($script);
    echo $this->render("@app/views/layouts/models/_ajaxRenderModal", [
        'headerTitleId' => 'holidayModalTitle',
        'modalId' => "holidayModal",
        'contentId' => "holidayModalContent",
        'size' => 'lg'
    ]);

?>