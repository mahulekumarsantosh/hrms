<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MasterHoliday */

$this->title = 'Create Master Holiday';
$this->params['breadcrumbs'][] = ['label' => 'Master Holidays', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-holiday-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>