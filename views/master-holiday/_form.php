<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\MasterHoliday */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="card">

    <?php
    $form = ActiveForm::begin([
        'enableClientValidation' => false,
        'enableAjaxValidation' => true,
        'id' => 'form-add-user',
        'class' => 'form-horizontal',
        //'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-3 col-form-label',
                'wrapper' => 'col-sm-6',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]);
    ?>

    <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-12">
                <?= $form->field($model, 'from_date')->widget(DatePicker::className(), [
                    'options' => ['class' => 'form-control calculate'],
                    'dateFormat' => 'yyyy-MM-dd'
                ]) ?>
            </div>
            <div class="col-md-12">
                <?= $form->field($model, 'to_date')->widget(DatePicker::className(), [
                    'options' => ['class' => 'form-control calculate'],
                    'dateFormat' => 'yyyy-MM-dd'
                ]) ?>
            </div>
        </div>
    </div>
    <div class="card-footer">
        <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>