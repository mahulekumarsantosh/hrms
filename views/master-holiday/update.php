<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MasterHoliday */

$this->title = 'Update Master Holiday: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Master Holidays', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="master-holiday-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>