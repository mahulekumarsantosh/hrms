<?php

use yii\helpers\Html;
?>
<div class="login-box">
    <div class="row">
        <div class="col-sm-12 col-md-8 col-lg-8">
            <div class="forgot-content-bg">
            </div>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-4 justify-content-center align-self-center flex-column">
            <div class="m-4">
                            <div class="login-logo text-info text-bold">
                                Forgot Password
                            </div>
                            <?php
                            if (Yii::$app->session->hasFlash('success')) {
                                $message = Yii::$app->session->getFlash('success');
                                echo "<div class='alert alert-success'>
                                    $message
                                </div>";
                            }
                            ?>
                            <?php
                            $form = \yii\bootstrap4\ActiveForm::begin([
                                        'id' => 'form-forgot-password',
                                    ])
                            ?>

                            <?=
                                    $form->field($model, 'email', [
                                        'options' => ['class' => 'form-group has-feedback'],
                                        'inputTemplate' => '{input}<div class="input-group-append"><div class="input-group-text"><span class="fas fa-envelope"></span></div></div>',
                                        'template' => '{beginWrapper}{input}{error}{endWrapper}',
                                        'wrapperOptions' => ['class' => 'input-group mb-3']
                                    ])
                                    ->label(false)
                                    ->textInput(['placeholder' => $model->getAttributeLabel('email')])
                            ?>


                            <div class="row">
                                <div class="col-4">
                                    <?= Html::submitButton('Verify', ['class' => 'btn btn-success btn-block']) ?>
                                </div>
                                <div class="col-8">
                                    <div class="mt-2 rememberme text-right">
                                        <?= Html::a('SignIn', ['/site/login'],['class'=>'text-info text-bold']) ?>
                                    </div>
                                </div>
                            </div>

                            <?php \yii\bootstrap4\ActiveForm::end(); ?>
                            <?php //Html::a('I forgot my password', ['/site/forgot-password']) ?>
            </div>
        </div>
    </div>
</div>  
