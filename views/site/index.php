<?php

use yii\helpers\Url;
$this->title = 'Dashboard';
 
?>
<div class="card">
    <div class="card-body">
        <div class="row pt-3">
            <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
                <div class="card">
                    <div class="card-header p-3 pt-2">
                        <div
                            class=" icon-lg  bg-gradient-info shadow-dark text-center border-radius-xl mt-n4 position-absolute">
                            <i class="fas fa-tasks dashboard-icon"></i>
                        </div>
                        <div class="text-end pt-1">
                            <p class="text-sm mb-0 text-capitalize">Total Customers</p>
                            <h4 class="mb-0"><?= $customer; ?></h4>
                        </div>
                    </div>
                    <hr class="dark horizontal my-0">
                    <div class="card-footer p-3">
                    <p class="mb-0"><span class="text-success text-sm font-weight-bolder"><a class="text-success" href="<?= Url::to(['customer/index']); ?>">More Info <i class="fas fa-arrow-circle-right"></i></a> </p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
                <div class="card">
                    <div class="card-header p-3 pt-2">
                        <div
                            class="icon icon-lg icon-shape bg-gradient-danger shadow-primary text-center border-radius-xl mt-n4 position-absolute">
                            <i class="fas fa-list dashboard-icon"></i>
                        </div>
                        <div class="text-end pt-1">
                            <p class="text-sm mb-0 text-capitalize">Total Vendors</p>
                            <h4 class="mb-0"><?= $vendor; ?></h4>
                        </div>
                    </div>
                    <hr class="dark horizontal my-0">
                    <div class="card-footer p-3">
                    <p class="mb-0 "><span class="text-success text-sm font-weight-bolder"><a class="text-success" href="<?= Url::to(['customer/index']); ?>">More Info <i class="fas fa-arrow-circle-right"></i></a> </p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
                <div class="card">
                    <div class="card-header p-3 pt-2">
                        <div
                            class="icon icon-lg icon-shape bg-gradient-success shadow-success text-center border-radius-xl mt-n4 position-absolute">
                            <i class="fas fa-align-justify dashboard-icon"></i>
                        </div>
                        <div class="text-end pt-1">
                            <p class="text-sm mb-0 text-capitalize">Total Items</p>
                            <h4 class="mb-0 "><?= $items; ?></h4>
                        </div>
                    </div>
                    <hr class="dark horizontal my-0">
                    <div class="card-footer p-3">
                    <p class="mb-0 "><span class="text-success text-sm font-weight-bolder"><a class="text-success" href="<?= Url::to(['item/index']); ?>">More Info <i class="fas fa-arrow-circle-right"></i></a> </p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-sm-6">
                <div class="card">
                    <div class="card-header p-3 pt-2">
                        <div
                            class="icon icon-lg icon-shape bg-gradient-warning shadow-info text-center border-radius-xl mt-n4 position-absolute">
                            <i class="fas fa-list dashboard-icon"></i>
                        </div>
                        <div class="text-end pt-1">
                            <p class="text-sm mb-0 text-capitalize">Total Brands</p>
                            <h4 class="mb-0"><?= $brands; ?></h4>
                        </div>
                    </div>
                    <hr class="dark horizontal my-0">
                    <div class="card-footer p-3">
                        <p class="mb-0 "><span class="text-success text-sm font-weight-bolder "><a class="text-success" href="<?= Url::to(['brands/index']); ?>">More Info <i class="fas fa-arrow-circle-right"></i></a> </p>
                    </div>
                </div>
            </div>
        </div>        
    </div>
    <div class="card-body">
        <h3>Stock current Value: <?= $stock_current_value; ?> </h3>
    </div>

    <?php 
    
    if(!empty($stockdata)) { ?>
    <div class="card-body">
        <div class="row">
            <div class="col-md-12" style="height: 500px">
                <div class="card card-outline card-info">  
                    <div class="card-header"><h4>Today's Available Stock</h4></div>                 
                    <div class="card-body">
                        <div id="container"></div>
                    </div>
                </div>
            </div>            
        </div>
    </div>   
    <?php } ?> 
</div>
<?php
    //  $this->registerJsFile("@web/css/highchart/highcharts.js",['depends' => [\yii\web\JqueryAsset::className() ]]);
    //  $this->registerJsFile("@web/css/highchart/exporting.js",['depends' => [\yii\web\JqueryAsset::className() ]]);
    //  $this->registerJsFile("@web/css/highchart/export-data.js",['depends' => [\yii\web\JqueryAsset::className() ]]);
    //  $this->registerJsFile("@web/css/highchart/accessibility.js",['depends' => [\yii\web\JqueryAsset::className() ]]);
?>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>

<script>
    Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
   
    xAxis: {
        type: 'category',
        labels: {
            rotation: -45,
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Total Stock'
        }
    },
    legend: {
        enabled: false
    },
    tooltip: {
        pointFormat: 'Total Stock: <b>{point.y} </b>'
    },
    credits: {
    enabled: false
   },
    series: [{
        name: 'Population',
        data: <?= $stockdata; ?>,
        dataLabels: {
            enabled: true,
            //rotation: -90,
            color: '#000000',
            align: 'center',
           
            y: 10, // 10 pixels down from the top
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    }]
});
</script>