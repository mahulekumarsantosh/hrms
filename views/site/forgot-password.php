<?php

use yii\helpers\Html;
?>
<div class="container-fluid">
    <div class="row no-gutter">
    <div class="d-none d-md-flex col-md-8 col-lg-9 bg-image no-padding"></div>
        <div class="col-md-4 col-lg-3 no-padding">
            <div class="login d-flex align-items-center py-5">
                <div class="container">
                    <div class="row">
                        <div class="col-md-10 col-lg-10 mx-auto">
                            <!--            <div class="card">
                                <div class="card-body login-card-body">-->
                                <div class="login-logo">
                                Forgot Password
                                <span></span>
                                </div>

                            <?php
                            $form = \yii\bootstrap4\ActiveForm::begin([
                                        'id' => 'form-forgot-password',
                                    ])
                            ?>

                            <?=
                                    $form->field($model, 'mobile', [
                                        'options' => ['class' => 'form-group has-feedback'],
                                        'inputTemplate' => '{input}<div class="input-group-append"><div class="input-group-text"><span class="fas fa-envelope"></span></div></div>',
                                        'template' => '{beginWrapper}{input}{error}{endWrapper}',
                                        'wrapperOptions' => ['class' => 'input-group mb-3']
                                    ])
                                    ->label(false)
                                    ->textInput(['placeholder' => $model->getAttributeLabel('mobile')])
                            ?>


                            <div class="row">
                                <div class="col-4">
                                    <?= Html::submitButton('Send OTP', ['class' => 'btn btn-default btn-block']) ?>
                                </div>
                                <div class="col-8">
                                <div class="mt-2 rememberme text-right">
                                <?= Html::a('SignIn', ['/site/login']) ?>
                                </div>
                                </div>
                            </div>

                            <?php \yii\bootstrap4\ActiveForm::end(); ?>

                            <!--        <div class="social-auth-links text-center mb-3">
            <p>- OR -</p>
            <a href="#" class="btn btn-block btn-primary">
                <i class="fab fa-facebook mr-2"></i> Sign in using Facebook
            </a>
            <a href="#" class="btn btn-block btn-danger">
                <i class="fab fa-google-plus mr-2"></i> Sign in using Google+
            </a>
        </div>-->
                            <!-- /.social-auth-links -->

<!--                            <p class="mb-1">
                                <?= Html::a('I forgot my password', ['/site/forgot-password']) ?>
                                <a href="forgot-password.html">I forgot my password</a>
                            </p>-->
                    <!--        <p class="mb-0">
                                <a href="register.html" class="text-center">Register a new membership</a>
                            </p>-->
                            <!--</div>-->
                            <!-- /.login-card-body -->
                            <!--</div>-->

                            <!--        </div>
                                </div>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>

