<?php

use yii\helpers\Html;
?>

<div class="login-box">
    <div class="row">
        <div class="col-sm-12 col-md-8 col-lg-8">
            <div class="login-content-bg">
            </div>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-4 justify-content-center align-self-center flex-column">
            <div class="m-4">
                            <!--            <div class="card">
                                <div class="card-body login-card-body">-->
                            <div class="login-logo">
                                Reset Password
                                <span>Update your password before login </span>
                            </div>

                            <?php
                            if (Yii::$app->session->hasFlash('error')) {
                                $message = Yii::$app->session->getFlash('error');
                                echo "<div class='alert alert-warning'>
                                    $message
                                </div>";
                            }
                            ?>
                            <?php
                            $form = \yii\bootstrap4\ActiveForm::begin([
                                        'id' => 'form-forgot-password',
                                    ])
                            ?>

                            <?=
                                    $form->field($model, 'password', [
                                        'options' => ['class' => 'form-group has-feedback'],
                                        'inputTemplate' => '{input}<div class="input-group-append"><div class="input-group-text"><span class="fas fa-lock"></span></div></div>',
                                        'template' => '{beginWrapper}{input}{error}{endWrapper}',
                                        'wrapperOptions' => ['class' => 'input-group mb-3']
                                    ])
                                    ->label(false)
                                    ->passwordInput(['placeholder' => "Password"])
                            ?>

                            <?=
                                    $form->field($model, 'cpassword', [
                                        'options' => ['class' => 'form-group has-feedback'],
                                        'inputTemplate' => '{input}<div class="input-group-append"><div class="input-group-text"><span class="fas fa-lock"></span></div></div>',
                                        'template' => '{beginWrapper}{input}{error}{endWrapper}',
                                        'wrapperOptions' => ['class' => 'input-group mb-3']
                                    ])
                                    ->label(false)
                                    ->passwordInput(['placeholder' => "Confirm Password"])
                            ?>

                            <div class="row">

                                <div class="col-4">
                                    <?= Html::submitButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
                                </div>
                                <div class="col-8">
                                <div class="mt-2 text-info text-bold text-right">
                                <?= Html::a('SignIn', ['/site/login']) ?>
                                </div>
                                </div>
                            </div>

                            <?php \yii\bootstrap4\ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>  
