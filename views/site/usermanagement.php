<div class="callout callout-info">
    Page Under ims
</div>
<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'User Management';
$this->params['breadcrumbs'][] = $this->title;
?>
<!--div class="callout callout-info">
    Page Under ims
</div-->
<div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Responsive Hover Table</h3>

                <div class="card-tools">
                <button type="button" class="btn btn-block btn-primary"><i class="nav-icon fas fa-plus"></i> Add New</button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-striped table-bordered">
                  <thead>
                    <tr>
                     <th>Username</th>
                      <th>First Name</th>
                      <th>Email</th>
                      <th>Mobile No.</th>
                      <th>Status</th>
                      <th>Created At</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                    <td>samay</td>
                    <td>Samay</td>
                    <td>samay.skc@gmail.com</td>
                    <td width="100">7999251596</td>
                    <td>Active</td>
                    <td width="100">07-04-2020</td>
                    <td>Active</td>
                    
                    </tr>
                    <tr>
                    <td>manoj</td>
                    <td>Manoj</td>
                    <td>manojsahu85@gmail.com</td>
                    <td width="100">9200006040</td>
                    <td>Active</td><td width="100">20-05-2020</td>
                    <td class="text-center" width="80"><a href=""><i class="fa fa-pencil" title="Update"></i> </a></td>
                    </tr>
                    
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>