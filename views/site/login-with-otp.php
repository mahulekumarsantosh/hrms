<?php

use yii\helpers\Html;

$this->title = 'Login';
?>
<div class="login-box">
    <div class="row">
        <div class="col-sm-12 col-md-8 col-lg-8">
            <div class="login-content-bg">
            </div>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-4 justify-content-center align-self-center flex-column">
            <div class="m-4">
                <div class="login-logo text-info text-bold">
                    Login
                    <span class="text-dark">Sign in to start your session</span>
                </div>

                <?php $form = \yii\bootstrap4\ActiveForm::begin(['id' => 'login-form']) ?>

                <?=
                        $form->field($model, 'username', [
                            'options' => ['class' => 'form-group has-feedback'],
                            'inputTemplate' => '{input}<div class="input-group-append"><div class="input-group-text"><span class="fas fa-user"></span></div></div>',
                            'template' => '{beginWrapper}{input}{error}{endWrapper}',
                            'wrapperOptions' => ['class' => 'input-group mb-3']
                        ])
                        ->label(false)
                        ->textInput(['placeholder' => $model->getAttributeLabel('username')])
                ?>
                <div class="row">
                    <div class="col-8">
                        <div class="mb-1 rememberme">
                            <?= Html::a('I forgot my password', ['/site/email-reset-password'], ['class' => 'text-info']) ?>
                        </div>
                        <div class="mb-1 rememberme">
                            <?= Html::a('Login with Password', ['/site/login'], ['class' => 'text-info']) ?>
                        </div>
                    </div>
                    <div class="col-4">
                        <?= Html::submitButton('Sign In', ['class' => 'btn btn-success btn-block']) ?>
                    </div>
                </div>
                <?php \yii\bootstrap4\ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>  

