<?php
/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
$errorCode = 404;
if (preg_match('/\(#(.*?)\)/', $name, $match) == 1) {
    $errorCode = $match[1];
}
$this->params['breadcrumbs'] = [['label' => $this->title]];
?>
<div class="container-fluid">
    <div class="bg-white text-center error-box">
        <h2>We are sorry,</h2>
        <p class="text-gray h2"><?= Html::encode($message) ?></p>
        <!--<p class="text-gray">but the page you were looking for can't be found.</p>-->
        <div class="error-code">
            <?php echo $errorCode ?>
            <!--4<span class="text-primary">0</span>4-->
        </div>
        <?= Html::a("Go Home", ['/'], ['class' => "btn btn-lg btn-warning "]) ?>

    </div>
</div>