<?php

use yii\helpers\Html;

$this->title = 'Login';
?>
<div class="login-box">
    <div class="row">
        <div class="col-sm-12 col-md-8 col-lg-8">
            <div class="login-content-bg">
            </div>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-4 justify-content-center align-self-center flex-column">
            <div class="m-4">
                <div class="login-logo text-info text-bold">
                    Login OTP Verification
                    <span class="text-dark">Validate OTP to start your session</span>
                </div>

                <?php $form = \yii\bootstrap4\ActiveForm::begin(['id' => 'form-otp']) ?>

                <?=
                        $form->field($model, 'otp', [
                            'options' => ['class' => 'form-group has-feedback'],
                            'inputTemplate' => '{input}<div class="input-group-append"><div class="input-group-text"><span class="fas fa-lock"></span></div></div>',
                            'template' => '{beginWrapper}{input}{error}{endWrapper}',
                            'wrapperOptions' => ['class' => 'input-group mb-3']
                        ])
                        ->label(false)
                        ->textInput(['placeholder' => $model->getAttributeLabel('otp')])
                ?>


                <div class="row">
                    <div class="col-4">
                         <?= Html::submitButton('Verify', ['class' => 'btn btn-success btn-block']) ?>
                       
                    </div>
                    <div class="col-8 mt-2 text-info text-bold text-right">
                        <?= Html::a('Sign in', ['/site/login'], ['class' => 'text-info']) ?>
                    </div>
                </div>

                <?php \yii\bootstrap4\ActiveForm::end(); ?>


                <div class="mb-1 rememberme">
                    
                </div>
            </div>
        </div>
    </div>
</div>  

