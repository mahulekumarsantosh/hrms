<?php

use yii\helpers\Html;
$this->title = 'Login';
?>

<div class="login-box">
    <div class="row">       
        <div class="col-sm-12 col-md-12 col-lg-12 justify-content-center align-self-center flex-column">
            <div class="text-center pt-4">
                <i class="fas fa-user-circle text-success" style="font-size: 80px;"></i>
            </div>            
            <div class="m-4">
                <div class="login-logo text-info text-bold">
                    Login
                    <span class="text-dark">Sign in to start your session</span>
                </div>

                <?php $form = \yii\bootstrap4\ActiveForm::begin(['id' => 'login-form']) ?>

                <?=
                        $form->field($model, 'username', [
                            'options' => ['class' => 'form-group has-feedback'],
                            'inputTemplate' => '{input}<div class="input-group-append"><div class="input-group-text"><span class="fas fa-user"></span></div></div>',
                            'template' => '{beginWrapper}{input}{error}{endWrapper}',
                            'wrapperOptions' => ['class' => 'input-group mb-3']
                        ])
                        ->label(false)
                        ->textInput(['placeholder' => $model->getAttributeLabel('username')])
                ?>

                <?=
                        $form->field($model, 'password', [
                            'options' => ['class' => 'form-group has-feedback'],
                            'inputTemplate' => '{input}<div class="input-group-append"><div class="input-group-text"><span class="fas fa-lock"></span></div></div>',
                            'template' => '{beginWrapper}{input}{error}{endWrapper}',
                            'wrapperOptions' => ['class' => 'input-group mb-3']
                        ])
                        ->label(false)
                        ->passwordInput(['placeholder' => $model->getAttributeLabel('password')])
                ?>

                <div class="row">
                    <div class="col-8">
                        <?=
                        $form->field($model, 'rememberMe')->checkbox([
                            'template' => '<div class="icheck-primary">{input}{label}</div>',
                            'labelOptions' => [
                                'class' => 'rememberme text-info'
                            ],
                            'uncheck' => null
                        ])
                        ?>
                    </div>
                    <div class="col-4">
<?= Html::submitButton('Sign In', ['class' => 'btn btn-success btn-block']) ?>
                    </div>
                </div>

<?php \yii\bootstrap4\ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>  

