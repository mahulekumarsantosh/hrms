<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\QuotationDetailSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Quotation Details';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="quotation-detail-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Quotation Detail', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'exportTitle' => $this->title,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'quotation_id',
            'item_id',
            'quantity',
            'price',
            //'total',
            //'discount_per',
            //'discount_amount',
            //'tax_per',
            //'cgst',
            //'sgst',
            //'igst',
            //'grand_total',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
