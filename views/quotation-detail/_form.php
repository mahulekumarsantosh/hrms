<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\QuotationDetail */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="quotation-detail-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'quotation_id')->textInput() ?>

    <?= $form->field($model, 'item_id')->textInput() ?>

    <?= $form->field($model, 'quantity')->textInput() ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'total')->textInput() ?>

    <?= $form->field($model, 'discount_per')->textInput() ?>

    <?= $form->field($model, 'discount_amount')->textInput() ?>

    <?= $form->field($model, 'tax_per')->textInput() ?>

    <?= $form->field($model, 'cgst')->textInput() ?>

    <?= $form->field($model, 'sgst')->textInput() ?>

    <?= $form->field($model, 'igst')->textInput() ?>

    <?= $form->field($model, 'grand_total')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
