<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EmployeePfDetails */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="employee-pf-details-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'employee_id')->textInput() ?>

    <?= $form->field($model, 'is_pf')->dropDownList([ '0', '1', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'uan_no')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_esic')->dropDownList([ '0', '1', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'ip_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_lic')->dropDownList([ '0', '1', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'policy_no')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_insurance')->dropDownList([ '0', '1', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'insurance_no')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'updated_by')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
