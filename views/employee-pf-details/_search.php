<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EmployeePfDetailsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="employee-pf-details-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'employee_id') ?>

    <?= $form->field($model, 'is_pf') ?>

    <?= $form->field($model, 'uan_no') ?>

    <?= $form->field($model, 'is_esic') ?>

    <?php // echo $form->field($model, 'ip_number') ?>

    <?php // echo $form->field($model, 'is_lic') ?>

    <?php // echo $form->field($model, 'policy_no') ?>

    <?php // echo $form->field($model, 'is_insurance') ?>

    <?php // echo $form->field($model, 'insurance_no') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
