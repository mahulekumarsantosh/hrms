<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EmployeePfDetails */

$this->title = 'Create Employee Pf Details';
$this->params['breadcrumbs'][] = ['label' => 'Employee Pf Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employee-pf-details-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
