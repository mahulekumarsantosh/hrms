<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EmployeePfDetailsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Employee Pf Details';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employee-pf-details-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Employee Pf Details', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'employee_id',
            'is_pf',
            'uan_no',
            'is_esic',
            //'ip_number',
            //'is_lic',
            //'policy_no',
            //'is_insurance',
            //'insurance_no',
            //'created_at',
            //'created_by',
            //'updated_at',
            //'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
