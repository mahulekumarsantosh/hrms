<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use app\models\MasterDepartment;
use app\models\MasterDesignation;
use app\models\MasterShift;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model app\models\EmployeeOfficialDetails */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="card">

    <?php
    $form = ActiveForm::begin([
        'enableClientValidation' => false,
        'enableAjaxValidation' => true,
        'id' => 'form-add-user',
        'class' => 'form-horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}"
        ],
    ]);
    ?>
    <div class="card-body">
        <div class="row">
            <div class="col-md-4">
            <?= $form->field($model, "department_id")->widget(Select2::className(), [
                        'data' => ArrayHelper::map(MasterDepartment::find()->where(['company_id' => Yii::$app->user->identity->company_id])->all(), 'id', 'name'),
                        'options' => [ 'placeholder' => 'Select Department'],            
                ]) ?>
            </div>
            <div class="col-md-4">
            <?= $form->field($model, "designation_id")->widget(Select2::className(), [
                        'data' => ArrayHelper::map(MasterDesignation::find()->where(['company_id' => Yii::$app->user->identity->company_id])->all(), 'id', 'name'),
                        'options' => [ 'placeholder' => 'Select Designation'],            
                ]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'basic_salary')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
            <?= $form->field($model, 'doj')->widget(DatePicker::className(), [
                'options' => ['class' => 'form-control', 'changeYear'=> true,],
                'dateFormat' => 'dd-MM-yyyy',
                'clientOptions' => [
                    'maxDate' => date('d-m-Y'),
                    'changeMonth'=> true,
                    'changeYear'=> true,
                    'yearRange' => '1970:2030',
                ]              
            ]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'security_deposit')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, "shift")->widget(Select2::className(), [
                        'data' => ArrayHelper::map(MasterShift::find()->where(['company_id' => Yii::$app->user->identity->company_id])->all(), 'company_shift_id', 'shift_name'),
                        'options' => [ 'placeholder' => 'Select Department'],            
                ]) ?>
            </div>
        </div>
    </div>
    <div class="card-footer">
        <?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>