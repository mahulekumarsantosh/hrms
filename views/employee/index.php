<?php

use yii\helpers\Html;
use app\components\GridView;
use app\components\Encryption;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EmployeeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Employees';
$this->params['breadcrumbs'][] = $this->title;
?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,        
        'exportTitle' => $this->title,
        'panel' => ['type' => GridView::TYPE_SECONDARY, 'heading' => $this->title],
        'export' => ['showConfirmAlert' => false,],
        'pjax' => true,
        'toolbar' => [
            Html::a('<i class="fa fa-plus"></i> Add New', ['create'], ['class' => 'btn btn-sm btn-success employee float-right', 'title' => 'Add Employee',  'data-pjax' => 0]),
            '{export}',
            '{perPage}',
        ],
        'options' => [
            'class' => 'grid-primary',
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'first_name',
                'label' => 'EMP Name',
                'value' => function($model){
                    return $model->first_name." ".$model->middle_name." ".$model->last_name;
                }
            ],            
            'employee_code',
            'mobile',            
            'email',
            //'gender',
            [
                'label' => 'DOB',
                'contentOptions' => ['width' => 100],
                 'value' => function($model){
                    return date('d-m-Y', strtotime($model->dob));
                 }
            ],            
            //'state',
            //'district',
            //'city',
            //'address:ntext',
            //'pincode',
            //'profile_photo',
            //'company_id',            
            'aadhar_no',
            'pan_no',
            //'created_at',
            //'created_by',
            //'updated_at',
            //'updated_by',

            [
                'header' => "Action",
                'headerOptions' => ['class' => "skip-export text-center", 'width' => 150],
                'format' => 'raw',
                'contentOptions' => ['class' => "skip-export text-center", 'width' => 150],
                'value' => function($model) {
                    $list = [];   
                    $list[] = Html::a('<i class="fa fa-pencil-alt" title="Update"></i> ', ['update', "id" => Encryption::Encrypt($model->id)], ['class' => 'btn btn-xs btn-info', 'title' => 'Update Employee', 'data-pjax' => 0]);
                    return implode('', $list);
                },
            ],
        ],
    ]); ?>

</div>
<?php
$script = <<< JS
    $("body").on('click', '.employee',function(e) {
        e.preventDefault();
        $("#employeeModalContent").load($(this).attr('href'));
        $("#employeeModalTitle").text($(this).attr('title'));
        $('#employeeModal').modal('show');
    });        
    
JS;
$this->registerJs($script);
echo $this->render("@app/views/layouts/models/_ajaxRenderModal", [
    'headerTitleId' => 'employeeModalTitle',
    'modalId' => "employeeModal",
    'contentId' => "employeeModalContent",
    'size' => 'xl'
]);

?>

