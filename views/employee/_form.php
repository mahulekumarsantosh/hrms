<?php

use app\models\MasterDistrict;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;
use kartik\select2\Select2;
use app\models\MasterState;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Employee */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="card">
    <?php
    $form = ActiveForm::begin([
        'enableClientValidation' => false,
        'enableAjaxValidation' => true,
        'id' => 'form-add-user',
        'class' => 'form-horizontal',
        //'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-3 col-form-label',
                'wrapper' => 'col-sm-6',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]);
    ?>
    <div class="card-body">
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'first_name')->textInput(['maxlength' => 20]) ?>
            </div>
            <div class="col-md-4">
            <?= $form->field($model, 'middle_name')->textInput(['maxlength' => 20]) ?>
            </div>
            <div class="col-md-4">
            <?= $form->field($model, 'last_name')->textInput(['maxlength' => 20]) ?>
            </div>            
            <div class="col-md-4">
            <?= $form->field($model, 'mobile')->textInput(['maxlength' => 10]) ?>
            </div>
            <div class="col-md-4">
            <?= $form->field($model, 'alternate_mobile')->textInput(['maxlength' => 10]) ?>
            </div>
            <div class="col-md-4">
            <?= $form->field($model, 'email')->textInput(['maxlength' => 50]) ?>
            </div>
            <div class="col-md-4">
            <?= $form->field($model, 'employee_code')->textInput(['maxlength' => 10]) ?>
            </div>
            <div class="col-md-4">
            <?= $form->field($model, "gender")->widget(Select2::className(), [
                        'data' => Yii::$app->params['gender'],
                        'options' => [ 'placeholder' => 'Select Gender'],            
                ]) ?>
            <?php // $form->field($model, 'gender')->inline()->radioList(Yii::$app->params['gender']); ?>
            </div>
            <div class="col-md-4">
            <?= $form->field($model, 'dob')->widget(DatePicker::className(), [
                'options' => ['class' => 'calculate form-control'],
                'dateFormat' => 'dd-MM-yyyy',
                'clientOptions' => [
                    'maxDate' => date('d-m-Y', strtotime('-18 year')),
                    'changeMonth'=> true,
                    'changeYear'=> true,
                    'yearRange' => '1970:2030',
                ]
            ]) ?>
            </div>
            <div class="col-md-4">
            <?= $form->field($model, "state")->widget(Select2::className(), [
                        'data' => ArrayHelper::map(MasterState::find()->all(), 'id', 'name'),
                        'options' => [ 'placeholder' => 'Select State'],            
                ]) ?>
            </div>
            <div class="col-md-4">
            <?= $form->field($model, "district")->widget(Select2::className(), [
                        'data' => $model->isNewRecord ? [] : ArrayHelper::map(MasterDistrict::find()->where(['state_id' => $model->state])->all(), 'id', 'name'),
                        'options' => [ 'placeholder' => 'Select District'],            
                ]) ?>
            </div>
            <div class="col-md-4">
            <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
            <?= $form->field($model, 'address')->textInput() ?>
            </div>
            <div class="col-md-4">
            <?= $form->field($model, 'pincode')->textInput(['maxlength' => 6]) ?>
            </div>
            <!-- <div class="col-md-4">
            <?php // $form->field($model, 'profile_photo')->textInput(['maxlength' => true]) ?>
            </div>             -->
            <div class="col-md-4">
            <?= $form->field($model, 'aadhar_no')->textInput(['maxlength' => 12]) ?>
            </div>
            <div class="col-md-4">
            <?= $form->field($model, 'pan_no')->textInput(['maxlength' => 10, 'class' => 'form-control input-uppercase']) ?>
            </div>
          
        </div>
    </div>
    <div class="card-footer">
        <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>

<?php
$getDistrict = Url::to(['employee/get-district']);

$script = <<< JS
    
    $('.input-uppercase').keyup(function () {        
        this.value = this.value.toUpperCase();
    });

    $('#employee-state').change(function(){
       var stateId = $(this).val();
       $.post('$getDistrict', {stateId:stateId}, function(data){
            $('#employee-district').html(data);
       });
    });
JS;
$this->registerJs($script);
?>