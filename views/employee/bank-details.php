<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EmployeeBankDetails */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="card">

    <?php
    $form = ActiveForm::begin([
        'enableClientValidation' => false,
        'enableAjaxValidation' => true,
        'id' => 'form-add-user',
        'class' => 'form-horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        ],
    ]);
    ?>
    <div class="card-body">
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'account_number')->textInput(['maxlength' => 16]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'bank_name')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'ifsc_code')->textInput(['maxlength' => 11, 'class' => 'form-control input-uppercase']) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'bank_code')->textInput(['maxlength' => true, 'class' => 'form-control input-uppercase']) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'bank_address')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
    </div>

    <div class="card-footer">
        <?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-default']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?php
$script = <<< JS
    
    $('.input-uppercase').keyup(function () {        
        this.value = this.value.toUpperCase();
    });
JS;
$this->registerJs($script);
?>