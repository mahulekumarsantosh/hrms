<?php

use yii\helpers\Url;
use app\components\Encryption;

/* @var $this yii\web\View */
/* @var $model app\models\User */
extract($data);
$this->title = 'Update Employee Details';
$action = Yii::$app->controller->action->id;
$empid = Yii::$app->session->get('empid');
?>
<div class="user-update">
    <div class="row">
        <!-- /.col -->
        <div class="col-md-12">
            <div class="card card-primary card-outline card-outline-tabs">
                <div class="card-header p-0 border-bottom-0">
                    <ul class="nav nav-tabs">
                        <li class="nav-item"><a class="nav-link <?= $action == 'update' ? 'active' : '' ?>"  href="<?= Url::to(['update', 'id' => Encryption::Encrypt($empid)]) ?>">Basic Details</a></li></li>
                        <li class="nav-item"><a class="nav-link <?= $action == 'official-details' ? 'active' : '' ?>"  href="<?= Url::to(['official-details', 'id' => Encryption::Encrypt($empid)]) ?>">Official Details</a></li>
                        <li class="nav-item"><a class="nav-link <?= $action == 'bank-details' ? 'active' : '' ?>"  href="<?= Url::to(['bank-details', 'id' => Encryption::Encrypt($empid)]) ?>">Bank Details </a></li>                        
                        <li class="nav-item"><a class="nav-link <?= $action == 'other-details' ? 'active' : '' ?>"  href="<?= Url::to(['other-details', 'id' => Encryption::Encrypt($empid)]) ?>">Other Details</a></li>                        
                    </ul>
                </div>
                <div class="card-body">
                    <div class="tab-content py-3 ">
                        <?= $this->render("$action", $data) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
