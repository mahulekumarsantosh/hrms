<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EmployeePfDetails */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="card">

    <?php
    $form = ActiveForm::begin([
        'enableClientValidation' => false,
        'enableAjaxValidation' => true,
        'id' => 'form-add-user',
        'class' => 'form-horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}"
        ],
    ]);
    ?>
    <div class="card-body">
        <div class="row">
            <div class="col-md-3">
                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, "is_pf")->checkbox(['value' => "1"]); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'uan_no')->textInput(['maxlength' => true, 'placeholder' => 'UAN No.', 'style' => ['display' => $model->is_pf ? '' : 'none']])->label(false) ?>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, "is_esic")->checkbox(['value' => "1"]); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'ip_number')->textInput(['maxlength' => 12, 'placeholder' => 'IP No.', 'style' => ['display' => $model->is_esic ? '' : 'none']])->label(false) ?>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, "is_lic")->checkbox(['value' => "1"]); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'policy_no')->textInput(['maxlength' => true, 'placeholder' => 'Policy No.', 'style' => ['display' => $model->is_lic ? '' : 'none']])->label(false) ?>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, "is_insurance")->checkbox(['value' => "1"]); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'insurance_no')->textInput(['maxlength' => true, 'placeholder' => 'Insurance No.', 'style' => ['display' => $model->is_insurance ? '' : 'none']])->label(false) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card-footer">
        <?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<< JS
    $('#employeepfdetails-is_pf').click(function(){
        if($(this).is(':checked')) {        
             $("#employeepfdetails-uan_no").show();
        } else {
            $("#employeepfdetails-uan_no").hide();
            $("#employeepfdetails-uan_no").val('');
        }
    });

    $('#employeepfdetails-is_esic').click(function(){
        if($(this).is(':checked')) {        
             $("#employeepfdetails-ip_number").show();
        } else {
            $("#employeepfdetails-ip_number").hide();
            $("#employeepfdetails-ip_number").val('');
        }
    });

    $('#employeepfdetails-is_lic').click(function(){
        if($(this).is(':checked')) {        
             $("#employeepfdetails-policy_no").show();
        } else {
            $("#employeepfdetails-policy_no").hide();
            $("#employeepfdetails-policy_no").val('');
        }
    });

    $('#employeepfdetails-is_insurance').click(function(){
        if($(this).is(':checked')) {        
             $("#employeepfdetails-insurance_no").show();
        } else {
            $("#employeepfdetails-insurance_no").hide();
            $("#employeepfdetails-insurance_no").val('');
        }
    });
    
JS;
$this->registerJs($script);
?>