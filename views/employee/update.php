<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Employee */

$this->title = 'Update Employee: ' . $model->first_name;
$this->params['breadcrumbs'][] = ['label' => 'Employees', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->first_name];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="employee-update">   

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
