<?php

use yii\helpers\Html;
use app\components\GridView;
use app\components\Encryption;
use kartik\date\DatePicker;
use app\components\TimeHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    #w0
    {
        width:100px;
        border: 1px solid #d2d6de !important;
    }
</style>
<?=

GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'exportTitle' => $this->title,
    'panel' => ['type' => GridView::TYPE_DEFAULT, 'heading' => $this->title],
    'export' => ['showConfirmAlert' => false,],
    'pjax' => true,
    'toolbar' => [
        Html::a('<i class="fa fa-plus"></i> Add New', ['create'], ['class' => 'btn btn-sm btn-primary', 'data-pjax' => 0]),
        '{export}',
        '{perPage}',
    ],
    'options' => [
        'class' => 'grid-primary',
    ],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn',
            'headerOptions' => ['width' => 50],
        ],
//                'id',
        //  'username',
        [
            "attribute" => 'username',
//                    "format" => 'raw',
//                    "value" => function($model) {
//                        return Html::a($model->username, ['/manage/user/view', 'id' => $model->id]);
//                    }
        ],
        'fname',
        'email',
        [
            'attribute' => 'mobile',
            'contentOptions' => ['width' => 100],
        ],
        //'email:email',
        //  'status',
        [
            "attribute" => 'status',
            "filter" => [10 => "Active", 0 => "Deactive"],
            "headerOptions" => ["width" => 50],
            "format" => 'raw',
            "value" => function($model) {
                return ($model->status == '10') ? "Active" : "Deactive";
            }
        ],
        //'fname',
        //'lname',
        //'mobile',               
        [
            'attribute' => 'created_at',
            'contentOptions' => ['width' => 100],
            'format' => ['date', 'php:d-m-Y'],
            'filter' => \yii\jui\DatePicker::widget([
                'name' => $searchModel->formName() . '[created_at]',
                'attribute' => 'dateFrom',
                'value' => TimeHelper::DateFilter($searchModel->created_at, "d-m-Y"),
                'options' => [],
                'clientOptions' => [
                    'changeMonth' => true,
                    'changeYear' => true,
                    'autoSize' => true,
                ]
            ]),
        /* 'filter' => DatePicker::widget([
          'name' => $searchModel->formName() . '[created_at]',
          'value' => TimeHelper::DateFilter($searchModel->created_at, "d-m-Y"),
          'options' => [],
          'layout' => "{input}{remove}",
          'pluginOptions' => [
          'format' => 'dd-mm-yyyy',
          'todayHighlight' => true,
          ]
          ]), */
        ],
        //'updated_at',
        //'verification_token',
        // ['class' => 'yii\grid\ActionColumn'],
        [
            'header' => "Action",
            'headerOptions' => ['class' => "skip-export text-center", 'width' => 150],
            'format' => 'raw',
            'contentOptions' => ['class' => "skip-export text-center", 'width' => 150],
            'value' => function($model) {
                $list = [];
//                        \Yii::$app->user->can('View User Detail') ? $list[] = Html::a('<i class="fa fa-eye" title="View"></i> ', ['view', "id" => Encryption::Encrypt($model->id)]) : null;
                 $list[] = Html::a('<i class="fa fa-pencil-alt" title="Update"></i> ', ['update', "id" => Encryption::Encrypt($model->id)], ['class' => 'btn btn-xs btn-info', 'data-pjax' => 0]);
//                        $list[] = Html::a('<i class="fa fa-cog" title="Change Password"></i> ', ['change-password', "id" => $model->id], ['data-method' => "post"]);
//                        \Yii::$app->user->can('Delete User') ? $list[] = Html::a('<i class="fa fa-trash" title="Delete"></i> ', ['delete', "id" => Encryption::Encrypt($model->id)],['data-confirm'=>"Are You Sure to Delete this Record.?",'data-method'=>"POST"]) : null;
                return implode('', $list);
            },
        ],
    ],
]);
?>