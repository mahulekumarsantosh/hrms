<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\MasterProject;
use bogdik\wizardwidget\WizardWidget;
use app\models\Company;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */

$profileImage = Yii::getAlias('@web') . "/upload/image/profile.jpeg";
//$permission = array_chunk($permission, 3);
//print "<pre>";
//print_r($permission);
//die;
$data = array(
    'Mazda' => array(
        'app' => 'app1',
        'name' => 'Application',
    ),
    'Volvo' => array(
        'volvo-b9tl' => 'B9TL',
        'volvo-l90e-radlader' => 'L90E Radlader',
    ),
);
?>
<style>
    .project-permission {
        font-weight: normal;
        width:30%;
    }
    .permission {
        font-weight: normal;
        width:100%;
    }

    .fileImageContainer{
        height:200px;
        width:200px;
        border: 3px solid #fff;
        -webkit-box-shadow: 0 0 13px 0 rgba(0,0,0,.1);
        box-shadow: 0 0 13px 0 rgba(0,0,0,.1);
        position: relative;
        background-image: url('<?= $profileImage ?>');
        border-radius: 50%;
        background-repeat: no-repeat;
        background-size: cover;
    }

    .fileImageContainer > span{
        justify-content: center;
        cursor: pointer;
        background-color: #fff;
        -webkit-box-shadow: 0 0 13px 0 rgba(0,0,0,.1);
        box-shadow: 0 0 13px 0 rgba(0,0,0,.1);
        -webkit-transition: all .3s;
        transition: all .3s;
        padding: 2px;
        width: 20px;
        height: 20px;
        text-align: center;
        border-radius: 50%;
        position: absolute;
        /*        right: -8px;
                top: -8px;*/
        right: 20%;
        top: 0%;
    }
    #user-profile_file {
        width: 0!important;
        height: 0!important;
        overflow: hidden;
        opacity: 0;
    }
    hr{
        border: 1px solid #f4f4f4;
    }
    /*    .masonry-column {
            padding: 0 1px;
        }
    
        .masonry-grid > div .thumbnail {
            margin: 5px 1px;
        }*/

</style>
<?php
$form = ActiveForm::begin([
            'enableClientValidation' => false,
            'enableAjaxValidation' => true,
            'id' => 'form-add-user',
            'class' => 'form-horizontal',
            'layout' => 'horizontal',
            'fieldConfig' => [
                'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                'horizontalCssClasses' => [
                    'label' => 'col-sm-3 col-form-label',
                    'wrapper' => 'col-sm-6',
                    'error' => '',
                    'hint' => '',
                ],
            ],
        ]);
?>
<div class="card">
    <div class="card-header">
        <h3 class="card-title"><?= Html::encode($this->title) ?></h3>
        <div class="card-tools">
            <?= Html::a('<i class="fa fa-arrow-left"></i> Back', ['/manage/user'], ['class' => 'btn btn-primary btn-sm']) ?>
        </div>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-12 pb-3">
                <?=
                $form->field($model, 'profile_file', [
                    'template' => '{label} <div class="col-sm-2 offset-md-5">'
                    . '<div class="fileImageContainer"><span class="fa fa-pencil-alt"></span><div id="fileImage"></div></div>{input}{error}{hint}</div>'
                ])->fileInput()->label(false);
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'fname')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'lname')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'mobile')->textInput(['maxlength' => 10]) ?>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'username')->textInput(['maxlength' => true, 'autocomplete' => 'off']) ?>
            </div>
            <div class="col-md-6">
                <?php // $form->field($model, 'password')->passwordInput(['maxlength' => true, 'autocomplete' => 'off']) ?>
                <?= $form->field($model, 'password')->passwordInput(['maxlength' => true, 'autocomplete' => 'off']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?php if(Yii::$app->user->id == 1){ ?>              
            <?= $form->field($model, "company_id")->widget(Select2::className(), [
                                'data' => ArrayHelper::map(Company::find()->all(), 'id', 'company_name'),
                                //'disabled' => !$modelDetail->isNewRecord,
                                'options' => [
                                    'placeholder' => 'Select Brand...',
                                ],
            
                            ]); ?>
                            <?php } ?>
            </div>
        </div>
        <?php ///$form->field($model, 'cpassword')->passwordInput(['maxlength' => true])     ?>
    </div>
    <div class="card-footer">
        <?= Html::submitButton('<i class="fa fa-save fa-fw"></i> Save', ['class' => 'btn btn-success', 'name' => 'updateProfile']) ?>
        <?= Html::a('<i class="fa fa-times fa-fw"></i> Cancel', ['index'], ['class' => 'btn btn-secondary']) ?>
    </div>
</div>



<?php ActiveForm::end(); ?>
<?php
$script = <<< JS
   $(function() {
        
        
        
        $('.fileImageContainer').on('click', function() {
            $('#user-profile_file').click();
        });
 
    });    
    function readURL(input) {
        if (input.files && input.files[0]) {
          var reader = new FileReader();
          reader.onload = function(e) {
            $('.fileImageContainer').css('background-image', "url('"+e.target.result+"')");
          }
          reader.readAsDataURL(input.files[0]);
        }
      }

    $('#user-profile_file').change(function() {
        readURL(this);
    });
JS;
$this->registerJs($script);
?>
