<?php

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
?>


<?php
$form = ActiveForm::begin([
        'enableClientValidation' => false,
        'enableAjaxValidation' => true,
        'id' => 'form-update-user', 
        'layout' => 'horizontal',
        'class' =>'form-horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-3 col-form-label',
                'wrapper' => 'col-sm-6',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]);
?>
    <div class="row">
        <div class="col-md-9">
            <?= $form->field($model, 'old_password')->passwordInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'cpassword')->passwordInput(['maxlength' => true]) ?>
        </div>
    </div>
<hr />
<div class="row">
        <div class="col-md-9">
    <?= Html::submitButton('<i class="fa fa-retweet fa-fw"></i> Change', ['class' => 'btn btn-success', 'name' => 'changePass']) ?>
    <?= Html::a('<i class="fa fa-times fa-fw"></i> Cancel', ["index"], ['class' => 'btn btn-secondary']) ?>
</div>
</div>
<?php ActiveForm::end(); ?>
