<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

        <div class="box-tools pull-right">

             <?=Html::a('<i class="fa fa-arrow-left"></i> Back',['/manage/user'],['class'=>'btn btn-primary btn-sm'])?>
        </div>
    </div>
    <div class="box-body">

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="">
                    <div class="row">
                        <div class="col-sm-3 col-md-3">
                            <img src="http://placehold.it/380x500" alt="" class="img-rounded img-responsive" width="200px" height="200px"/>
                        </div>
                        <div class="col-sm-9 col-md-9">
                            <table class="table table-striped">

                                <tr>
                                    <th>Username</th>
                                    <td><?= $model->username ?></td>
                                </tr>
                                <tr>
                                    <th>Name</th>
                                    <td><?= $model->fname . ' ' . $model->lname ?></td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td><?= $model->email ?></td>
                                </tr>
                                <tr>
                                    <th>Mobile</th>
                                    <td><?= $model->mobile ?></td>
                                </tr>

                            </table>                           

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

