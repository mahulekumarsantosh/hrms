<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\ActiveForm;
use app\components\Encryption;
use app\components\AlertHelper;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'Update Profile: ' . $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->username, 'url' => ['view', 'id' => Encryption::Encrypt($model->id)]];
$this->params['breadcrumbs'][] = 'Update';

$page = Yii::$app->controller->action->id;
$userImagePath = Yii::getAlias("@web/") . Yii::$app->avatar->show($model->fname . " " . $model->lname, 550);
?>
<div class="user-update">
    <div class="row">
        <div class="col-md-3">
            <!-- Profile Image -->
            <div class="card card-gray card-outline">
                <div class="card-body card-profile">
                    <?php
                    $filePath = Yii::getAlias("@webroot/upload/user/$model->id/$model->profile_file");
                    $imgUrl = is_file($filePath) ? ["/upload/user/$model->id/$model->profile_file"] : $userImagePath;
                    // $imgUrl = is_file($filePath) ? ["/upload/user/$model->id/$model->profile_file"] : "http://placehold.it/400x400";
                    ?>
                    <div class="text-center">
                        <?= Html::img($imgUrl, ['class' => "profile-user-img img-responsive img-circle",]) ?>
                    </div>
                    <h3 class="profile-username text-center"><?= $model->fname . ' ' . $model->lname ?></h3>
                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <b>Name</b> <a class="float-right"><?= $model->fname . ' ' . $model->lname ?></a>
                        </li>
                        <li class="list-group-item">
                            <b>Email</b> <a class="float-right"><?= $model->email ?></a>
                        </li>
                        <li class="list-group-item ">
                            <b>Mobile No.</b> <a class="float-right"><?= $model->mobile ?></a>
                        </li>
                    </ul>
                </div>
                <!-- /.card-body -->
            </div>            
        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <!--            <div class="card card-primary card-outline card-outline-tabs">
                            <div class="card-header p-0 border-bottom-0">-->
            <div class=" card card-gray card-tabs">
                <div class="card-header p-0 pt-1 border-bottom-0">
                    <ul class="nav nav-tabs nav-tabs-right">
                        <?php // if (\Yii::$app->user->can('Access User list')) { ?>
                            <li class="nav-item"><a class="nav-link <?= $page == 'update' ? 'active' : '' ?>" href="<?= Url::to(['update', 'id' => Encryption::Encrypt($model->id)]) ?>" >Profile</a></li>
                        <?php // } ?>
                        <?php // if (\Yii::$app->user->can('Update user password')) { ?>
                            <li class="nav-item"><a class="nav-link <?= $page == 'change-password' ? 'active' : '' ?>" href="<?= Url::to(['change-password', 'id' => Encryption::Encrypt($model->id)]) ?>" class="changePass">Change Password</a></li>
                        <?php // } ?>
                        <li class="nav-item"><a class="nav-link <?= $page == 'theme' ? 'active' : '' ?>" href="<?= Url::to(['theme', 'id' => Encryption::Encrypt($model->id)]) ?>" >Theme</a></li>
                    </ul>
                </div>
                <div class="card-body px-0 px-3">
                    <div class="tab-content">
                        <?php if ($page == 'update') { ?>
                            <div class="active tab-pane" id="profile">
                                <?php
                                $form = ActiveForm::begin([
                                            'enableClientValidation' => false,
                                            'enableAjaxValidation' => true,
                                            'id' => 'form-add-user',
                                            'layout' => 'horizontal',
                                            'class' => 'form-horizontal',
                                            'fieldConfig' => [
                                                'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                                                'horizontalCssClasses' => [
                                                    'label' => 'col-sm-3 col-form-label',
                                                    'wrapper' => 'col-sm-7',
                                                    'error' => '',
                                                    'hint' => '',
                                                ],
                                            ],
                                ]);
                                ?>
                                <div class="row">
                                    <div class="col-md-6">
                                        <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
                                        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
                                        <?= $form->field($model, 'fname')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-md-6">
                                        <?= $form->field($model, 'lname')->textInput(['maxlength' => true]) ?>
                                        <?= $form->field($model, 'mobile')->textInput() ?>
                                        <?= $form->field($model, 'status')->inline()->radioList([10 => "Active", 0 => "Deactive"], ['prompt' => 'Select--']) ?>
                                    </div>
                                </div>

                                <div class="row">
                                    <?php if (!$model->id) { ?>
                                        <div class="col-md-6">
                                            <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?></div>
                                        <div class="col-md-6">
                                            <?= $form->field($model, 'cpassword')->passwordInput(['maxlength' => true]) ?>
                                        </div>
                                    <?php } ?> 
                                </div>
                                <hr />
                                <div class="row">
                                    <div class="col-md-12 pt-2">
                                        <?= Html::submitButton('<i class="fa fa-save fa-fw"></i> Save', ['class' => 'btn btn-success', 'name' => 'updateProfile']) ?>
                                        <?= Html::a('<i class="fa fa-times fa-fw"></i> Cancel', ['index'], ['class' => 'btn btn-secondary']) ?>
                                    </div>
                                </div>
                                <?php ActiveForm::end(); ?>
                            </div>
                        <?php } ?> 
                        <?php if ($page == 'change-password') { ?>
                            <div class="active tab-pane" id="password">
                                <?=
                                $this->render('_formChangePassword', [
                                    'model' => $model
                                ])
                                ?>

                            </div>
                        <?php } ?> 
                        <?php if ($page == 'project-access') { ?>
                            <div class="active tab-pane" id="project-access">
                                <?=
                                $this->render('_project_access', [
                                    'model' => $model,
                                    'projectList' => $projectList,
                                ])
                                ?>
                            </div>
                        <?php } ?> 
                        <?php if ($page == 'permission') { ?>
                            <div class="active tab-pane" id="settings">
                                <?=
                                $this->render('_permission', [
                                    'model' => $model,
                                    'authitem' => $authitem
                                ])
                                ?>
                            </div>
                        <?php } ?>
                        <?php if ($page == 'notification') { ?>
                            <div class="active tab-pane" id="settings">
                                <?=
                                $this->render('_notification', [
                                    'model' => $model,
                                    'notifications' => $notifications,
                                ])
                                ?>
                            </div>
                        <?php } ?>
                        <?php if ($page == 'theme') { ?>
                            <div class="active tab-pane" id="settings">
                                <?=
                                $this->render('@app/views/profile/theme', [
                                    'model' => $model,
                                    "navbar" => $navbar,
                                    "sidebar" => $sidebar,
                                    "brandbar" => $brandbar,
                                ])
                                ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
