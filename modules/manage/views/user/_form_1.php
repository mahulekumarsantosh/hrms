<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\MasterProject;
use bogdik\wizardwidget\WizardWidget;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */

$profileImage = Yii::getAlias('@web') . "/upload/image/profile.jpeg";
$permission = array_chunk($permission, 3);
//print "<pre>";
//print_r($permission);
//die;
$data = array(
    'Mazda' => array(
        'app' => 'app1',
        'name' => 'Application',
    ),
    'Volvo' => array(
        'volvo-b9tl' => 'B9TL',
        'volvo-l90e-radlader' => 'L90E Radlader',
    ),
);
?>
<style>
    .project-permission {
        font-weight: normal;
        width:30%;
    }
    .permission {
        font-weight: normal;
        width:100%;
    }

    .fileImageContainer{
        height:200px;
        width:200px;
        border: 3px solid #fff;
        -webkit-box-shadow: 0 0 13px 0 rgba(0,0,0,.1);
        box-shadow: 0 0 13px 0 rgba(0,0,0,.1);
        position: relative;
        background-image: url('<?= $profileImage ?>');
        border-radius: 50%;
        background-repeat: no-repeat;
        background-size: cover;
    }

    .fileImageContainer > span{
        justify-content: center;
        cursor: pointer;
        background-color: #fff;
        -webkit-box-shadow: 0 0 13px 0 rgba(0,0,0,.1);
        box-shadow: 0 0 13px 0 rgba(0,0,0,.1);
        -webkit-transition: all .3s;
        transition: all .3s;
        padding: 2px;
        width: 20px;
        height: 20px;
        text-align: center;
        border-radius: 50%;
        position: absolute;
        /*        right: -8px;
                top: -8px;*/
        right: 20%;
        top: 0%;
    }
    #user-profile_file {
        width: 0!important;
        height: 0!important;
        overflow: hidden;
        opacity: 0;
    }
    hr{
        border: 1px solid #f4f4f4;
    }
    /*    .masonry-column {
            padding: 0 1px;
        }
    
        .masonry-grid > div .thumbnail {
            margin: 5px 1px;
        }*/

</style>
<?php
$form = ActiveForm::begin([
            'enableClientValidation' => false,
            'enableAjaxValidation' => true,
            'id' => 'form-add-user',
            'class' => 'form-horizontal',
            'layout' => 'horizontal',
            'fieldConfig' => [
                'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                'horizontalCssClasses' => [
                    'label' => 'col-sm-4',
                    'wrapper' => 'col-sm-6',
                    'error' => '',
                    'hint' => '',
                ],
            ],
        ]);
?>
<div class="box box-solid">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        <div class="box-tools pull-right">
            <?= Html::a('<i class="fa fa-arrow-left"></i> Back', ['/manage/user'], ['class' => 'btn btn-primary btn-sm']) ?>
        </div>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <!--<div class="text-center">-->  
                        <?=
                        $form->field($model, 'profile_file', [
                            'template' => '{label} <div class="col-sm-2 col-sm-offset-5">'
                            . '<div class="fileImageContainer"><span class="fa fa-pencil"></span><div id="fileImage"></div></div>{input}{error}{hint}</div>'
                        ])->fileInput()->label(false);
                        ?>
                        <!--</div>-->
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'fname')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'lname')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'mobile')->textInput() ?>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'username')->textInput(['maxlength' => true, 'autocomplete' => 'off']) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'password')->passwordInput(['maxlength' => true, 'autocomplete' => 'off']) ?>
                    </div>
                </div>
                <?php ///$form->field($model, 'cpassword')->passwordInput(['maxlength' => true])     ?>
                <hr>
                <div class="row">
                    <div class="col-md-12">

                        <?=
                        $form->field($project, 'project_id', [
                            'template' => "{label}\n<div class='col-sm-10'>{input}</div>\n{hint}\n{error}",
                            'labelOptions' => [ 'class' => 'col-form-label col-sm-2']
                        ])->checkboxList(ArrayHelper::map(MasterProject::find()->all(), 'id', 'project_name'), [
                            'item' => function($index, $label, $name, $checked, $value) {
                                return "<label class='project-permission'><input tabindex='{$index}' class='book' type='checkbox' {$checked} name='{$name}' value='{$value}'> {$label}</label>";
                            }])->label("Project Access");
                        ?>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <?php
                        $wizard_config = [
                            'id' => 'stepwizard',
                            'steps' => [
                                1 => [
                                    'title' => 'Step 1',
                                    'icon' => 'glyphicon glyphicon-cloud-download',
                                    'content' => '<h3>Step 1</h3>This is step 1',
                                    'buttons' => [
                                        'next' => [
                                            'title' => 'Forward',
                                            'options' => [
                                                'class' => 'disabled'
                                            ],
                                        ],
                                    ],
                                ],
                                2 => [
                                    'title' => 'Step 2',
                                    'icon' => 'glyphicon glyphicon-cloud-upload',
                                    'content' => '<h3>Step 2</h3>This is step 2',
                                    'skippable' => true,
                                ],
                                3 => [
                                    'title' => 'Step 3',
                                    'icon' => 'glyphicon glyphicon-transfer',
                                    'content' => '<h3>Step 3</h3>This is step 3',
                                ],
                            ],
                            'complete_content' => "You are done!", // Optional final screen
                            'start_step' => 2, // Optional, start with a specific step
                        ];
                        ?>

                        <?= WizardWidget::widget($wizard_config); ?>

                    </div>
                </div>


                <!--<hr>-->
            </div>


            <div class="col-md-4">

            </div>
        </div>

    </div>


    <div class="box-footer">
        <?= Html::submitButton('<i class="fa fa-save fa-fw"></i> Save', ['class' => 'btn btn-success', 'name' => 'updateProfile']) ?>
        <?= Html::a('<i class="fa fa-close fa-fw"></i> Cancel', ['index'], ['class' => 'btn btn-default']) ?>
    </div>
</div>



<?php ActiveForm::end(); ?>
<?php
$script = <<< JS
   $(function() {
        
        
        
        $('.fileImageContainer').on('click', function() {
            $('#user-profile_file').click();
        });
 
    });    
    function readURL(input) {
        if (input.files && input.files[0]) {
          var reader = new FileReader();
          reader.onload = function(e) {
            $('.fileImageContainer').css('background-image', "url('"+e.target.result+"')");
          }
          reader.readAsDataURL(input.files[0]);
        }
      }

    $('#user-profile_file').change(function() {
        readURL(this);
    });
JS;
$this->registerJs($script);
?>
