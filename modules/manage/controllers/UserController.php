<?php

namespace app\modules\manage\controllers;

use Yii;
use yii\caching\DbDependency;
use app\models\User;
use app\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use app\models\AuthItem;
use app\models\AuthProject;
use app\models\MasterProject;
use app\models\AuthItemChild;
use app\models\AuthAssignment;
use app\components\Encryption;
use app\components\Cache;
use yii\web\UploadedFile;
use app\models\NotificationSetup;
use app\components\ThemeHelper;
/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if(Yii::$app->user->id != 1){
            $dataProvider->query->andWhere(['company_id' => Yii::$app->user->identity->company_id]);
        }
        $dataProvider->pagination->pageSize = Yii::$app->request->get('per-page') ?? 10;

        //accept admin
        $dataProvider->query->andFilterWhere(['<>', 'id', 1]);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        $id = Encryption::Decrypt($id);

        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    public function actionTheme($id) {
        $id = Encryption::Decrypt($id);
        $model = $this->findModel($id);
        $navbars = ThemeHelper::$navbar;
        $sidebars = ThemeHelper::$sidebar;
        $brandbars = ThemeHelper::$brandbar;
        return $this->render('update', [
                    "navbar" => $navbars,
                    "sidebar" => $sidebars,
                    "brandbar" => $brandbars,
                    "model" => $model,
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new User();
        //$project = new AuthProject();
        $model->scenario = "create";

        if ($model->load(Yii::$app->request->post())) {

            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }

//            echo "<pre>";
//            print_r($_POST);
//            print_r($_FILES);
//            die;
            $model->created_at = strtotime(date('Y-m-d H:i:s'));
            $model->updated_at = strtotime(date('Y-m-d H:i:s'));

            $model->fname = ucfirst($model->fname);
            $model->lname = ucfirst($model->lname);
            $model->password_hash = Yii::$app->security->generatePasswordHash($model->password);
            $model->auth_key = Yii::$app->security->generateRandomString();
            $model->status = 10;
            $model->company_id = $model->company_id ? $model->company_id : Yii::$app->user->identity->company_id;

            if ($model->save()) {
                $image = UploadedFile::getInstance($model, 'profile_file');
                if ($image) {
                    $model->profile_file = microtime() . "." . $image->extension;
                    $model->save();
                    $fileDir = Yii::getAlias("@webroot/upload/user/$model->id");
                    $filePath = Yii::getAlias("@webroot/upload/user/$model->id/$model->profile_file");
                    is_dir($fileDir) ? "" : mkdir($fileDir, 777, true);
                    $image->saveAs($filePath);
                }
                Yii::$app->session->setFlash("success", "Created Successfully..!!");
                return $this->redirect(['index']);
            }
            //print_r($model->errors);
            //return $this->redirect(['view', 'id' => $model->id]);
        }


//        $projectList = MasterProject::find()->alias('mp')
//                        ->select(['mp.id', 'mp.project_name', 'ap.id auth'])
//                        ->leftJoin('auth_project ap', "ap.project_id = mp.id")
//                        ->andWhere(['is_deleted' => 0])
//                        ->orderBy('project_name')
//                        ->cache(Cache::$validity, new DbDependency(['sql' => 'SELECT MAX(created_at),count(user_id) FROM ' . AuthProject::tableName()]))
//                        ->indexBy('id')->asArray()->all();

        return $this->render('create', [
                    'model' => $model,
                        //'project' => $project,
//                    'permission' => Cache::cacheMasterPermissions()
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $id = Encryption::Decrypt($id);
        $model = $this->findModel($id);
        $model->scenario = "profile_update";
        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

                return ActiveForm::validate($model);
            }
            $model->updated_at = strtotime(date('Y-m-d H:i:s'));
            $model->fname = ucfirst($model->fname);
            $model->lname = ucfirst($model->lname);
            Yii::$app->session->setFlash("success", "Updated Successfully..!!");
            if ($model->save()) {

                return $this->redirect(Yii::$app->request->referrer);
            }
        }
//        $displayModal = true;
        return $this->render('update', [
                    'model' => $model,
//                    'displayModal' => $displayModal,
        ]);
    }

    public function actionChangePassword($id) {
        //  $id = Encryption::Decrypt($id);
        $id = Encryption::Decrypt($id);
        $model = $this->findModel($id);
        $model->scenario = "passchange";

        if ($model->load(Yii::$app->request->post())) {

            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }

            if (!Yii::$app->getSecurity()->validatePassword($model->old_password, Yii::$app->user->identity->password_hash)) {
                $model->addError("old_password", "Invalid Password");
                return $this->render('update', [
                            'model' => $model,
                ]);
            }

            if ($model->validate(['password', 'cpassword', 'old_password'])) {
                $modelForUpdate = $this->findModel($id);
                $modelForUpdate->password_hash = Yii::$app->security->generatePasswordHash($model->password);

                if ($modelForUpdate->save(true, ['password_hash'])) {
                    Yii::$app->session->setFlash("success", "Password has been changed.");
                    return $this->redirect(Yii::$app->request->referrer);
                }
            }
        }
        return $this->render('update', [
                    'model' => $model,
//                    'displayModal' => $displayModal,
        ]);
//        return $this->renderAjax('_formChangePassword', [
//                    'model' => $model,
//        ]);
    }

    public function actionProjectAccess($id) {
        $userId = Encryption::Decrypt($id);
        $model = $this->findModel($userId);
        if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
            $postData = Yii::$app->request->post();
            $condition = ['project_id' => $postData['project_id'], 'user_id' => $userId];
            $authProjectModel = AuthProject::findOne($condition);

            if ($authProjectModel) {
                $authProjectModel->delete();
            } else {
                $authProjectModel = new AuthProject();
                $authProjectModel->project_id = $postData['project_id'];
                $authProjectModel->user_id = $userId;
                $authProjectModel->created_by = Yii::$app->user->id;
                $authProjectModel->save();
            }
        }

        $projectList = MasterProject::find()->alias('mp')
                        ->select(['mp.id', 'mp.project_name', 'mp.type', 'ap.id auth'])
                        ->leftJoin('auth_project ap', "ap.project_id = mp.id and ap.user_id = $userId")
                        ->andWhere(['is_deleted' => 0])
                        ->orderBy('project_name')
                        ->cache(Cache::$validity, new DbDependency(['sql' => 'SELECT MAX(created_at),count(user_id) FROM ' . AuthProject::tableName()]))
                        ->indexBy('id')->asArray()->all();

        return $this->render('update', [
                    'model' => $model,
                    'projectList' => $projectList,
        ]);
    }

    public function actionNotification($id) {
        $id = Encryption::Decrypt($id);
        $model = $this->findModel($id);

        if (Yii::$app->request->isPost) {
            $postData = Yii::$app->request->post();
            $authItem = $postData['authItem'];

            $notificationModel = NotificationSetup::findOne(['auth_item' => $authItem, 'user_id' => $id]);

            if (!$notificationModel) {
                $notificationModel = new NotificationSetup();
                $notificationModel->auth_item = $authItem;
                $notificationModel->user_id = $id;
                $notificationModel->is_sms = 0;
                $notificationModel->is_email = 0;
                $notificationModel->created_by = Yii::$app->user->id;
            } else {
                $notificationModel->updated_by = Yii::$app->user->id;
            }

            $notificationModel->is_sms = isset($postData['is_sms']) ? $postData['is_sms'] : $notificationModel->is_sms;
            $notificationModel->is_email = isset($postData['is_email']) ? $postData['is_email'] : $notificationModel->is_email;

            if ($notificationModel->save()) {
                return 1;
            }
            return 0;
        }

        $notifications = Cache::cacheUserNotification($model->id);
//        echo '<pre>';
//        print_r($notifications);
//        die;
        return $this->render('update', [
                    'model' => $model,
                    'notifications' => $notifications,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $id = Encryption::Decrypt($id);
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        //Yii::$app->cache->flush();
        $model = \app\components\Cache::cacheUserData($id);
        if ($model) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionPermission($id) {
        $id = Encryption::Decrypt($id);
        $model = $this->findModel($id);
        $permission = Cache::cacheUserpermissions($model->id);

        return $this->render('update', [
                    'model' => $model,
                    'authitem' => $permission
        ]);
    }

    public function actionDbpermission($id) {
        $status = false;
        $userid = Encryption::Decrypt($id);
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (is_numeric($userid) && Yii::$app->request->isAjax && Yii::$app->request->post()) {
            $ischecked = Yii::$app->request->post('ischecked');
            $permission = Yii::$app->request->post('permission');

            $auth = Yii::$app->authManager;
            $pobject = $auth->getPermission(trim($permission));
            $permission_status = $auth->checkAccess($userid, trim($permission));

            if ($ischecked == 'true' && !$permission_status && $pobject) {
                $auth->assign($pobject, $userid);
                $status = true;
            }

            if ($ischecked == 'false' && $permission_status && $pobject) {
                $auth->revoke($pobject, $userid);
                $status = true;
            }
        }
        return $status;
    }

}
