<?= \app\components\AdminLte3menu::widget([
    'items' => [
       ['label' => 'Home', 'icon' => 'home', 'url' => ['/site/index']],
       ['label' => 'MAIN NAVIGATION',   'header' => true],
        ['label' => 'User', 'icon' => 'tasks', 'url' => ['/manage/user/index'], 'visible' => \Yii::$app->user->can('Access User list')],
    ],
]);
?>

