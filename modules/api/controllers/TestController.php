<?php

namespace app\modules\api\controllers;

//use yii\web\Controller;
use yii\rest\ActiveController;

/**
 * Default controller for the `api` module
 */
class TestController extends ActiveController
{
    public $modelClass = 'app\models\MasterProject';
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return [1];
    }
}
