<?php

namespace app\modules\api;

/**
 * api module definition class
 */
class Api extends \yii\base\Module {

    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\api\controllers';

    /**
     * {@inheritdoc}
     */
//    public function behaviors() {
//        return [
//            'access' => [
//                'class' => \yii\filters\AccessControl::className(),
//                'rules' => [
//                    [
//                        'allow' => true,
//                    ]
//                ],
//            ],
//        ];
//    }

    public function init() {
        parent::init();

//        \Yii::$app->setComponents([
//            'response' => [
//                'class' => 'yii\web\Response',
//                'on beforeSend' => function ($event) {
//                    \yii::createObject([
//                        'class' => \yiier\helpers\ResponseHandler::class,
//                        'event' => $event,
//                    ])->formatResponse();
//                },
//                'formatters' => [
//                    \yii\web\Response::FORMAT_JSON => [
//                        'class' => 'yii\web\JsonResponseFormatter',
//                        'prettyPrint' => true, //YII_DEBUG, // use "pretty" output in debug mode
//                        'encodeOptions' => JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE
//                    ],
//                ],
//                'format' => \yii\web\Response::FORMAT_JSON,
//            ],
//            'request' => [
//                'cookieValidationKey' => 'nhKtmhdRs4BNihOh5Uii5jtIGqxmBKxt',
//                'class' => \yii\web\Request::class,
//                'parsers' => [
//                    'application/json' => 'yii\web\JsonParser',
//                ]
//            ]
//        ]);

//        $this->components['response'] = [
//            'class' => 'yii\web\Response',
//            'on beforeSend' => function ($event) {
//                yii::createObject([
//                    'class' => yiier\helpers\ResponseHandler::class,
//                    'event' => $event,
//                ])->formatResponse();
//            },
//        ];
        // custom initialization code goes here
    }

}
