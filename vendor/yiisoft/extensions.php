<?php

$vendorDir = dirname(__DIR__);

return array (
  '2amigos/yii2-ckeditor-widget' => 
  array (
    'name' => '2amigos/yii2-ckeditor-widget',
    'version' => '2.1.0.0',
    'alias' => 
    array (
      '@dosamigos/ckeditor' => $vendorDir . '/2amigos/yii2-ckeditor-widget/src',
    ),
  ),
  'borales/yii2-phone-input' => 
  array (
    'name' => 'borales/yii2-phone-input',
    'version' => '0.3.0.0',
    'alias' => 
    array (
      '@borales/extensions/phoneInput' => $vendorDir . '/borales/yii2-phone-input/src',
    ),
  ),
  'yiisoft/yii2-bootstrap4' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap4',
    'version' => '2.0.10.0',
    'alias' => 
    array (
      '@yii/bootstrap4' => $vendorDir . '/yiisoft/yii2-bootstrap4/src',
    ),
  ),
  'hail812/yii2-adminlte-widgets' => 
  array (
    'name' => 'hail812/yii2-adminlte-widgets',
    'version' => '1.0.3.0',
    'alias' => 
    array (
      '@hail812/adminlte/widgets' => $vendorDir . '/hail812/yii2-adminlte-widgets/src',
    ),
  ),
  'hail812/yii2-adminlte3' => 
  array (
    'name' => 'hail812/yii2-adminlte3',
    'version' => '1.1.3.0',
    'alias' => 
    array (
      '@hail812/adminlte3' => $vendorDir . '/hail812/yii2-adminlte3/src',
    ),
  ),
  'kartik-v/yii2-krajee-base' => 
  array (
    'name' => 'kartik-v/yii2-krajee-base',
    'version' => '3.0.1.0',
    'alias' => 
    array (
      '@kartik/base' => $vendorDir . '/kartik-v/yii2-krajee-base/src',
    ),
  ),
  'kartik-v/yii2-dialog' => 
  array (
    'name' => 'kartik-v/yii2-dialog',
    'version' => '1.0.6.0',
    'alias' => 
    array (
      '@kartik/dialog' => $vendorDir . '/kartik-v/yii2-dialog/src',
    ),
  ),
  'kartik-v/yii2-grid' => 
  array (
    'name' => 'kartik-v/yii2-grid',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@kartik/grid' => $vendorDir . '/kartik-v/yii2-grid/src',
    ),
  ),
  'kartik-v/yii2-mpdf' => 
  array (
    'name' => 'kartik-v/yii2-mpdf',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@kartik/mpdf' => $vendorDir . '/kartik-v/yii2-mpdf/src',
    ),
  ),
  'kartik-v/yii2-popover-x' => 
  array (
    'name' => 'kartik-v/yii2-popover-x',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@kartik/popover' => $vendorDir . '/kartik-v/yii2-popover-x/src',
    ),
  ),
  'kartik-v/yii2-widget-depdrop' => 
  array (
    'name' => 'kartik-v/yii2-widget-depdrop',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@kartik/depdrop' => $vendorDir . '/kartik-v/yii2-widget-depdrop/src',
    ),
  ),
  'kartik-v/yii2-widget-fileinput' => 
  array (
    'name' => 'kartik-v/yii2-widget-fileinput',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@kartik/file' => $vendorDir . '/kartik-v/yii2-widget-fileinput/src',
    ),
  ),
  'kartik-v/yii2-widget-select2' => 
  array (
    'name' => 'kartik-v/yii2-widget-select2',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@kartik/select2' => $vendorDir . '/kartik-v/yii2-widget-select2/src',
    ),
  ),
  'kartik-v/yii2-widget-timepicker' => 
  array (
    'name' => 'kartik-v/yii2-widget-timepicker',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@kartik/time' => $vendorDir . '/kartik-v/yii2-widget-timepicker/src',
    ),
  ),
  'mdmsoft/yii2-admin' => 
  array (
    'name' => 'mdmsoft/yii2-admin',
    'version' => '2.12.0.0',
    'alias' => 
    array (
      '@mdm/admin' => $vendorDir . '/mdmsoft/yii2-admin',
    ),
  ),
  'miloschuman/yii2-highcharts-widget' => 
  array (
    'name' => 'miloschuman/yii2-highcharts-widget',
    'version' => '8.0.0.0',
    'alias' => 
    array (
      '@miloschuman/highcharts' => $vendorDir . '/miloschuman/yii2-highcharts-widget/src',
    ),
  ),
  'pheme/yii2-toggle-column' => 
  array (
    'name' => 'pheme/yii2-toggle-column',
    'version' => '0.7.0.0',
    'alias' => 
    array (
      '@pheme/grid' => $vendorDir . '/pheme/yii2-toggle-column',
    ),
  ),
  'sizeg/yii2-jwt' => 
  array (
    'name' => 'sizeg/yii2-jwt',
    'version' => '2.0.2.0',
    'alias' => 
    array (
      '@sizeg/jwt' => $vendorDir . '/sizeg/yii2-jwt',
    ),
  ),
  'vivekmarakana/yii2-dynamicform' => 
  array (
    'name' => 'vivekmarakana/yii2-dynamicform',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@wbraganca/dynamicform' => $vendorDir . '/vivekmarakana/yii2-dynamicform/src',
    ),
  ),
  'yiier/yii2-helpers' => 
  array (
    'name' => 'yiier/yii2-helpers',
    'version' => '2.1.1.0',
    'alias' => 
    array (
      '@yiier/helpers' => $vendorDir . '/yiier/yii2-helpers/src',
    ),
  ),
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap/src',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '2.1.18.0',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug/src',
    ),
  ),
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker/src',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.1.4.0',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii/src',
    ),
  ),
  'yiisoft/yii2-jui' => 
  array (
    'name' => 'yiisoft/yii2-jui',
    'version' => '2.0.7.0',
    'alias' => 
    array (
      '@yii/jui' => $vendorDir . '/yiisoft/yii2-jui',
    ),
  ),
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '2.1.2.0',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer/src',
    ),
  ),
  'yiithings/yii2-dotenv' => 
  array (
    'name' => 'yiithings/yii2-dotenv',
    'version' => '1.0.2.0',
    'alias' => 
    array (
      '@yiithings/dotenv' => $vendorDir . '/yiithings/yii2-dotenv/src',
    ),
  ),
  'zertex/yii2-avatar-generator' => 
  array (
    'name' => 'zertex/yii2-avatar-generator',
    'version' => '1.1.6.0',
    'alias' => 
    array (
      '@zertex/avatar_generator' => $vendorDir . '/zertex/yii2-avatar-generator/src',
    ),
  ),
  'kartik-v/yii2-bootstrap4-dropdown' => 
  array (
    'name' => 'kartik-v/yii2-bootstrap4-dropdown',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@kartik/bs4dropdown' => $vendorDir . '/kartik-v/yii2-bootstrap4-dropdown/src',
    ),
  ),
);
