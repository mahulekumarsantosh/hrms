<?php

namespace app\controllers;

use Yii;
use app\models\Purchase;
use app\models\PurchaseSearch;
use app\models\PurchaseDetail;
use app\models\Item;
use app\models\Model;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\components\Encryption;
use app\models\ManageStock;
/**
 * PurchaseController implements the CRUD actions for Purchase model.
 */
class PurchaseController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions() {

        return [
            'pdf-purchase-challan' => [
                'class' => \app\components\Export::className(),
                'modelClass' => false,
                'fileName' => "Purchase Challan",
                'viewFile' => "pdf-purchase",
                'pdfFormat' => 'A4-P',
                //'marginLeft' => 25,                
            ],
            
        ];
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Purchase models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PurchaseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['company_id' => Yii::$app->user->identity->company_id]);
        $dataProvider->sort = ['defaultOrder' => ['id' => SORT_DESC]];
        $dataProvider->pagination->pageSize = Yii::$app->request->queryParams['per-page'] ?? 10;
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Purchase model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Purchase model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Purchase();
        $modelDetails = null;            
        if ($model->load(Yii::$app->request->post())) {
            $modelDetails = Model::createMultiple(PurchaseDetail::classname());
            Model::loadMultiple($modelDetails, Yii::$app->request->post());

            // validate all models
           // $valid = $model->validate() & Model::validateMultiple($modelDetails);

            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ArrayHelper::merge( ActiveForm::validateMultiple($modelDetails), ActiveForm::validate($model)); 
            } 
            $model->created_by = Yii::$app->user->identity->id;
            $model->company_id = Yii::$app->user->identity->company_id;
           // if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();

                try {
                    if ($flag = $model->save(false)) {
                        foreach ($modelDetails as $modelDetail) {
                            $modelDetail->purchase_id = $model->id;
                            $modelDetail->grand_total = $model->tax_type == 2 ? $modelDetail->grand_total : $modelDetail->grand_total - $modelDetail->igst;
                            $modelDetail->tax_per = $model->tax_type == 2 ? $modelDetail->tax_per : 0;
                            $modelDetail->igst = $model->tax_type == 2 ? $modelDetail->igst : '0';
                            $modelDetail->cgst = $modelDetail->igst / 2;
                            $modelDetail->sgst = $modelDetail->igst / 2;
                            $modelDetail->company_id = Yii::$app->user->identity->company_id;
                            if($modelDetail->discount_per > 0){
                                $modelDetail->discount_amount = ($modelDetail->total * $modelDetail->discount_per) /100;
                            }                            
                            if ($flag = $modelDetail->save(false)) {
                                $item = Item::findOne($modelDetail->item_id);
                                $item->stock += $modelDetail->quantity;
                                $flag = $item->save();
                                if($flag){
                                    $check_data = ManageStock::find()->where(['date' => date('Y-m-d'), 'item_id' => $modelDetail->item_id])->one();
                                    if($check_data){
                                        Yii::$app->db->createCommand()->update('manage_stock', ['in_stock' => $check_data->in_stock + $modelDetail->quantity], ['date' => date('Y-m-d'), 'item_id' => $modelDetail->item_id])->execute();
                                    } else{
                                        $last_closing = ManageStock::find()->orderBy(['id' => SORT_DESC])->where(['item_id' => $modelDetail->item_id])->one();
                                        $stock = new ManageStock();
                                        $stock->date = date('Y-m-d');
                                        $stock->item_id = $modelDetail->item_id;
                                        $stock->in_stock = $modelDetail->quantity;
                                        $stock->company_id = Yii::$app->user->identity->company_id;
                                        $stock->opening_stock = $last_closing->opening_stock + $last_closing->in_stock - $last_closing->out_stock;
                                        $stock->save();
                                    }
                                }
                            }
                            if (!$flag) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }

                    if ($flag) {
                        $transaction->commit();
                        Yii::$app->session->setFlash("success", "Purchase created successfully..!!");
                        return $this->redirect(['index']);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
           // }
        }

        return $this->render('create', [
            'model' => $model,
            'modelDetails' => empty($modelDetails) ? [new PurchaseDetail] : $modelDetails,
        ]);
    }

    /**
     * Updates an existing Purchase model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $id = Encryption::Decrypt($id);
        $model = $this->findModel($id);
        $modelDetails = $model->getPurchasesDetails()->all();

        if ($model->load(Yii::$app->request->post())) {
            $oldModelDetails = array();
            foreach ($modelDetails as $modelDetail) {
                $oldModelDetails[$modelDetail->id] = [
                    'item_id' => $modelDetail->item_id,
                    'quantity' => $modelDetail->quantity
                ];
            }
            $oldIDs = ArrayHelper::map($modelDetails, 'id', 'id');
            $modelDetails = Model::createMultiple(PurchaseDetail::classname(), $modelDetails);
            Model::loadMultiple($modelDetails, Yii::$app->request->post());
            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelDetails, 'id', 'id')));

            // validate all models
            $valid = $model->validate() & Model::validateMultiple($modelDetails);
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ArrayHelper::merge( ActiveForm::validateMultiple($modelDetails), ActiveForm::validate($model)); 
            } 
            
            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        $model->updated_by = Yii::$app->user->identity->id;
                        $model->company_id = Yii::$app->user->identity->company_id;
                        if (!empty($deletedIDs)) {
                            $flag = PurchaseDetail::deleteAll(['id' => $deletedIDs]) > 0;
                            if ($flag) {
                                foreach ($deletedIDs as $id) {
                                    $item = Item::findOne($oldModelDetails[$id]['item_id']);
                                    $item->stock -= $oldModelDetails[$id]['quantity'];
                                    if (!($flag = $item->save())) {
                                        $transaction->rollBack();
                                        break;
                                    }
                                }
                            } else {
                                $transaction->rollBack();
                            }
                        }
                        if ($flag) {
                            foreach ($modelDetails as $modelDetail) {
                                $quantity = $modelDetail->quantity;
                                if (!empty($modelDetail->id) && $modelDetail->item_id == $oldModelDetails[$modelDetail->id]['item_id']) {
                                    $quantity -= $oldModelDetails[$modelDetail->id]['quantity'];
                                }
                                $modelDetail->purchase_id = $model->id; 
                                $modelDetail->company_id = Yii::$app->user->identity->company_id;  
                                $modelDetail->grand_total = $model->tax_type == 2 ? $modelDetail->grand_total : $modelDetail->grand_total - $modelDetail->igst;
                                $modelDetail->tax_per = $model->tax_type == 2 ? $modelDetail->tax_per : 0;
                                $modelDetail->igst = $model->tax_type == 2 ? $modelDetail->igst : '0';                            
                                $modelDetail->cgst = $modelDetail->igst / 2;
                                $modelDetail->sgst = $modelDetail->igst / 2;
                                if($modelDetail->discount_per > 0){
                                    $modelDetail->discount_amount = ($modelDetail->total * $modelDetail->discount_per) /100;
                                }  
                                if (($flag = $modelDetail->save(false)) && $quantity !== 0) {
                                    $item = Item::findOne($modelDetail->item_id);
                                    $item->stock += $quantity;
                                    $flag = $item->save();
                                    if($flag){
                                        $check_data = ManageStock::find()->where(['date' => date('Y-m-d'), 'item_id' => $modelDetail->item_id])->one();
                                        if($check_data){
                                            Yii::$app->db->createCommand()->update('manage_stock', ['in_stock' => $check_data->in_stock + $quantity], ['date' => date('Y-m-d'), 'item_id' => $modelDetail->item_id])->execute();
                                        } else{
                                            $last_closing = ManageStock::find()->orderBy(['id' => SORT_DESC])->where(['item_id' => $modelDetail->item_id])->one();
                                            $stock = new ManageStock();
                                            $stock->date = date('Y-m-d');
                                            $stock->company_id = Yii::$app->user->identity->company_id;  
                                            $stock->item_id = $modelDetail->item_id;
                                            $stock->in_stock = $modelDetail->quantity;
                                            $stock->opening_stock = $last_closing->opening_stock + $last_closing->in_stock - $last_closing->out_stock;
                                            $stock->save();
                                        }
                                    }
                                }
                                if (!$flag) {
                                    $transaction->rollBack();
                                    break;
                                }
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        Yii::$app->session->setFlash("success", "Purchase updated successfully..!!");
                        return $this->redirect(['index']);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
            'modelDetails' => empty($modelDetails) ? [new PurchaseDetail] : $modelDetails,
        ]);
    }

    /**
     * Deletes an existing Purchase model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $modelDetails = $model->getPurchasesDetails()->all();
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            foreach ($modelDetails as $modelDetail) {
                $item = Item::findOne($modelDetail->item_id);
                $item->stock -= $modelDetail->quantity;
                if (!($flag = $item->save())) {
                    $transaction->rollBack();
                    break;
                }
            }
            if ($flag) {
                if ($model->delete()) {
                    $transaction->commit();
                } else {
                    $transaction->rollBack();
                }
            }
        } catch (Exception $e) {
            $transaction->rollBack();
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Purchase model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Purchase the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Purchase::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionPurchaseChalan($id){
        $id = Encryption::Decrypt($id);
        return $this->render('purchase-chalan', [
            'model' => $this->findModel($id)
        ]);
    }
    public function actionGetitem(){
        $brand = $_POST['brand'];
        $item = Item::find()->where(['brand' => $brand, 'company_id' => Yii::$app->user->identity->company_id])->all();
        $option = "<option>Select Item</option>";
        foreach($item as $val){
            $option .= "<option value='".$val->id."'>".$val->name."</option>";
        }
       return $option;
    }
}
