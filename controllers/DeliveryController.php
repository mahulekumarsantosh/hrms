<?php

namespace app\controllers;

use Yii;
use app\models\Delivery;
use app\models\DeliverySearch;
use app\models\DeliveryDetail;
use app\models\Item;
use app\models\Model;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\components\Encryption;
use app\models\ManageStock;

/**
 * DeliveryController implements the CRUD actions for Delivery model.
 */
class DeliveryController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions() {

        return [
            'pdf-delivery-challan' => [
                'class' => \app\components\Export::className(),
                'modelClass' => false,
                'fileName' => "Delivery",
                'viewFile' => "pdf-delivery",
                'pdfFormat' => 'A4-P',
                //'marginLeft' => 25,                
            ],
            
        ];
    }
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Delivery models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DeliverySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['company_id' => Yii::$app->user->identity->company_id]);
        $dataProvider->sort = ['defaultOrder' => ['id' => SORT_DESC]];
        $dataProvider->pagination->pageSize = Yii::$app->request->queryParams['per-page'] ?? 10;
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Delivery model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Delivery model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Delivery();
        $modelDetails = null;

        if ($model->load(Yii::$app->request->post())) {
            $modelDetails = Model::createMultiple(DeliveryDetail::classname());
            Model::loadMultiple($modelDetails, Yii::$app->request->post());

            // validate all models
            
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ArrayHelper::merge( ActiveForm::validateMultiple($modelDetails), ActiveForm::validate($model)); 
            }            
            $model->created_by = Yii::$app->user->identity->id;
            $model->company_id = Yii::$app->user->identity->company_id;
            $valid = $model->validate();
            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        foreach ($modelDetails as $modelDetail) {                           
                            $modelDetail->delivery_id = $model->id;
                            $modelDetail->company_id = Yii::$app->user->identity->company_id;                            
                            if ($flag = $modelDetail->save(false)) {
                                $item = Item::findOne($modelDetail->item_id);
                                $item->stock -= $modelDetail->quantity;
                                $flag = $item->save();
                                if($flag){
                                    $check_data = ManageStock::find()->where(['date' => date('Y-m-d'), 'item_id' => $modelDetail->item_id])->one();
                                    if($check_data){
                                        Yii::$app->db->createCommand()->update('manage_stock', ['out_stock' => $check_data->out_stock + $modelDetail->quantity], ['date' => date('Y-m-d'), 'item_id' => $modelDetail->item_id, 'company_id' => Yii::$app->user->identity->company_id])->execute();
                                    } else{
                                        $last_closing = ManageStock::find()->orderBy(['id' => SORT_DESC])->where(['item_id' => $modelDetail->item_id])->one();
                                        $stock = new ManageStock();
                                        $stock->date = date('Y-m-d');
                                        $stock->company_id = Yii::$app->user->identity->company_id; 
                                        $stock->item_id = $modelDetail->item_id;
                                        $stock->out_stock = $modelDetail->quantity;
                                        $stock->opening_stock = $last_closing->opening_stock + $last_closing->in_stock - $last_closing->out_stock;
                                        $stock->save();
                                    }
                                }
                            }
                            if (!$flag) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }

                    if ($flag) {
                        $transaction->commit();
                        Yii::$app->session->setFlash("success", "Delivery Challan created successfully..!!");
                        return $this->redirect(['index']);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
            'modelDetails' => empty($modelDetails) ? [new DeliveryDetail] : $modelDetails,
        ]);
    }

    /**
     * Updates an existing Delivery model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $id = Encryption::Decrypt($id);
        $model = $this->findModel($id);
        $modelDetails = $model->getdeliveryDetails()->all();

        if ($model->load(Yii::$app->request->post())) {
            $oldModelDetails = array();
            foreach ($modelDetails as $modelDetail) {
                $oldModelDetails[$modelDetail->id] = [
                    'item_id' => $modelDetail->item_id,
                    'quantity' => $modelDetail->quantity
                ];
            }
            $oldIDs = ArrayHelper::map($modelDetails, 'id', 'id');
            $modelDetails = Model::createMultiple(DeliveryDetail::classname(), $modelDetails);
            Model::loadMultiple($modelDetails, Yii::$app->request->post());
            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelDetails, 'id', 'id')));

            // validate all models
            $valid = $model->validate() & Model::validateMultiple($modelDetails);
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ArrayHelper::merge( ActiveForm::validateMultiple($modelDetails), ActiveForm::validate($model)); 
            }
            $model->updated_by = Yii::$app->user->identity->id;
            $model->company_id = Yii::$app->user->identity->company_id;
            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        if (!empty($deletedIDs)) {
                            if ($flag = DeliveryDetail::deleteAll(['id' => $deletedIDs]) > 0) {
                                foreach ($deletedIDs as $id) {
                                    $item = Item::findOne($oldModelDetails[$id]['item_id']);
                                    $item->stock += $oldModelDetails[$id]['quantity'];
                                    if (!($flag = $item->save())) {
                                        $transaction->rollBack();
                                        break;
                                    }
                                }
                            } else {
                                $transaction->rollBack();
                            }
                        }
                        if ($flag) {
                            foreach ($modelDetails as $modelDetail) {
                                $quantity = $modelDetail->quantity;
                                if (!empty($modelDetail->id) && $modelDetail->item_id == $oldModelDetails[$modelDetail->id]['item_id']) {
                                    $quantity -= $oldModelDetails[$modelDetail->id]['quantity'];
                                }
                                $modelDetail->delivery_id = $model->id; 
                                $modelDetail->company_id = Yii::$app->user->identity->company_id;                               
                                if (($flag = $modelDetail->save(false)) && $quantity !== 0) {
                                    $item = Item::findOne($modelDetail->item_id);
                                    $item->stock -= $quantity;
                                    if($flag){
                                        $check_data = ManageStock::find()->where(['date' => date('Y-m-d'), 'item_id' => $modelDetail->item_id])->one();
                                        if($check_data){
                                            Yii::$app->db->createCommand()->update('manage_stock', ['out_stock' => $check_data->out_stock + $quantity], ['date' => date('Y-m-d'), 'item_id' => $modelDetail->item_id, 'company_id' => Yii::$app->user->identity->company_id])->execute();
                                        } else{
                                            $last_closing = ManageStock::find()->orderBy(['id' => SORT_DESC])->where(['item_id' => $modelDetail->item_id])->one();
                                            $stock = new ManageStock();
                                            $stock->date = date('Y-m-d');
                                            $stock->company_id = Yii::$app->user->identity->company_id; 
                                            $stock->item_id = $modelDetail->item_id;
                                            $stock->out_stock = $modelDetail->quantity;
                                            $stock->opening_stock = $last_closing->opening_stock + $last_closing->in_stock - $last_closing->out_stock;
                                            $stock->save();
                                        }
                                    }
                                    $flag = $item->save();
                                }
                                if (!$flag) {
                                    $transaction->rollBack();
                                    break;
                                }
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        Yii::$app->session->setFlash("success", "Delivery updated successfully..!!");
                        return $this->redirect(['index']);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
            'modelDetails' => empty($modelDetails) ? [new DeliveryDetail] : $modelDetails,
        ]);
    }

    /**
     * Deletes an existing Delivery model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Delivery model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Delivery the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Delivery::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    public function actionDeliveryChalan($id){
        $id = Encryption::Decrypt($id);
        return $this->render('delivery-chalan', [
            'model' => $this->findModel($id)
        ]);
    }

    public function actionGetitem(){
        $brand = $_POST['brand'];
        $item = Item::find()->where(['brand' => $brand, 'company_id' => Yii::$app->user->identity->company_id])->all();
        $option = "<option>Select Item</option>";
        foreach($item as $val){
            $option .= "<option value='".$val->id."'>".$val->name."</option>";
        }
       return $option;
    }
}
