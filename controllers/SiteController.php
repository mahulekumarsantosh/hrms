<?php

namespace app\controllers;

use Yii;
use yii\helpers\Url;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ForgetPassword;
use app\models\SignupForm;
use app\models\ContactForm;
use app\components\Encryption;
use app\models\UserOtp;
use app\models\Entity;
use app\models\Document;
use app\models\UserOtpVerify;
use app\components\SmsHelper;
use app\models\Brands;
use app\models\Customer;
use app\models\Item;


class SiteController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {

        $customer = Customer::find()->where(['type' => '2', 'company_id' => Yii::$app->user->identity->company_id])->count();
        $vendor = Customer::find()->where(['type' => '1', 'company_id' => Yii::$app->user->identity->company_id])->count();
        $items = Item::find()->where(['company_id' => Yii::$app->user->identity->company_id])->count();
        $brands = Brands::find()->where(['company_id' => Yii::$app->user->identity->company_id])->count();
        $stock_current_value = Item::find()->where(['company_id' => Yii::$app->user->identity->company_id])->sum('stock * current_price');
        $stock = Item::find()->select('name, brand, stock')->where(['company_id' => Yii::$app->user->identity->company_id])->orderBy('brand')->all();
        $data = [];
        foreach($stock as $val){
            $data[] = array($val->brand." ".$val->name, $val->stock);
        }   
        return $this->render('index',
        [
            'customer' => $customer,
            'vendor' => $vendor,
            'items' => $items,
            'brands' => $brands,
            'stock_current_value' => $stock_current_value,
            'stockdata' => json_encode($data)
        ]
        
        );
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        $model->scenario = 'login';
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user = $model->getUser();
//            echo "<pre>";
//            print_r($user->pass_updated_at + 3600 * 24 * 30);
//            die;
             if($model->login()) {
                Yii::$app->session->setFlash("success", "Loggedin successfully..!");
                return $this->goBack();
            }
        }

        $model->password = '';

        return $this->render('login', [
                    'model' => $model,
        ]);
    }

    public function actionLoginWithOtp() {

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        $model->scenario = 'login-with-otp';
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user = $model->getUser();
            $otpModel = UserOtp::generateOtp($user->id);
            $otpMessage = "Login OTP is {$otpModel->otp}";
            SmsHelper::Send($otpMessage, $user->mobile);

            Yii::$app->mailer->compose()
                    ->setTo($user->email)
                    ->setFrom(['info@suntechnos.in' => 'no reply'])
                    ->setSubject("Login OTP")
                    ->setTextBody($otpMessage)
                    ->send();

            Yii::$app->session->setFlash("success", "Reset link sent to your registered mobile and email id.");
            return $this->redirect(['login-otp']);
        }

        return $this->render('login-with-otp', [
                    'model' => $model,
        ]);
    }

    public function actionLoginOtp() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new UserOtpVerify();
        $model->scenario = 'session_otp_validate';

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $loginModel = new LoginForm();
            $loginModel->scenario = 'login_by_id';
            $loginModel->id = $model->otpModel->user_id;

            if ($loginModel->login()) {
                Yii::$app->session->setFlash("success", "Loggedin successfully..!");
                return $this->goBack();
            }
        }

        $model->otp = '';

        return $this->render('otp', [
                    'model' => $model,
        ]);
    }

    public function actionEmailResetPassword() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new ForgetPassword();
        $model->scenario = "email-reset-password";

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validateUser()) {
                $otpModel = $model->generateToken();
                $link = Url::to(['/site/reset-password', 'token' => Encryption::Encrypt($otpModel->token)], true);
                Yii::$app->mailer->compose()
                        ->setTo($model->email)
                        ->setFrom(['noreply.biztech@gmail.com' => 'no reply'])
                        ->setSubject("Reset Password Link")
                        ->setTextBody("Your Reset password link is $link")
                        ->send();
            }
            Yii::$app->session->setFlash("success", "Reset link sent to your registered email id.");
            return $this->refresh();
        }


        return $this->render('email-reset-password', [
                    'model' => $model,
        ]);
    }

    public function actionResetPassword($token) {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new ForgetPassword();
        // var_dump($model);
        // exit;
        $model->scenario = "reset-password-reset";
        if (($tokenModel = $model->validateToken(Encryption::Decrypt($token)))) {
            $user = \app\models\User::findOne($tokenModel->user_id);
            $model->email = $user->email;
            $model->is_first_login = $user->is_first_login;

            if ($model->is_first_login == 1) {
                $model->is_first_login = 0;
            }

            if ($model->load(Yii::$app->request->post()) && $model->resetPassword()) {
                $loginModel = new LoginForm;
                $user = $model->getUser();
                $loginModel->username = $user->username;
                $loginModel->password = $model->password;
                $loginModel->login();
                UserOtp::expireOldToken($user->id, UserOtp::TYPE_TOKEN);
                return $this->goHome();
            }
        } else {
            Yii::$app->session->setFlash("error", "Reset link is expired.");
        }

        return $this->render('forgot-password-reset', [
                    'model' => $model,
        ]);
    }

    public function actionForgotPassword() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new ForgetPassword();
        $model->scenario = "reset-password";

        if ($model->load(Yii::$app->request->post()) && $model->validateUser()) {
            $otpModel = $model->generateOtp();
            //otp sms code here

            Yii::$app->session->set("reset-password-mobile", $model->mobile);
            Yii::$app->session->setFlash("success", "OPT sent to your registered mobile number");
            return $this->redirect(['/site/forgot-password-otp']);
        }

//        $model->password = '';

        return $this->render('forgot-password', [
                    'model' => $model,
        ]);
    }

    public function actionForgotPasswordOtp() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        if (!Yii::$app->session->has("reset-password-mobile")) {
            return $this->redirect(['/site/login']);
        }

        $model = new ForgetPassword();
        $model->scenario = "reset-password-otp";
        $model->mobile = Yii::$app->session->get("reset-password-mobile");
        if ($model->load(Yii::$app->request->post()) && $otpStatus = $model->validateOtp()) {
            if ($otpStatus === true) {
                //verified
                Yii::$app->session->set("reset-password-otp-verified", $model->mobile);
                return $this->redirect(['/site/forgot-password-reset']);
            } elseif ($otpStatus === false) {
                //expired
                Yii::$app->session->destroy();
                Yii::$app->session->setFlash("danger", "OPT Expired.");
            } else {
                //wrong attempt
                Yii::$app->session->setFlash("warning", "OPT was wrong.");
            }
        }

        $model->otp = '';

        return $this->render('forgot-password-otp', [
                    'model' => $model,
        ]);
    }

    public function actionForgotPasswordOtpResend() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        if (!Yii::$app->session->has("reset-password-mobile")) {
            return $this->redirect(['/site/login']);
        }

        $model = new ForgetPassword();
        $model->mobile = Yii::$app->session->get("reset-password-mobile");
        if ($otpModel = $model->findOtp()) {
            $otpModel->otp;
            $otpModel->send_count = 1 + $otpModel->send_count;
            $otpModel->expiry_datetime = date('Y-m-d H:i:s', strtotime('+5 min'));
            $otpModel->save();

            Yii::$app->session->setFlash("success", "OPT sent to your registered mobile number");
            //otp sms code here
            return $this->redirect(Yii::$app->request->referrer);
        } else {
            Yii::$app->session->setFlash("danger", "OPT Expired.");
        }
    }

    public function actionForgotPasswordReset() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        if (!Yii::$app->session->has("reset-password-otp-verified")) {
            return $this->redirect(['/site/login']);
        }

        $model = new ForgetPassword();
        $model->scenario = "reset-password-reset";
        $model->mobile = Yii::$app->session->get("reset-password-otp-verified");
        if ($model->load(Yii::$app->request->post()) && $model->resetPassword()) {
            Yii::$app->session->destroy();
            $loginModel = new LoginForm;
            $user = $model->getUser();
            $loginModel->username = $user->username;
            $loginModel->password = $model->password;
            $loginModel->login();
            return $this->goHome();
        }

        $model->otp = '';

        return $this->render('forgot-password-reset', [
                    'model' => $model,
        ]);
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup() {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
            Yii::$app->session->setFlash('success', 'Thank you for registration. Please check your inbox for verification email.');
            return $this->goHome();
        }

        return $this->render('signup', [
                    'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout() {
        Yii::$app->user->logout();
        return $this->goHome();
    }

//    public function actionLogout2() {
//        Yii::$app->user->logout();
//        return $this->goHome();
//    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact() {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
                    'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout() {
        return $this->render('about');
    }

    public function actionUnderims() {
        return $this->render('underims');
    }

    public function actionDownload($id) {
        if (!Yii::$app->user->isGuest) {
            $model = Document::findOne(Encryption::Decrypt($id));
            $path = Yii::getAlias("@webroot/documents/{$model->parent_type}/{$model->parent_id}/{$model->file_name}");
            if (file_exists($path)) {
                return Yii::$app->response->sendFile($path, $model->file_name);
            }
        }

        throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
    }

    public function actionClearCache() {
        Yii::$app->cache->flush();
        return $this->redirect(Yii::$app->request->referrer);
    }

}
