<?php

namespace app\controllers;

use Yii;
use app\models\Quotation;
use app\models\QuotationSearch;
use app\models\QuotationDetail;
use app\models\Item;
use app\models\Model;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\components\Encryption;

/**
 * QuotationController implements the CRUD actions for Quotation model.
 */
class QuotationController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions() {

        return [
            'pdf-quotation-challan' => [
                'class' => \app\components\Export::className(),
                'modelClass' => false,
                'fileName' => "Quotation",
                'viewFile' => "pdf-quotation",
                'pdfFormat' => 'A4-P',
                //'marginLeft' => 25,                
            ],
            
        ];
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Quotation models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new QuotationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['company_id' => Yii::$app->user->identity->company_id]);
        $dataProvider->sort = ['defaultOrder' => ['id' => SORT_DESC]];
        $dataProvider->pagination->pageSize = Yii::$app->request->queryParams['per-page'] ?? 10;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Quotation model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Quotation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Quotation();
        $modelDetails = null;

        if ($model->load(Yii::$app->request->post())) {
            $modelDetails = Model::createMultiple(QuotationDetail::classname());
            Model::loadMultiple($modelDetails, Yii::$app->request->post());

            // validate all models
            $valid = $model->validate();

            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ArrayHelper::merge( ActiveForm::validateMultiple($modelDetails), ActiveForm::validate($model)); 
            } 
            $model->created_by = Yii::$app->user->identity->id;
            $model->company_id = Yii::$app->user->identity->company_id;
           
            //if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                //pred('Hiiiii');
                try {
                    if ($flag = $model->save(false)) {
                        foreach ($modelDetails as $modelDetail) {
                            $modelDetail->quotation_id = $model->id;
                            $modelDetail->grand_total = $model->tax_type == 2 ? $modelDetail->grand_total : $modelDetail->grand_total - $modelDetail->igst;
                            $modelDetail->tax_per = $model->tax_type == 2 ? $modelDetail->tax_per : 0;
                            $modelDetail->igst = $model->tax_type == 2 ? $modelDetail->igst : '0'; 
                            $modelDetail->cgst = $modelDetail->igst / 2;
                            $modelDetail->sgst = $modelDetail->igst / 2;
                            $modelDetail->company_id = Yii::$app->user->identity->company_id;
                            if($modelDetail->discount_per > 0){
                                $modelDetail->discount_amount = ($modelDetail->total * $modelDetail->discount_per) /100;
                            }                            
                            if ($flag = $modelDetail->save(false)) {
                              
                            }
                            if (!$flag) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }

                    if ($flag) {
                        $transaction->commit();
                        Yii::$app->session->setFlash("success", "Quotation created successfully..!!");
                        return $this->redirect(['index']);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
           // }
        }

        return $this->render('create', [
            'model' => $model,
            'modelDetails' => empty($modelDetails) ? [new QuotationDetail] : $modelDetails,
        ]);
    }

    /**
     * Updates an existing Quotation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $id = Encryption::Decrypt($id);
        $model = $this->findModel($id);
        $modelDetails = $model->getQuotationDetails()->all();

        if ($model->load(Yii::$app->request->post())) {
            $oldModelDetails = array();
            foreach ($modelDetails as $modelDetail) {
                $oldModelDetails[$modelDetail->id] = [
                    'item_id' => $modelDetail->item_id,
                    'quantity' => $modelDetail->quantity
                ];
            }
            $oldIDs = ArrayHelper::map($modelDetails, 'id', 'id');
            $modelDetails = Model::createMultiple(QuotationDetail::classname(), $modelDetails);
            Model::loadMultiple($modelDetails, Yii::$app->request->post());
            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelDetails, 'id', 'id')));

            // validate all models
            $valid = $model->validate() & Model::validateMultiple($modelDetails);
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ArrayHelper::merge( ActiveForm::validateMultiple($modelDetails), ActiveForm::validate($model)); 
            } 
            
            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        $model->updated_by = Yii::$app->user->identity->id;
                        $model->company_id = Yii::$app->user->identity->company_id;
                        if (!empty($deletedIDs)) {
                            $flag = QuotationDetail::deleteAll(['id' => $deletedIDs]) > 0;
                            if ($flag) {
                             
                            } else {
                                $transaction->rollBack();
                            }
                        }
                        if ($flag) {
                            foreach ($modelDetails as $modelDetail) {
                                $quantity = $modelDetail->quantity;
                                if (!empty($modelDetail->id) && $modelDetail->item_id == $oldModelDetails[$modelDetail->id]['item_id']) {
                                    $quantity -= $oldModelDetails[$modelDetail->id]['quantity'];
                                }
                                $modelDetail->quotation_id = $model->id; 
                                $modelDetail->company_id = Yii::$app->user->identity->company_id;
                                $modelDetail->grand_total = $model->tax_type == 2 ? $modelDetail->grand_total : $modelDetail->grand_total - $modelDetail->igst;
                                $modelDetail->tax_per = $model->tax_type == 2 ? $modelDetail->tax_per : 0;
                                $modelDetail->igst = $model->tax_type == 2 ? $modelDetail->igst : '0';                            
                                $modelDetail->cgst = $modelDetail->igst / 2;
                                $modelDetail->sgst = $modelDetail->igst / 2;
                                if($modelDetail->discount_per > 0){
                                    $modelDetail->discount_amount = ($modelDetail->total * $modelDetail->discount_per) /100;
                                }  
                                if (($flag = $modelDetail->save(false)) && $quantity !== 0) {
                                    
                                }
                                if (!$flag) {
                                    $transaction->rollBack();
                                    break;
                                }
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        Yii::$app->session->setFlash("success", "Quotation updated successfully..!!");
                        return $this->redirect(['index']);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
            'modelDetails' => empty($modelDetails) ? [new PurchaseDetail] : $modelDetails,
        ]);
    }

    /**
     * Deletes an existing Quotation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Quotation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Quotation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Quotation::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionQuotationChalan($id){
        $id = Encryption::Decrypt($id);
        return $this->render('quotation-chalan', [
            'model' => $this->findModel($id)
        ]);
    }

    public function actionGetitem(){
        $brand = $_POST['brand'];
        $item = Item::find()->where(['brand' => $brand, 'company_id' => Yii::$app->user->identity->company_id])->all();
        $option = "<option>Select Item</option>";
        foreach($item as $val){
            $option .= "<option value='".$val->id."'>".$val->name."</option>";
        }
       return $option;
    }
}
