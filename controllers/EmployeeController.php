<?php

namespace app\controllers;

use Yii;
use app\models\Employee;
use app\models\EmployeeSearch;
use app\models\MasterDistrict;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use app\components\Encryption;
use app\models\EmployeeBankDetails;
use app\models\EmployeeOfficialDetails;
use app\models\EmployeePfDetails;

/**
 * EmployeeController implements the CRUD actions for Employee model.
 */
class EmployeeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Employee models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EmployeeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);       
        $dataProvider->query->andWhere(['company_id' => Yii::$app->user->identity->company_id]);        
        $dataProvider->sort = ['defaultOrder' => ['first_name' => SORT_ASC]];
        $dataProvider->pagination->pageSize = Yii::$app->request->queryParams['per-page'] ?? 10;
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Employee model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Employee model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Employee();

        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }

            $model->dob = date('Y-m-d', strtotime($model->dob));            
            $model->company_id = Yii::$app->user->identity->company_id;
            $model->created_by = Yii::$app->user->identity->id;
            if ($model->save()) {                
                Yii::$app->session->setFlash("success", "Employee created successfully..!!");
                return $this->redirect(['index']);
            }
        }

        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Employee model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $id = Encryption::Decrypt($id);
        Yii::$app->session->set('empid', $id);
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            $model->dob = date('Y-m-d', strtotime($model->dob)); 
            $model->updated_by = Yii::$app->user->identity->id;
            if ($model->save()) {                
                Yii::$app->session->setFlash("success", "Employee Basic details Updated successfully..!!");
                //return $this->redirect(['index']);
            }
        }

        return $this->render('employee-details', [
            'data' => [
                'model' => $model,
            ]
        ]);
    }

    /**
     * Deletes an existing Employee model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Employee model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Employee the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Employee::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionGetDistrict(){
        $stateId = Yii::$app->request->post()['stateId'];
        $data = MasterDistrict::find()->where(['state_id' => $stateId])->all();
        $option = "<option value=''>Select District</option>";
        foreach($data as $val){
            $option .= "<option '".$val['id']."'>".$val['name']."</option>";
        }
        return $option;
    }
    public function actionBankDetails($id){
        $id = Encryption::Decrypt($id);
        $modelUpdate = EmployeeBankDetails::find()->where(['employee_id' => $id])->one();        
        $model = $modelUpdate ? $modelUpdate : new EmployeeBankDetails();
        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            } 
            $model->employee_id = $id;
            $model->created_by = Yii::$app->user->identity->id;
            if ($model->save()) {                
                Yii::$app->session->setFlash("success", "Bank Details updated successfully..!!");
                //return $this->redirect(['index']);
            }
        }
        return $this->render('employee-details', [
            'data' => [
                'model' => $model,
            ]
        ]);
    }

    public function actionOfficialDetails($id){
        $id = Encryption::Decrypt($id);
        $modelUpdate = EmployeeOfficialDetails::find()->where(['employee_id' => $id])->one();        
        $model = $modelUpdate ? $modelUpdate : new EmployeeOfficialDetails();
        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            } 
            $model->employee_id = $id;
            $model->doj = date('Y-m-d', strtotime($model->doj));
            $model->created_by = Yii::$app->user->identity->id;
            if ($model->save()) {                
                Yii::$app->session->setFlash("success", "Official Details updated successfully..!!");
                //return $this->redirect(['index']);
            }
        }
        return $this->render('employee-details', [
            'data' => [
                'model' => $model,
            ]
        ]);
    }

    public function actionOtherDetails($id){
        
        $id = Encryption::Decrypt($id);
        $modelUpdate = EmployeePfDetails::find()->where(['employee_id' => $id])->one();        
        $model = $modelUpdate ? $modelUpdate : new EmployeePfDetails();
        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            } 
            $model->employee_id = $id;
            $model->created_by = Yii::$app->user->identity->id;
            if ($model->save()) {                
                Yii::$app->session->setFlash("success", "Other Details updated successfully..!!");
               // return $this->redirect(['index']);
            }
        }
        return $this->render('employee-details', [
            'data' => [
                'model' => $model,
            ]
        ]);
    }
    
}
