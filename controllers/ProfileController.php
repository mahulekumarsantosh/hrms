<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\UserDocuments;
use yii\widgets\ActiveForm;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use app\components\Encryption;
use app\components\ThemeHelper;

/**
 * Profile controller for the `user` module
 */
class ProfileController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex() {
        $userId = Yii::$app->user->id;
        $user = User::findOne($userId);
        $model = clone $user;
        $model->scenario = 'profile_update';
        $validateAttribute = ['fname', 'lname', 'mobile', 'updated_by', 'updated_at'];
        if ($model->load(Yii::$app->request->post())) {
            $model->updated_at = time();
//            $model->updated_by = Yii::$app->user->id;
            $model->fname = ucfirst($model->fname);

            $model->lname = ucfirst($model->lname);

            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model, $validateAttribute);
            }

            if ($model->save(true, $validateAttribute)) {
                Yii::$app->session->setFlash("success", "Updated Successfully..!!");
                return $this->redirect(Yii::$app->request->referrer);
            }
        }

        return $this->render('profile', [
                    'data' => [
                        'user' => $user,
                        'model' => $model,
                        'userId' => $userId,
                    ]
        ]);
    }

    public function actionUpdatePassword() {
        $userId = Yii::$app->user->id;
        $user = User::findOne($userId);
        $model = clone $user;
        $model->scenario = 'passchange';
        $validateAttribute = ['password', 'cpassword', 'password_hash', 'updated_by', 'updated_at'];

        if ($model->load(Yii::$app->request->post())) {
            $model->updated_at = time();
//            $model->updated_by = Yii::$app->user->id;
            $model->password_hash = Yii::$app->security->generatePasswordHash($model->password);

            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model, $validateAttribute);
            }

            if ($model->save(true, $validateAttribute)) {
                Yii::$app->session->setFlash("success", "Updated Successfully..!!");
                return $this->redirect(Yii::$app->request->referrer);
            }
        }
        return $this->render('profile', [
                    'data' => [
                        'user' => $user,
                        'model' => $model,
                        'userId' => $userId,
                    ]
        ]);
    }

    public function actionChangePassword() {
        $userId = Yii::$app->user->id;
        $user = User::findOne($userId);
        $model = clone $user;
        $model->scenario = 'passchange';
        $validateAttribute = ['password', 'cpassword', 'old_password', 'password_hash', 'updated_by', 'updated_at', 'is_first_login'];

        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model, $validateAttribute);
            }
            $model->updated_at = time();
            $model->password_hash = Yii::$app->security->generatePasswordHash($model->password);
            $model->is_first_login = 0;
            if ($model->save(true, $validateAttribute)) {
                Yii::$app->session->setFlash("success", "Password Changed Successfully..!!");
                return $this->goHome();
            }
        }
        return $this->renderAjax('change-password', [
                    'model' => $model,
        ]);
    }

    public function actionTheme() {
        $userId = Yii::$app->user->id;
        $user = User::findOne($userId);
        $model = clone $user;
        $navbars = ThemeHelper::$navbar;
        $sidebars = ThemeHelper::$sidebar;
        $brandbars = ThemeHelper::$brandbar;
        return $this->render('profile', ['data' => [
                        "navbar" => $navbars,
                        "sidebar" => $sidebars,
                        "brandbar" => $brandbars,
                        "model" => $model,
                        "user" => $user,
                    ]
        ]);
    }

    public function actionSetTheme() {
        $user_id = !empty(Yii::$app->request->get('user_id')) ? Encryption::Decrypt(Yii::$app->request->get('user_id')) : Yii::$app->user->id;
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $keytype = Yii::$app->request->post('keytype');
        $keyvalue = Yii::$app->request->post('keyvalue');
        if (isset($_COOKIE["ims_user_$user_id"])) {
            $cookie = $_COOKIE["ims_user_$user_id"];
            $cookie = stripslashes($cookie);
            $savedCardArray = json_decode($cookie, true);
            $savedCardArray[$keytype] = $keyvalue;
            $savedCardArray['user_id'] = $user_id;
            $json = json_encode($savedCardArray);
            setcookie("ims_user_$user_id", $json, time() + 86400 * 365, '/');
        } else {
            $cardArray = array(
                $keytype => $keyvalue,
                'user_id' => $user_id
            );
            $json = json_encode($cardArray);
            setcookie("ims_user_$user_id", $json, time() + 86400 * 365, '/');
        }
    }

    protected function findModel($id) {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
