<?php

namespace app\controllers;

use Yii;
use app\models\Company;
use app\models\CompanySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use app\components\Encryption;
use app\models\MasterLeave;
use app\models\MasterLeaveSearch;
use app\models\MasterPayrollDeduction;
use app\models\MasterPayrollDeductionSearch;
use app\models\MasterPayrollEarning;
use app\models\MasterPayrollEarningSearch;
use app\models\MasterShift;
use app\models\MasterShiftSearch;
use yii\web\UploadedFile;
use app\models\MasterWeekOff;
use app\models\Model;
use yii\helpers\ArrayHelper;

/**
 * CompanyController implements the CRUD actions for Company model.
 */
class CompanyController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Company models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CompanySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if (Yii::$app->user->id != 1) {
            $dataProvider->query->andWhere(['id' => Yii::$app->user->identity->company_id]);
        }
        $dataProvider->sort = ['defaultOrder' => ['id' => SORT_DESC]];
        $dataProvider->pagination->pageSize = Yii::$app->request->queryParams['per-page'] ?? 10;
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Company model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Company model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Company();
        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            $model->created_by = Yii::$app->user->identity->id;
            if ($model->save()) {
                $image = UploadedFile::getInstance($model, 'logo');
                if ($image) {
                    $model->logo = microtime() . "." . $image->extension;
                    $model->save();
                    $fileDir = Yii::getAlias("@webroot/images/logo/");
                    $filePath = Yii::getAlias("@webroot/images/logo/$model->logo");
                    is_dir($fileDir) ? "" : mkdir($fileDir, 777, true);
                    $image->saveAs($filePath);
                }
                Yii::$app->session->setFlash("success", "Company created successfully..!!");
                return $this->redirect(['index']);
            }
        }

        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Company model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate()
    {
        $id = Yii::$app->user->identity->company_id;
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            $model->updated_by = Yii::$app->user->identity->id;
            if ($model->save()) {
                Yii::$app->session->setFlash("success", "Company updated successfully..!!");
            }

            return $this->render('company_details', [
                'data' => [
                    'model' => $model,
                ]
            ]);
        }
        return $this->render('company_details', [
            'data' => [
                'model' => $model,
            ]
        ]);
    }

    /**
     * Deletes an existing Company model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Company model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Company the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Company::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionCompanyweekoff()
    {
        $userId = Yii::$app->user->identity->company_id;
        $getData = MasterWeekOff::find()->where(['company_id' => $userId])->one();
        if ($getData) {
            $model = MasterWeekOff::findOne($getData->id);
            $model->day_ids = explode(',', $model->day_ids);
        } else {
            $model = new MasterWeekOff();
        }

        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            $a = Yii::$app->request->post();
            $model->day_ids = implode(',', $a['MasterWeekOff']['day_ids']);

            $model->company_id = $userId;

            if ($model->id != '') $model->updated_by = Yii::$app->user->identity->id;
            else $model->created_by = Yii::$app->user->identity->id;

            if ($model->save()) {
                Yii::$app->session->setFlash("success", "Weekoff updated successfully..!!");
            }

            $model->day_ids = explode(',', $model->day_ids);

            return $this->render('company_details', [
                'data' => [
                    'model' => $model,
                ]
            ]);
        }

        return $this->render('company_details', [
            'data' => [
                'model' => $model,
            ]
        ]);
    }

    public function actionPayrolldeduction()
    {
        $searchModel = new MasterPayrollDeductionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $userId = Yii::$app->user->identity->company_id;
        $model = new MasterPayrollDeduction();
        $getData = MasterPayrollDeduction::find()->where(['company_id' => $userId])->all();
        if (!empty($getData)) {
            $modelDetails =  $getData;
        } else {
            $modelDetails = null;
        }

        if ($model->load(Yii::$app->request->post())) {
            
        }

        return $this->render('company_details', [
            'data' => [
                'model' => $model,
                'modelDetails' => empty($modelDetails) ? [new MasterPayrollDeduction] : $modelDetails,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        ]);
    }

    public function actionPayrollearning()
    {
        $searchModel = new MasterPayrollEarningSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $userId = Yii::$app->user->identity->company_id;
        $model = new MasterPayrollEarning();
        $getData = MasterPayrollEarning::find()->where(['company_id' => $userId])->all();
        if (!empty($getData)) {
            $modelDetails =  $getData;
        } else {
            $modelDetails = null;
        }

        if ($model->load(Yii::$app->request->post())) {
            
        }

        return $this->render('company_details', [
            'data' => [
                'model' => $model,
                'modelDetails' => empty($modelDetails) ? [new MasterPayrollEarning] : $modelDetails,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        ]);
    }

    public function actionCompanyleave()
    {
        $searchModel = new MasterLeaveSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $userId = Yii::$app->user->identity->company_id;
        $model = new MasterLeave();
        $getData = MasterLeave::find()->where(['company_id' => $userId])->all();
        if (!empty($getData)) {
            $modelDetails =  $getData;
        } else {
            $modelDetails = null;
        }

        if ($model->load(Yii::$app->request->post())) {

        }

        return $this->render('company_details', [
            'data' => [
                'model' => $model,
                'modelDetails' => empty($modelDetails) ? [new MasterLeave] : $modelDetails,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        ]);
    }

    public function actionCompanyshift()
    {
        $searchModel = new MasterShiftSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $userId = Yii::$app->user->identity->company_id;
        $model = new MasterShift();
        $getData = MasterShift::find()->where(['company_id' => $userId])->all();
        if (!empty($getData)) {
            $modelDetails =  $getData;
        } else {
            $modelDetails = null;
        }

        if ($model->load(Yii::$app->request->post())) {

        }

        return $this->render('company_details', [
            'data' => [
                'model' => $model,
                'modelDetails' => empty($modelDetails) ? [new MasterShift] : $modelDetails,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        ]);
    }
}
