<?php

namespace app\components;

use Yii;
use yii\bootstrap4\Alert;

//use hail812\adminlte3\widgets\Alert;

class AlertHelper {

    public static function widget() {
        if (Yii::$app->session->hasFlash('success')) {
            echo Alert::widget(['options' => ['class' => 'alert-success',], 'body' => Yii::$app->session->getFlash('success')]);
        }
        if (Yii::$app->session->hasFlash('error')) {
            echo Alert::widget(['options' => ['class' => 'alert-danger',], 'body' => Yii::$app->session->getFlash('error')]);
        }
        if (Yii::$app->session->hasFlash('warning')) {
            echo Alert::widget(['options' => ['class' => 'alert-warning',], 'body' => Yii::$app->session->getFlash('warning')]);
        }
    }

}
