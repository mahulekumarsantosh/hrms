<?php

function pre($data, $die = false) {
    echo '<pre>';
    print_r($data);
    echo '</pre>';
    if ($die) {
        die;
    }
}

function pred($data) {
    pre($data, true);
}
