<?php

namespace common\components;

class DynamicTableColumn {

    public static $serial = 0;

    public static function increment() {
        static::$serial++;
    }

    public static function reset() {
        static::$serial = 0;
    }

    public static $dynamicFields; // = [];
    public static $dynamicValues;

    public static function setValue($dynamicValues) {
        static::$dynamicValues = \yii\helpers\ArrayHelper::map($dynamicValues, 'puf_id', 'value');
    }

    public static function setFields($dynamicFields) {
        foreach ($dynamicFields as $val) {
            static::increment();
            static::$dynamicFields[static::$serial] = $val['id'];
        }
        static::reset();
    }

    public static function getValue() {
        if (isset(static::$dynamicValues[static::$dynamicFields[static::$serial]])) {
            return static::$dynamicValues[static::$dynamicFields[static::$serial]];
        } else {
            return "";
        }
    }

}
