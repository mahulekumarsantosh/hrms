<?php

namespace app\components;

use Yii;

class Menu {
    /*
     * for menu
     */

    public static $activeMenu = false;

    public static function set($key) {
        self::$activeMenu = $key;
    }

    public static function get() {
        return self::$activeMenu;
    }

    public static function isActive($key) {
        return self::$activeMenu == $key ? "active" : '';
    }

    /*
     * for module
     */

    public static $activeModule = false;

    public static function setModule($key) {
        self::$activeModule = $key;
    }

    public static function getModule() {
        return self::$activeModule;
    }

    public static function isActiveModule($key) {
        return self::$activeModule == $key ? true : false;
    }

}
