<?php

namespace app\components;

use Yii;
//use hail812\adminlte3\widgets\Menu;

class AdminLte3menu extends \hail812\adminlte3\widgets\Menu {

    public function init() {
        parent::init();
        $this->options = [
            'class' => ' nav nav-pills nav-sidebar flex-column nav-child-indent ',
            'data-widget' => 'treeview',
            'role' => 'menu',
            'data-accordion' => 'false'
        ];
    }

}
