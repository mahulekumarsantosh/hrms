<?php

namespace app\components;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\dialog\Dialog;
use yii\helpers\Inflector;
use yii\helpers\Json;
use yii\web\JsExpression;
use kartik\grid\GridExportAsset;

class GridView extends \kartik\grid\GridView {

    public $headtitle;
    public $orientation;
    public $exportTitle = false;
    public $exportHeader;
    public $reportDate;
    public $materialId;
    public $layout = 0;
    public $toolbar = [
        '{export}',
        '{perPage}',
    ];
    public $resizableColumns = false;

    public function init() {
        parent::init();
        $this->containerOptions['class'] = ' p-2 ';
        $this->layout = $this->initSwitchLayout();
        $beforeHeader = [
        'columns' => [
        ['content' => $this->exportHeader ?? $this->exportTitle, 'options' => ['colspan' => count($this->columns), 'class' => 'nodisplay text-center text-bold', 'style' => "padding: 15px;"]],
        ],
        ];
        count($this->beforeHeader) > 0 ?
                        array_unshift($this->beforeHeader, $beforeHeader) :
                        $this->beforeHeader = [$beforeHeader];
        $pdfFooter = [
            'L' => [
                'content' => '',
                'font-size' => 8,
                'font-style' => 'B',
                'color' => '#999999'
            ],
            'R' => [
                'content' => '[ {PAGENO} ]',
                'font-size' => 10,
                'font-style' => 'B',
                'font-family' => 'serif',
                'color' => '#333333'
            ],
            'line' => TRUE,
        ];

        $this->exportConfig = [
            GridView::EXCEL => [
                'label' => Yii::t('kvgrid', 'Excel'),
                'icon' => 'fa fa-file-excel',
                'iconOptions' => ['class' => 'text-success'],
                'showHeader' => true,
                'showPageSummary' => true,
                'showFooter' => true,
                'showCaption' => true,
                'filename' => Yii::t('kvgrid', $this->exportTitle == false ? "export" : \yii\helpers\Inflector::id2camel($this->exportTitle)),
                'alertMsg' => Yii::t('kvgrid', 'The EXCEL export file will be generated for download.'),
                'options' => ['title' => Yii::t('kvgrid', 'Microsoft Excel 95+')],
                'mime' => 'application/vnd.ms-excel',
                'config' => [
                    'worksheet' => Yii::t('kvgrid', 'ExportWorksheet'),
                    'cssFile' => '',
                ]
            ],
            GridView::PDF => [
                'label' => Yii::t('kvgrid', 'PDF'),
                'icon' => 'fa fa-file-pdf',
                'iconOptions' => ['class' => 'text-danger'],
                'showHeader' => true,
                'showPageSummary' => true,
                'showFooter' => true,
                'showCaption' => true,
                'filename' => Yii::t('kvgrid', $this->exportTitle == false ? "export" : \yii\helpers\Inflector::id2camel($this->exportTitle)),
                'alertMsg' => Yii::t('kvgrid', 'The PDF export file will be generated for download.'),
                'options' => ['title' => Yii::t('kvgrid', 'Portable Document Format')],
                'mime' => 'application/pdf',
                'config' => [
                    'cssFile' => '@webroot/css/report.css',
                    'mode' => 'c',
                    'format' => 'A4',
                    'orientation' => !empty($this->orientation) ? $this->orientation : "P",
                    'destination' => 'D',
                    'marginTop' => 10,
                    'marginBottom' => 15,
                    'marginLeft' => 10,
                    'marginRight' => 10,
                    'cssInline' => '.kv-wrap{padding:20px;}' .
//                    'table tr th{font-size:12px;padding:0px;margin:0px}' .
//                    'table tr td{font-size:10px;padding:0px;margin:0px}' .
                    '.kv-align-center{text-align:center;}' .
                    '.kv-align-left{text-align:left;}' .
                    '.kv-align-right{text-align:right;}' .
                    '.kv-align-top{vertical-align:top!important;}' .
                    '.kv-align-bottom{vertical-align:bottom!important;}' .
                    '.kv-align-middle{vertical-align:middle!important;}' .
                    '.kv-page-summary{border-top:4px double #ddd;font-weight: bold;}' .
                    '.kv-table-footer{border-top:4px double #ddd;font-weight: bold;}' .
                    '.kv-table-caption{font-size:1.5em;padding:8px;border:1px solid #ddd;border-bottom:none;}' .
                    '.hideinpdf{ height:0 !important; }',
//                    '.text-center{text-aligh:center;}' .
//                    '.text-bold{font-weight:bolder;}' .
//                    '.datata{background-color:red}',
                    'methods' => [
                        'SetHeader' => false,
                        'SetFooter' => [
                            ['odd' => $pdfFooter, 'even' => $pdfFooter]
                        ],
                    ],
                    'options' => [
                        'title' => "Title",
                        'subject' => Yii::t('kvgrid', 'PDF export generated by kartik-v/yii2-grid extension'),
                        'keywords' => Yii::t('kvgrid', 'krajee, grid, export, yii2-grid, pdf'),
                    ],
                    'contentBefore' => $this->getHeader(),
//                    'contentAfter' => ''
                ]
            ],
        ];
    }

    protected function getHeader() {
        //$image = Html::img(Yii::getAlias('@webroot/images/1/0.00289000 1610714922.png'), ['width' => '100px']);
        $filePath = Yii::getAlias("@webroot/upload/setting/" . Yii::$app->settings->company_logo);

        $imgUrl = is_file($filePath) ? ["/upload/setting/" . Yii::$app->settings->company_logo] : "@web/images/project-management.png";
        $image = Html::img($imgUrl, ['alt' => Yii::$app->settings->company_name, 'width' => '90']);
        $companyName = Yii::$app->settings->company_name;


        return $test = "<table border='0' width='100%' class='companyinfo' style='border: none;' >
                <tr>
                    <td width='12%'>$image</td>
                    <td style='padding: 0 0 0 20px' class='text-left'>
                        <h1 style='font-size:25px; font-weight:bolder; font-weight: bold;'>" . $companyName . "</h1>
                        <p><i>4/12, Second Floor, Shree Tower Dr. Verma Nursing Home Street, <br>New Shanti Nagar, Raipur<i></p>
                    </td>
                    <td class='text-right' style='vertical-align: bottom;'><p><b>Date:</b> " . date('d M Y') . "</p></td>
                </tr>
            </table><hr>";

        //<div class='text-center report-title'><b>" . $this->exportTitle . "</b></div><hr>
//        <table width='100%' style='margin-top:5px'>
//             <tr>
//                <td style='padding: 5px 4px; border-top:1px solid #ccc; border-bottom:1px solid #ccc ' class='text-center'>
//                    <p style='font-size:15px;  color : #373a3c'>" . $this->exportTitle . "</p>
//                </td>
//            </tr>
//        </table>
//        $header = '<htmlpageheader name="firstpage" style="display:none;>
//     ' . $test . '
//</htmlpageheader>
//
//<htmlpageheader name="otherpages" style="display:none">
//    <div style="text-align:center">{PAGENO}</div>
//</htmlpageheader><sethtmlpageheader name="firstpage" value="on" show-this-page="1" />
//<sethtmlpageheader name="otherpages" value="on" />
//';
//
//        return $header;
//        return "<table width='100%'>
//                <tr>
//                    <td width='10%'><img width='10%' src='$imgUrl' /></td>
//                    <td style='padding: 0 0 0 20px' class='text-left'>
//                        <h1 style='font-size:25px; font-weight:bolder'>".$companyName."</h1><br>
//                        <p><i>4/12, Second Floor, Shree Tower Dr. Verma Nursing Home Street, <br>New Shanti Nagar, Raipur<i></p>
//                    </td>
//                    <td class='text-right' style='vertical-align: bottom;'><p>Date: ".date('d M Y')."</p></td>
//                </tr>
//            </table>
//             <table width='100%' style='margin-top:5px'>
//             <tr>
//                <td style='padding: 5px 4px; border-top:1px solid #ccc; border-bottom:1px solid #ccc ' class='text-center'>
//                    <p style='font-size:15px;  color : #373a3c'>".$this->exportTitle."</p>
//                </td>
//            </tr>
//        </table>";
    }

    protected function initLayout() {
        $this->export['options']['class'] = " btn-outline-secondary btn-sm ";
        Html::addCssClass($this->filterRowOptions, 'skip-export');
        if ($this->resizableColumns && $this->persistResize) {
            $key = empty($this->resizeStorageKey) ? Yii::$app->user->id : $this->resizeStorageKey;
            $gridId = empty($this->options['id']) ? $this->getId() : $this->options['id'];
            $this->containerOptions['data-resizable-columns-id'] = (empty($key) ? "kv-{$gridId}" : "kv-{$key}-{$gridId}");
        }
        if ($this->hideResizeMobile) {
            Html::addCssClass($this->options, 'hide-resize');
        }
        $this->replaceLayoutTokens([
            '{toolbarContainer}' => $this->renderToolbarContainer(),
            '{toolbar}' => $this->renderToolbar(),
            '{export}' => $this->renderExport(),
            '{perPage}' => $this->renderPerPage(),
            '{toggleData}' => $this->renderToggleData(),
            '{items}' => Html::tag('div', '{items}', $this->containerOptions),
        ]);
        if (is_array($this->replaceTags) && !empty($this->replaceTags)) {
            foreach ($this->replaceTags as $key => $value) {
                if ($value instanceof \Closure) {
                    $value = call_user_func($value, $this);
                }
                $this->layout = str_replace($key, $value, $this->layout);
            }
        }
    }

    public $perPageId = false;

    protected function getPerPageId() {
        if ($this->perPageId === false) {
            $this->perPageId = 'per-page-' . $this->getId();
        }
        return $this->perPageId;
    }

    protected function renderPerPage() {

        if ($this->dataProvider->pagination != false) {

//if (isset($this->dataProvider->pagination)) {
//            $this->dataProvider->pagination->setPage = $this->dataProvider->pagination->pageSize ?? 2;
//            echo $this->dataProvider->pagination->pageSize;
            $dropDownList = Html::dropDownList($this->dataProvider->pagination->pageSizeParam, $this->dataProvider->pagination->pageSize, Yii::$app->params['gridPageSize'], [
                        'id' => $this->getPerPageId(),
                        'class' => 'btn btn-sm btn-outline-secondary dropdown-toggle custom-grid-ddl',
            ]);
            return Html::tag('div', $dropDownList, ['class' => ""]);
        }
    }

    protected function getClientOptions() {
        $filterUrl = isset($this->filterUrl) ? $this->filterUrl : Yii::$app->request->url;
        $id = $this->filterRowOptions['id'];

        $perPageId = $this->getPerPageId();

        $filterSelector = "#$id input, #$id select, #$perPageId";
        if (isset($this->filterSelector)) {
            $filterSelector .= ', ' . $this->filterSelector;
        }

        return [
            'filterUrl' => Url::to($filterUrl),
            'filterSelector' => $filterSelector,
        ];
    }

    protected function renderToolbar() {
        if (empty($this->toolbar) || (!is_string($this->toolbar) && !is_array($this->toolbar))) {
            return '';
        }
        if (is_string($this->toolbar)) {
            return $this->toolbar;
        }
        $toolbar = '';
        foreach ($this->toolbar as $item) {
            if (is_array($item)) {
                $content = ArrayHelper::getValue($item, 'content', '');
                $options = ArrayHelper::getValue($item, 'options', []);
                static::initCss($options, 'btn-group');
                $toolbar .= Html::tag('div', $content, $options);
            } else {
                $toolbar .= "\n&nbsp;{$item}";
            }
        }
        return $toolbar;
    }

    protected function registerAssets() {
        $view = $this->getView();
        $script = '';
        if ($this->bootstrap) {
            \kartik\grid\GridViewAsset::register($view);
        }
        Dialog::widget($this->krajeeDialogSettings);
        $gridId = $this->options['id'];
        $NS = '.' . str_replace('-', '_', $gridId);
        if ($this->export !== false && is_array($this->export) && !empty($this->export)) {
            GridExportAsset::register($view);
            if (!isset($this->_module->downloadAction)) {
                $action = ["/gridview/export/download"];
            } else {
                $action = (array) $this->_module->downloadAction;
            }
            $gridOpts = Json::encode(
                            [
                                'gridId' => $gridId,
                                'action' => Url::to($action),
                                'module' => $this->moduleId,
                                'encoding' => ArrayHelper::getValue($this->export, 'encoding', 'utf-8'),
                                'bom' => (int) ArrayHelper::getValue($this->export, 'bom', 1),
                                'target' => ArrayHelper::getValue($this->export, 'target', self::TARGET_BLANK),
                                'messages' => $this->export['messages'],
                                'exportConversions' => $this->exportConversions,
                                'skipExportElements' => $this->export['skipExportElements'],
                                'showConfirmAlert' => ArrayHelper::getValue($this->export, 'showConfirmAlert', true),
                            ]
            );
            $gridOptsVar = 'kvGridExp_' . hash('crc32', $gridOpts);
            $view->registerJs("var {$gridOptsVar}={$gridOpts};");
            foreach ($this->exportConfig as $format => $setting) {
                $id = "jQuery('#{$gridId} .export-{$format}')";
                $genOpts = Json::encode(
                                [
                                    'filename' => $setting['filename'],
                                    'showHeader' => $setting['showHeader'],
                                    'showPageSummary' => $setting['showPageSummary'],
                                    'showFooter' => $setting['showFooter'],
                                ]
                );
                $genOptsVar = 'kvGridExp_' . hash('crc32', $genOpts);
                $view->registerJs("var {$genOptsVar}={$genOpts};");
                $expOpts = Json::encode(
                                [
                                    'dialogLib' => ArrayHelper::getValue($this->krajeeDialogSettings, 'libName', 'krajeeDialog'),
                                    'gridOpts' => new JsExpression($gridOptsVar),
                                    'genOpts' => new JsExpression($genOptsVar),
                                    'alertMsg' => ArrayHelper::getValue($setting, 'alertMsg', false),
                                    'config' => ArrayHelper::getValue($setting, 'config', []),
                                ]
                );
                $expOptsVar = 'kvGridExp_' . hash('crc32', $expOpts);
                $view->registerJs("var {$expOptsVar}={$expOpts};");
                $script .= "{$id}.gridexport({$expOptsVar});";
            }
        }
        $contId = '#' . $this->containerOptions['id'];
        $container = "jQuery('{$contId}')";
        if ($this->resizableColumns) {
            $rcDefaults = [];
            if ($this->persistResize) {
                GridResizeStoreAsset::register($view);
            } else {
                $rcDefaults = ['store' => null];
            }
            $rcOptions = Json::encode(array_replace_recursive($rcDefaults, $this->resizableColumnsOptions));
            GridResizeColumnsAsset::register($view);
            $script .= "{$container}.resizableColumns('destroy').resizableColumns({$rcOptions});";
        }
        if ($this->floatHeader) {
            GridFloatHeadAsset::register($view);
            // fix floating header for IE browser when using group grid functionality
            $skipCss = '.kv-grid-group-row,.kv-group-header,.kv-group-footer'; // skip these CSS for IE
            $js = 'function($table){return $table.find("tbody tr:not(' . $skipCss . '):visible:first>*");}';
            $opts = [
                'floatTableClass' => 'kv-table-float',
                'floatContainerClass' => 'kv-thead-float',
                'getSizingRow' => new JsExpression($js),
            ];
            if ($this->floatOverflowContainer) {
                $opts['scrollContainer'] = new JsExpression("function(){return {$container};}");
            }
            $this->floatHeaderOptions = array_replace_recursive($opts, $this->floatHeaderOptions);
            $opts = Json::encode($this->floatHeaderOptions);
            $script .= "jQuery('#{$gridId} .kv-grid-table:first').floatThead({$opts});";
            // integrate resizeableColumns with floatThead
            if ($this->resizableColumns) {
                $script .= "{$container}.off('{$NS}').on('column:resize{$NS}', function(e){" .
                        "jQuery('#{$gridId} .kv-grid-table:nth-child(2)').floatThead('reflow');" .
                        '});';
            }
        }
        $psVar = 'ps_' . Inflector::slug($this->containerOptions['id'], '_');
        if ($this->perfectScrollbar) {
            GridPerfectScrollbarAsset::register($view);
            $script .= "var {$psVar} = new PerfectScrollbar('{$contId}', " .
                    Json::encode($this->perfectScrollbarOptions) . ');';
        }
        $this->genToggleDataScript();
        $script .= $this->_toggleScript;
        $this->_gridClientFunc = 'kvGridInit_' . hash('crc32', $script);
        $this->options['data-krajee-grid'] = $this->_gridClientFunc;
        $this->options['data-krajee-ps'] = $psVar;
        $view->registerJs("var {$this->_gridClientFunc}=function(){\n{$script}\n};\n{$this->_gridClientFunc}();");
    }

    public function initSwitchLayout() {
        $layoutArray = [];
        $layoutArray[0] = <<< HTML
    <div>
        <div class="pl-2 pr-2">
            <div class="float-left">
                {summary}
            </div>
            <div class="btn-toolbar kv-grid-toolbar toolbar-container float-right">
            {toolbar}
            </div>
          <div class="clearfix"></div>
        </div>
   \n{items}\n
        <div class="pl-2 pr-2">
       {pager}
        </div>   
</div>   
HTML;

//        $mainLayout = $layoutArray[$this->layout];
        if (isset($this->layout)) {
            if (is_numeric($this->layout)) {
                $mainLayout = $layoutArray[$this->layout];
            } else {
                $mainLayout = $this->layout;
            }
        }else{
            $mainLayout = $layoutArray[$this->layout];
        }

        return $mainLayout;
    }

}
