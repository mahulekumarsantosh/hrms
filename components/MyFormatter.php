<?php

namespace app\components;

use Yii;
use app\components\TimeHelper;

class MyFormatter extends \yii\i18n\Formatter {

    public function asInCurrency($value, $decimalLen = 2, $label = false) {
        $value = StringHelper::ConvertDecimal($value, $decimalLen);
        if (strpos($value, ".") > 0) {
            list($num, $decimalNum) = explode('.', $value);
        } else {
            $num = $value;
            $decimalNum = 0;
        }

        $explrestunits = "";
        if (strlen($num) > 3) {
            $lastthree = substr($num, strlen($num) - 3, strlen($num));
            $restunits = substr($num, 0, strlen($num) - 3); // extracts the last three digits
            $restunits = (strlen($restunits) % 2 == 1) ? "0" . $restunits : $restunits; // explodes the remaining digits in 2's formats, adds a zero in the beginning to maintain the 2's grouping.
            $expunit = str_split($restunits, 2);
            for ($i = 0; $i < sizeof($expunit); $i++) {
                // creates each of the 2's group and adds a comma to the end
                if ($i == 0) {
                    $explrestunits .= (int) $expunit[$i] . ","; // if is first value , convert into integer
                } else {
                    $explrestunits .= $expunit[$i] . ",";
                }
            }
            $thecash = $explrestunits . $lastthree;
        } else {
            $thecash = $num;
        }
        $prefix = $suffix = "";
        if ($label == 'L' || $label == 'l') {
            $prefix = Yii::$app->settings->currency_label;
        }
        if ($label == 'R' || $label == 'r') {
            $suffix = Yii::$app->settings->currency_label;
        }
        return "$prefix$thecash.$decimalNum$suffix";
    }

    public function asDmy($date, $format = 'd-m-Y') {
        return TimeHelper::DateFilter($date, $format);
    }

    public function asMyDate($date, $format = false) {
        if ($format === false) {
            $format = Yii::$app->settings->date_format;
        }
        return TimeHelper::DateFilter($date, $format);
    }

}
