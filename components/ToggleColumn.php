<?php

namespace app\components;

use Closure;
use \rmrevin\yii\fontawesome\FontAwesome;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;

/**
 * Class Menu
 * Theme menu widget.
 */
class ToggleColumn extends \pheme\grid\ToggleColumn {

    public $iconOn = 'check';
    public $iconOff = 'times';

    protected function renderDataCellContent($model, $key, $index) {
        $url = [$this->action, 'id' => $model->{$this->primaryKey}];

        $attribute = $this->attribute;
        $value = $model->$attribute;

        if ($value === null || $value == true) {
            $icon = $this->iconOn;
            $title = $this->offText;
        } else {
            $icon = $this->iconOff;
            $title = $this->onText;
        }
        return Html::a(
                        '<span class="fa fa-' . $icon . '"></span>', $url, [
                    'title' => $title,
                    'class' => 'toggle-column',
                    'data-method' => 'post',
                    'data-pjax' => '0',
                        ]
        );
    }

}
