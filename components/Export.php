<?php

namespace app\components;

use Yii;
use yii\base\Action;
use yii\base\InvalidConfigException;
use frontend\models\Setting;

class Export extends Action {

    public $fileName;
    public $modelClass;
    public $modelFunction = 'search';
    public $title = false;
    public $viewFile;
    public $pdfFormat = 'A4-P';
    public $marginLeft = 5;
    public $marginTop = 5;
    public $beforeQuery = false;
    public $SetWatermarkImage = "";

    public function __construct($id, $controller, $config = []) {
        $this->fileName = 'Excel' . time();
        $this->viewFile = '_gridView';
        parent::__construct($id, $controller, $config);
    }

    public function run() {
        $type = $this->controller->action->id;
        $content = $this->controller->renderPartial($this->viewFile, [
            'fileName' => $this->fileName,
            'export' => true,
        ]);
        $content = preg_replace('/<\/?a[^>]*>/', '', $content);

        //for adding title 
        if ($this->title) {
            $caption = "<caption>$this->title</caption>";
            $content = str_replace('><thead>', ">$caption<thead>", $content);
        }

        if (strpos($type, 'excel') !== false) {
            $options = ['mimeType' => 'application/vnd.ms-excel'];
            $ExportToExcel = new \app\components\ExportToExcel;


            $content = str_replace('<table', "<table border='1' style='border-color: #ccc;' ", $content);

            return $ExportToExcel->exportExcel($content, "$this->fileName.xls", $options);
        } else if (strpos($type, 'pdf') !== false) {
            $mpdf = new \app\components\ExportToPdf([
                'format' => $this->pdfFormat,
                'margin_top' =>  $this->marginTop,
                'margin_left' => $this->marginLeft,
                'margin_right' => 5,
            ]);
            
//            $mpdf->SetWatermarkImage(\yii\helpers\Url::to(['/images/Cancelled.jpg'])); 
            $mpdf->SetWatermarkImage($this->SetWatermarkImage);
            $mpdf->WriteHTML($content, 2);
            $mpdf->SetTitle($this->fileName);
            $mpdf->Output("$this->fileName.pdf", 'I');
        }
    }

}
