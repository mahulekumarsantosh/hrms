<?php

namespace common\components;

use Yii;

class GeneralRepository {

    /**
     * Creates and executes an INSERT SQL statement for several rows.
     * 
     * Usage:
     * $rows = array(
     *      array('id' => 1, 'name' => 'John'),
     *      array('id' => 2, 'name' => 'Mark')
     * );
     * GeneralRepository::insertSeveral(User::model()->tableName(), $rows);
     * 
     * @param string $table the table that new rows will be inserted into.
     * @param array $array_columns the array of column datas array(array(name=>value,...),...) to be inserted into the table.
     * @return integer number of rows affected by the execution.
     */
    public static function insertSeveral($table, $array_columns) {
        $connection = Yii::$app->db;
        $sql = '';
        $params = array();
        $i = 0;
        foreach ($array_columns as $columns) {
            $names = array();
            $placeholders = array();
            foreach ($columns as $name => $value) {
                $names[] = $name;
                $placeholders[] = $value;
            }
            if ($i == 0) {
                $sql = 'INSERT INTO ' . $connection->quoteTableName($table)
                        . " (" . implode(', ', $names) . ") VALUES 
                        ('" . implode("','", $placeholders) . "')";
            } else {
                $sql .= ",('" . implode("','", $placeholders) . "')";
            }
            $i++;
        }
        $command = Yii::$app->db->createCommand($sql);
        return $command->execute($params);
    }

}
