<?php

namespace app\components;

use app\models\SmsLog;

class SmsHelper {

    const USERNAME = "suntechnos";
    const PASSWORD = "abc@2019";
    const SENDERID = "XXXX";
    const CHANNEL = "Trans";

    public static function Send($msg, $mobile) {
        $url = 'http://sms.xpressdndsms.com/api/mt/SendSMS?' .
                http_build_query([
                    'user' => self::USERNAME,
                    'password' => self::PASSWORD,
                    'senderid' => self::SENDERID,
                    'channel' => self::CHANNEL,
                    'DCS' => 0,
                    'flashsms' => 0,
                    'number' => $mobile,
                    'text' => $msg,
                    'route' => 1
        ]);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = json_decode(curl_exec($ch));
        SmsLog::SaveSms($msg, $mobile, $result);
    }

}
