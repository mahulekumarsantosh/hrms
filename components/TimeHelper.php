<?php

namespace app\components;

//use Yii;

class TimeHelper {

    public static function Format($date, $format = "d-m-Y") {
        return date($format, strtotime($date));
    }

    public static function YMD($date) {
        return date("Y-m-d", strtotime($date));
    }

    public static function DMY($date) {
        return date("d-m-Y", strtotime($date));
    }

    public static function DateFilter($date, $format = "Y-m-d") {
        if (empty($date)) {
            return "";
        } else {
            return self::Format($date, $format);
        }
    }

    public static function DateIncDec($date, $changeTo = '+1 day', $format = "Y-m-d") {
        return date($format, strtotime($changeTo, strtotime($date)));
    }

    public static function AlertClass($dateYMD) {
        $time = strtotime($dateYMD);
       
        $currentTime = time();
        $zDay = strtotime("+0 day", $currentTime);
        $tDay = strtotime("+30 day", $currentTime);

//         if($time == "" || $time == null){
//            return "info";
//        }
        if ($time < $currentTime && $time != "") {
            return "danger";
        }
        if ($tDay < $time ) {
            return "success";
        }
        if ($zDay < $time && $time < $tDay) {
            return "warning";
        }
       // return 'success';
    }

    public static function createdAt() {
        return date("Y-m-d H:i:s");
    }

    public static function Ago($dateYMD) {
        return self::TimeAgo($dateYMD);
    }

    public static function TimeAgo($date) {
        $time_difference = time() - strtotime($date);

        if ($time_difference < 1) {
            return 'less than 1 second ago';
        }
        $condition = array(12 * 30 * 24 * 60 * 60 => 'year',
            30 * 24 * 60 * 60 => 'month',
            24 * 60 * 60 => 'day',
            60 * 60 => 'hour',
            60 => 'minute',
            1 => 'second'
        );

        foreach ($condition as $secs => $str) {
            $d = $time_difference / $secs;

            if ($d >= 1) {
                $t = round($d);
                return $t . ' ' . $str . ( $t > 1 ? 's' : '' ) . ' ago';
            }
        }
    }

    public static $months = [
        1 => 'January',
        2 => 'February',
        3 => 'March',
        4 => 'April',
        5 => 'May',
        6 => 'June',
        7 => 'July ',
        8 => 'August',
        9 => 'September',
        10 => 'October',
        11 => 'November',
        12 => 'December',
    ];

    public static function getMonthNames($monthNo = false) {
        if ($monthNo === false) {
            return self::$months;
        }
        return self::$months[$monthNo];
    }

    public static function getYears($from = 1980, $to = false) {
        if (!$to) {
            $to = date("Y");
        }
        $year = [];
        for ($from; $from <= $to; $from++) {
            $year[$from] = $from;
        }
        krsort($year);
        return $year;
    }

    public static function alertGenerater($endDate) {
        $currentTime = time();
        switch (Self::dateDiffInDays($endDate, $currentTime)) {
            case "10":
                return "warning";
                break;
            case "5":
                return "danger";
                break;
            default:
                return "success";
        }
    }

    public static function getFinancialYears($from = 1980, $to = false) {
        if (!$to) {
            $to = date("Y");
        }
        $year = [];
        for ($from; $from <= $to; $from++) {
            $next = $from + 1;
            $year[$from] = $from . "-" . $next;
        }
        krsort($year);
        return $year;
    }

    function dateDiffInDays($endDate, $currentTime) {
        // Calulating the difference in timestamps 
        $diff = strtotime($endDate) - strtotime($currentTime);

        // 1 day = 24 hours 
        // 24 * 60 * 60 = 86400 seconds 
        return abs(round($diff / 86400));
    }

    function dateDiff($date1, $date2) {
        $date1 = date_create(self::Format($date1, "Y-m-d"));
        $date2 = date_create(self::Format($date2, "Y-m-d"));

        $diff = date_diff($date2, $date1);
        return !empty($diff) ? $diff->format("%R%a days") : '';
    }

    
     public static function TimeFormat($date, $format = "d-m-Y h:i:s A") {
        return date($format, strtotime($date));
    }
    
    
    public static function dateRange($fromdate, $todate) {
        $fromdate = \DateTime::createFromFormat('Y-m-d', $fromdate);
        $todate = \DateTime::createFromFormat('Y-m-d', $todate);
        $datePeriod = new \DatePeriod(
                $fromdate, new \DateInterval('P1D'), $todate->modify('+1 day')
        );

        $dates = [];
        foreach ($datePeriod as $date) {
            $dates[] = $date->format("Y-m-d");
        }
        return $dates;
    }
}

?>