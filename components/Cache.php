<?php

namespace app\components;

use Yii;
use yii\caching\DbDependency;

class Cache {

    public static $validity = 24 * 60 * 60;

    public static function cacheUserData($id) {
        $db = Yii::$app->db; // or Category::getDb()
        $dep = new DbDependency();
        $dep->sql = 'SELECT max(updated_at) FROM user';
        return $db->cache(function ($db) use ($id) {
                    return \app\models\User::find()->where(['id' => $id])->one();
                }, self::$validity, $dep);
    }

}
