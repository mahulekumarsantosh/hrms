<?php

namespace app\components;

use yii\helpers\Html;
use common\components\Url;
use Yii;

class StringHelper {

    public static function toUrl($str) {
        return strtolower(str_replace(" ", '-', $str));
    }

    public static function WordCutter($str, $count = 5) {
        $data = trim(preg_replace('/\s+/', ' ', $str));
        $data = explode(" ", $data);
        $datacount = count($data);
//        print_r($data);
        $temp_ = [];
        $i = 1;
        if ($datacount >= $count) {
            foreach ($data as $val) {
                $temp_[] = $val;
                if ($i >= $count)
                    break;
                $i++;
            }
            $temp_[] = "...";
        }else {
            $temp_ = $data;
        }
        return implode(" ", $temp_);
    }

    /*
     * to remove extra space form string
     */

    public static function RemoveExtraSpace($str) {
        return trim(preg_replace('/\s+/', ' ', $str));
    }

    public static function ConvertUrlPath($str) {
        $data = trim(preg_replace('/\s+/', ' ', $str));
        return strtolower(str_replace(" ", "-", $data));
    }

    public static function ConvertDecimal($number, $digit = 2) {
        if (empty($number)) {
            $number = 0;
        }
        return number_format($number, $digit, '.', '');
    }

    public static function NewsUrl($title, $cate, $newsUrl, $onlyUrl = false) {
        $url = Url::to(['/site/readone', "category" => StringHelper::toUrl($cate), 'news_url' => StringHelper::toUrl($newsUrl)]);
        if ($onlyUrl === true) {
            return $url;
        }
        return Html::a($title, $url, [
                    "title" => StringHelper::Clean($title),
        ]);
    }

    public static function TagUrl($tagName, $tagUrl, array $options = []) {
        return Html::a(self::UrlToWord($tagName), ['site/tags-news', 'tag_url' => self::WordToUrl($tagUrl)], $options);
    }

    public static function WordToUrl($tagName) {
        return strtolower(str_replace(" ", "-", self::RemoveExtraSpace(self::Clean($tagName))));
    }

    public static function UrlToWord($tagName) {
        return ucwords(str_replace("-", " ", self::RemoveExtraSpace($tagName)));
    }

    public static function newsImagePath($val, $type = "", $alias = false) {


        $folder = [
            "optimize" => "optimize/",
            "thumbnail" => "thumbnail/",
            "" => "",
        ];
        $commonPath = 'newsimages/' . date("Y/M/", strtotime($val['c_at'])) . $folder[$type] . $val['img_path'];

        $imagePath = \Yii::getAlias("@backend/web/theme/$commonPath");
        if (in_array($val['news_type'], [1, 3]) && is_file($imagePath)) {
            $imageUrl = Url::toCdn([$commonPath]);
        } else if (in_array($val['news_type'], [2])) {
            $imageUrl = "https://i.ytimg.com/vi/$val[vid_url]/hqdefault.jpg";
        } else {
            $default = [
                "optimize" => "defaultOptimize.jpg",
                "thumbnail" => "defaultThumb.jpg",
                "" => "default.jpg",
            ];


            $commonPath = "newsimages/$default[$type]";

            $imagePath = \Yii::getAlias("@backend/web/theme/$commonPath");
            $imageUrl = Url::toCdn([$commonPath]);
        }
        //return both image path and image url
        if ($alias === true) {
            return [
                0 => $imagePath,
                1 => $imageUrl,
            ];
        }
        return $imageUrl;
    }

    /*
     * to clean all special charactor
     */

    public static function Clean($string) {
        $string = strip_tags($string); // remove all html tags
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
        return str_replace('-', ' ', $string); // Replaces all spaces with hyphens.
    }

    public static function jsonData() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return [
            "response" => false,
            "data" => [],
            "msg" => '',
            "error" => false,
            "errorMsg" => false,
//            "csrfToken" => Yii::$app->request->csrfToken,
        ];
    }

    //DocIcons
    public static $DocIcons = [
       'jpg' => 'fa fa-file-image',
       'jpeg' => 'fa fa-file-image',
       'png' => 'fa fa-file-image',
       'docx' => 'fa fa-file',
       'doc' => 'fa fa-file',
       'pdf' => 'fa fa-file-pdf',
       'rar' => 'fa fa-file-archive',
       'zip' => 'fa fa-file-archive',
   ];

    public static function getDocIcons($ext) {
        return @static::$DocIcons[$ext];
    }

    //WarrantyGuarantee
    public static $WarrantyGuarantee = [
        1 => 'Warranty',
        2 => 'Guarantee'
    ];

    public static function getWarrantyGuaranteeAll() {
        return static::$WarrantyGuarantee;
    }

    public static function getWarrantyGuarantee($id) {
        return static::$WarrantyGuarantee[$id];
    }

    public static function getFlore() {
        return [
            "Ground Floor" => "Ground Floor",
            "1 Floor" => "1 Floor",
            "2 Floor" => "2 Floor",
            "3 Floor" => "3 Floor",
            "4 Floor" => "4 Floor",
            "5 Floor" => "5 Floor",
            "6 Floor" => "6 Floor",
            "7 Floor" => "7 Floor",
            "8 Floor" => "8 Floor",
        ];
    }

    public static function getTaxPercent() {
        return [
            '0' => '0',
            '5' => '5',
            '9' => '9',
            '18' => '18'
        ];
    }

    public static function ConvertCurrencyNumberToWordFormat($amount) {
        $no = floor($amount);
        $point = round($amount - $no, 2) * 100;
        $hundred = null;
        $digits_1 = strlen($no);
        $i = 0;
        $str = array();
        $words = array('0' => '', '1' => 'One', '2' => 'Two',
            '3' => 'Three', '4' => 'Four', '5' => 'Five', '6' => 'Six',
            '7' => 'Seven', '8' => 'Eight', '9' => 'Nine',
            '10' => 'Ten', '11' => 'Eleven', '12' => 'Twelve',
            '13' => 'Thirteen', '14' => 'Fourteen',
            '15' => 'Fifteen', '16' => 'Sixteen', '17' => 'Seventeen',
            '18' => 'Eighteen', '19' => 'Nineteen', '20' => 'Twenty',
            '30' => 'Thirty', '40' => 'Forty', '50' => 'Fifty',
            '60' => 'Sixty', '70' => 'Seventy',
            '80' => 'Eighty', '90' => 'Ninety');
        $digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore');
        while ($i < $digits_1) {
            $divider = ($i == 2) ? 10 : 100;
            $amount = floor($no % $divider);
            $no = floor($no / $divider);
            $i += ($divider == 10) ? 1 : 2;
            if ($amount) {
                $plural = (($counter = count($str)) && $amount > 9) ? '' : null;
                $hundred = ($counter == 1 && $str[0]) ? '' : null;
                $str [] = ($amount < 21) ? $words[$amount] .
                        " " . $digits[$counter] . $plural . " " . $hundred :
                        $words[floor($amount / 10) * 10]
                        . " " . $words[$amount % 10] . " "
                        . $digits[$counter] . $plural . " " . $hundred;
            } else
                $str[] = null;
        }
        $str = array_reverse($str);
        $result = implode('', $str);
        $points = ($point) ?
                " Point " . $words[$point / 10] . " " .
                $words[$point = $point % 10] : '';
        $paisa = $points != "" ? " Paisa " : "";
        $result = $result . "Rupees  " . $points .  $paisa . " Only";
         
         echo  $result;
        
    }

}
