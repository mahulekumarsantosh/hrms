<?php

namespace app\components;

class ThemeHelper {

    public static $navbar = [
        'navbar-dark navbar-primary' => '#007bff',
        'navbar-dark navbar-secondary' => '#6c757d',
        'navbar-dark navbar-info' => '#17a2b8',
        'navbar-dark navbar-success' => '#28a745',
        'navbar-dark navbar-danger' => '#dc3545',
        'navbar-dark navbar-indigo' => '#6610f2',
        'navbar-dark navbar-purple' => '#6f42c1',
        'navbar-dark navbar-pink' => '#e83e8c',
        'navbar-dark navbar-teal' => '#20c997',
        'navbar-dark navbar-cyan' => '#17a2b8',
        'navbar-dark navbar-dark' => '#6c757d',
        'navbar-dark navbar-gray-dark' => '#343a40',
        'navbar-dark navbar-gray' => '#f8f9fa',
        'navbar-light navbar-light' => '#ffc107',
        'navbar-light navbar-warning' => '#fff',
        'navbar-light navbar-white' => '#007bff',
        'navbar-light navbar-orange' => '#fd7e14',
    ];
    public static $sidebar = [
        'bg-primary' => '#007bff',
        'bg-warning' => '#6c757d',
        'bg-info' => '#17a2b8',
        'bg-danger' => '#dc3545',
        'bg-success' => '#28a745',
        'bg-indigo' => '#6610f2',
        'bg-purple' => '#6f42c1',
        'bg-navy' => '#001f3f',
        'bg-fuchsia' => '#f012be',
        'bg-pink' => '#e83e8c',
        'bg-maroon' => '#d81b60',
        'bg-orange' => '#fd7e14',
        'bg-lime' => '#01ff70',
        'bg-teal' => '#20c997',
        'bg-olive' => '#3d9970',
    ];
    public static $brandbar = [
        'bg-primary' => '#007bff',
        'bg-secondary' => '#6c757d',
        'bg-info' => '#17a2b8',
        'bg-success' => '#28a745',
        'bg-danger' => '#dc3545',
        'bg-indigo' => '#6610f2',
        'bg-purple' => '#6f42c1',
        'bg-pink' => '#e83e8c',
        'bg-teal' => '#20c997',
        'bg-cyan' => '#17a2b8',
        'bg-dark' => '#6c757d',
        'bg-gray-dark' => '#343a40',
        'bg-gray' => '#f8f9fa',
        'bg-light' => '#ffc107',
        'bg-warning' => '#fff',
        'bg-white' => '#007bff',
        'bg-orange' => '#fd7e14',
    ];

}
