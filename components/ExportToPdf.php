<?php

namespace app\components;

use Yii;
use Mpdf\Mpdf;
use frontend\models\Setting;

class ExportToPdf extends Mpdf {

    public function __construct(array $config = []) {
        parent::__construct($config);

//        $this->SetWatermarkImage(\yii\helpers\Url::to(['/images/Cancelled.jpg']));
        $this->showWatermarkImage = true;
        $this->SetHTMLFooter("<p class='footer-date'>Generated at: " . date('r') . "</p>");
        $this->WriteHTML(" 
            body{font-family: arial;}
            .hideInPdf{display:none;}
            .table{width:100%}
            .table-bordered{border-collapse:collapse}
            .table-bordered,.table-bordered tr ,.table-bordered td,.table-bordered th {border:1px solid #ccc;}
            .full-row{width: 100%;}
            .clear{clear:both;}
            .footer-date,.approve{font-size:10px;}
            .summary{display:none;}
            .text-center{text-align:center;}
            .text-right{text-align:right;}
            .text-bold{font-weight:bold;}
            .text-success{color: #28a745;}
            .text-danger{color: #dc3545;}
            a{color:black;text-decoration:none;}
            .text-head{font-size: 9px;font-weight: bold; text-align: left;}
            .caption-head{font-size: 9px;font-weight: bold; text-align: left;}
            .purchase-table, .purchase-table td, .purchase-table th { border: 1px solid #999; border-collapse: collapse; padding:5px; }
            .purchase-table{padding:0px; width:100%;}
            .purchase-table thead{text-align: center}
            .purchase-table td p, .purchase-table th p { margin: 5px 0px}
            
            .table-font-10 th{font-size: 10px;  line-height: 10px;}
            .table-font-10 td{font-size: 10px; line-height: 10px;}
            .table-font-12 th{font-size: 11px;}
            .table-font-12 td{font-size: 11px; valign: top;}
            .table-font-13 td{font-size: 16px; valign: top;}
            .border-none td{border: 1px solid #fff !important;}            
            .border-pdf td{border: 1px solid #000 !important;}
            .companyname{font-size: 22px; margin: 0; padding: 0; font-weight: bold;text-align: center;margin-bottom: 5px;}
            .reportname{font-size: 16px; margin: 0; padding: 0; font-weight: bold;text-align: center;}
            .subheading{font-size: 14px; margin: 20px auto; padding: 0; font-weight: bold;text-align: center;border-radius: 8px;background-color: #444; color: #fff; border-radius: 8px; padding: 5px 15px;}
            .subheading span{}
            .hr8{border-bottom: 1px solid #ccc; margin: 8px auto;}
            .width_a4_50{width: 500px;}
            .d_label_w120{width: 90px;}
            .d_label_w400{width: 350px;}
            .custom-padding2 th{padding: 5px;}
            .custom-padding2 td{padding: 5px; margin: 0; line-height: 11px;}  
            .custom-pdf-list p{font-size: 10px;}
            .small-box{}
            .small-box .inner{display: inline;}
            .small-box .inner p{display: inline; margin: 0;}
            .small-box .inner h3{display: inline;  margin: 0; margin-bottom: 20px;}
        ", 1);
    }

}
