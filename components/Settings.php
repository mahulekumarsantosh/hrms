<?php

namespace app\components;

use Yii;
use app\models\Setting;

class Settings extends \yii\base\Component {

    public $values;
    public $isInit = false;

    public function __get($name) {
        if ($this->isInit === false) {
            Setting::valueInit();
            $this->values = Setting::$val;
            $this->isInit = true;
        }
        if (isset($this->values[$name])) {
            return $this->values[$name];
        }
    }

}

?>